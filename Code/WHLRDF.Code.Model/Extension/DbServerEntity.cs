﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHLRDF.Code.Model
{
    public partial class DbServerEntity
    {
        public virtual string parent { get; set; }
        public virtual string id
        {
            get
            {
                return DbServerId+"_S";
            }
        }
        public virtual string SourceId
        {
            get
            {
                return DbServerId.ToString();
            }
        }
        public virtual int DbServerType
        {
            get
            {
                return 0;
            }
        }
      
        public virtual string text
        {
            get
            {
                return LocalName;
            }
        }
        private  bool mchildren=true;
        /// <summary>
        /// 
        /// </summary>
        public virtual bool children
        {
            get {
               
                return mchildren;
            }
            set {
                mchildren = value;
            }
        }
        public virtual string icon { get => "fa fa-database"; }
        /// <summary>
        /// 下级路径
        /// </summary>
        public virtual string action
        {
            get;set;
        }
    }
}
