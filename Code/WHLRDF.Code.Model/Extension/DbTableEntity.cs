﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHLRDF.Code.Model
{
    public partial class DbTableEntity
    {
        public virtual string parent { get => this.DbServerId.ToString() + (this.IsTable?"_T":"_V"); }
        public virtual string id
        {
            get
            {
                return DbTableId+"_DT";
            }
        }
        public virtual string SourceId
        {
            get
            {
                return DbTableId.ToString();
            }
        }
        public virtual int DbServerType
        {
            get
            {
                return 2;
            }
        }
        public virtual string text
        {
            get
            {
                return DbTableName+(!string.IsNullOrWhiteSpace(LocalName)&& LocalName !=DbTableName? "("+LocalName+")":"");
            }
        }
        ///// <summary>
        ///// 
        ///// </summary>
        //public virtual object[] children
        //{
        //    get {
        //        return new object[] { new { text = "Columns", SourceId=Id,Id = Id + "_C", DServerbType = 2, ChildrenAction = "GetTreeColumn", children = new string[] { } } };
        //    }
        //}
        /// <summary>
        /// 下级路径
        /// </summary>
        public virtual string action
        {
            get
            {
                return "GetTreeTable";
            }
        }

       
      
        /// <summary>
        /// 
        /// </summary>
        public virtual bool children { get=>false; }
        
        public virtual string icon { get => "fa fa-table"; }
        public virtual int ProviderType
        {
            set;
            get;
        }

        public virtual List<DbColumnEntity> ColumnEntities { get; set; }
    }
}
