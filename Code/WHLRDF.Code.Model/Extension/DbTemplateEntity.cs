﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHLRDF.Code.Model
{
    public partial class DbTemplateEntity
    {

        public virtual string id { get => this.TemplateId; }

        public virtual string text { get => this.TemplateName; }

        public virtual string parent { get => this.ParentId; }

        public virtual bool children { get => true; }

        public virtual string icon { get => "fa fa-file-code-o"; }
        /// <summary>
        ///  模板内容
        /// </summary>
        // [Property("DataTContent", LocalName = " 模板内容", ReourceKeyName = "lbl_COMM_DataTemplate_DataTContent", ColumnType = "string", ControlType = 5, MaxLength = -1, Min = 0)]
        public virtual string DataTContent { get; set; }
       
    }
}
