﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WHLRDF.Code.Model;

namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 服务器选择实体
    /// </summary>
    public class TargetDbServerModel
    {
        public string SourceServerId { get; set; }

        public string SourceTableId { get; set; }

        /// <summary>
        /// 服务器列表
        /// </summary>
        public List<DbServerEntity> DbServers{ get; set; }

        public string TargetServerId { get; set; }

        public string TargetTableName { get; set; }
    }
}
