﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 服务器
    /// </summary>
    [Table("CODE_DbServer", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DbServerEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DbServer";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DbServerId";

        /// <summary>
        /// 排序
        /// </summary>
        public const string __OrderNo = "OrderNo";

        /// <summary>
        /// 服务器名称
        /// </summary>
        public const string __DbServerName = "DbServerName";

        /// <summary>
        /// 别名
        /// </summary>
        public const string __LocalName = "LocalName";

        /// <summary>
        /// 连接字符串
        /// </summary>
        public const string __ConntionString = "ConntionString";

        /// <summary>
        /// 数据库类型
        /// </summary>
        public const string __ProviderType = "ProviderType";

        /// <summary>
        /// 同步目标服务器
        /// </summary>
        public const string __ParentId = "ParentId";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";
        #endregion
        #region 属性
        private string _DbServerId;
        /// <summary>
        /// 主键
        /// </summary>
        [DbColumn("DbServerId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, IsGridVisible = false, ControlType = 1, MaxLength = 50)]
        public virtual string DbServerId
        {
            get
            {
                return _DbServerId;
            }
            set
            {
                _DbServerId = value;

            }
        }
        private int _OrderNo;
        /// <summary>
        /// 排序
        /// </summary>
        [DbColumn("OrderNo", LocalName = "排序", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 6)]
        public virtual int OrderNo
        {
            get
            {
                return _OrderNo;
            }
            set
            {
                SetData(__OrderNo,value);
                _OrderNo = value;

            }
        }
        private string _DbServerName;
        /// <summary>
        /// 服务器名称
        /// </summary>
        [DbColumn("DbServerName", LocalName = "服务器名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbServerName
        {
            get
            {
                return _DbServerName;
            }
            set
            {
                SetData(__DbServerName,value);
                _DbServerName = value;

            }
        }
        private string _LocalName;
        /// <summary>
        /// 别名
        /// </summary>
        [DbColumn("LocalName", LocalName = "别名", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {
                SetData(__LocalName,value);
                _LocalName = value;

            }
        }
        private string _ConntionString;
        /// <summary>
        /// 连接字符串
        /// </summary>
        [DbColumn("ConntionString", LocalName = "连接字符串", ColumnType = "string", AllowDBNull = false, MaxLength = 500, ControlType = 1)]
        public virtual string ConntionString
        {
            get
            {
                return _ConntionString;
            }
            set
            {
                SetData(__ConntionString,value);
                _ConntionString = value;

            }
        }
        private int _ProviderType;
        /// <summary>
        /// 数据库类型
        /// </summary>
        [DbColumn("ProviderType", LocalName = "数据库类型", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 3)]
        public virtual int ProviderType
        {
            get
            {
                return _ProviderType;
            }
            set
            {
                SetData(__ProviderType,value);
                _ProviderType = value;

            }
        }
     
        private string _ParentId;
        /// <summary>
        /// 上级服务器
        /// </summary>
        [DbColumn("ParentId", LocalName = "上级服务器", ColumnType = "string", AllowDBNull = true, IsGridVisible = false,  ControlType = 1,MaxLength =50)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {
                SetData(__ParentId, value);
                _ParentId = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", MaxLength = 500, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark,value);
                _Remark = value;

            }
        }
        #endregion
    }
}