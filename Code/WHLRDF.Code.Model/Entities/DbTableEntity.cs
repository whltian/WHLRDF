﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据库表
    /// </summary>
    [Table("CODE_DbTable", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DbTableEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DbTable";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DbTableId";

        /// <summary>
        ///  数据库id
        /// </summary>
        public const string __DbServerId = "DbServerId";

        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNo = "OrderNo";

        /// <summary>
        ///  表名
        /// </summary>
        public const string __DbTableName = "DbTableName";

        /// <summary>
        ///  本地名称
        /// </summary>
        public const string __LocalName = "LocalName";

        /// <summary>
        ///  是否是表
        /// </summary>
        public const string __IsTable = "IsTable";

        /// <summary>
        ///  所属模块
        /// </summary>
        public const string __DbNamespace = "DbNamespace";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  启用创建人
        /// </summary>
        public const string __IsEnableCreate = "IsEnableCreate";

        /// <summary>
        ///  启用修改人
        /// </summary>
        public const string __IsEnableLastModify = "IsEnableLastModify";

        /// <summary>
        ///  启用假删除
        /// </summary>
        public const string __IsEnableIsDeleted = "IsEnableIsDeleted";

    

        #endregion
        #region 属性
        private string _DbTableId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("DbTableId", LocalName = "主键", ColumnType = "string",  IsPrimaryKey = true, AllowDBNull = false, ControlType = 1,MaxLength =50)]
        public virtual string DbTableId
        {
            get
            {
                return _DbTableId;
            }
            set
            {
                _DbTableId = value;

            }
        }
        private string _DbServerId;
        /// <summary>
        ///  数据库id
        /// </summary>
        [DbColumn("DbServerId", LocalName = "数据库id", ColumnType = "string", AllowDBNull = false, IsGridVisible = false,  ControlType = 1, MaxLength = 50)]
        public virtual string DbServerId
        {
            get
            {
                return _DbServerId;
            }
            set
            {
                SetData(__DbServerId,value);
                _DbServerId = value;

            }
        }
        private int _OrderNo;
        /// <summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNo", LocalName = "排序", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int OrderNo
        {
            get
            {
                return _OrderNo;
            }
            set
            {
                SetData(__OrderNo,value);
                _OrderNo = value;

            }
        }
        private string _DbTableName;
        /// <summary>
        ///  表名
        /// </summary>
        [DbColumn("DbTableName", LocalName = "表名", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbTableName
        {
            get
            {
                return _DbTableName;
            }
            set
            {
                SetData(__DbTableName,value);
                _DbTableName = value;

            }
        }
        private string _LocalName;
        /// <summary>
        ///  本地名称
        /// </summary>
        [DbColumn("LocalName", LocalName = "本地名称", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {
                SetData(__LocalName,value);
                _LocalName = value;

            }
        }
        private bool _IsTable;
        /// <summary>
        ///  是否是表
        /// </summary>
        [DbColumn("IsTable", LocalName = "是否是表", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsTable
        {
            get
            {
                return _IsTable;
            }
            set
            {
                SetData(__IsTable,value);
                _IsTable = value;

            }
        }
        private string _DbNamespace;
        /// <summary>
        ///  所属模块
        /// </summary>
        [DbColumn("DbNamespace", LocalName = "所属模块", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string DbNamespace
        {
            get
            {
                return _DbNamespace;
            }
            set
            {
                SetData(__DbNamespace,value);
                _DbNamespace = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", MaxLength = 255, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark,value);
                _Remark = value;

            }
        }
        private bool _IsEnableCreate;
        /// <summary>
        ///  启用创建人
        /// </summary>
        [DbColumn("IsEnableCreate", LocalName = "启用创建人", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsEnableCreate
        {
            get
            {
                return _IsEnableCreate;
            }
            set
            {
                SetData(__IsEnableCreate,value);
                _IsEnableCreate = value;

            }
        }
        private bool _IsEnableLastModify;
        /// <summary>
        ///  启用修改人
        /// </summary>
        [DbColumn("IsEnableLastModify", LocalName = "启用修改人", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsEnableLastModify
        {
            get
            {
                return _IsEnableLastModify;
            }
            set
            {
                SetData(__IsEnableLastModify,value);
                _IsEnableLastModify = value;

            }
        }
        private bool _IsEnableIsDeleted;
        /// <summary>
        ///  启用假删除
        /// </summary>
        [DbColumn("IsEnableIsDeleted", LocalName = "启用假删除", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsEnableIsDeleted
        {
            get
            {
                return _IsEnableIsDeleted;
            }
            set
            {
                SetData(__IsEnableIsDeleted,value);
                _IsEnableIsDeleted = value;

            }
        }
     
        #endregion
    }
}
