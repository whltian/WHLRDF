﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据常用验证列表
    /// </summary>
    [Table("CODE_DataVaild", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DataVaildEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DataVaild";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DataVaildId";

        /// <summary>
        ///  键值
        /// </summary>
        public const string __KeyName = "KeyName";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  正则
        /// </summary>
        public const string __Regex = "Regex";

        #endregion
        #region 属性
        private int _DataVaildId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("DataVaildId", LocalName = "主键", ColumnType = "int", Identifier = true, IsPrimaryKey = true, AllowDBNull = false, VaildKey = "IsInt", ControlType = 2)]
        public virtual int DataVaildId
        {
            get
            {
                return _DataVaildId;
            }
            set
            {
                _DataVaildId = value;

            }
        }
        private string _KeyName;
        /// <summary>
        ///  键值
        /// </summary>
        [DbColumn("KeyName", LocalName = "键值", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string KeyName
        {
            get
            {
                return _KeyName;
            }
            set
            {
                SetData(__KeyName,value);
                _KeyName = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark,value);
                _Remark = value;

            }
        }
        private string _Regex;
        /// <summary>
        ///  正则
        /// </summary>
        [DbColumn("Regex", LocalName = "正则", ColumnType = "string", MaxLength = 1000, ControlType = 1)]
        public virtual string Regex
        {
            get
            {
                return _Regex;
            }
            set
            {
                SetData(__Regex,value);
                _Regex = value;

            }
        }
        #endregion
    }
}
