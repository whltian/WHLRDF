﻿

using System.Linq;
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据常用模板
    /// </summary>
    [Table("CODE_DbTemplate", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DbTemplateEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DbTemplate";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "TemplateId";

        /// <summary>
        ///  名称
        /// </summary>
        public const string __TemplateName = "TemplateName";

        /// <summary>
        ///  上级
        /// </summary>
        public const string __ParentId = "ParentId";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  深度
        /// </summary>
        public const string __Path = "Path";

        #endregion
        #region 属性
        private string _TemplateId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("TemplateId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string TemplateId
        {
            get
            {
                return _TemplateId;
            }
            set
            {
                _TemplateId = value;

            }
        }
        private string _TemplateName;
        /// <summary>
        ///  名称
        /// </summary>
        [DbColumn("TemplateName", LocalName = "名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string TemplateName
        {
            get
            {
                return _TemplateName;
            }
            set
            {
                SetData(__TemplateName, value);
                _TemplateName = value;

            }
        }
        private string _ParentId;
        /// <summary>
        ///  上级
        /// </summary>
        [DbColumn("ParentId", LocalName = "上级", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {
                SetData(__ParentId, value);
                _ParentId = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", MaxLength = 200, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark, value);
                _Remark = value;

            }
        }
        private string _Path;
        /// <summary>
        ///  深度
        /// </summary>
        [DbColumn("Path", LocalName = "深度", ColumnType = "string", MaxLength = 300, ControlType = 1)]
        public virtual string Path
        {
            get
            {
                return _Path;
            }
            set
            {
                SetData(__Path, value);
                _Path = value;

            }
        }
        #endregion
        #region 事件
        public DbTemplateEntity()
        {
            this.OnLoaded += DbTemplateEntity_OnLoaded;
        }

        /// <summary>
        /// 加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DbTemplateEntity_OnLoaded(EntityBase sender, CustomArgs e)
        {
            if (sender != null)
            {
                string path = ApplicationEnvironments.Site.BaseDirectory+ ApplicationEnvironments.Site.ConfigPath.Where(x => x.RootKey ==ConfigPathKeyType.config).FirstOrDefault().Path +"/code/"+ this.Path + this.TemplateName + ".tt";
                this.DataTContent = FileHelper.ReadLine(path.ToLower());
            }
        }
        #endregion
    }
}
