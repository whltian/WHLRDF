﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 属性列表
    /// </summary>
    [Table("CODE_DbColumn", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DbColumnEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DbColumn";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ColumnId";

        /// <summary>
        ///  表
        /// </summary>
        public const string __DbTableId = "DbTableId";

        /// <summary>
        ///  是否主键
        /// </summary>
        public const string __IsPrimary = "IsPrimary";

        /// <summary>
        ///  是否自增
        /// </summary>
        public const string __IsIdentifier = "IsIdentifier";

        /// <summary>
        ///  是否唯一
        /// </summary>
        public const string __IsUnique = "IsUnique";

        /// <summary>
        ///  字段名
        /// </summary>
        public const string __ColumnName = "ColumnName";

        /// <summary>
        ///  描述
        /// </summary>
        public const string __ColumnCaption = "ColumnCaption";

        /// <summary>
        ///  资源Key(暂无)
        /// </summary>
        public const string __ColumnKeyName = "ColumnKeyName";

        /// <summary>
        ///  是否允许为空
        /// </summary>
        public const string __AllowDBNull = "AllowDBNull";

        /// <summary>
        ///  是否允许新增
        /// </summary>
        public const string __AllowInsert = "AllowInsert";

        /// <summary>
        ///  是否允许修改
        /// </summary>
        public const string __AllowUpdate = "AllowUpdate";

        /// <summary>
        ///  控件类型
        /// </summary>
        public const string __ControlType = "ControlType";

        /// <summary>
        ///  字段类型
        /// </summary>
        public const string __DbFieldRelationTypeId = "DbFieldRelationTypeId";

        /// <summary>
        /// 系统类型
        /// </summary>
        public const string __SysFieldTypeId = "SysFieldTypeId";

        /// <summary>
        ///  长度
        /// </summary>
        public const string __MaxLength = "MaxLength";

        /// <summary>
        ///  小数
        /// </summary>
        public const string __Scale = "Scale";

        /// <summary>
        ///  列表显示
        /// </summary>
        public const string __IsGridVisible = "IsGridVisible";

        /// <summary>
        ///  列表宽度
        /// </summary>
        public const string __GridWidth = "GridWidth";

        /// <summary>
        ///  是否系统
        /// </summary>
        public const string __IsSystem = "IsSystem";

        /// <summary>
        ///  编辑显示
        /// </summary>
        public const string __IsEditVisible = "IsEditVisible";

        /// <summary>
        ///  是否启用
        /// </summary>
        public const string __IsEnabled = "IsEnabled";

        /// <summary>
        ///  是否排序
        /// </summary>
        public const string __IsSort = "IsSort";

        /// <summary>
        ///  是否默认排序
        /// </summary>
        public const string __IsDefaultSort = "IsDefaultSort";

        /// <summary>
        ///  是否条件
        /// </summary>
        public const string __IsFilter = "IsFilter";

        /// <summary>
        ///  默认条件
        /// </summary>
        public const string __IsDefaultFilter = "IsDefaultFilter";

        /// <summary>
        ///  描述宽度
        /// </summary>
        public const string __LabelWidth = "LabelWidth";

        /// <summary>
        ///  控件宽度
        /// </summary>
        public const string __InputWidth = "InputWidth";

        /// <summary>
        ///  排序排序
        /// </summary>
        public const string __OrderNo = "OrderNo";

        /// <summary>
        ///  icon
        /// </summary>
        public const string __GroupIcon = "GroupIcon";

        /// <summary>
        ///  默认搜索值
        /// </summary>
        public const string __DefaultFilterValue = "DefaultFilterValue";

        /// <summary>
        ///  格式
        /// </summary>
        public const string __Format = "Format";

        /// <summary>
        ///  正则Key
        /// </summary>
        public const string __VaildKey = "VaildKey";

        /// <summary>
        ///  是否固定
        /// </summary>
        public const string __IsFrozen = "IsFrozen";

        /// <summary>
        ///  最小值
        /// </summary>
        public const string __Min = "Min";

        /// <summary>
        ///  最大值
        /// </summary>
        public const string __Max = "Max";

        /// <summary>
        ///  默认值
        /// </summary>
        public const string __DefaultValue = "DefaultValue";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  正则
        /// </summary>
        public const string __RegValidate = "RegValidate";

        /// <summary>
        ///  数据类型
        /// </summary>
        public const string __DbTypeName = "DbTypeName";

        #endregion
        #region 属性
        private string _ColumnId;
        /// <summary>
        /// 主键
        /// </summary>
        [DbColumn("ColumnId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, IsGridVisible = false, MaxLength = 50, ControlType = 2)]
        public virtual string ColumnId
        {
            get
            {
                return _ColumnId;
            }
            set
            {
                _ColumnId = value;

            }
        }
        private string _DbTableId;
        /// <summary>
        ///  表
        /// </summary>
        [DbColumn("DbTableId", LocalName = "表", ColumnType = "string", AllowDBNull = false, IsGridVisible = false, ControlType =1,MaxLength =50)]
        public virtual string DbTableId
        {
            get
            {
                return _DbTableId;
            }
            set
            {
                SetData(__DbTableId,value);
                _DbTableId = value;

            }
        }
        private bool _IsPrimary;
        /// <summary>
        ///  是否主键
        /// </summary>
        [DbColumn("IsPrimary", LocalName = "是否主键", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsPrimary
        {
            get
            {
                return _IsPrimary;
            }
            set
            {
                SetData(__IsPrimary,value);
                _IsPrimary = value;

            }
        }
        private bool _IsIdentifier;
        /// <summary>
        ///  是否自增
        /// </summary>
        [DbColumn("IsIdentifier", LocalName = "是否自增", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsIdentifier
        {
            get
            {
                return _IsIdentifier;
            }
            set
            {
                SetData(__IsIdentifier,value);
                _IsIdentifier = value;

            }
        }
        private bool _IsUnique;
        /// <summary>
        ///  是否唯一
        /// </summary>
        [DbColumn("IsUnique", LocalName = "是否唯一", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsUnique
        {
            get
            {
                return _IsUnique;
            }
            set
            {
                SetData(__IsUnique,value);
                _IsUnique = value;

            }
        }
        private string _ColumnName;
        /// <summary>
        ///  字段名
        /// </summary>
        [DbColumn("ColumnName", LocalName = "字段名", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ColumnName
        {
            get
            {
                return _ColumnName;
            }
            set
            {
                SetData(__ColumnName,value);
                _ColumnName = value;

            }
        }
        private string _ColumnCaption;
        /// <summary>
        ///  描述
        /// </summary>
        [DbColumn("ColumnCaption", LocalName = "描述", ColumnType = "string", AllowDBNull = true, MaxLength = 200, ControlType = 1)]
        public virtual string ColumnCaption
        {
            get
            {
                return _ColumnCaption;
            }
            set
            {
                SetData(__ColumnCaption,value);
                _ColumnCaption = value;

            }
        }
        private string _ColumnKeyName;
        /// <summary>
        ///  资源Key(暂无)
        /// </summary>
        [DbColumn("ColumnKeyName", LocalName = "资源Key(暂无)", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string ColumnKeyName
        {
            get
            {
                return _ColumnKeyName;
            }
            set
            {
                SetData(__ColumnKeyName,value);
                _ColumnKeyName = value;

            }
        }
        private bool _AllowDBNull;
        /// <summary>
        ///  是否允许为空
        /// </summary>
        [DbColumn("AllowDBNull", LocalName = "是否允许为空", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool AllowDBNull
        {
            get
            {
                return _AllowDBNull;
            }
            set
            {
                SetData(__AllowDBNull,value);
                _AllowDBNull = value;

            }
        }
        private bool _AllowInsert;
        /// <summary>
        ///  是否允许新增
        /// </summary>
        [DbColumn("AllowInsert", LocalName = "是否允许新增", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool AllowInsert
        {
            get
            {
                return _AllowInsert;
            }
            set
            {
                SetData(__AllowInsert,value);
                _AllowInsert = value;

            }
        }
        private bool _AllowUpdate;
        /// <summary>
        ///  是否允许修改
        /// </summary>
        [DbColumn("AllowUpdate", LocalName = "是否允许修改", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool AllowUpdate
        {
            get
            {
                return _AllowUpdate;
            }
            set
            {
                SetData(__AllowUpdate,value);
                _AllowUpdate = value;

            }
        }
        private int _ControlType;
        /// <summary>
        ///  控件类型
        /// </summary>
        [DbColumn("ControlType", LocalName = "控件类型", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6,IsGridVisible =false)]
        public virtual int ControlType
        {
            get
            {
                return _ControlType;
            }
            set
            {
                SetData(__ControlType,value);
                _ControlType = value;

            }
        }
        private int _DbFieldRelationTypeId;
        /// <summary>
        ///  字段类型
        /// </summary>
        [DbColumn("DbFieldRelationTypeId", LocalName = "字段类型", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6, IsGridVisible = false)]
        public virtual int DbFieldRelationTypeId
        {
            get
            {
                return _DbFieldRelationTypeId;
            }
            set
            {
                SetData(__DbFieldRelationTypeId,value);
                _DbFieldRelationTypeId = value;

            }
        }

        private int _SysFieldTypeId;
        /// <summary>
        ///  系统类型
        /// </summary>
        [DbColumn("SysFieldTypeId", LocalName = "系统类型", ColumnType = "int", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int SysFieldTypeId
        {
            get
            {
                return _SysFieldTypeId;
            }
            set
            {
                SetData(__SysFieldTypeId,value);
                _SysFieldTypeId = value;

            }
        }
        private int _MaxLength;
        /// <summary>
        ///  长度
        /// </summary>
        [DbColumn("MaxLength", LocalName = "长度", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int MaxLength
        {
            get
            {
                return _MaxLength;
            }
            set
            {
                SetData(__MaxLength,value);
                _MaxLength = value;

            }
        }
        private int _Scale;
        /// <summary>
        ///  小数
        /// </summary>
        [DbColumn("Scale", LocalName = "小数", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int Scale
        {
            get
            {
                return _Scale;
            }
            set
            {
                SetData(__Scale,value);
                _Scale = value;

            }
        }
        private bool _IsGridVisible;
        /// <summary>
        ///  列表显示
        /// </summary>
        [DbColumn("IsGridVisible", LocalName = "列表显示", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsGridVisible
        {
            get
            {
                return _IsGridVisible;
            }
            set
            {
                SetData(__IsGridVisible,value);
                _IsGridVisible = value;

            }
        }
        private int _GridWidth;
        /// <summary>
        ///  列表宽度
        /// </summary>
        [DbColumn("GridWidth", LocalName = "列表宽度", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int GridWidth
        {
            get
            {
                return _GridWidth;
            }
            set
            {
                SetData(__GridWidth,value);
                _GridWidth = value;

            }
        }
        private bool _IsSystem;
        /// <summary>
        ///  是否系统
        /// </summary>
        [DbColumn("IsSystem", LocalName = "是否系统", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsSystem
        {
            get
            {
                return _IsSystem;
            }
            set
            {
                SetData(__IsSystem,value);
                _IsSystem = value;

            }
        }
        private bool _IsEditVisible;
        /// <summary>
        ///  编辑显示
        /// </summary>
        [DbColumn("IsEditVisible", LocalName = "编辑显示", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsEditVisible
        {
            get
            {
                return _IsEditVisible;
            }
            set
            {
                SetData(__IsEditVisible,value);
                _IsEditVisible = value;

            }
        }
        private bool _IsEnabled;
        /// <summary>
        ///  是否启用
        /// </summary>
        [DbColumn("IsEnabled", LocalName = "是否启用", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsEnabled
        {
            get
            {
                return _IsEnabled;
            }
            set
            {
                SetData(__IsEnabled,value);
                _IsEnabled = value;

            }
        }
        private bool _IsSort;
        /// <summary>
        ///  是否排序
        /// </summary>
        [DbColumn("IsSort", LocalName = "是否排序", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsSort
        {
            get
            {
                return _IsSort;
            }
            set
            {
                SetData(__IsSort,value);
                _IsSort = value;

            }
        }
        private bool _IsDefaultSort;
        /// <summary>
        ///  是否默认排序
        /// </summary>
        [DbColumn("IsDefaultSort", LocalName = "是否默认排序", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsDefaultSort
        {
            get
            {
                return _IsDefaultSort;
            }
            set
            {
                SetData(__IsDefaultSort,value);
                _IsDefaultSort = value;

            }
        }
        private bool _IsFilter;
        /// <summary>
        ///  是否条件
        /// </summary>
        [DbColumn("IsFilter", LocalName = "是否条件", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsFilter
        {
            get
            {
                return _IsFilter;
            }
            set
            {
                SetData(__IsFilter,value);
                _IsFilter = value;

            }
        }
        private bool _IsDefaultFilter;
        /// <summary>
        ///  默认条件
        /// </summary>
        [DbColumn("IsDefaultFilter", LocalName = "默认条件", ColumnType = "bool", AllowDBNull = false, IsGridVisible = false, ControlType = 4)]
        public virtual bool IsDefaultFilter
        {
            get
            {
                return _IsDefaultFilter;
            }
            set
            {
                SetData(__IsDefaultFilter,value);
                _IsDefaultFilter = value;

            }
        }
        private int _LabelWidth;
        /// <summary>
        ///  描述宽度
        /// </summary>
        [DbColumn("LabelWidth", LocalName = "描述宽度", ColumnType = "int", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int LabelWidth
        {
            get
            {
                return _LabelWidth;
            }
            set
            {
                SetData(__LabelWidth,value);
                _LabelWidth = value;

            }
        }
        private int _InputWidth;
        /// <summary>
        ///  控件宽度
        /// </summary>
        [DbColumn("InputWidth", LocalName = "控件宽度", ColumnType = "int", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int InputWidth
        {
            get
            {
                return _InputWidth;
            }
            set
            {
                SetData(__InputWidth,value);
                _InputWidth = value;

            }
        }
        private int _OrderNo;
        /// <summary>
        ///  排序排序
        /// </summary>
        [DbColumn("OrderNo", LocalName = "排序排序", ColumnType = "int", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int OrderNo
        {
            get
            {
                return _OrderNo;
            }
            set
            {
                SetData(__OrderNo,value);
                _OrderNo = value;

            }
        }
        private string _GroupIcon;
        /// <summary>
        ///  icon
        /// </summary>
        [DbColumn("GroupIcon", LocalName = "icon", ColumnType = "string", MaxLength = 100, IsGridVisible = false, ControlType = 1)]
        public virtual string GroupIcon
        {
            get
            {
                return _GroupIcon;
            }
            set
            {
                SetData(__GroupIcon,value);
                _GroupIcon = value;

            }
        }
        private string _DefaultFilterValue;
        /// <summary>
        ///  默认搜索值
        /// </summary>
        [DbColumn("DefaultFilterValue", LocalName = "默认搜索值", ColumnType = "string", MaxLength = 100, ControlType = 1, IsGridVisible = false)]
        public virtual string DefaultFilterValue
        {
            get
            {
                return _DefaultFilterValue;
            }
            set
            {
                SetData(__DefaultFilterValue,value);
                _DefaultFilterValue = value;

            }
        }
        private string _Format;
        /// <summary>
        ///  格式
        /// </summary>
        [DbColumn("Format", LocalName = "格式", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string Format
        {
            get
            {
                return _Format;
            }
            set
            {
                SetData(__Format,value);
                _Format = value;

            }
        }
        private string _VaildKey;
        /// <summary>
        ///  正则Key
        /// </summary>
        [DbColumn("VaildKey", LocalName = "正则Key", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string VaildKey
        {
            get
            {
                return _VaildKey;
            }
            set
            {
                SetData(__VaildKey,value);
                _VaildKey = value;

            }
        }
        private bool _IsFrozen;
        /// <summary>
        ///  是否固定
        /// </summary>
        [DbColumn("IsFrozen", LocalName = "是否固定", ColumnType = "bool", AllowDBNull = false, ControlType = 4, IsGridVisible = false)]
        public virtual bool IsFrozen
        {
            get
            {
                return _IsFrozen;
            }
            set
            {
                SetData(__IsFrozen,value);
                _IsFrozen = value;

            }
        }
        private int _Min;
        /// <summary>
        ///  最小值
        /// </summary>
        [DbColumn("Min", LocalName = "最小值", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int Min
        {
            get
            {
                return _Min;
            }
            set
            {
                SetData(__Min,value);
                _Min = value;

            }
        }
        private int _Max;
        /// <summary>
        ///  最大值
        /// </summary>
        [DbColumn("Max", LocalName = "最大值", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int Max
        {
            get
            {
                return _Max;
            }
            set
            {
                SetData(__Max,value);
                _Max = value;

            }
        }
        private string _DefaultValue;
        /// <summary>
        ///  默认值
        /// </summary>
        [DbColumn("DefaultValue", LocalName = "默认值", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string DefaultValue
        {
            get
            {
                return _DefaultValue;
            }
            set
            {
                SetData(__DefaultValue,value);
                _DefaultValue = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark,value);
                _Remark = value;

            }
        }
        private string _RegValidate;
        /// <summary>
        ///  正则
        /// </summary>
        [DbColumn("RegValidate", LocalName = "正则", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1, IsGridVisible = false)]
        public virtual string RegValidate
        {
            get
            {
                return _RegValidate;
            }
            set
            {
                SetData(__RegValidate,value);
                _RegValidate = value;

            }
        }
        private string _DbTypeName;
        /// <summary>
        ///  数据类型
        /// </summary>
        [DbColumn("DbTypeName", LocalName = "数据类型", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbTypeName
        {
            get
            {
                return _DbTypeName;
            }
            set
            {
                SetData(__DbTypeName,value);
                _DbTypeName = value;

            }
        }
        #endregion
    }
}
