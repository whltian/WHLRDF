﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 字段映射关系
    /// </summary>
    [Table("CODE_DbFieldRelationType", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DbFieldRelationTypeEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_DbFieldRelationType";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DbFieldRelationTypeId";

        /// <summary>
        ///  数据库类型
        /// </summary>
        public const string __DbProviderId = "DbProviderId";

        /// <summary>
        ///  字段名称
        /// </summary>
        public const string __DbTypeName = "DbTypeName";

        /// <summary>
        ///  系统类型
        /// </summary>
        public const string __SysFieldTypeId = "SysFieldTypeId";

        #endregion
        #region 属性
        private int _DbFieldRelationTypeId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("DbFieldRelationTypeId", LocalName = "主键", ColumnType = "int", Identifier = true, IsPrimaryKey = true, AllowDBNull = false, VaildKey = "IsInt", ControlType = 2)]
        public virtual int DbFieldRelationTypeId
        {
            get
            {
                return _DbFieldRelationTypeId;
            }
            set
            {
                _DbFieldRelationTypeId = value;
            }
        }
        private int _DbProviderId;
        /// <summary>
        ///  数据库类型
        /// </summary>
        [DbColumn("DbProviderId", LocalName = "数据库类型", ColumnType = "int", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int DbProviderId
        {
            get
            {
                return _DbProviderId;
            }
            set
            {
                SetData(__DbProviderId,value);
                _DbProviderId = value;

            }
        }
        private string _DbTypeName;
        /// <summary>
        ///  字段名称
        /// </summary>
        [DbColumn("DbTypeName", LocalName = "字段名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbTypeName
        {
            get
            {
                return _DbTypeName;
            }
            set
            {
                SetData(__DbTypeName,value);
                _DbTypeName = value;

            }
        }
        private int _SysFieldTypeId;
        /// <summary>
        ///  系统类型
        /// </summary>
        [DbColumn("SysFieldTypeId", LocalName = "系统类型", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int SysFieldTypeId
        {
            get
            {
                return _SysFieldTypeId;
            }
            set
            {
                SetData(__SysFieldTypeId,value);
                _SysFieldTypeId = value;

            }
        }
        #endregion
    }
}
