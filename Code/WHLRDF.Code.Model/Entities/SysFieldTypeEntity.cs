﻿
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 系统字段类型
    /// </summary>
    [Table("CODE_SysFieldType", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class SysFieldTypeEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "CODE_SysFieldType";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "SysFieldTypeId";

        /// <summary>
        ///  类型
        /// </summary>
        public const string __DbTypeName = "DbTypeName";

        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNo = "OrderNo";

        /// <summary>
        ///  本地名称
        /// </summary>
        public const string __LocalName = "LocalName";

        /// <summary>
        ///  数据库字段
        /// </summary>
        public const string __DbTypeValue = "DbTypeValue";

        /// <summary>
        ///  正则表达式
        /// </summary>
        public const string __VaildKey = "VaildKey";

        /// <summary>
        ///  是否允许为空
        /// </summary>
        public const string __IsDbNull = "IsDbNull";

        /// <summary>
        ///  是否有长度
        /// </summary>
        public const string __IsLength = "IsLength";

        /// <summary>
        ///  控件类型
        /// </summary>
        public const string __ControlType = "ControlType";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        #endregion
        #region 属性
        private int _SysFieldTypeId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("SysFieldTypeId", LocalName = "主键", ColumnType = "int", Identifier = true, IsPrimaryKey = true, AllowDBNull = false, IsGridVisible = false, VaildKey = "IsInt", ControlType = 2)]
        public virtual int SysFieldTypeId
        {
            get
            {
                return _SysFieldTypeId;
            }
            set
            {
                _SysFieldTypeId = value;

            }
        }
        private string _DbTypeName;
        /// <summary>
        ///  类型
        /// </summary>
        [DbColumn("DbTypeName", LocalName = "类型", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbTypeName
        {
            get
            {
                return _DbTypeName;
            }
            set
            {
                SetData(__DbTypeName,value);
                _DbTypeName = value;

            }
        }
        private int _OrderNo;
        /// <summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNo", LocalName = "排序", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int OrderNo
        {
            get
            {
                return _OrderNo;
            }
            set
            {
                SetData(__OrderNo, value);
                _OrderNo = value;

            }
        }
        private string _LocalName;
        /// <summary>
        ///  本地名称
        /// </summary>
        [DbColumn("LocalName", LocalName = "本地名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {
                SetData(__LocalName, value);
                _LocalName = value;

            }
        }
        private string _DbTypeValue;
        /// <summary>
        ///  数据库字段
        /// </summary>
        [DbColumn("DbTypeValue", LocalName = "数据库字段", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string DbTypeValue
        {
            get
            {
                return _DbTypeValue;
            }
            set
            {
                SetData(__DbTypeValue, value);
                _DbTypeValue = value;

            }
        }
        private string _VaildKey;
        /// <summary>
        ///  正则表达式
        /// </summary>
        [DbColumn("VaildKey", LocalName = "正则表达式", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string VaildKey
        {
            get
            {
                return _VaildKey;
            }
            set
            {
                SetData(__VaildKey, value);
                _VaildKey = value;

            }
        }
        private bool _IsDbNull;
        /// <summary>
        ///  是否允许为空
        /// </summary>
        [DbColumn("IsDbNull", LocalName = "是否允许为空", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsDbNull
        {
            get
            {
                return _IsDbNull;
            }
            set
            {
                SetData(__IsDbNull, value);
                _IsDbNull = value;

            }
        }
        private bool _IsLength;
        /// <summary>
        ///  是否有长度
        /// </summary>
        [DbColumn("IsLength", LocalName = "是否有长度", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsLength
        {
            get
            {
                return _IsLength;
            }
            set
            {
                SetData(__IsLength, value);
                _IsLength = value;

            }
        }
        private int _ControlType;
        /// <summary>
        ///  控件类型
        /// </summary>
        [DbColumn("ControlType", LocalName = "控件类型", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int ControlType
        {
            get
            {
                return _ControlType;
            }
            set
            {
                SetData(__ControlType, value);
                _ControlType = value;

            }
        }
        private string _Remark;
        /// <summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", MaxLength = 500, ControlType = 1)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark, value);
                _Remark = value;

            }
        }
        #endregion
    }
}

