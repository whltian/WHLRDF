﻿
namespace WHLRDF.Code.Model
{
    public class EntityMappingProfile : MappingProfileBase
    {
        public EntityMappingProfile()
        {
            CreateMap<DbTableEntity, DbTreeDto>();
            CreateMap<DbServerEntity, DbTreeDto>();
        }
    }
}
