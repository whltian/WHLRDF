﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 树的映射关系
    /// </summary>
    public class DbTreeDto
    {
        public string id { get; set; }

        public string text { get; set; }

        public string parent { get; set; }

        public virtual int DbServerType { get; set; }

        public virtual string DbServerId { get; set; }

        public virtual string icon { get; set; }
        /// <summary>
        /// 下级路径
        /// </summary>
        public virtual string action { get; set; }

        public virtual string SourceId { get; set; }
        public virtual int ProviderType { get; set; }

        public bool children { get; set; }

        public string DbTableId { get; set; }
    }
}
