﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 属性列表EF 字段映射
    /// </summary>
    public partial class DbColumnMap : ClassMap<DbColumnEntity>
    {
        public override void Configure(EntityTypeBuilder<DbColumnEntity>
        builder)
        {
            builder.ToTable<DbColumnEntity>("CODE_DbColumn");
          
            builder.HasKey(x => x.ColumnId);
            builder.Property(x => x.ColumnId).HasColumnName("ColumnId").HasMaxLength(50);

            /// <summary>
            ///  表
            /// </summary>
            builder.Property(x => x.DbTableId).HasColumnName("DbTableId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  是否主键
            /// </summary>
            builder.Property(x => x.IsPrimary).HasColumnName("IsPrimary").HasDefaultValue(false);

            /// <summary>
            ///  是否自增
            /// </summary>
            builder.Property(x => x.IsIdentifier).HasColumnName("IsIdentifier").HasDefaultValue(false);

            /// <summary>
            ///  是否唯一
            /// </summary>
            builder.Property(x => x.IsUnique).HasColumnName("IsUnique").HasDefaultValue(false);

            /// <summary>
            ///  字段名
            /// </summary>
            builder.Property(x => x.ColumnName).HasColumnName("ColumnName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.ColumnCaption).HasColumnName("ColumnCaption").HasMaxLength(200).IsRequired(true);

            /// <summary>
            ///  资源Key(暂无)
            /// </summary>
            builder.Property(x => x.ColumnKeyName).HasColumnName("ColumnKeyName").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  是否允许为空
            /// </summary>
            builder.Property(x => x.AllowDBNull).HasColumnName("AllowDBNull").HasDefaultValue(true);

            /// <summary>
            ///  是否允许新增
            /// </summary>
            builder.Property(x => x.AllowInsert).HasColumnName("AllowInsert").HasDefaultValue(true);

            /// <summary>
            ///  是否允许修改
            /// </summary>
            builder.Property(x => x.AllowUpdate).HasColumnName("AllowUpdate").HasDefaultValue(true);

            /// <summary>
            ///  控件类型
            /// </summary>
            builder.Property(x => x.ControlType).HasColumnName("ControlType").HasDefaultValue(1);

            /// <summary>
            ///  字段类型
            /// </summary>
            builder.Property(x => x.DbFieldRelationTypeId).HasColumnName("DbFieldRelationTypeId").HasDefaultValue(0);

            /// <summary>
            ///  系统类型
            /// </summary>
            builder.Property(x => x.SysFieldTypeId).HasColumnName("SysFieldTypeId").HasDefaultValue(0);

            /// <summary>
            ///  长度
            /// </summary>
            builder.Property(x => x.MaxLength).HasColumnName("MaxLength").HasDefaultValue(-1);

            /// <summary>
            ///  小数
            /// </summary>
            builder.Property(x => x.Scale).HasColumnName("Scale").HasDefaultValue(0);

            /// <summary>
            ///  列表显示
            /// </summary>
            builder.Property(x => x.IsGridVisible).HasColumnName("IsGridVisible").HasDefaultValue(true);

            /// <summary>
            ///  列表宽度
            /// </summary>
            builder.Property(x => x.GridWidth).HasColumnName("GridWidth").HasDefaultValue(100);

            /// <summary>
            ///  是否系统
            /// </summary>
            builder.Property(x => x.IsSystem).HasColumnName("IsSystem").HasDefaultValue(false);

            /// <summary>
            ///  编辑显示
            /// </summary>
            builder.Property(x => x.IsEditVisible).HasColumnName("IsEditVisible").HasDefaultValue(true);

            /// <summary>
            ///  是否启用
            /// </summary>
            builder.Property(x => x.IsEnabled).HasColumnName("IsEnabled").HasDefaultValue(true);

            /// <summary>
            ///  是否排序
            /// </summary>
            builder.Property(x => x.IsSort).HasColumnName("IsSort").HasDefaultValue(true);

            /// <summary>
            ///  是否默认排序
            /// </summary>
            builder.Property(x => x.IsDefaultSort).HasColumnName("IsDefaultSort").HasDefaultValue(false);

            /// <summary>
            ///  是否条件
            /// </summary>
            builder.Property(x => x.IsFilter).HasColumnName("IsFilter").HasDefaultValue(true);

            /// <summary>
            ///  默认条件
            /// </summary>
            builder.Property(x => x.IsDefaultFilter).HasColumnName("IsDefaultFilter").HasDefaultValue(false);

            /// <summary>
            ///  描述宽度
            /// </summary>
            builder.Property(x => x.LabelWidth).HasColumnName("LabelWidth").HasDefaultValue(120);

            /// <summary>
            ///  控件宽度
            /// </summary>
            builder.Property(x => x.InputWidth).HasColumnName("InputWidth").HasDefaultValue(200);

            /// <summary>
            ///  排序排序
            /// </summary>
            builder.Property(x => x.OrderNo).HasColumnName("OrderNo").HasDefaultValue(1);

            /// <summary>
            ///  icon
            /// </summary>
            builder.Property(x => x.GroupIcon).HasColumnName("GroupIcon").HasMaxLength(100).HasDefaultValue("").IsRequired(false);

            /// <summary>
            ///  默认搜索值
            /// </summary>
            builder.Property(x => x.DefaultFilterValue).HasColumnName("DefaultFilterValue").HasMaxLength(100).HasDefaultValue("").IsRequired(false);

            /// <summary>
            ///  格式
            /// </summary>
            builder.Property(x => x.Format).HasColumnName("Format").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  正则Key
            /// </summary>
            builder.Property(x => x.VaildKey).HasColumnName("VaildKey").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  是否固定
            /// </summary>
            builder.Property(x => x.IsFrozen).HasColumnName("IsFrozen").HasDefaultValue(false);

            /// <summary>
            ///  最小值
            /// </summary>
            builder.Property(x => x.Min).HasColumnName("Min").HasDefaultValue(-1);

            /// <summary>
            ///  最大值
            /// </summary>
            builder.Property(x => x.Max).HasColumnName("Max").HasDefaultValue(0);

            /// <summary>
            ///  默认值
            /// </summary>
            builder.Property(x => x.DefaultValue).HasColumnName("DefaultValue").HasMaxLength(100).HasDefaultValue("").IsRequired(true);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).HasDefaultValue("").IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            /// <summary>
            ///  正则
            /// </summary>
            builder.Property(x => x.RegValidate).HasColumnName("RegValidate").HasMaxLength(200).HasDefaultValue("").IsRequired(true);

            /// <summary>
            ///  数据类型
            /// </summary>
            builder.Property(x => x.DbTypeName).HasColumnName("DbTypeName").HasMaxLength(50).HasDefaultValue("").IsRequired(true);
            base.Configure(builder);
        }
    }
}