﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据常用模板EF 字段映射
    /// </summary>
    public partial class DbTemplateMap : ClassMap<DbTemplateEntity>
    {
        public override void Configure(EntityTypeBuilder<DbTemplateEntity>
        builder)
        {
            builder.ToTable<DbTemplateEntity>("CODE_DbTemplate");
            
            builder.HasKey(x => x.TemplateId);
            builder.Property(x => x.TemplateId).HasColumnName("TemplateId").HasMaxLength(50);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.TemplateName).HasColumnName("TemplateName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  上级
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  深度
            /// </summary>
            builder.Property(x => x.Path).HasColumnName("Path").HasMaxLength(300).IsRequired(false);


            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x => x.DataTContent);
            builder.Ignore(x => x.id);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.children);
            builder.Ignore(x => x.icon);
        }
    }
}

