﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据库EF 字段映射
    /// </summary>
    public partial class DbServerMap : ClassMap<DbServerEntity>
    {
        public override void Configure(EntityTypeBuilder<DbServerEntity>
        builder)
        {
            builder.ToTable<DbServerEntity>("CODE_DbServer");
           
            builder.HasKey(x => x.DbServerId);
            builder.Property(x => x.DbServerId).HasColumnName("DbServerId").HasMaxLength(50);//.ValueGeneratedOnAdd();

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNo).HasColumnName("OrderNo").HasDefaultValue(0);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.DbServerName).HasColumnName("DbServerName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  本地名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  链接字符串
            /// </summary>
            builder.Property(x => x.ConntionString).HasColumnName("ConntionString").HasMaxLength(500).IsRequired(true);

            /// <summary>
            ///  类型
            /// </summary>
            builder.Property(x => x.ProviderType).HasColumnName("ProviderType").HasDefaultValue(1);


            /// <summary>
            /// 同步目标服务器
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(10).IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(10).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.id);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.SourceId);
            builder.Ignore(x => x.children);
            builder.Ignore(x => x.action);
            builder.Ignore(x => x.DbServerType);
            base.Configure(builder);
        }
    }
}