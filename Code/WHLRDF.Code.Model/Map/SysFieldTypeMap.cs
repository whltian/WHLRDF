﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 系统字段类型EF 字段映射
    /// </summary>
    public partial class SysFieldTypeMap : ClassMap<SysFieldTypeEntity>
    {
        public override void Configure(EntityTypeBuilder<SysFieldTypeEntity>
        builder)
        {
            builder.ToTable<SysFieldTypeEntity>("CODE_SysFieldType");
            
            builder.HasKey(x => x.SysFieldTypeId);
            builder.Property(x => x.SysFieldTypeId).HasColumnName("SysFieldTypeId").ValueGeneratedOnAdd();

            /// <summary>
            ///  类型
            /// </summary>
            builder.Property(x => x.DbTypeName).HasColumnName("DbTypeName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNo).HasColumnName("OrderNo").HasDefaultValue(0);

            /// <summary>
            ///  本地名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  数据库字段
            /// </summary>
            builder.Property(x => x.DbTypeValue).HasColumnName("DbTypeValue").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  正则表达式
            /// </summary>
            builder.Property(x => x.VaildKey).HasColumnName("VaildKey").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  是否允许为空
            /// </summary>
            builder.Property(x => x.IsDbNull).HasColumnName("IsDbNull").HasDefaultValue(false);

            /// <summary>
            ///  是否有长度
            /// </summary>
            builder.Property(x => x.IsLength).HasColumnName("IsLength").HasDefaultValue(false);

            /// <summary>
            ///  控件类型
            /// </summary>
            builder.Property(x => x.ControlType).HasColumnName("ControlType").HasDefaultValue(1);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            base.Configure(builder);
        }
    }
}