﻿


using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 字段映射关系EF 字段映射
    /// </summary>
    public partial class DbFieldRelationTypeMap : ClassMap<DbFieldRelationTypeEntity>
    {
        public override void Configure(EntityTypeBuilder<DbFieldRelationTypeEntity>
        builder)
        {
            builder.ToTable<DbFieldRelationTypeEntity>("CODE_DbFieldRelationType");
            
            builder.HasKey(x => x.DbFieldRelationTypeId);
            builder.Property(x => x.DbFieldRelationTypeId).HasColumnName("DbFieldRelationTypeId").ValueGeneratedOnAdd();

            /// <summary>
            ///  数据库类型
            /// </summary>
            builder.Property(x => x.DbProviderId).HasColumnName("DbProviderId");

            /// <summary>
            ///  字段名称
            /// </summary>
            builder.Property(x => x.DbTypeName).HasColumnName("DbTypeName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  系统类型
            /// </summary>
            builder.Property(x => x.SysFieldTypeId).HasColumnName("SysFieldTypeId");

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}