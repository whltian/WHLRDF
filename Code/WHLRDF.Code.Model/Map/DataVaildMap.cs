﻿


using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据常用验证列表EF 字段映射
    /// </summary>
    public partial class DataVaildMap : ClassMap<DataVaildEntity>
    {
        public override void Configure(EntityTypeBuilder<DataVaildEntity>
        builder)
        {
            builder.ToTable<DataVaildEntity>("CODE_DataVaild");
            
            builder.HasKey(x => x.DataVaildId);
            builder.Property(x => x.DataVaildId).HasColumnName("DataVaildId").ValueGeneratedOnAdd();

            /// <summary>
            ///  键值
            /// </summary>
            builder.Property(x => x.KeyName).HasColumnName("KeyName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  正则
            /// </summary>
            builder.Property(x => x.Regex).HasColumnName("Regex").HasMaxLength(1000).IsRequired(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}