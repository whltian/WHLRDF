﻿


using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Code.Model
{
    /// <summary>
    /// 数据库表EF 字段映射
    /// </summary>
    public partial class DbTableMap : ClassMap<DbTableEntity>
    {
        public override void Configure(EntityTypeBuilder<DbTableEntity>
        builder)
        {
            builder.ToTable<DbTableEntity>("CODE_DbTable");
            
            builder.HasKey(x => x.DbTableId);
            builder.Property(x => x.DbTableId).HasColumnName("DbTableId").HasMaxLength(50);

            /// <summary>
            ///  数据库id
            /// </summary>
            builder.Property(x => x.DbServerId).HasColumnName("DbServerId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNo).HasColumnName("OrderNo").HasDefaultValue(0);

            /// <summary>
            ///  表名
            /// </summary>
            builder.Property(x => x.DbTableName).HasColumnName("DbTableName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  本地名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(200).IsRequired(true);

            /// <summary>
            ///  是否是表
            /// </summary>
            builder.Property(x => x.IsTable).HasColumnName("IsTable").HasDefaultValue(true);

            /// <summary>
            ///  所属模块
            /// </summary>
            builder.Property(x => x.DbNamespace).HasColumnName("DbNamespace").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(255).IsRequired(false);

            /// <summary>
            ///  启用创建人
            /// </summary>
            builder.Property(x => x.IsEnableCreate).HasColumnName("IsEnableCreate").HasDefaultValue(true);

            /// <summary>
            ///  启用修改人
            /// </summary>
            builder.Property(x => x.IsEnableLastModify).HasColumnName("IsEnableLastModify").HasDefaultValue(true);

            /// <summary>
            ///  启用假删除
            /// </summary>
            builder.Property(x => x.IsEnableIsDeleted).HasColumnName("IsEnableIsDeleted").HasDefaultValue(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            base.Configure(builder);

            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.id);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.SourceId);
            builder.Ignore(x => x.children);
            builder.Ignore(x => x.action);
            builder.Ignore(x => x.DbServerType);
            builder.Ignore(x => x.ProviderType);
            builder.Ignore(x => x.ColumnEntities);

        }
    }
}