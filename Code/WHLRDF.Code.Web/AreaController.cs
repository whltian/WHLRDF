﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;
namespace WHLRDF.Code.Web
{

    [MenuPage(1200, "200", "代码生成管理", "Code", "fa-code")]
    [Area("Code")]
    [Route("Code/{controller=Home}/{action=Index}/{id?}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "Code";
            }
        }
    }
}
