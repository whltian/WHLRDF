﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.Code.BLL;
using WHLRDF.Code.Model;
using WHLRDF.ORM;

namespace WHLRDF.Code.Web.Controllers
{
    [MenuPage(40, "040", "模板管理", "Code/DbTemplate", "fa-pencil-square")]
    public class DbTemplateController : AreaController
    {
        #region Service注入

        public IDbTemplateService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid ligerGrid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(ligerGrid)));
        }

        /// <summary>
        /// 保存 编辑方法
        ///</summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false)]
        public IActionResult Edit(string id, DbTemplateEntity entity)
        {
            if (this.IsPost)
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            if (string.IsNullOrWhiteSpace(entity.ParentId))
                            {
                                entity.ParentId = "000";
                            }
                            return Json(AjaxResult.Success(entity));
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                }
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
        [HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
        #region
        [HttpGet]
        public IActionResult _Edit(string id, DbTemplateEntity entity)
        {
            //string parentid = entity.ParentId;
            //if (!string.IsNullOrEmpty(id))
            //{
            //    entity = mService.GetById(id);
            //}
            //if (entity == null)
            //{
            //    entity = new DbTemplateEntity
            //    {
            //        ParentId = parentid
            //    };
            //}
            return View(entity);
        }
        #endregion
    }
}
