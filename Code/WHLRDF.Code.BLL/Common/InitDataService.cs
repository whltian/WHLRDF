﻿
using WHLRDF.Code.Model;
using WHLRDF.ORM;
using System;
using System.Collections.Generic;
namespace WHLRDF.Code.BLL
{

    public class InitializeService : SerivceBase ,IInitializeService{

        #region 私有变量
        private List<DbTemplateEntity> dbTemplateEntities = null;
        private List<SysFieldTypeEntity> sysFieldTypeEntities = null;
        private List<DataVaildEntity> dataVaildEntities = null;
        private List<DbFieldRelationTypeEntity> dbFieldRelationTypeEntities = null;
  
        #endregion

        public bool Read(IDbRepository dbRepository, ref Dictionary<string, object> dicResult, ref string strError)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            dbTemplateEntities = dbRepository.Query<DbTemplateEntity>(criter);
            sysFieldTypeEntities = dbRepository.Query<SysFieldTypeEntity>(criter);
            dataVaildEntities = dbRepository.Query<DataVaildEntity>(criter);
            dbFieldRelationTypeEntities = dbRepository.Query<DbFieldRelationTypeEntity>(criter);
           
            if (dicResult.ContainsKey(nameof(dbTemplateEntities)))
            {
                strError = nameof(dbTemplateEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(sysFieldTypeEntities)))
            {
                strError = nameof(sysFieldTypeEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(dataVaildEntities)))
            {
                strError = nameof(dataVaildEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(dbFieldRelationTypeEntities)))
            {
                strError = nameof(dbFieldRelationTypeEntities) + "已存在";
                return false;
            }
            dicResult.Add(nameof(dbTemplateEntities), dbTemplateEntities);
            dicResult.Add(nameof(sysFieldTypeEntities), sysFieldTypeEntities);
            dicResult.Add(nameof(dataVaildEntities), dataVaildEntities);
            dicResult.Add(nameof(dbFieldRelationTypeEntities), dbFieldRelationTypeEntities);
            return true;
        }

        public bool Write(IDbRepository dbRepository, Dictionary<string, object> dicResult, ref string strError)
        {
            try
            {
                if (dicResult.ContainsKey(nameof(dbTemplateEntities)) && dicResult[nameof(dbTemplateEntities)] != null)
                {
                    dbTemplateEntities = JSONHelper.FromJson<List<DbTemplateEntity>>(dicResult[nameof(dbTemplateEntities)].ToJson());
                    if (dbTemplateEntities != null && dbTemplateEntities.Count > 0)
                    {
                        dbTemplateEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(sysFieldTypeEntities)) && dicResult[nameof(sysFieldTypeEntities)] != null)
                {
                    sysFieldTypeEntities = JSONHelper.FromJson<List<SysFieldTypeEntity>>(dicResult[nameof(sysFieldTypeEntities)].ToJson());
                    if (sysFieldTypeEntities != null && sysFieldTypeEntities.Count > 0)
                    {
                        sysFieldTypeEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(dataVaildEntities)) && dicResult[nameof(dataVaildEntities)] != null)
                {
                    dataVaildEntities = JSONHelper.FromJson<List<DataVaildEntity>>(dicResult[nameof(dataVaildEntities)].ToJson());
                    if (dataVaildEntities != null && dataVaildEntities.Count > 0)
                    {
                        dataVaildEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(dbFieldRelationTypeEntities)) && dicResult[nameof(dbFieldRelationTypeEntities)] != null)
                {
                    dbFieldRelationTypeEntities = JSONHelper.FromJson<List<DbFieldRelationTypeEntity>>(dicResult[nameof(dbFieldRelationTypeEntities)].ToJson());
                    if (dbFieldRelationTypeEntities != null && dbFieldRelationTypeEntities.Count > 0)
                    {
                        dbFieldRelationTypeEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
               
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }

            if (dbTemplateEntities != null && dbTemplateEntities.Count > 0)
            {
                dbRepository.BatchInsert<DbTemplateEntity>(dbTemplateEntities, ref strError);
            }
            if (sysFieldTypeEntities != null && sysFieldTypeEntities.Count > 0)
            {
                dbRepository.BatchInsert<SysFieldTypeEntity>(sysFieldTypeEntities, ref strError);
            }
            if (dataVaildEntities != null && dataVaildEntities.Count > 0)
            {
                dbRepository.BatchInsert<DataVaildEntity>(dataVaildEntities, ref strError);
            }
            if (dbFieldRelationTypeEntities != null && dbFieldRelationTypeEntities.Count > 0)
            {
                dbRepository.BatchInsert<DbFieldRelationTypeEntity>(dbFieldRelationTypeEntities, ref strError);
            }
           
            return true;
        }
    }
}
