﻿

using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Code.Model;
using WHLRDF.ORM;

namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 字段映射关系实例
    /// </summary>
    public class DbFieldRelationTypeService : SerivceBase, IDbFieldRelationTypeService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DbFieldRelationTypeEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DbFieldRelationTypeEntity.__IsDeleted, false);
            return this.Query<DbFieldRelationTypeEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public DbFieldRelationTypeEntity GetById(string id)
        {
            return this.GetById<DbFieldRelationTypeEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DbFieldRelationTypeEntity entity, ref string strError)
        {
            if (Exist(entity.DbTypeName,entity.DbProviderId, entity.DbFieldRelationTypeId))
            {
                strError = entity.DbTypeName + "已存在，请不要重复输入";
                return false;
            }
            
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate<DbFieldRelationTypeEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<DbFieldRelationTypeEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DbFieldRelationTypeEntity.__DbTypeName 
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<DbFieldRelationTypeEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name,int providerType, int key)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.And(Expression.Eq(DbFieldRelationTypeEntity.__DbTypeName, name),
                Expression.NotEq(DbFieldRelationTypeEntity._PrimaryKeyName, key)), Expression.Eq(DbFieldRelationTypeEntity.__DbProviderId, providerType))
                );
            var result = this.Query<DbFieldRelationTypeEntity>(criter).FirstOrDefault();
            return result != null;
        }

        public List<DbFieldRelationTypeEntity> GetAll(ProviderType providerType)
        {
            ICriterion criter = Expression.And(Expression.Eq("IsDeleted", false), Expression.Eq("DbProviderId", providerType.GetHashCode()));
            return this.Query<DbFieldRelationTypeEntity>(criter).ToList();

        }

        /// <summary>
        /// 通过typename获取类型实体
        /// </summary>
        /// <param name="providerType"></param>
        /// <param name="dbTypeName"></param>
        /// <returns></returns>
        public DbFieldRelationTypeEntity GetByTypeName(ProviderType providerType, string dbTypeName)
        {
            ICriterion criter = Expression.And(Expression.Eq(DbFieldRelationTypeEntity.__DbTypeName, dbTypeName), Expression.And(Expression.Eq(DbFieldRelationTypeEntity.__IsDeleted, false), Expression.Eq(DbFieldRelationTypeEntity.__DbProviderId, providerType.GetHashCode())));
           return this.Select<DbFieldRelationTypeEntity>(criter);
        }
        #endregion

        #region 事件

        #endregion
    }
}