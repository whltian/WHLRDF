﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Code.Model;
using WHLRDF.ORM;

namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 系统字段类型实例
    /// </summary>
    public class SysFieldTypeService : SerivceBase, ISysFieldTypeService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<SysFieldTypeEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(SysFieldTypeEntity.__IsDeleted, false);
            return this.Query<SysFieldTypeEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public SysFieldTypeEntity GetById(string id)
        {
            return this.GetById<SysFieldTypeEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(SysFieldTypeEntity entity, ref string strError)
        {
            if (Exist(entity.DbTypeName, entity.SysFieldTypeId))
            {
                strError = entity.DbTypeName + "已存在，请不要重复输入";
                return false;
            }
           
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate<SysFieldTypeEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<SysFieldTypeEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                SysFieldTypeEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<SysFieldTypeEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, int key)
        {
        
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(SysFieldTypeEntity.__DbTypeName, name),
                Expression.NotEq(SysFieldTypeEntity._PrimaryKeyName, key))
                );
            var result = this.Query<SysFieldTypeEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

        #region 事件

        #endregion
    }
}