﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Code.Model;
using WHLRDF.ORM;
namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 数据库实例
    /// </summary>
    public class DbServerService : SerivceBase, IDbServerService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DbServerEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DbServerEntity.__IsDeleted, false);
            return this.Query<DbServerEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public DbServerEntity GetById(string id)
        {
            return this.GetById<DbServerEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DbServerEntity entity, ref string strError)
        {
            if (Exist(entity.LocalName, entity.DbServerId))
            {
                strError = entity.LocalName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.DbServerId))
            {
                entity.DbServerId = MyGenerateHelper.GenerateOrder();
            }
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate<DbServerEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<DbServerEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DbServerEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<DbServerEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 获取子服务器
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<DbServerEntity> GetChildren(string parentId)
        {
            ICriterion criter = Expression.Eq(DbServerEntity.__IsDeleted, false);
            if (string.IsNullOrWhiteSpace(parentId)|| parentId.Equals("0"))
            {
                criter = Expression.And(criter, Expression.IsNull(DbServerEntity.__ParentId));
            }
            else {
                criter = Expression.And(criter, Expression.Eq(DbServerEntity.__ParentId,parentId));
            }
            return this.Query<DbServerEntity>(criter).ToList();

        }
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, string key)
        {
          

            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(DbServerEntity.__LocalName, name),
                Expression.NotEq(DbServerEntity._PrimaryKeyName, key))
                );
            var result = this.Query<DbServerEntity>(criter).FirstOrDefault();
            return result != null;
        }

        public TargetDbServerModel GetTargetDbServer(TargetDbServerModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.SourceServerId))
            {
                var entity = this.GetById(model.SourceServerId);
                if (entity != null)
                {
                    string parentId = string.IsNullOrWhiteSpace(entity.ParentId) ? entity.DbServerId : entity.ParentId;
                    entity = this.GetById(parentId);
                    model.DbServers = this.GetChildren(parentId);
                    if (model.DbServers == null)
                    {
                        model.DbServers = new List<DbServerEntity>();
                    }
                    model.DbServers.Insert(0, entity);
                }

            }
            return model;
        }
        #endregion

        #region 事件

        #endregion
    }
}