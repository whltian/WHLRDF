﻿using WHLRDF.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Code.Model;
namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 属性列表实例
    /// </summary>
    public class DbColumnService : SerivceBase, IDbColumnService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DbColumnEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DbColumnEntity.__IsDeleted, false);
            return this.Query<DbColumnEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public DbColumnEntity GetById(string id)
        {
            return this.GetById<DbColumnEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DbColumnEntity entity, ref string strError)
        {
            if (Exist(entity.ColumnName, entity.ColumnId))
            {
                strError = entity.ColumnName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.ColumnId))
            {
                entity.ColumnId = MyGenerateHelper.GenerateOrder();
            }
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate<DbColumnEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<DbColumnEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DbColumnEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<DbColumnEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, string key)
        {
            key = (string.IsNullOrWhiteSpace(key)) ? "" : key;

            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(DbColumnEntity.__ColumnName, name),
                Expression.NotEq(DbColumnEntity._PrimaryKeyName, key))
                );
            var result = this.Query<DbColumnEntity>(criter).FirstOrDefault();
            return result != null;
        }

        public bool Save(List<DbColumnEntity> lstEntity, ref string strError)
        {

            using (var tran = this.Begin())
            {
                try
                {
                    foreach (var entity in lstEntity)
                    {
                        this.SaveOrUpdate<DbColumnEntity>(entity);
                    }
                    this.Commit();
                }
                catch (Exception ex)
                {

                    this.Rollback();
                    throw ex;
                }
            }
            //SessionFactory.Flush();;

            return true;

        }
        #endregion

        #region 事件

        #endregion
    }
}