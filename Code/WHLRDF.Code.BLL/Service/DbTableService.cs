﻿

using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Code.Model;
using WHLRDF.ORM;
namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 数据库表实例
    /// </summary>
    public class DbTableService : SerivceBase, IDbTableService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DbTableEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DbTableEntity.__IsDeleted, false);
            return this.Query<DbTableEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id"></param>
        /// </summary>
        public DbTableEntity GetById(string id)
        {
            return this.GetById<DbTableEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DbTableEntity entity, ref string strError)
        {
            if (Exist(entity.DbTableName, entity.DbTableId,entity.DbServerId))
            {
                strError = entity.DbTableName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.DbTableId))
            {
                entity.DbTableId = MyGenerateHelper.GenerateOrder();
            }
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate<DbTableEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<DbTableEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DbTableEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<DbTableEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, string key,string dbServerId)
        {
         
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(DbTableEntity.__DbTableName, name),
                 Expression.And(Expression.Eq(DbTableEntity.__DbServerId, dbServerId),
                 Expression.NotEq(DbTableEntity._PrimaryKeyName, key)))
                );
            var result = this.Query<DbTableEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

        #region 事件

        #endregion
    }
}