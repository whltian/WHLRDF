﻿using System.Collections.Generic;
using WHLRDF.Code.Model;
using WHLRDF.ORM;

namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 字段映射关系接口
    /// </summary>
    public partial interface IDbFieldRelationTypeService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<DbFieldRelationTypeEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        DbFieldRelationTypeEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(DbFieldRelationTypeEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid ligerGrid);
        #endregion

        #region 扩展方法
        List<DbFieldRelationTypeEntity> GetAll(ProviderType providerType);

        /// <summary>
        /// 通过名称数据库驱动获取类型
        /// </summary>
        /// <param name="providerType">数据库驱动</param>
        /// <param name="columnTypeName">字段类型</param>
        /// <returns></returns>
        DbFieldRelationTypeEntity GetByTypeName(ProviderType providerType,string columnTypeName);
        #endregion

    }
}