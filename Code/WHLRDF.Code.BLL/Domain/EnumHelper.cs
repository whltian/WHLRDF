﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 控件类别
    /// </summary>
    public enum ControlType
    {
        /// <summary>
        /// 文本框
        /// </summary>
        [Description("text")]
        text = 1,
        /// <summary>
        /// 隐藏文本框
        /// </summary>
        [Description("hidden")]
        hidden = 2,
        /// <summary>
        /// 下拉框
        /// </summary>
        [Description("select")]
        select = 3,
        /// <summary>
        /// checkbox
        /// </summary>
        [Description("checkbox")]
        checkbox = 4,
        /// <summary>
        /// 文本域
        /// </summary>
        [Description("textarea")]
        textarea = 5,
        /// <summary>
        /// 整数
        /// </summary>
        [Description("number")]
        number = 6,
        /// <summary>
        /// 日期
        /// </summary>
        [Description("datetime")]
        date = 7,
        /// <summary>
        /// 编辑器
        /// </summary>
        [Description("summernote")]
        summernote = 8,
        /// <summary>
        /// 代码编辑器
        /// </summary>
        [Description("codemirror")]
        codemirror = 10

    }
}
