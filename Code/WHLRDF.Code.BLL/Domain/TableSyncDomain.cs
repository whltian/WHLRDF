﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.ORM;
namespace WHLRDF.Code.BLL
{
    /// <summary>
    /// 同步数据对象
    /// </summary>
    public class TableSyncDomain
    {
        public string hql { get; set; }

        public DataParameterCollection dbParameters { get; set; }
    }
}
