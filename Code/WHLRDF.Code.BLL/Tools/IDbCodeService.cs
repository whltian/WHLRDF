﻿using Microsoft.AspNetCore.Http;
using WHLRDF.Code.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WHLRDF.ORM;

namespace WHLRDF.Code.BLL
{
    public interface IDbCodeService
    {
        bool IsDbServerExist(DbServerEntity entity);

        bool IsDbTableExist(DbTableEntity entity);

        bool DbTableSave(DbTableEntity entity, ref string strError);

        bool DbDelete(string KeyId, string dbServerType, ref string strError);

        /// <summary>
        /// 通过模板生成文件
        /// </summary>
        /// <param name="tableId">表id</param>
        /// <param name="templateid">模板id</param>
        /// <param name="strResult">返回值</param>
        /// <returns></returns>
        bool CreateCode(string tableId, string templateid, ref string strResult);

        /// <summary>
        /// 获取表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="IsTable"></param>
        /// <returns></returns>
        List<DbTableEntity> GetDbTable(string id, bool IsTable);

        /// <summary>
        /// 获取字段
        /// </summary>
        /// <param name="tableid">表</param>
        /// <returns></returns>
        List<DbColumnEntity> GetColumns(string tableid);

        /// <summary>
        /// 获取字段
        /// </summary>
        /// <param name="tableid"></param>
        /// <param name="entity"></param>
        /// <param name="providerType"></param>
        /// <returns></returns>
        List<DbColumnEntity> GetColumns(string tableid, ref DbTableEntity entity, ref int providerType);

        LigerGrid GetDbSource(LigerGrid grid, string id);

        byte[] Export(LigerGrid grid, string id);

        /// <summary>
        /// 更新修改的列
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dbColumn"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveColumnsCell(string id, DbColumnEntity dbColumn, ref string strError);

        /// <summary>
        /// 批量生成模板
        /// </summary>
        /// <param name="tableId"></param>
        /// <param name="templateid"></param>
        /// <param name="strError"></param>
        /// <param name="round"></param>
        /// <param name="maxTableLength"></param>
        /// <returns></returns>
        Task<bool> CreateCodes(string tableId, string templateid,  int round = 0,int maxTableLength=0);

        /// <summary>
        /// 同步表数据
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ligerGrid"></param>
        /// <returns></returns>
        TaskModel syncTableData(TargetDbServerModel model, LigerGrid ligerGrid);

        /// <summary>
        /// 同步表结构
        /// </summary>
        /// <param name="tableid">表名</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool syncTableStruct(TargetDbServerModel model, ref string strError);

        /// <summary>
        /// 下载导出模板
        /// </summary>
        /// <param name="tableid">主键</param>
        /// <param name="tableName">表名</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        byte[] DownTableTemplate(string tableid,ref string tableName, ref string strError);

        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="file"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool Import(string tableid,IFormFile file, ref string strError);

        /// <summary>
        /// 表结构导出
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        byte[] ExportTable(string tableid, ref string strError);

        byte[] ExportWordTable(string tableid, ref string strError);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentid"></param>
        /// <param name="dbServerType"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        List<DbTreeDto> GetDbTrees(string parentid, int dbServerType, string action);
    }
}
