
using System;
using System.Data;
using System.Data.Common;
using WHLRDF.ORM.Ado;

namespace WHLRDF.ORM
{
  
	public partial class AdoRepository: IAdoRepository, IDisposable
    {
        #region 所有原始方法
        Provider<AbstractDatabase> _provider;
        /// <summary>
        /// 构造函数
        /// </summary>
        public AdoRepository()
		{
            // ReflectionHelper.CheckCallingAssembly(Assembly.GetCallingAssembly());
            DbVersion = ApplicationEnvironments.Site.ProviderVersion;
            var dbtype = Type.GetType($"{this.GetType().Namespace}.{ApplicationEnvironments.Site.ProviderType.ToString()}.AdoDataAccess,{this.GetType().Namespace}.{ApplicationEnvironments.Site.ProviderType.ToString()}");
            _provider =CreateInstance(dbtype, ApplicationEnvironments.Site.ConnectionString, DbVersion);
        }

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="connection">提供连接类</param>
        public AdoRepository(Provider<AbstractDatabase> provider,string version)
		{
            // ReflectionHelper.CheckCallingAssembly(Assembly.GetCallingAssembly());
            DbVersion = version;
            //从参数中取得
            _provider = provider;
       
        }

        public AdoRepository(Type dataInstanceType, string conntionString,string version)
        {
            DbVersion = version;
            _provider =CreateInstance(dataInstanceType, conntionString, version);
        }
        public AdoRepository(ProviderType providerType, string conntionString,string version)
        {
            string strProviderName = providerType.ToString();
            if (strProviderName.IndexOf("_") > 0)
            {
                strProviderName = strProviderName.Substring(0, strProviderName.IndexOf("_"));
            }
            var dbtype = Type.GetType($"{this.GetType().Namespace}.{strProviderName}.AdoDataAccess,{this.GetType().Namespace}.{strProviderName}");
            _provider = CreateInstance(dbtype, conntionString, version);
            DbVersion = version;
        }
        /// <summary>
        /// 创建并获取数据库驱动
        /// </summary>
        /// <param name="providerType"></param>
        /// <param name="conntionString"></param>
        /// <returns></returns>
        public Provider<AbstractDatabase> CreateInstance(Type dataInstanceType, string conntionString,string version)
        {
            return ProviderFactory.CreateProvider<AbstractDatabase>(conntionString, dataInstanceType, version);
        }
        /// <summary>
        /// 获取Connection
        /// </summary>
        /// <returns>Connection</returns>
        public DbConnection GetConnection()
		{
            return _provider.Database.GetConnection(_provider.ConnectionString);
		}

        public IProvider GetProvider()
        {
            return _provider;
        }

        

        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
		{
            _provider.Database.Close();
		}
        public bool AutoOpenClose
        {
            get {
                return _provider.Database.AutoOpenClose;
            }
        }
        public void Open()
        {
            _provider.Database.Open();
        }
        public IDbTransaction Begin()
        {
           return _provider.Database.Begin();
        }
        public void Commit()
        {
            _provider.Database.Commit();
        }
        public void Rollback()
        {
            _provider.Database.Rollback();
        }
		/// <summary>
		/// 执行查询
		/// </summary>
		/// <param name="sqlCommand"></param>
        public int Execute(string sqlCommand)
        {
            return Execute(sqlCommand,null);
        }

        public int Execute(string sqlCommand,DataParameterCollection parameterValues)
        {
            return _provider.Database.ExecuteNonQuery(CommandType.Text, sqlCommand, parameterValues != null? parameterValues.ToArray():null);
        }
        public int ExecuteProcedure(string procedureName)
        {
            return ExecuteProcedure(procedureName, null);
        }

        public int ExecuteProcedure(string procedureName, DataParameterCollection parameterValues)
        {
            return _provider.Database.ExecuteProcedure(procedureName, parameterValues != null ? parameterValues.ToArray() : null);
        }
        /// <summary>
        /// 执行sql并返回DataSet
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public DataTable DataTable(string sqlCommand)
        {
            return DataTable(sqlCommand,null);
        }
        public DataTable DataTable(string sqlCommand, DataParameterCollection parameterValues)
        {
            return _provider.Database.Fill(CommandType.Text, sqlCommand, parameterValues != null ? parameterValues.ToArray() : null);
        }

		/// <summary>
		/// 执行存储过程并返回DataSet
		/// </summary>
		/// <param name="storedProcedureName"></param>
		/// <returns></returns>
        public DataTable StoredProcedure(string storedProcedureName)
        {
            return this.StoredProcedure(storedProcedureName,null);
        }		

		/// <summary>
		/// 执行存储过程并返回DataSet
		/// </summary>
		/// <param name="storedProcedureName"></param>
		/// <param name="parameterValues"></param>
		/// <returns></returns>
        public DataTable StoredProcedure(string storedProcedureName, DataParameterCollection parameterValues)
        {
            return _provider.Database.ExecuteProcedureForDataTable(storedProcedureName, "newTable", parameterValues != null ? parameterValues.ToArray() : null);
        }


        /// <summary>
        /// 执行sql并返回DataReader
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public IDataReader Reader(string sqlCommand)
        {

            return this.Reader(sqlCommand,null);
        }

        public IDataReader Reader(string sqlCommand,DataParameterCollection parameters)
        {

            return this.Reader(CommandType.Text,sqlCommand, parameters);
        }

        public IDataReader Reader(CommandType commandType,string sqlCommand, DataParameterCollection parameters)
        {
            return _provider.Database.ExecuteReader(commandType, sqlCommand, parameters != null ? parameters.ToArray() : null);
        }

        public object Scalar(string hql)
        {
            return _provider.Database.ExecuteScalar(hql);
        }

        public object Scalar(string hql, DataParameterCollection Parameters)
        {
            return Scalar(CommandType.Text, hql, Parameters);
        }

        public object Scalar(CommandType commandType,string ProcText, DataParameterCollection Parameters)
        {
            return _provider.Database.ExecuteScalar(commandType, ProcText, Parameters != null ? Parameters.ToArray() : null);
        }
        public object Scalar(string sql, ICriterion criterion)
        {
            DataParameterCollection dbParameters = new DataParameterCollection(this);
            string where = criterion!=null?criterion.ToString(ref dbParameters):"";
            sql = string.Format(sql, where);
            return this.Scalar(sql, dbParameters);
        }


        public DataParameter GetParameter(string name, object value)
        {
            return new DataParameter(name, value) {  };
        }
        public string GetParameter(string name)
        {
            return _provider.Database.GetParameter(name);
        }

        public string GetLikeParameter(string name)
        {
            return _provider.Database.GetLikeParameter(name);
        }
        /// <summary>
        /// 选择自动递增列的最大值
        /// </summary>
        /// <returns></returns>
        public int SelectIdentity(string primaryKeyName)
		{

            return _provider.Database.SelectIdentity(primaryKeyName);
			
		}

        /// <summary>
        /// 选择自动递增列的最大值
        /// </summary>
        /// <returns></returns>
        public string IdentitySql(string primaryKeyName)
        {
            return _provider.Database.Identity(primaryKeyName);
        }
        public void Dispose()
        {
            _provider.Database.Close();
        }

      

        public DateTime Now
        {
            get
            {
                return _provider.Database.GetNow();
            }
        }

        public ProviderType DbProviderType { get => _provider.Database.DbProviderType; }

        public string DbVersion { get; set; }

        public DataParameterCollection GetDbParameters()
        {
            return new DataParameterCollection(this);
        }
        #endregion



    }
}
