﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace WHLRDF.ORM.Ado
{
     public interface IAdoRepository: IDbRepository
    {
        DbConnection GetConnection();

        IProvider GetProvider();

        

        //IDbTransaction Begin();

        //void Commit();

        //void Rollback();
      

        #region 查询
        DataTable DataTable(string sqlCommand);

        DataTable DataTable(string sqlCommand, DataParameterCollection parameterValues);

        DataTable StoredProcedure(string storedProcedureName);

        DataTable StoredProcedure(string storedProcedureName, DataParameterCollection parameterValues);

        IDataReader Reader(string sqlCommand);

       

        IDataReader Reader(CommandType commandType, string sqlCommand, DataParameterCollection parameters);
        #endregion

        #region Scalar
        object Scalar(CommandType commandType, string ProcText, DataParameterCollection Parameters);
        #endregion


    }
}
