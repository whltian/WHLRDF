﻿//------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2010 , Jirisoft , Ltd. 
//------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Reflection;
using WHLRDF.Log;
using WHLRDF.ORM.DbExtensions;
namespace WHLRDF.ORM.Ado
{
   
    public abstract class AbstractDatabase: IQuery, IDbSource
    {
        
        private DbConnection dbConnection = null;

        #region public DbConnection DbConnection 数据库适配器
        /// <summary>
        /// 数据库连接
        /// </summary>
        public DbConnection DbConnection
        {
            get
            {
                if (this.dbConnection == null|| this.dbConnection.State!=ConnectionState.Open)
                {
                    // 若没打开，就变成自动打开关闭的
                    this.Open();
                    this.AutoOpenClose = true;
                }
                return this.dbConnection;
            }
            set
            {
                this.dbConnection = value;
            }
        }
        #endregion


        private DbCommand dbCommand = null;

        #region public DbCommand DbCommand 命令
        /// <summary>
        /// 命令
        /// </summary>
        public DbCommand DbCommand
        {
            get
            {
                return this.dbCommand;
            }

            set
            {
                this.dbCommand = value;
            }
        }
        #endregion



        private DbDataAdapter dbDataAdapter = null;

        #region public DbDataAdapter DbDataAdapter 数据库适配器
        /// <summary>
        /// 数据库适配器
        /// </summary>
        public DbDataAdapter DbDataAdapter
        {
            get
            {
                return this.dbDataAdapter;
            }

            set
            {
                this.dbDataAdapter = value;
            }
        }
        #endregion

        private string connectionString = string.Empty;

        #region public string ConnectionString 数据库连接
        /// <summary>
        /// 数据库连接
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return this.connectionString;
            }

            set
            {
                this.connectionString = value;
            }
        }
        #endregion

        private DbTransaction dbTransaction = null;
        // 是否已在事务之中
        private bool inTransaction = false;
        
        #region public bool InTransaction 是否已采用事务
        /// <summary>
        /// 是否已采用事务
        /// </summary>
        public bool InTransaction
        {
            get
            {
                return this.inTransaction;
            }

            set
            {
                this.inTransaction = value;
            }
        }
        #endregion
        
        /// <summary>
        /// 数据库版本号
        /// </summary>
        public string Version { get; set; }
       

        private bool autoOpenClose = false;
        /// <summary>
        /// 默认打开关闭数据库选项（默认为否）
        /// </summary>
        public bool AutoOpenClose
        {
            get
            {
                return autoOpenClose;
            }
            set
            {
                autoOpenClose = value;
            }
        }

        public abstract ProviderType DbProviderType { get; }

        /// <summary>
        /// DbProviderFactory实例
        /// </summary>
        public abstract DbProviderFactory DbFactory{get;}

        public AbstractDatabase()
        {
            this.ConnectionString = ApplicationEnvironments.Site.ConnectionString;
            Version = ApplicationEnvironments.Site.Version;
        }
        public AbstractDatabase(string connectionString,string version)
        {
            this.ConnectionString = connectionString;
            Version = version;
        }
        #region public IDbConnection Open()

        /// <summary>
        /// 这时主要的获取数据库连接的方法
        /// </summary>
        /// <returns>数据库连接</returns>
        public IDbConnection Open()
		{
            #if (DEBUG)
                int milliStart = Environment.TickCount;
			#endif
            // 这里是获取一个连接的详细方法
            if (String.IsNullOrEmpty(this.ConnectionString))
            {
                this.ConnectionString = ApplicationEnvironments.Site.ConnectionString;
            }
            this.Open(this.ConnectionString);
            // 写入调试信息
            #if (DEBUG)
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            return this.dbConnection;
		}
		#endregion

        #region public IDbConnection Open(string connectionString)

       
        /// <summary>
		/// 获得新的数据库连接
		/// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
		/// <returns>数据库连接</returns>
        public IDbConnection Open(string connectionString)
		{
            // 写入调试信息
            #if (DEBUG)
                int milliStart = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif			
            // 这里数据库连接打开的时候，就判断注册属性的有效性
            //if (!Provider.CheckRegister())
            //{
            //    // 若没有进行注册，让程序无法打开数据库比较好。
            //    connectionString = string.Empty;

            //    // 抛出异常信息显示给客户
            //    throw new Exception("connectionString is Empty");
            //}
            // 若是空的话才打开
            if (this.dbConnection == null || this.dbConnection.State == ConnectionState.Closed)
            {
                this.ConnectionString = connectionString;
                this.dbConnection =DbFactory.CreateConnection();
                this.dbConnection.ConnectionString = this.ConnectionString;
                this.dbConnection.Open();

                // 创建对象
                // this.dbCommand = this.DbConnection.CreateCommand();
                // this.dbCommand.Connection = this.dbConnection;
                // this.dbDataAdapter = this.dbProviderFactory.CreateDataAdapter();
                // this.dbDataAdapter.SelectCommand = this.dbCommand;

                // 写入调试信息
                #if (DEBUG)
                    int milliEnd = Environment.TickCount;
                    Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
                #endif
            }
            return this.dbConnection;
		}
		#endregion
        
        #region public IDbConnection GetDbConnection() 获取数据库连接
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns>数据库连接</returns>
        public IDbConnection GetDbConnection()
        {
            return this.dbConnection;
        }
        #endregion

        #region public IDbTransaction GetDbTransaction() 获取数据源上执行的事务
        /// <summary>
        /// 获取数据源上执行的事务
        /// </summary>
        /// <returns>数据源上执行的事务</returns>
        public IDbTransaction GetDbTransaction()
        {
            return this.dbTransaction;
        }
        #endregion

        #region public IDbCommand GetDbCommand() 获取数据源上命令
        /// <summary>
        /// 获取数据源上命令
        /// </summary>
        /// <returns>数据源上命令</returns>
        public IDbCommand GetDbCommand()
        {
            return this.DbConnection.CreateCommand();
        }
        #endregion
        
        #region public IDataReader ExecuteReader(string commandText)
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>结果集流</returns>
        public IDataReader ExecuteReader(string commandText)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
#if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            // 自动打开
            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            this.dbCommand = this.DbConnection.CreateCommand();
            this.dbCommand.CommandType = CommandType.Text;
            this.dbCommand.CommandText = commandText;
            if (this.InTransaction)
            {
                this.dbCommand.Transaction = this.dbTransaction;
            }

            // DbDataReader dbDataReader = this.dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            // 这里要关闭数据库才可以的
            DbDataReader dbDataReader = null;
            if (this.AutoOpenClose)
            {
                dbDataReader = this.dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            else
            {
                dbDataReader = this.dbCommand.ExecuteReader();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
#if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);

            return dbDataReader;
        }
        #endregion

        #region public IDataReader ExecuteReader(string commandText, DbParameter[] dbParameters);
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>结果集流</returns>
        public IDataReader ExecuteReader(string commandText, DbParameter[] dbParameters)
        {
            return this.ExecuteReader(CommandType.Text, commandText, dbParameters);
        }
        #endregion

        #region public IDataReader ExecuteReader(CommandType commandType, string commandText, DbParameter[] dbParameters)
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>结果集流</returns>
        public IDataReader ExecuteReader(CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            // 自动打开
            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            this.dbCommand = this.DbConnection.CreateCommand();
            this.dbCommand.CommandText = commandText;
            this.dbCommand.CommandType = commandType;
            if (this.dbTransaction != null)
            {
                this.dbCommand.Transaction = this.dbTransaction;
            }
            if (dbParameters != null)
            {
                this.dbCommand.Parameters.Clear();
                this.dbCommand.Parameters.AddRange(dbParameters);
            }

            // 这里要关闭数据库才可以的
            DbDataReader dbDataReader = null;
            if (this.AutoOpenClose)
            {
                dbDataReader = this.dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            else
            {
                dbDataReader = this.dbCommand.ExecuteReader();
                this.dbCommand.Parameters.Clear();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);

            return dbDataReader;
        }
        #endregion
        
        #region public int ExecuteNonQuery(string commandText)
        /// <summary>
        /// 执行查询, SQL BUILDER 用了这个东西？参数需要保存, 不能丢失.
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>影响行数</returns>
        public int ExecuteNonQuery(string commandText)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            
            // 自动打开
            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            this.dbCommand = this.DbConnection.CreateCommand();
            this.dbCommand.CommandType = CommandType.Text;
            this.dbCommand.CommandText = commandText;
            if (this.InTransaction)
            {
                this.dbCommand.Transaction = this.dbTransaction;
            }
          
            int returnValue = this.dbCommand.ExecuteNonQuery();

            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Close();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);
            return returnValue;
		}
        #endregion

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        public int ExecuteNonQuery(string commandText, DbParameter[] dbParameters)
        {
            return this.ExecuteNonQuery(CommandType.Text, commandText, dbParameters);
        }
        
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        public int ExecuteNonQuery(CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            return this.ExecuteNonQuery(this.dbTransaction, commandType, commandText, dbParameters);
        }
        
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        public int ExecuteNonQuery(IDbTransaction transaction, CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            this.dbCommand = this.DbConnection.CreateCommand();
            this.dbCommand.CommandText = commandText;
            this.dbCommand.CommandType = commandType;
            if (this.dbTransaction != null)
            {
                this.dbCommand.Transaction = this.dbTransaction;
            }
            if (dbParameters != null)
            {
                this.dbCommand.Parameters.Clear();
                this.dbCommand.Parameters.AddRange(dbParameters);
            }
            int returnValue = this.dbCommand.ExecuteNonQuery();

            // 自动关闭
            if (this.AutoOpenClose)
            {
                this.Close();
            }
            else
            {
                this.dbCommand.Parameters.Clear();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);

            return returnValue;
        }

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>object</returns>
        public object ExecuteScalar(string commandText)
        {
			return this.ExecuteScalar(CommandType.Text, commandText, new DbParameter[0]);
        }
        
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>Object</returns>
        public object ExecuteScalar(string commandText, DbParameter[] dbParameters)
        {
            return this.ExecuteScalar(CommandType.Text, commandText, dbParameters);
        }
        
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>Object</returns>
        public object ExecuteScalar(CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            return this.ExecuteScalar(this.dbTransaction, commandType, commandText, dbParameters);
        }
        
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>Object</returns>
        public object ExecuteScalar(IDbTransaction transaction, CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            
            // 自动打开
            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            this.dbCommand = this.DbConnection.CreateCommand();
            this.dbCommand.CommandText = commandText;
            this.dbCommand.CommandType = commandType;
            if (this.dbTransaction != null)
            {
                this.dbCommand.Transaction = this.dbTransaction;
            }
            if (dbParameters != null)
            {
                this.dbCommand.Parameters.Clear();
                this.dbCommand.Parameters.AddRange(dbParameters);
            }
            object returnValue = this.dbCommand.ExecuteScalar();
            
            // 自动关闭
            if (this.AutoOpenClose)
            {
                this.Close();
            }
            else
            {
                this.dbCommand.Parameters.Clear();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);
            return returnValue;
        }


        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="commandText">查询</param>
        /// <returns>数据表</returns>
        public DataTable Fill(string commandText)
        {
            DataTable dataTable = new DataTable();
            return this.Fill(dataTable, CommandType.Text, commandText, new DbParameter[0]);
        }

        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="dataTable">目标数据表</param>
        /// <param name="commandText">查询</param>
        /// <returns>数据表</returns>
        public DataTable Fill(DataTable dataTable, string commandText)
        {
            return this.Fill(dataTable, CommandType.Text, commandText, new DbParameter[0]);
        }

        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据表</returns>
        public DataTable Fill(string commandText, DbParameter[] dbParameters)
        {
            DataTable dataTable = new DataTable();
            return this.Fill(dataTable, CommandType.Text, commandText, dbParameters);
        }

        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="dataTable">目标数据表</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据表</returns>
        public DataTable Fill(DataTable dataTable, string commandText, DbParameter[] dbParameters)
        {
            return this.Fill(dataTable, CommandType.Text, commandText, dbParameters);
        }

        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据表</returns>
        public DataTable Fill(CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            DataTable dataTable = new DataTable();
            return this.Fill(dataTable, commandType, commandText, dbParameters);
        }
        
        /// <summary>
        /// 填充数据表
        /// </summary>
        /// <param name="dataTable">目标数据表</param>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据表</returns>
        public DataTable Fill(DataTable dataTable, CommandType commandType, string commandText, DbParameter[] dbParameters)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            using (this.dbCommand = this.DbConnection.CreateCommand())
            {   
                //this.dbCommand.Parameters.Clear();
                //if ((dbParameters != null) && (dbParameters.Length > 0))
                //{
                //    for (int i = 0; i < dbParameters.Length; i++)
                //    {
                //        if (dbParameters[i] != null)
                //        {
                //            this.dbDataAdapter.SelectCommand.Parameters.Add(dbParameters[i]);
                //        }
                //    }
                //}
                this.dbCommand.CommandText = commandText;
                this.dbCommand.CommandType = commandType;
                if (this.InTransaction)
                {
                    // 这个不这么写，也不行，否则运行不能通过的
                    this.dbCommand.Transaction = this.dbTransaction;
                }
                this.dbDataAdapter =GetDataAdapter();
                this.dbDataAdapter.SelectCommand = this.dbCommand;
                if ((dbParameters != null) && (dbParameters.Length > 0))
                {
                    this.dbCommand.Parameters.AddRange(dbParameters);
                }
                this.dbDataAdapter.Fill(dataTable);
                this.dbDataAdapter.SelectCommand.Parameters.Clear();
            }

            // 自动关闭
            if (this.AutoOpenClose)
            {
                this.Close();
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);
            return dataTable;
        }

        #region public DataSet Fill(DataSet dataSet, string commandText, string tableName)
        /// <summary>
        /// 填充数据集
        /// </summary>
        /// <param name="dataSet">目标数据集</param>
        /// <param name="commandText">查询</param>
        /// <param name="tableName">填充表</param>
        /// <returns>数据集</returns>
        public DataSet Fill(DataSet dataSet, string commandText, string tableName)
        {
            return this.Fill(dataSet, CommandType.Text, commandText, tableName, null);
        }
        #endregion

        #region public DataSet Fill(DataSet dataSet, string commandText, string tableName, DbParameter[] dbParameters)
        /// <summary>
        /// 填充数据集
        /// </summary>
        /// <param name="dataSet">数据集</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="tableName">填充表</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据集</returns>
        public DataSet Fill(DataSet dataSet, string commandText, string tableName, DbParameter[] dbParameters)
        {
            return this.Fill(dataSet, CommandType.Text, commandText, tableName, dbParameters);
        }
        #endregion

        #region public DataSet Fill(DataSet dataSet, CommandType commandType, string commandText, string tableName, DbParameter[] dbParameters)
        /// <summary>
        /// 填充数据集
        /// </summary>
        /// <param name="dataSet">数据集</param>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="tableName">填充表</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据集</returns>
        public DataSet Fill(DataSet dataSet, CommandType commandType, string commandText, string tableName, DbParameter[] dbParameters)
        {
            int milliStart = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            // 自动打开
            if (this.AutoOpenClose)
            {
                this.Open();
            }
            else
            {
                if (this.DbConnection == null)
                {
                    this.AutoOpenClose = true;
                    this.Open();
                }
            }

            using (this.dbCommand = this.DbConnection.CreateCommand())
            {
                //this.dbCommand.Parameters.Clear();
                //if ((dbParameters != null) && (dbParameters.Length > 0))
                //{
                //    for (int i = 0; i < dbParameters.Length; i++)
                //    {
                //        if (dbParameters[i] != null)
                //        {
                //            this.dbDataAdapter.SelectCommand.Parameters.Add(dbParameters[i]);
                //        }
                //    }
                //}
                this.dbCommand.CommandText = commandText;
                this.dbCommand.CommandType = commandType;
                if ((dbParameters != null) && (dbParameters.Length > 0))
                {
                    this.dbCommand.Parameters.AddRange(dbParameters);
                }

                this.dbDataAdapter = GetDataAdapter();
                this.dbDataAdapter.SelectCommand = this.dbCommand;
                this.dbDataAdapter.Fill(dataSet, tableName);

                if (this.AutoOpenClose)
                {
                    this.Close();
                }
                else
                {
                    this.dbDataAdapter.SelectCommand.Parameters.Clear();
                }
            }
            int milliEnd = Environment.TickCount;
            // 写入调试信息
            #if (DEBUG)

            Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            // 写入日志
            this.WriteLog(commandText, TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds);
            return dataSet;
        }
        #endregion

        #region public int ExecuteProcedure(string procedureName)
        /// <summary>
        /// 执行数据库查询
        /// </summary>
        /// <param name="procedureName">存储过程</param>
        /// <returns>int</returns>
        public int ExecuteProcedure(string procedureName)
        {
            return this.ExecuteNonQuery(CommandType.StoredProcedure, procedureName, new DbParameter[0]);
        }
        #endregion

        #region public int ExecuteProcedure(string procedureName, DbParameter[] dbParameters)
        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="procedureName">存储过程名</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        public int ExecuteProcedure(string procedureName, DbParameter[] dbParameters)
        {
            return this.ExecuteNonQuery(CommandType.StoredProcedure, procedureName, dbParameters);
        }
        #endregion

        #region public DataTable ExecuteProcedureForDataTable(string procedureName, string tableName, DbParameter[] dbParameters)
        /// <summary>
        /// 执行数据库脚本
        /// </summary>
        /// <param name="procedureName">存储过程</param>
        /// <param name="tableName">填充表</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据集</returns>
        public DataTable ExecuteProcedureForDataTable(string procedureName, string tableName, DbParameter[] dbParameters)
        {
            DataTable dataTable = new DataTable(tableName);
            this.Fill(dataTable, CommandType.StoredProcedure, procedureName, dbParameters);
            return dataTable;
        }
        #endregion

        #region public IDbTransaction BeginTransaction()
        /// <summary>
        /// 事务开始
        /// </summary>
        /// <returns>事务</returns>
        public IDbTransaction Begin()
        {
            // 写入调试信息
            #if (DEBUG)
                int milliStart = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
            
            var dbconnection = this.DbConnection;
            this.AutoOpenClose = false;
            if (!this.InTransaction|| this.dbTransaction==null||this.dbTransaction.Connection.State!=ConnectionState.Open)
            {
               
                this.InTransaction = true;
               
                this.dbTransaction = dbconnection.BeginTransaction();
                // this.dbCommand.Transaction = this.dbTransaction;
            }

            // 写入调试信息
            #if (DEBUG)
                int milliEnd = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            return this.dbTransaction;
        }
        #endregion

        #region public void Commit()
        /// <summary>
        /// 递交事务
        /// </summary>
        public void Commit()
        {
            // 写入调试信息
            #if (DEBUG)
                int milliStart = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            if (this.InTransaction)
            {
                // 事务已经完成了，一定要更新标志信息
                this.InTransaction = false;
                if (this.dbTransaction != null && this.dbTransaction.Connection.State == ConnectionState.Open)
                {
                    this.dbTransaction.Commit();
                   // this.dbTransaction.Connection.Close();
                }
                this.AutoOpenClose = true;

            }

            // 写入调试信息
            #if (DEBUG)
                int milliEnd = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
        }
        #endregion

        #region public void RollbackTransaction()
        /// <summary>
        /// 回滚事务
        /// </summary>
        public void Rollback()
        {
            // 写入调试信息
            #if (DEBUG)
                int milliStart = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            if (this.InTransaction)
            {
                this.InTransaction = false;
                
                if (this.dbTransaction != null&& this.dbTransaction.Connection !=null&& this.dbTransaction.Connection.State == ConnectionState.Open)
                {
                    this.dbTransaction.Rollback();
                   // this.dbTransaction.Connection.Close();
                }
            }

            // 写入调试信息
            #if (DEBUG)
                int milliEnd = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
        }
        #endregion

        #region public void Close()
        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        public void Close()
        {
            // 写入调试信息
            #if (DEBUG)
                int milliStart = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " :Begin: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif

            if (this.dbConnection != null)
            {
                this.dbConnection.Close();
                this.dbConnection.Dispose();
            }
            this.dbConnection = null;
            this.dbCommand = null;
            this.dbDataAdapter = null;
            this.dbTransaction = null;
            // 写入调试信息
            #if (DEBUG)
                int milliEnd = Environment.TickCount;
                Trace.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Ticks: " + TimeSpan.FromMilliseconds(milliEnd - milliStart).ToString() + " :End: " + MethodBase.GetCurrentMethod().ReflectedType.Name + "." + MethodBase.GetCurrentMethod().Name);
            #endif
        }
        #endregion

        #region public void WriteLog(string commandText) 写入sql查询句日志
        /// <summary>
        /// 写入sql查询日志
        /// </summary>
        /// <param name="commandText">sql查询</param>
        public void WriteLog(string commandText, double tickTimes)
        {
            LogHelper.SQLInfo(commandText, tickTimes, "ADO", this.DbProviderType.ToString());
        }
        #endregion


        #region 扩展
        public abstract DbConnection GetConnection(string connectionString);

        public abstract DbDataAdapter GetDataAdapter();

        public int SelectIdentity(string primaryKeyName)
        {
            object resultValue = this.ExecuteScalar(CommandType.Text, Identity(primaryKeyName), null);
            return resultValue.ToInt();
        }

        public abstract string Now();

        public DateTime GetNow()
        {
            object resultValue = this.ExecuteScalar(CommandType.Text, Now(), null);
            return ConvertHelper.ToDateTime(resultValue);
        }

        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public abstract DbParameter[] GetParameter(List<DataParameter> parameters);
        

        public abstract DbParameter GetParameter(string name,object value);

        public abstract string GetParameter(string parameter);

        public abstract string GetLikeParameter(string parameter);

        public abstract string Identity(string primaryKeyName);

        public abstract void SqlBulkCopyData(DataTable dataTable);

        /// <summary>
        /// 禁用自增
        /// </summary>
        /// <returns></returns>
        public abstract string Identity_Disabled(string tableName);

        /// <summary>
        /// 启用自增
        /// </summary>
        /// <returns></returns>
        public abstract string Identity_Enable(string tableName);
        #endregion
        #region  参数
        public abstract DbParameter MakeOutParam(string paramName, DbType DbType, int Size);

        public abstract DbParameter MakeInParam(string paramName, DbType DbType, int Size, object Value);

        public abstract DbParameter MakeParam(string paramName, DbType DbType, Int32 Size, ParameterDirection Direction, object Value);
        #endregion
        #region IQuery

        public abstract string RecordCount(string hql, bool createSql = true);


       public abstract string GetRecordCountSql(string tableName);

        public abstract string GetSelectSql(string tableName, string queryFieldBuilder, bool isDeleted);

        public abstract string QueryPage(string hql, Order[] orders, int pageIndex, int pageSize);
        #endregion

        #region IDbSource
        public abstract string GetTable(string DBName);

        public abstract string GetView(string DBName);

        public abstract string GetDbColumn(string tableName, string dbServerName, bool IsTable);

        public abstract string GetSource(string tableName);

        public abstract string CreateTable(string table, List<DbColumnAttribute> dbColumns);

        public abstract string AddColumn(string table, DbColumnAttribute dbColumns);

        public abstract string DeleteTable(string table);

        public abstract string DeleteColumn(string table, string columnName);

        /// <summary>
        /// 更改字段描述
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="description"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        public abstract string UpdateDescriptionSql(string tableName, string columnName, string description, bool isAdd);

        /// <summary>
        /// 判断描述是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">字段名</param>
        /// <returns></returns>
        public abstract string ExistDescriptionSql(string tableName, string columnName);
        #endregion


    }
}