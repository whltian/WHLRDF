﻿//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using WHLRDF.Log;

//namespace WHLRDF.ORM.EF
//{
//    /// <summary>
//    /// EF CRUD
//    /// </summary>
//    public partial class EFDbRepository
//    {
//        private EFRepository _dbFactory;
//        public EFDbRepository(EFRepository dbFactory)
//        {
//            _dbFactory = dbFactory;
//        }
//        #region update
//        public bool Update<TEntity>(TEntity entity) where TEntity : class, new()
//        {
//            if (entity == null)
//                throw new Exception("Entity is null");
//            _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = false;
//            DataProxy dbFactory = new DataProxy(typeof(TEntity));
//            bool flag = false;
//            try
//            {
//                if (_dbFactory.Current.Entry<TEntity>(entity).State == EntityState.Detached)
//                {

//                }
//                SetLastModify(entity, dbFactory);
//                _dbFactory.Current.Attach<TEntity>(entity);
//                if (this.Update<TEntity>(entity, dbFactory))
//                {
//                    flag = _dbFactory.Current.SaveChanges() > 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = true;
//            }
           

//            return flag;
//        }
//        private void SetLastModify(object entity, DataProxy dbFactory)
//        {
//            if (entity != null && entity is EntityBase)
//            {
//                var baseEntity = entity as EntityBase;
//                if (baseEntity.DataCollection.Count >= 1)
//                {
//                    if (dbFactory.TableAttr != null && dbFactory.TableAttr.IsLastModify)
//                    {
//                        baseEntity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
//                        baseEntity.LastModifyDate = DateTime.Now;
//                    }
//                }
//            }
//        }
//        private bool Update<TEntity>(TEntity entity, DataProxy dbFactory) where TEntity : class, new()
//        {
         
//           // _dbFactory.Current.Attach<TEntity>(entity);
//            if (entity != null && entity is EntityBase)
//            {

//                var baseEntity = entity as EntityBase;
//                if (baseEntity.DataCollection.Count >= 1)
//                {
                    
//                    var properties = _dbFactory.Current.Entry<TEntity>(entity).Properties;
//                    foreach (var field in properties)
//                    {
//                        if (dbFactory.PrimaryKeyName== field.Metadata.PropertyInfo.Name)
//                        {
//                            continue;
//                        }
//                        field.IsModified = false;
//                        field.IsModified = baseEntity.DataCollection.Keys.Contains(field.Metadata.PropertyInfo.Name);
//                    }
//                }
//            }
//            return true;
//        }
//        public bool Update<TEntity>(List<TEntity> entities) where TEntity : class, new()
//        {
//            if (entities == null || entities.Count <= 0)
//            {
//                throw new Exception("Entity is null");
//            }
//            _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = false;
//            DataProxy dbFactory = new DataProxy(typeof(TEntity));
//            bool flag = false;
//            try
//            {
//                foreach (var entity in entities)
//                {
//                    SetLastModify(entity, dbFactory);
//                }
//                _dbFactory.Current.AttachRange(entities);
//                foreach (var entity in entities)
//                {
//                    this.Update<TEntity>(entity);
//                }
              
//                flag = _dbFactory.Current.SaveChanges() > 0;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = true;
//            }
            
//            return flag;


//        }


//        #endregion

//        #region delete
//        public bool Delete<TEntity>(TEntity entity) where TEntity : class, new()
//        {
//            if (entity == null)
//                throw new Exception("Entity is null");
//            _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = false;
//            bool flag = false;
//            try
//            {
//                DataProxy dbFactory = new DataProxy(typeof(TEntity));
//                if (dbFactory != null && dbFactory.TableAttr.IsDeleted)
//                {
//                    var baseEntity = entity as EntityBase;
//                    _dbFactory.Current.Attach<TEntity>(entity);
//                    baseEntity.IsDeleted = true;
//                    this.Update<TEntity>(entity, dbFactory);
//                    flag = _dbFactory.Current.SaveChanges() > 0;
//                }
//                if (!flag)
//                {
//                    _dbFactory.Current.Remove<TEntity>(entity);

//                   // dbFactory.RemoveCacheEntity(typeof(TEntity), new object[] { dbFactory.GetPrimaryId(entity) });
//                    flag = _dbFactory.Current.SaveChanges() > 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                LogHelper.Error(this,ex);
//            }
//            finally
//            {
//                _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = true;
//            }
//            return flag;
         
//        }
    
//        public bool Delete<TEntity>(List<TEntity> entities) where TEntity : class, new()
//        {
//            if (entities == null)
//                throw new Exception("Entity is null");
//            _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = false;
//            bool flag = false;
//            try
//            {
//                DataProxy dbFactory = new DataProxy(typeof(TEntity));
//                if (dbFactory != null && dbFactory.TableAttr.IsDeleted)
//                {
//                    _dbFactory.Current.AttachRange(entities);
//                    foreach (var entity in entities)
//                    {
//                        var baseEntity = entity as EntityBase;
//                        baseEntity.IsDeleted = true;
//                        this.Update<TEntity>(entity, dbFactory);
//                    }
//                    flag= _dbFactory.Current.SaveChanges() > 0;
//                }
//                if (!flag)
//                {
//                    _dbFactory.Current.RemoveRange(entities);
//                    flag = _dbFactory.Current.SaveChanges() > 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//            finally
//            {
//                _dbFactory.Current.ChangeTracker.AutoDetectChangesEnabled = true;
//            }
//            return flag;

//        }
//        #endregion
//        #region Insert
//        public bool Insert<TEntity>(TEntity entity)where TEntity:class,new()
//        {
//            if (entity == null)
//                throw new Exception("Entity is null");
//            var table = this.GetTableAttribute(entity.GetType());
//            if (table != null)
//            {
//                var baseEntity = entity as EntityBase;
//                if (table.IsCreated)
//                {
//                    baseEntity.CreateBy = ApplicationEnvironments.DefaultSession.UserId;
//                    baseEntity.CreateDate = DateTime.Now;
//                }
//                if (table.IsLastModify)
//                {
//                    baseEntity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
//                    baseEntity.LastModifyDate = DateTime.Now;
//                }
//            }
            
//            _dbFactory.Current.Add<TEntity>(entity);
//            return _dbFactory.Current.SaveChanges() > 0;
//        }

//        public bool Insert<TEntity>(List<TEntity> entities) where TEntity : class, new()
//        {
//            if (entities == null|| entities.Count<=0)
//                throw new Exception("Entity is null");
//            var table = this.GetTableAttribute(typeof(TEntity));
//            if (table != null)
//            {
//                foreach (var entity in entities)
//                {
//                    var baseEntity = entity as EntityBase;
//                    if (table.IsCreated)
//                    {
//                        baseEntity.CreateBy = ApplicationEnvironments.DefaultSession.UserId;
//                        baseEntity.CreateDate = DateTime.Now;
//                    }
//                    if (table.IsLastModify)
//                    {
//                        baseEntity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
//                        baseEntity.LastModifyDate = DateTime.Now;
//                    }
//                }
                
//            }
//            _dbFactory.Current.AddRange(entities);
//            return _dbFactory.Current.SaveChanges() > 0;
//        }
//        #endregion
//        private TableAttribute GetTableAttribute(Type type)
//        {
//           return (TableAttribute)type.GetCustomAttributes(typeof(TableAttribute), true).FirstOrDefault();
//        }
//    }
//}
