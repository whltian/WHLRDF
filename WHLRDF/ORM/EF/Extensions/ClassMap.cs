﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WHLRDF.ORM
{
    public abstract class ClassMap<T> : IEntityTypeConfiguration<T>where T:class
    {
        /// <summary>
        /// 忽略父类中的属性
        /// </summary>
        /// <param name="builder"></param>
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            //builder.Ignore("IsDeleted");
            //builder.Ignore("LastModifyDate");
            //builder.Ignore("LastModifyUserId");
            //builder.Ignore("CreateDate");
            //builder.Ignore("CreateBy");
            builder.Ignore("County");
            builder.Ignore("City");
            builder.Ignore("Province");
            builder.Ignore("TableName");
            builder.Ignore("PrimaryKeyName");
            builder.Ignore("DataCollection");
            builder.Ignore("EntityFields");
            builder.Ignore("DbFields");
            builder.Ignore("QueryFieldBuilder");
            builder.Ignore("Identifier");
            builder.Ignore("ThisAttribute");
            builder.Ignore("DbRepository");
            builder.Ignore("State");
        }
    }
}
