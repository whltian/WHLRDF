﻿
namespace WHLRDF.ORM
{

    /// <summary>
    /// 数据访问驱动
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// 数据库连接字符
        /// </summary>
        string ConnectionString
        {
            get;
            set;
        }


    }
}