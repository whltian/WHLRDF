﻿
using System.Collections.Generic;

namespace WHLRDF.ORM.DbExtensions
{
    /// <summary>
    /// 数据库操作接口
    /// </summary>
    public interface IDbSource
    {
        /// <summary>
        /// 获取数据库的
        /// </summary>
        /// <returns></returns>
        string GetTable(string DBName);
        /// <summary>
        /// 获取数据库的
        /// </summary>
        /// <returns></returns>
        string GetView(string DBName);
        /// <summary>
        /// 通过表名或者视图名获取字段
        /// </summary>
        /// <param name="tableName">链接</param>
        /// <param name="dbServerName">表名</param>
        /// <returns></returns>
        string GetDbColumn(string tableName, string dbServerName, bool IsTable);

        string CreateTable(string table, List<DbColumnAttribute> dbColumns);

        string AddColumn(string table, DbColumnAttribute columnItem);

        string DeleteTable(string table);

        string DeleteColumn(string table,string columnName);
        ///// <summary>
        ///// 获取数据源
        ///// </summary>
        ///// <param name="tableName"></param>
        ///// <param name="grid"></param>
        ///// <returns></returns>
        //LigerGrid GetSource(string tableName, LigerGrid grid);

        ///// <summary>
        ///// 批量导入数据
        ///// </summary>
        ///// <param name="strTableName"></param>
        ///// <param name="dtData"></param>
        ///// <param name="strError"></param>
        ///// <returns></returns>
        //bool SqlBulkCopyInsert( string strTableName, DataTable dtData, ref string strError);
        /// <summary>
        /// 更改字段描述
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="description"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        string UpdateDescriptionSql(string tableName, string columnName, string description, bool isAdd);

        /// <summary>
        /// 判断描述是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">字段名</param>
        /// <returns></returns>
        string ExistDescriptionSql(string tableName, string columnName);
    }
}
