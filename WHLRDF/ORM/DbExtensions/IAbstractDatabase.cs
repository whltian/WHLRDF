﻿//------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2010 , Jirisoft , Ltd. 
//------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;


namespace WHLRDF.ORM
{
   
    public interface IAbstractDatabase 
    {
        

        ProviderType DbProviderType { get; }
        /// <summary>
        /// DbProviderFactory实例
        /// </summary>
        DbProviderFactory DbFactory{get;}

        #region public IDbConnection Open()

        /// <summary>
        /// 这时主要的获取数据库连接的方法
        /// </summary>
        /// <returns>数据库连接</returns>
        IDbConnection Open();
        #endregion
        IDbConnection Open(string connectionString);


        #region public IDbConnection GetDbConnection() 获取数据库连接
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns>数据库连接</returns>
        IDbConnection GetDbConnection();
        #endregion

        #region public IDbTransaction GetDbTransaction() 获取数据源上执行的事务
        /// <summary>
        /// 获取数据源上执行的事务
        /// </summary>
        /// <returns>数据源上执行的事务</returns>
        IDbTransaction GetDbTransaction();
        #endregion



        #region public IDataReader ExecuteReader(string commandText)
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>结果集流</returns>
        IDataReader ExecuteReader(string commandText);
        #endregion

        #region public IDataReader ExecuteReader(string commandText, DbParameter[] dbParameters);
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>结果集流</returns>
        IDataReader ExecuteReader(string commandText, DbParameter[] dbParameters);
        #endregion

        #region public IDataReader ExecuteReader(CommandType commandType, string commandText, DbParameter[] dbParameters)
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>结果集流</returns>
        IDataReader ExecuteReader(CommandType commandType, string commandText, DbParameter[] dbParameters);
        #endregion

        #region public int ExecuteNonQuery(string commandText)
        /// <summary>
        /// 执行查询, SQL BUILDER 用了这个东西？参数需要保存, 不能丢失.
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>影响行数</returns>
        int ExecuteNonQuery(string commandText);
        #endregion

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        int ExecuteNonQuery(string commandText, DbParameter[] dbParameters);

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        int ExecuteNonQuery(CommandType commandType, string commandText, DbParameter[] dbParameters);


        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <returns>object</returns>
        object ExecuteScalar(string commandText);

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>Object</returns>
        object ExecuteScalar(string commandText, DbParameter[] dbParameters);

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="commandType">命令分类</param>
        /// <param name="commandText">sql查询</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>Object</returns>
        object ExecuteScalar(CommandType commandType, string commandText, DbParameter[] dbParameters);


        #region public int ExecuteProcedure(string procedureName)
        /// <summary>
        /// 执行数据库查询
        /// </summary>
        /// <param name="procedureName">存储过程</param>
        /// <returns>int</returns>
        int ExecuteProcedure(string procedureName);
        #endregion

        #region public int ExecuteProcedure(string procedureName, DbParameter[] dbParameters)
        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="procedureName">存储过程名</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>影响行数</returns>
        int ExecuteProcedure(string procedureName, DbParameter[] dbParameters);
        #endregion

        #region public DataTable ExecuteProcedureForDataTable(string procedureName, string tableName, DbParameter[] dbParameters)
        /// <summary>
        /// 执行数据库脚本
        /// </summary>
        /// <param name="procedureName">存储过程</param>
        /// <param name="tableName">填充表</param>
        /// <param name="dbParameters">参数集</param>
        /// <returns>数据集</returns>
        DataTable ExecuteProcedureForDataTable(string procedureName, string tableName, DbParameter[] dbParameters);
        #endregion

        #region public IDbTransaction BeginTransaction()
        /// <summary>
        /// 事务开始
        /// </summary>
        /// <returns>事务</returns>
        DbTransaction Begin();
        #endregion

        #region public void Commit()
        /// <summary>
        /// 递交事务
        /// </summary>
        void Commit();
        #endregion

        #region public void RollbackTransaction()
        /// <summary>
        /// 回滚事务
        /// </summary>
        void Rollback();
        #endregion

        #region public void Close()
        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        void Close();
        #endregion

        #region public void WriteLog(string commandText) 写入sql查询句日志
        /// <summary>
        /// 写入sql查询日志
        /// </summary>
        /// <param name="commandText">sql查询</param>
        void WriteLog(string commandText, double tickTimes);
        #endregion


        #region 扩展
        DbConnection GetConnection(string connectionString);

        DbDataAdapter GetDataAdapter();

        int SelectIdentity(string tableName);

       string Now();
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
       DbParameter[] GetParameter(List<DataParameter> parameters);
        

        DbParameter GetParameter(string name,object value);

        string GetParameter(string name);

       string Identity(string where);

        void SqlBulkCopyData(DataTable dataTable);

        /// <summary>
        /// 禁用自增
        /// </summary>
        /// <returns></returns>
       string Identity_Disabled(string tableName);

        /// <summary>
        /// 启用自增
        /// </summary>
        /// <returns></returns>
        string Identity_Enable(string tableName);
        #endregion
        #region 
        DbParameter MakeOutParam(string paramName, DbType DbType, int Size);

        DbParameter MakeInParam(string paramName, DbType DbType, int Size, object Value);

        DbParameter MakeParam(string paramName, DbType DbType, Int32 Size, ParameterDirection Direction, object Value);
        #endregion
    }
}