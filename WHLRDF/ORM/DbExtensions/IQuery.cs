﻿
namespace WHLRDF.ORM.DbExtensions
{
    public interface IQuery
    {
      
        string RecordCount(string hql, bool createSql = true);

        string GetRecordCountSql(string tableName);

        string GetSelectSql(string tableName, string queryFieldBuilder, bool isDeleted);


       /// <summary>
       /// 通过sql语句进行分页查询
       /// </summary>
       /// <param name="hql"></param>
       /// <param name="orders"></param>
       /// <param name="pageIndex"></param>
       /// <param name="pageSize"></param>
       /// <returns></returns>
        string QueryPage(string hql, Order[] orders,int pageIndex, int pageSize);

        /// <summary>
        /// 获取数据库的
        /// </summary>
        /// <returns></returns>
        string GetTable(string DBName);
        /// <summary>
        /// 获取数据库的
        /// </summary>
        /// <returns></returns>
        string GetView(string DBName);
     

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="grid"></param>
        /// <returns></returns>
        string GetSource(string tableName);
    }
}
