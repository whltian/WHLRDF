﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace WHLRDF.ORM
{
    public interface IDbRepository: IDisposable{

       

        #region Execute

        ProviderType DbProviderType { get; }

        string DbVersion { get; set; }

        bool AutoOpenClose { get; }

        void Close();

        void Open();


        IDbTransaction Begin();

        void Commit();

        void Rollback();
        int Execute(string sqlCommand);

        int Execute(string sqlCommand, DataParameterCollection parametersValues);

        object Scalar(string hql);

        object Scalar(string hql, ICriterion criterion);

        object Scalar(string hql, DataParameterCollection parametersValues);

        IDataReader Reader(string sqlCommand, DataParameterCollection parameters);

        int ExecuteProcedure(string procedureName);

        int ExecuteProcedure(string procedureName, DataParameterCollection parameters);
        #endregion

        #region Insert update Deleted
        /// <summary>
        /// 获取参数集合对象
        /// </summary>
        /// <returns></returns>
        DataParameterCollection GetDbParameters();

        /// <summary>
        /// 新增
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert<TEntity>(TEntity entity) where TEntity : class, new();

        /// <summary>
        /// 新增
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="strError">返回错误信息</param>
        /// <returns></returns>
        bool Insert<TEntity>(TEntity entity,ref string strError)where TEntity:class, new();

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="lstEntity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool BatchInsert<TEntity>(List<TEntity> lstEntity, ref string strError) where TEntity : class, new();



        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update<TEntity>(TEntity entity) where TEntity : class, new();
        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool Update<TEntity>(TEntity entity, ref string strError) where TEntity : class, new();
        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="criterion">sql</param>
        /// <returns></returns>
        bool Update<TEntity>(TEntity entity, ICriterion criterion) where TEntity : class, new();

        bool BatchUpdate<TEntity>(List<TEntity> lstEntity, ref string strError) where TEntity : class, new();

        bool SaveOrUpdate<TEntity>(TEntity entity) where TEntity : class, new();

        bool SaveOrUpdate<TEntity>(TEntity entity, ref string strError) where TEntity : class, new();

        bool BatchSave<T>(List<T> lstData) where T : class, new();

        bool Delete<T>(ICriterion criterion, bool isupdated=true) where T : class, new();

        bool Delete<T>(string ids, bool isupdated=true) where T : class, new();

        bool Delete<TEntity>(TEntity entity) where TEntity : class, new();
        #endregion

        /// <summary>
        /// 通过主键获取值
        /// </summary>
        /// <typeparam name="TEntity">实体</typeparam>
        /// <param name="id">主键</param>
        /// <returns></returns>
        TEntity GetById<TEntity>(string id) where TEntity : class, new();

    

        /// <summary>
        /// 通过组件获取实体 并更新现有实体
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="id">主键</param>
        /// <param name="action">委托</param>
        /// <returns></returns>
        TEntity GetById<TEntity>(string id,Func<TEntity, TEntity> action) where TEntity : class, new();

        /// <summary>
        /// 通过条件获取单个实体
        /// </summary>
        /// <typeparam name="TEntity">实体</typeparam>
        /// <param name="criterion">条件</param>
        /// <returns></returns>
        TEntity Select<TEntity>(ICriterion criterion) where TEntity : class, new();

        /// <summary>
        /// 通过sql语句以及条件获取对象
        /// </summary>
        /// <typeparam name="TEntity">实体</typeparam>
        /// <param name="hql">sql语句 带where 后跟 1=1 {0}或者 where 条件 {0}</param>
        /// <param name="criterion">条件</param>
        /// <returns></returns>
        TEntity Select<TEntity>(string hql,ICriterion criterion) where TEntity : class, new();

        /// <summary>
        /// 通过查询条件获取列表
        /// </summary>
        /// <typeparam name="T">对象</typeparam>
        /// <param name="criterion">查询条件</param>
        /// <returns></returns>
        List<T> Query<T>(ICriterion criterion) where T : class, new();

        /// <summary>
        /// 通过条件获取结果并排序
        /// </summary>
        /// <typeparam name="T">对象</typeparam>
        /// <param name="criterion">查询条件</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        List<T> Query<T>(ICriterion criterion, Order order) where T : class, new();

        /// <summary>
        /// 通过sql语句获取列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">sql语句 带where 后跟 1=1 {0}或者 where 条件 {0}</param>
        /// <param name="criterion">条件</param>
        /// <returns></returns>
        List<T> Query<T>(string sql, ICriterion criterion) where T : class, new();

        /// <summary>
        /// 通过sql语句获取列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">sql语句 带where 后跟 1=1 {0}或者 where 条件 {0}</param>
        /// <param name="criterion"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        List<T> Query<T>(string sql,ICriterion criterion, Order order) where T : class, new();

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <typeparam name="T">泛型对象</typeparam>
        /// <param name="criterion">查询条件</param>
        /// <param name="order">排序</param>
        /// <param name="pageindex">索引</param>
        /// <param name="pageSize">每页显示数</param>
        /// <param name="recordCount">总行数</param>
        /// <returns></returns>
        List<T> Query<T>(ICriterion criterion, Order order, int pageindex, int pageSize, ref int recordCount) where T : class, new();

        /// <summary>
        /// sql分页查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="hql">sql语句 带where 后跟 1=1 {0}或者 where 条件 {0}</param>
        /// <param name="criterion"></param>
        /// <param name="order"></param>
        /// <param name="pageindex"></param>
        /// <param name="pageSize"></param>
        /// <param name="recordCount"></param>
        /// <returns></returns>
        List<T> Query<T>(string hql, ICriterion criterion, Order order, int pageindex, int pageSize, ref int recordCount) where T : class, new();

        LigerGrid Query<T>(LigerGrid ligerGrid, ICriterion criterion) where T : class, new();

        LigerGrid Query<T>(string hql, LigerGrid ligerGrid, ICriterion criterion) where T : class, new();


        DataParameter GetParameter(string name, object value);

        string GetParameter(string name);

        /// <summary>
        /// like 参数化
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        string GetLikeParameter(string name);

        int SelectIdentity(string primaryKeyName);

        string IdentitySql(string primaryKeyName);

        DbParameter MakeOutParam(string paramName, DbType DbType, int Size);

        DbParameter MakeInParam(string paramName, DbType DbType, int Size, object Value);

        DbParameter MakeParam(string paramName, DbType DbType, Int32 Size, ParameterDirection Direction, object Value);

        DateTime Now { get; }

        /// <summary>
        /// 禁用自增
        /// </summary>
        /// <returns></returns>
        bool Identity_Disabled(string tableName);

        /// <summary>
        /// 启用自增
        /// </summary>
        /// <returns></returns>
        bool Identity_Enable(string tableName);

       
        #region 获取数据库中的值
        List<Dictionary<string, object>> GetTable(string dbName);
        List<Dictionary<string, object>> GetView(string dbName);
        List<Dictionary<string, object>> GetDbColumn(string dbName, string dbServerName, bool isTable);

        LigerGrid GetSource(string dbName, LigerGrid grid);


        string CreateTable(string table, List<DbColumnAttribute> dbColumns);

        string AddColumn(string table, DbColumnAttribute columnItem);
        /// <summary>
        /// 更改字段描述
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="description"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        string UpdateDescriptionSql(string tableName, string columnName, string description, bool isAdd);

        /// <summary>
        /// 判断描述是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">字段名</param>
        /// <returns></returns>
        string ExistDescriptionSql(string tableName, string columnName);
        #endregion
    }
}
