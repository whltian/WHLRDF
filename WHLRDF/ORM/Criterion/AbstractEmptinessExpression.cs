using System;
using System.Text;

namespace WHLRDF.ORM
{
	[Serializable]
	public abstract class AbstractEmptinessExpression : AbstractCriterion
	{
		//private readonly TypedValue[] NO_VALUES = Array.Empty<TypedValue>();
		private readonly string propertyName;

		protected abstract bool ExcludeEmpty { get; }


		protected AbstractEmptinessExpression(string propertyName)
		{
			this.propertyName = propertyName;
		}

		//public override TypedValue[] GetTypedValues(ICriteria criteria, ICriteriaQuery criteriaQuery)
		//{
		//	return NO_VALUES;
		//}

		public override sealed string ToString()
		{
			return propertyName + (ExcludeEmpty ? " is not empty" : " is empty");
		}

        /// <summary>
        /// 参数转换
        /// </summary>
        /// <param name="dataParam"></param>
        /// <param name="isNameParams">是否将名称作为参数</param>
        /// <returns></returns>
        public override string ToString(ref DataParameterCollection dataParam, bool isNameParams = false)
        {
            return propertyName + (ExcludeEmpty ? " is not null" : " is null");
        }
        public override SqlString ToSqlString(ICriteria criteria, ICriteriaQuery criteriaQuery)
		{
			var entityName = criteriaQuery.GetEntityName(criteria, propertyName);
			var actualPropertyName = criteriaQuery.GetPropertyName(propertyName);
			var sqlAlias = criteriaQuery.GetSQLAlias(criteria, propertyName);

			//var factory = criteriaQuery.Factory;
			//var collectionPersister = GetQueryableCollection(entityName, actualPropertyName, factory);

			//var collectionKeys = collectionPersister.KeyColumnNames;
			//var ownerKeys = ((ILoadable)factory.GetEntityPersister(entityName)).IdentifierColumnNames;

			var innerSelect = new StringBuilder();
			//innerSelect.Append("(select 1 from ")
			//	.Append(collectionPersister.TableName)
			//	.Append(" where ")
			//	.Append(
			//	new ConditionalFragment().SetTableAlias(sqlAlias).SetCondition(ownerKeys, collectionKeys).ToSqlStringFragment());
			//if (collectionPersister.HasWhere)
			//{
			//	innerSelect.Append(" and (")
			//		.Append(collectionPersister.GetSQLWhereString(collectionPersister.TableName))
			//		.Append(") ");
			//}

			innerSelect.Append(")");

			return new SqlString(new object[] {ExcludeEmpty ? "exists" : "not exists", innerSelect.ToString()});
		}


	}
}
