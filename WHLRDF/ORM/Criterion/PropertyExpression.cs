namespace WHLRDF.ORM
{
	using System;
	using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Superclass for an <see cref="ICriterion"/> that represents a
    /// constraint between two properties (with SQL binary operators).
    /// </summary>
    [Serializable]
	public abstract class PropertyExpression : AbstractCriterion
	{
	
		private readonly string _lhsPropertyName;
		private readonly string _rhsPropertyName;
		private readonly IProjection _lhsProjection;
		private readonly IProjection _rhsProjection;

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsProjection">The projection.</param>
		/// <param name="rhsPropertyName">Name of the RHS property.</param>
		protected PropertyExpression(IProjection lhsProjection, string rhsPropertyName)
		{
			this._lhsProjection = lhsProjection;
			_rhsPropertyName = rhsPropertyName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsProjection">The LHS projection.</param>
		/// <param name="rhsProjection">The RHS projection.</param>
		protected PropertyExpression(IProjection lhsProjection, IProjection rhsProjection)
		{
			this._lhsProjection = lhsProjection;
			this._rhsProjection = rhsProjection;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsPropertyName">Name of the LHS property.</param>
		/// <param name="rhsPropertyName">Name of the RHS property.</param>
		protected PropertyExpression(string lhsPropertyName, string rhsPropertyName)
		{
			this._lhsPropertyName = lhsPropertyName;
			this._rhsPropertyName = rhsPropertyName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsPropertyName">Name of the LHS property.</param>
		/// <param name="rhsProjection">The RHS projection.</param>
		protected PropertyExpression(string lhsPropertyName, IProjection rhsProjection)
		{
			this._lhsPropertyName = lhsPropertyName;
			this._rhsProjection = rhsProjection;
		}

		/// <summary>
		/// Get the Sql operator to use for the property expression.
		/// </summary>
		protected abstract string Op { get; }

		public override SqlString ToSqlString(ICriteria criteria, ICriteriaQuery criteriaQuery)
		{
			//SqlString[] columnNames =
			//	CriterionUtil.GetColumnNames(_lhsPropertyName, _lhsProjection, criteriaQuery, criteria);
			//SqlString[] otherColumnNames =
			//	CriterionUtil.GetColumnNames(_rhsPropertyName, _rhsProjection, criteriaQuery, criteria);

			SqlStringBuilder sb = new SqlStringBuilder();
			//if (columnNames.Length > 1)
			//{
			//	sb.Add(StringHelper.OpenParen);
			//}
			//bool first = true;
			//foreach (SqlString sqlString in SqlStringHelper.Add(columnNames, Op, otherColumnNames))
			//{
			//	if (first == false)
			//	{
			//		sb.Add(" and ");
			//	}
			//	first = false;
			//	sb.Add(sqlString);
			//}

			//if (columnNames.Length > 1)
			//{
			//	sb.Add(StringHelper.ClosedParen);
			//}

			return sb.ToSqlString();
		}

		

		/// <summary></summary>
		public override string ToString()
		{
			return (_lhsProjection ?? (object)_lhsPropertyName) + Op + (_rhsProjection ?? (object)_rhsPropertyName);
		}
        /// <summary>
        /// 参数转换
        /// </summary>
        /// <param name="dataParam"></param>
        /// <param name="isNameParams">是否将名称作为参数</param>
        /// <returns></returns>
        public override string ToString(ref DataParameterCollection dataParam, bool isNameParams = false)
        {
            StringBuilder where = new StringBuilder();
            string lhsPropertyName = ""; 
            if (_lhsProjection != null)
            {
                where.Append(_lhsProjection.ToString(ref dataParam, isNameParams));
            }
            else
            {
                where.Append(_lhsPropertyName);
                lhsPropertyName = _lhsPropertyName;
                dataParam.Add(new DataParameter(_lhsPropertyName, _rhsPropertyName), ref lhsPropertyName);
            }
            where.Append(Op);
            if (_rhsProjection != null)
            {
                where.Append(_rhsProjection.ToString(ref dataParam, isNameParams));
            }
            else
            {
                where.Append(dataParam.GetParameter(lhsPropertyName));
            }
            return where.ToString();
        }

        public override IProjection[] GetProjections()
		{
			if(_lhsProjection != null && _rhsProjection != null)
			{
				return new IProjection[] {_lhsProjection, _rhsProjection};
			}
			if(_lhsProjection != null)
			{
				return new IProjection[] {_lhsProjection};
			}
			if(_rhsProjection != null)
			{
				return new IProjection[] {_rhsProjection};
			}
			return null;
		}
	}
}
