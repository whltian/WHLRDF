using System.Collections.Generic;

namespace WHLRDF.ORM
{
	/// <summary> 
	/// An instance of <see cref="ICriteriaQuery"/> is passed to criterion, 
	/// order and projection instances when actually compiling and
	/// executing the query. This interface is not used by application
	/// code. 
	/// </summary>
	public interface ICriteriaQuery
	{

		/// <summary>Get the name of the column mapped by a property path, ignoring projection alias</summary>
		string GetColumn(ICriteria criteria, string propertyPath);
		
		/// <summary>Get the names of the columns mapped by a property path, ignoring projection aliases</summary>
		string[] GetColumns(ICriteria criteria, string propertyPath);

	

		string[] GetColumnAliasesUsingProjection(ICriteria criteria, string propertyPath);

		/// <summary>Get the names of the columns mapped by a property path</summary>
		string[] GetColumnsUsingProjection(ICriteria criteria, string propertyPath);

	

		/// <summary>Get the entity name of an entity</summary>
		string GetEntityName(ICriteria criteria);

		/// <summary> 
		/// Get the entity name of an entity, taking into account
		/// the qualifier of the property path
		/// </summary>
		string GetEntityName(ICriteria criteria, string propertyPath);

		/// <summary>Get the root table alias of an entity</summary>
		string GetSQLAlias(ICriteria subcriteria);

		/// <summary> 
		/// Get the root table alias of an entity, taking into account
		/// the qualifier of the property path
		/// </summary>
		string GetSQLAlias(ICriteria criteria, string propertyPath);

		/// <summary>Get the property name, given a possibly qualified property name</summary>
		string GetPropertyName(string propertyName);

		/// <summary>Get the identifier column names of this entity</summary>
		string[] GetIdentifierColumns(ICriteria subcriteria);

		///// <summary>Get the identifier type of this entity</summary>
		//IType GetIdentifierType(ICriteria subcriteria);

		//TypedValue GetTypedIdentifierValue(ICriteria subcriteria, object value);

		string GenerateSQLAlias();

		int GetIndexForAlias();


	}
}