using Microsoft.EntityFrameworkCore.Infrastructure;
using WHLRDF.ORM.Ado;
using WHLRDF.ORM.EF;

namespace WHLRDF.ORM
{
  
    public class Provider<T>:IProvider
    {
        public Provider(string connectionString,T database)
        {
            ConnectionString = connectionString;
            //database.ConnectionString = connectionString;
            Database = database;
        }
        //public Provider(string connectionString, AbstractDbContext database)
        //{
        //    ConnectionString = connectionString;
        //    DbContext = database;
        //    EFDatabase = database.Database;
        //}
        #region Properties
        string _ConnectionString = "";
        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }
            set
            {
                _ConnectionString = value;
            }
        }

        T _Database ;
        public T Database
        {
            get
            {
                return _Database;
            }
            set
            {
                _Database = value;
            }
        }
        /// <summary>
        /// 检查注册码是否正确
        /// </summary>
        /// <returns>是否进行了注册</returns>
        public static bool CheckRegister()
        {
            bool returnValue = true;
        
            return returnValue;
        }
        #endregion
    }
}
