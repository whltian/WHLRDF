﻿using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using WHLRDF.ORM;

namespace WHLRDF.ORM
{
    public class SerivceBase:  IDisposable
    {
        [JsonIgnore]
        /// <summary>
        /// 缓存
        /// </summary>
        public ICacheService CacheService
        {
            get
            {
                return CacheHelper.CacheService;
            }
        }
        private IDbRepository _dbRepository;
        [JsonIgnore]
        /// <summary>
        /// ado net 
        /// </summary>
        public virtual IDbRepository DbRepository
        {
            get
            {
                if (_dbRepository == null)
                {
                    if (ApplicationEnvironments.IsWeb)
                    {
                        _dbRepository = AppHttpContext.GetSerivce<IDbRepository>();
                    }
                    else
                    {
                        if (ApplicationEnvironments.Site.ORMType==ORMType.EF)
                        {
                            _dbRepository = new EFRepository();
                        }
                        else {
                            _dbRepository = new AdoRepository();
                        }
                    }
                }
                return _dbRepository;
            }
        }

        [JsonIgnore]
        public string UserId {
            get {
                return ApplicationEnvironments.DefaultSession.UserId;
            }
        }
        [JsonIgnore]
        public ProviderType CurrentProviderType {
            get { return _dbRepository.DbProviderType; }
        }

        private ISerialService _serialService;
        /// <summary>
        /// 获取系统编号
        /// </summary>
        [JsonIgnore]
        public ISerialService SerialService
        {
            get
            {
                if (_serialService == null)
                {
                    _serialService = AppHttpContext.GetSerivce<ISerialService>();
                }
                return _serialService;
            }
            set
            {
                _serialService = value;
            }
        }
        /// <summary>
     
        public IDbRepository CreateRepository(ProviderType providerType, string connationString,string version)
        {
            IDbRepository dbRepository = new AdoRepository(providerType, connationString, version);
            //if (ApplicationEnvironments.Site.IsUseEF)
            //{
            //    dbRepository = new EFRepository(providerType,connationString);
            //}
            //else
            //{
            //    dbRepository = new AdoRepository( providerType, connationString);
            //}
            return dbRepository;
        }

        public T GetById<T>(string id)where T:class,new()
        {
            return DbRepository.GetById<T>(id);
        }

        public T GetById<T>(string id,Func<T,T> func) where T : class, new()
        {
            return DbRepository.GetById<T>(id, func);
        }
       
        public IDbTransaction Begin()
        {
            return DbRepository.Begin();
        }

        public void Commit()
        {
            DbRepository.Commit();
        }

        public void Rollback()
        {
            DbRepository.Rollback();
        }

        public int Execute(string sqlCommand)
        {
            return DbRepository.Execute(sqlCommand);
        }

        public int Execute(string sqlCommand, DataParameterCollection parametersValues)
        {
            return DbRepository.Execute(sqlCommand, parametersValues);
        }

        public object Scalar(string hql, ICriterion criterion)
        {
            return DbRepository.Scalar(hql, criterion);
        }

        public object ExecuteProcedure(string procedure)
        {
          return  ExecuteProcedure(procedure,null);
        }
        public int ExecuteProcedure(string procedure,DataParameterCollection dbParameters)
        {
           return DbRepository.ExecuteProcedure(procedure, dbParameters);
        }
        public IDataReader Reader(string sqlCommand, DataParameterCollection parameters)
        {
            return DbRepository.Reader(sqlCommand, parameters);
        }

        public bool Insert<T>(T entity) where T : class, new()
        {
            return DbRepository.Insert(entity);
        }

        public bool Insert<T>(T entity, ref string strError) where T : class, new()
        {
            return DbRepository.Insert<T>(entity,ref strError);
        }

        public bool BatchInsert<T>(List<T> lsT, ref string strError) where T : class, new()
        {
            return DbRepository.BatchInsert<T>(lsT, ref strError);
        }

        public bool Update<T>(T entity) where T : class, new()
        {
            return DbRepository.Update<T>(entity);
        }

        public bool Update<T>(T entity, ref string strError) where T : class, new()
        {
            return DbRepository.Update<T>(entity,ref strError);
        }

        public bool Update<T>(T entity, ICriterion criterion) where T : class, new()
        {
            return DbRepository.Update<T>(entity, criterion);
        }

        public bool BatchUpdate<T>(List<T> lsT, ref string strError) where T : class, new()
        {
            return DbRepository.BatchUpdate<T>(lsT, ref strError);
        }

        public bool SaveOrUpdate<T>(T entity) where T : class, new()
        {
            return DbRepository.SaveOrUpdate<T>(entity);
        }

        public bool SaveOrUpdate<T>(T entity, ref string strError) where T : class, new()
        {
            return DbRepository.SaveOrUpdate<T>(entity,ref strError);
        }

        public bool BatchSave<T>(List<T> lstData) where T : class, new()
        {
            return DbRepository.BatchSave<T>(lstData);
        }

        public bool Delete<T>(ICriterion criterion, bool isupdated = true) where T : class, new()
        {
            return DbRepository.Delete<T>(criterion,isupdated);
        }

        public bool Delete<T>(string ids, bool isupdated = true) where T : class, new()
        {
            return DbRepository.Delete<T>(ids, isupdated);
        }

        public bool Delete<T>(T entity) where T : class, new()
        {
            return DbRepository.Delete<T>(entity);
        }

        public T Select<T>(ICriterion criterion) where T : class, new()
        {
            return DbRepository.Select<T>(criterion);
        }

        public T Select<T>(string hql, ICriterion criterion) where T : class, new()
        {
            return DbRepository.Select<T>(hql,criterion);
        }

        public List<T> Query<T>(ICriterion criterion) where T : class, new()
        {
            return DbRepository.Query<T>( criterion);
        }

        public List<T> Query<T>(ICriterion criterion, Order order) where T : class, new()
        {
            return DbRepository.Query<T>(criterion, order);
        }

        public List<T> Query<T>(string sql, ICriterion criterion) where T : class, new()
        {
            return DbRepository.Query<T>(sql,criterion);
        }

        public List<T> Query<T>(string sql, ICriterion criterion, Order order) where T : class, new()
        {
            return DbRepository.Query<T>(sql, criterion, order);
        }

        public List<T> Query<T>(ICriterion criterion, Order order, int pageindex, int pageSize, ref int recordCount) where T : class, new()
        {
            return DbRepository.Query<T>(criterion, order, pageindex, pageSize,ref recordCount);
        }

        public List<T> Query<T>(string hql, ICriterion criterion, Order order, int pageindex, int pageSize, ref int recordCount) where T : class, new()
        {
            return DbRepository.Query<T>(hql,criterion, order, pageindex, pageSize, ref recordCount);
        }

        public LigerGrid Query<T>(LigerGrid ligerGrid, ICriterion criterion) where T : class, new()
        {
            return DbRepository.Query<T>(ligerGrid, criterion);
        }

        public LigerGrid Query<T>(string hql, LigerGrid ligerGrid, ICriterion criterion) where T : class, new()
        {
            return DbRepository.Query<T>(hql,ligerGrid, criterion);
        }

        public void Dispose()
        {
         
        }

        public SerivceBase() {

        }
       
    }
}
