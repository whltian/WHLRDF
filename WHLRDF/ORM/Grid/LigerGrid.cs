﻿

using System.Collections.Generic;

using System.Text;
using WHLRDF.ORM;

namespace WHLRDF
{
    /// <summary>
    /// 
    /// </summary>
    public class LigerGrid
    {
        #region datatable
        public int page { get; set; }
        public int start { get; set; }

        public int length { get; set; }
        #endregion
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string sortName { get; set; }
        public string sortOrder { get; set; }

        public int records { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        public string keyWord { get; set; }
        public object Rows { get; set; }
        public int Total { get; set; }

        public int PageTotal
        {
            get {
                return pageSize>0?(Total / pageSize) + (Total % pageSize==0?0:1):1;
            }
        }

        public int totalCount { get => Total;  }

        /// <summary>
        /// 查询条件
        /// </summary>
        public string Where { get; set; }

        public GroupFilter Group { get; set; }

        public List<FilterParam> LstParms { get; set; }

        private ICriterion _whereExpression;


        public ICriterion WhereExpression
        {
            get
            {
                if (_whereExpression == null)
                {
                    LstParms = new List<FilterParam>(); _whereExpression = this.CreateWhere(Group);
                }
                return _whereExpression;
            }
        }

        public Dictionary<string, string> FormData;

        /// <summary>
        /// 是否初始化查询条件
        /// </summary>
        public bool IsInitWhere = true;

        public LigerGrid()
        {
            string[] keys = new string[] { "pageindex", "pagesize", "sortname", "sortorder", "keyword", "total", "where" };
            FormData = new Dictionary<string, string>();
           
        }

       
        /// <summary>
        /// 添加查询条件
        /// </summary>
        /// <param name="fieldName">字段名</param>
        /// <param name="option">操作方式</param>
        /// <param name="value">值</param>
        /// <param name="type">类型</param>
        /// <returns></returns>
        public RuleFilter CreateRule(string fieldName,string option,
            string value,string type)
        {
            var rule = new RuleFilter {
                 field= fieldName,
                 op= option,
                  value= value,
                type=type
            };
           
            return rule;
        }
        public void AddRule(string fieldName, string option,
            string value, string type)
        {
           
            if (Group == null)
            {
                Group = new GroupFilter();
            }
            if (Group.rules == null)
            {
                Group.rules = new List<RuleFilter>();
            }
            Group.rules.Add(CreateRule(fieldName,option,value,type));
           
        }
        /// <summary>
        /// 添加分组查询
        /// </summary>
        /// <param name="lstRules">查询条件列表</param>
        /// <param name="option">分组操作符</param>
        public void AddGroup(List<RuleFilter> lstRules,string option)
        {
            if (Group == null)
            {
                Group = new GroupFilter();
            }
            if (Group.groups == null)
            {
                Group.groups = new List<GroupFilter>();
            }
            Group.groups.Add(new GroupFilter {
                 rules=lstRules,
                  op= option
            });
        }
        public ICriterion CreateWhere(GroupFilter group)
        {
            if (group != null)
            {
                StringBuilder strBuilder = new StringBuilder();
                ICriterion criterion=null;
                criterion= TranslatorFilter.CoreTranslate(criterion,group, strBuilder, LstParms);
                return criterion;
            }
            return null;
        }

        public string GetValue(string key)
        {
            if (AppHttpContext.IsPost)
            {
              
                if (AppHttpContext.Current.Request.HasFormContentType&& AppHttpContext.Current.Request.Form.ContainsKey(key))
                {
                    return AppHttpContext.Current.Request.Form[key];
                }
            }
            else
            {
                if (AppHttpContext.Current.Request.Query.ContainsKey(key))
                {
                    return AppHttpContext.Current.Request.Query[key];
                }
            }
            return "";
        }
        
    }
}
