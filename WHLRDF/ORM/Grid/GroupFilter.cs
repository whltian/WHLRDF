﻿using System.Collections.Generic;
namespace WHLRDF.ORM
{
    /// <summary>
    /// 对应前台 QueryFilter 的检索规则数据
    /// </summary>
    public class GroupFilter
    {
        public List<RuleFilter> rules { get; set; }
        public string op { get; set; }
        public IList<GroupFilter> groups { get; set; }
    }
   
    public class RuleFilter
    {
        public RuleFilter()
        {
        }
        //public RuleFilter(string field, string value)
        //    : this(field, value, "equal")
        //{
        //}

        //public RuleFilter(string field, string value, string op)
        //{
        //    this.field = field;
        //    this.value = value;
        //    this.op = op;
        //}

        public string field { get; set; }
        public string value { get; set; }
        public string op { get; set; }
        public string type { get; set; }
    }
}
