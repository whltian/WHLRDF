﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.ORM
{
    /// <summary>
    /// 系统编码生成
    /// </summary>
     public interface ISerialService
    {
        /// <summary>
        /// 获取编号方法
        /// </summary>
        /// <param name="serialRuleId">标识</param>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        /// <param name="dataParams">参数</param>
        /// <param name="strError"></param>
        string CreateSerialNo(string serialRuleId, Dictionary<string, string> dataParams, ref string strError);
    }
}
