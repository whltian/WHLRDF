using System;
using System.Reflection;
using System.Collections.Generic;
using WHLRDF.ORM.Ado;
using WHLRDF.ORM.EF;
using System.Runtime.Loader;
using System.Linq;

namespace WHLRDF.ORM
{
    public class ProviderFactory
    {
        ///// <summary>
        ///// 获取数据库对象
        ///// </summary>
        ///// <param name="connectionString">连接字符串　例如：server=.;database=pubs;uid=sa;pwd=;Max Pool Size=20000;</param>
        ///// <param name="assemblyName">Provider所在的程序集　例如：TopFounder</param>
        ///// <param name="ProviderType">Provider所在类的FullName 例如：TopFounder.DataBaseHelper.SqlServer</param>
        ///// <returns></returns>
        //public static IProvider CreateProvider(string connectionString,string assemblyName,string ProviderType)
        //{
        //    Assembly assembly = Assembly.Load(assemblyName);
        //    object db = assembly.CreateInstance(ProviderType);
        //    if (db is AbstractDatabase)
        //    {
        //        return new Provider<T>(connectionString,(T)db);
        //    }
        //    //else if (db is AbstractDbContext)
        //    //{
        //    //    return new Provider(connectionString, (AbstractDbContext)db);
        //    //}
        //    else
        //    {
        //        throw new InvalidCastException("");
        //    }
        //}
        /// <summary>
        /// 创建数据库实例
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="connectionString">字符串</param>
        /// <param name="providerType">数据库类型</param>
        /// <param name="version">版本</param>
        /// <returns></returns>
        public static Provider<T> CreateProvider<T>(string connectionString, Type providerType,string version)
        {
            object[] args1 = new object[] { connectionString, version };
        
            object db = Activator.CreateInstance(providerType, args1);
            if (db is T)
            {
                return new Provider<T>(connectionString, (T)db);
            }
            //else if (db is AbstractDbContext)
            //{
            //    return new Provider<AbstractDbContext>(connectionString, (AbstractDbContext)db);
            //}
            else
            {
                throw new InvalidCastException("实例出错，无法建立数据库连接");
            }
        }
        public static Provider<T> CreateProvider<T>(string connectionString, Type providerType, string version, List<object> args)
        {
            args.Insert(0, connectionString);

            object db = Activator.CreateInstance(providerType, args.ToArray());
            if (db is T)
            {
                return new Provider<T>(connectionString, (T)db);
            }
            //else if (db is AbstractDbContext)
            //{
            //    return new Provider(connectionString, (AbstractDbContext)db);
            //}
            else
            {
                throw new InvalidCastException("实例出错，无法建立数据库连接");
            }
        }
       

    }
    public enum ORMType
    {
        /// <summary>
        /// EF 
        /// </summary>
        EF = 1,
        /// <summary>
        /// ADO
        /// </summary>
        ADO = 2
    }
}
