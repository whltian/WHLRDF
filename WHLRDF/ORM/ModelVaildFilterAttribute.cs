﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;


namespace WHLRDF.ORM
{
    /// <summary>
    /// 验证实体属性是否正确
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Method)]
    public class ModelVaildFilterAttribute :Attribute,IAsyncActionFilter
    {
        public bool IsPost = true;
        public bool IsGet = false;

        public ModelVaildFilterAttribute(bool isPost = true, bool isGet = false)
        {
            IsPost = isPost;
            IsGet = isGet;
        }
        public Task OnActionExecutionAsync(ActionExecutingContext filterContext, ActionExecutionDelegate next)
        {
            if ((AppHttpContext.IsPost && IsPost) || (IsGet && !AppHttpContext.IsPost))
            {
                if (filterContext.ActionArguments != null && filterContext.ActionArguments.Count > 0)
                {
                    foreach (var arg in filterContext.ActionArguments)
                    {
                        if (arg.Value is EntityBase)
                        {
                            var entity = arg.Value as EntityBase;
                            //DbEntityHelper dbEntity = new DbEntityHelper(entity);
                            //CheckValidHelper.CheckVaild(dbEntity, filterContext.ModelState);//验证数据类型是否正确
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }
    }
}
