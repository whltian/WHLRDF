using System;

namespace WHLRDF
{
   
    public class DbColumnAttribute : Attribute
    {
        public DbColumnAttribute()
        {
            this.AllowDBNull = true;
            this.IsPrimaryKey = false;
            this.Insert = true;
            this.Update = true;
            this.IsHide = false;
            this.IsSystem = false;
            this.IsUnique = false;
            this.IsSort = true;
            this.IsFilter = true;
            this.IsDefaultSort = false;
            this.IsDefaultFilter = false;
            this.Identifier = false;
            this.IsEnabled = true;
            this.IsEditVisible = true;
            this.ControlType = 1;
            this.Min = -1;
            this.Max = -1;
            this.IsFrozen = false;
            this.IsGridVisible = true;
            //if (ConfigHelper.ColumnWidth != null && ConfigHelper.ColumnWidth > 60)
            //{
            //    this.GridWidth = ConfigHelper.ColumnWidth.Value;
            //}
            //else
            //{
                this.GridWidth = 150;
            //}
        }

        public DbColumnAttribute(string columnName)
            : this()
        {
            this.Name = columnName;
        }
        public DbColumnAttribute(string columnName, string caption)
            : this(columnName)
        {
            this.LocalName = caption;
        }
        public DbColumnAttribute(string columnName, string caption, string type)
            : this(columnName, caption)
        {
            this.ColumnType = type;
        }
        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 是否自动增长
        /// </summary>
        public bool Identifier { get; set; }

        /// <summary>
        /// 对应数据库字段名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 本地名称
        /// </summary>
        public string LocalName { get; set; }

        /// <summary>
        /// 资源名称
        /// </summary>
        public string ReourceKeyName { get; set; }
        
        /// <summary>
        /// 字段类型
        /// </summary>
        public string ColumnType { get; set; }
        
        /// <summary>
        /// 默认值
        /// </summary>
        public string Default { get; set; }
        
        /// <summary>
        /// 公式
        /// </summary>
        public string Formula { get; set; }
        
        /// <summary>
        /// 排序
        /// </summary>
        public string Index { get; set; }
        
        /// <summary>
        /// 是否允许新增
        /// </summary>
        public bool Insert { get; set; }
        
        /// <summary>
        /// 长度
        /// </summary>
        public int MaxLength { get; set; }
        
        /// <summary>
        /// 是否允许为null
        /// </summary>
        public bool AllowDBNull { get; set; }
        
        /// <summary>
        /// 对应的sqlType
        /// </summary>
        public string SqlType { get; set; }
        
        /// <summary>
        /// 是否唯一
        /// </summary>
        public bool IsUnique { get; set; }
        
        /// <summary>
        /// 是否允许修改
        /// </summary>
        public bool Update { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool IsHide { get; set; }

        /// <summary>
        /// 是否是系统字段
        /// </summary>
        public bool IsSystem { get; set; }

        /// <summary>
        /// 是否排序
        /// </summary>
        public bool IsSort { get; set; }

        /// <summary>
        /// 是否排序
        /// </summary>
        public bool IsDefaultSort { get; set; }

        /// <summary>
        /// 是否支持查询
        /// </summary>
        public bool IsFilter { get; set; }

        /// <summary>
        /// 是否支持查询
        /// </summary>
        public bool IsDefaultFilter { get; set; }

        /// <summary>
        /// 默认查询条件
        /// </summary>
        public string IsDefaultFilterValue { get; set; }

        /// <summary>
        /// 正则表达式
        /// </summary>
        public string VaildKey { get; set; }

        /// <summary>
        /// 正则表达式
        /// </summary>
        public string RegValidate { get; set; }

        /// <summary>
        /// Format
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// 列宽
        /// </summary>
        public int GridWidth { get; set; }

        /// <summary>
        /// 是否启用 默认启用
        /// </summary>
        public bool IsEnabled { get; set; }

        public bool IsEditVisible { get; set; }

        /// <summary>
        /// 控件类别
        /// </summary>
        public int ControlType { get; set; }


        /// <summary>
        ///
        /// </summary>
        public int Min { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int Max { get; set; }

        /// <summary>
        /// 固定列
        /// </summary>
        public bool IsFrozen { get; set; }

        public object ObjValue { get; set; }

        public bool Ignore { get; set; }

        /// <summary>
        /// grid显示
        /// </summary>
        public bool IsGridVisible { get; set; }

        public bool IsExcel { get; set; }

        public int SysFieldTypeId { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        public int Scale { get; set; }

    }
}
