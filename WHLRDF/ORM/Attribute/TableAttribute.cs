﻿using System;
namespace WHLRDF
{
    [System.Serializable, System.AttributeUsage(System.AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        /// <summary>
        /// 对应表名
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 实体名称
        /// </summary>
        public virtual string Caption { get; set; }

        /// <summary>
        /// 是否是表
        /// </summary>
        public virtual bool IsTable { get; set; }

        /// <summary>
        /// 是否记录日志
        /// </summary>
        public virtual bool IsLogging { get; set; }
      

        /// <summary>
        /// 是否启用删除 默认启用
        /// </summary>
        public virtual bool IsDeleted { get; set; }

        /// <summary>
        /// 是否启用最后修改记录 默认启用
        /// </summary>
        public virtual bool IsLastModify { get; set; }

        /// <summary>
        /// 是否启用创建记录 默认启用
        /// </summary>
        public virtual bool IsCreated { get; set; }

        /// <summary>
        /// 是否启用缓存
        /// </summary>
        public virtual bool IsAutoCache { get; set; }

        /// <summary>
        /// 缓存键
        /// </summary>
        public virtual string CacheKey { get => Name; }

        /// <summary>
        /// 事件编码
        /// </summary>
        public virtual string EventCode { get; set; }

        private int _cacheTime = ApplicationEnvironments.Site.SessionTimeout;
        /// <summary>
        /// 缓存时间 分钟
        /// </summary>
        public virtual int CacheTime { get=> _cacheTime>0? _cacheTime: ApplicationEnvironments.Site.SessionTimeout; set=> _cacheTime=value; }

        public virtual string PrimaryKey { get; set; }

        public TableAttribute()
        {
            this.IsLogging = true;
            this.IsCreated = true;
            this.IsLastModify = true;
            this.IsDeleted = true;
            this.IsTable = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">表名</param>
        public TableAttribute(string tableName):this()
        {
            this.Name = tableName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="tableCaption">描述</param>
        public TableAttribute(string tableName, string tableCaption)
            : this(tableName)
        {
            this.Caption = tableCaption;
        }
    }
}
