﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.ORM
{
    /// <summary>
    /// 系统初始化数据接口
    /// </summary>
    public interface IInitializeService
    {
        /// <summary>
        /// 获取需要初始化的数据列表
        /// </summary>
        /// <param name="dbRepository">数据仓储对象</param>
        /// <param name="dicResult">数据结果</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool Read(IDbRepository dbRepository,ref Dictionary<string, object> dicResult, ref string strError);

        /// <summary>
        /// 将数据写入到表中
        /// </summary>
        /// <param name="dbRepository">数据仓储</param>
        /// <param name="data">存储对象</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool Write(IDbRepository dbRepository,Dictionary<string, object> data,ref string strError);
    }
}
