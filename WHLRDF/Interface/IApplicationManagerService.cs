﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF
{
    /// <summary>
    /// 应用接口
    /// </summary>
    public interface IApplicationManagerService
    {
        /// <summary>
        /// 启动状态
        /// </summary>
        bool Restarting { get; }
        /// <summary>
        /// 启动
        /// </summary>
        void Start();

        /// <summary>
        /// 停止
        /// </summary>
        void Stop();

        /// <summary>
        /// 重启
        /// </summary>
        void Restart();
    }
}
