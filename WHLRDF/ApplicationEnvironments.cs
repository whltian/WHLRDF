﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;



namespace WHLRDF
{
    /// <summary>
    /// 系统类型枚举
    /// </summary>
    public enum SysPropertyType
    {
        String = 1,
        Int = 2,
        DateTime = 3,
        Guid = 4,
        Decimal = 5,
        Bool = 6,
        Long = 7,
        Float = 8,
        Btye = 9,
        Single = 10,
        Double = 11,
        Blob = 12,
        Ntext = 13
    }
    /// <summary>
    /// 静态类
    /// </summary>
    public partial class ApplicationEnvironments
    {
       
        public static ProviderType ProviderType
        {
            get;
            set;
        }

    
        static IApplicationSession _session;
        /// <summary>
        /// 获取或设置默认的当前用户的登录的信息
        /// </summary>
        /// <returns></returns>
        public static IApplicationSession DefaultSession
        {
            get
            {
               
                return _session;
            }
            set
            {
                _session = value;
            }
        }

        static IApplicationManagerService _appManagerService;
        /// <summary>
        /// 获取或设置默认的当前用户的登录的信息
        /// </summary>
        /// <returns></returns>
        public static IApplicationManagerService AppManagerService
        {
            get
            {
                return _appManagerService;
            }
            set
            {
                _appManagerService = value;
            }
        }

        public static SiteSettings Site { get; set; }
        /// <summary>
        /// 如果为windows程序，那么自动更新的路径
        /// </summary>
        public static string AutoUpdaterPath = "";
       
        /// <summary>
        /// 产品的项目名
        /// </summary>
        public static string ApplicationName {
            get
            {
                return Site.ApplicationName;
            }
        }
        /// <summary>
        /// 版本
        /// </summary>
        public static string Version {
            get {
                return Site.Version;
            }
        }

        /// <summary>
        /// 文件存储路径
        /// </summary>
        public static string BaseDirectory
        {
            get {
                return Site.BaseDirectory;
            }
        }
        public static string DefaultDirectory
        {
            get
            {
                return Site.DefaultDirectory;
            }
        }
      
        ///// <summary>
        ///// 网站域名如.haopoo.com
        ///// </summary>
        //public static string Domain
        //{
        //    get
        //    {
        //        string localpath = Current.Request.Url.LocalPath;
        //        if (!string.IsNullOrEmpty(localpath) && !localpath.Equals("/") && !localpath.Equals("//"))
        //        {
        //            return Current.Request.Url.ToString().Replace(localpath + Current.Request.Url.Query, "");
        //        }
        //        return Current.Request.Url.ToString();

        //    }
        //}

        /// <summary>
        /// 存放当前信息的cookie关键字
        /// </summary>
        public static string CookieKeys = "whlrom";

        /// <summary>
        ///  是否是web程序
        /// </summary>
        public static bool IsWeb
        {
            get
            {
                return Site.IsWeb;
            }
           
        }
        public static int CommandTimeout = 30000;

        public static bool IsDevelopment { get; set; }


        public static List<string> GettAssemblies()
        {
            List<string> lstFiles = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!lstFiles.Contains(assembly.FullName))
                {
                    lstFiles.Add(assembly.FullName);
                }
             
            }
            return lstFiles;
        }
        #region 系统文件路径
     
        /// <summary>
        /// 临时文件路径
        /// </summary>
        public static string TempPath
        {
            get
            {
                return  RootPath + "/template/";
            }
        }
       
        /// <summary>
        /// 正式文件路径
        /// </summary>
        public static string PublicPath
        {
            get
            {
                return  RootPath + "/public/";
            }
        }
        /// <summary>
        /// 资源配置文件路径
        /// </summary>
        public static string ConfigPath
        {
            get
            {
                return RootPath + "/config/";
            }
        }

        /// <summary>
        /// 正式文件路径
        /// </summary>
        public static string HttpRootPath
        {
            get
            {
                return  UploadRootPath.HttpPath.ToLower() ;
            }
        }

        /// <summary>
        /// 文件路径
        /// </summary>
        public static string RootPath
        {
            get
            {
                return "/"+UploadRootPath.Path.ToLower();
            }
        }


        private static ConfigPath _configPath;
        /// <summary>
        /// 系统资源目录
        /// </summary>
        public static ConfigPath UploadRootPath
        {
            get
            {
                if (_configPath == null)
                {
                    _configPath = ApplicationEnvironments.Site.ConfigPath.Where(x => x.RootKey == ConfigPathKeyType.upload).FirstOrDefault();
                }
                return _configPath;
            }
        }
        #endregion
    }

}
