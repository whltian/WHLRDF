﻿
using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.Log;

namespace WHLRDF
{
    /// <summary>
    /// 当期登录用户的环境变量接口
    /// </summary>
    public interface IApplicationSession
    {
       

        /// <summary>
        /// 用户Id
        /// </summary>
        string UserId
        {
            get;
        }
        /// <summary>
        /// 用户姓名
        /// </summary>
        string UserName
        {
            get;
        }

        /// <summary>
        /// 真实姓名
        /// </summary>
        string RealName
        {
            get;
        }

        /// <summary>
        /// 用户Token
        /// </summary>
        string UserToken
        {
            get;
        }

        T GetUser<T>() where T : class, new();

    
        /// <summary>
        /// 当期登录的IP
        /// </summary>
        string IPAddress
        {
            get;
           
        }
        /// <summary>
        /// 页面的URL
        /// </summary>
        string FormAddress
        {
            get;
           
        }
        /// <summary>
        /// 服务器的地址加网站的地址
        /// </summary>
        string ServerAddress
        {
            get;
         
        }

        /// <summary>
        /// 当通过接口访问时设置并获取权限
        /// </summary>
        /// <param name="value"></param>
       void SetToken(string value);

        /// <summary>
        /// 获取当前请求信息
        /// </summary>
        /// <returns></returns>
        HttpLoggingModel GetContextModel();

        void SendMessage(string userno,int messageType,string message);


        /// <summary>
        /// 获取用户头像地址
        /// </summary>
        /// <param name="strError"></param>
        /// <returns></returns>
        string GetUserAvatarUrl(string userid="");
    }
}
