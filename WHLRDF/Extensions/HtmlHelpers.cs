﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WHLRDF
{
    public static class HtmlHelpers
    {

        public static string IsSelected(this IHtmlHelper html, string controller = null, string action = null,string area=null, string cssClass = null)
        {
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            string currentArea = (string)html.ViewContext.RouteData.Values["area"];
            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;
            if (String.IsNullOrEmpty(area))
                area = currentArea;
            return controller.ToLower() == currentController.ToLower() && action.ToLower() == currentAction.ToLower() &&((currentArea != null && area.ToLower() == currentArea.ToLower()) || currentArea==null) ?
                cssClass : String.Empty;
        }
        public static string IsSelectedArea(this IHtmlHelper html, string routeUrl = null, string cssClass = null)
        {
            if (string.IsNullOrWhiteSpace(routeUrl))
                routeUrl = "";
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";
            string[] arrParam = routeUrl.Split(new char[] { '/' });
            string currentArea = (string)html.ViewContext.RouteData.Values["area"];
            string area = arrParam[0];
            
            //if (String.IsNullOrEmpty(area))
            //    area = currentArea;
            return ((currentArea != null && area.ToLower() == currentArea.ToLower()) ) ?
                cssClass : String.Empty;
        }
        /// <summary>
        /// 判断地址是不是当前地址
        /// </summary>
        /// <param name="html"></param>
        /// <param name="routeUrl"></param>
        /// <param name="cssClass"></param>
        /// <param name="isRootArea">是否不是areas中的文件</param>
        /// <returns></returns>
        public static string IsSelectedPage(this IHtmlHelper html,string routeUrl=null, string cssClass = null,bool isAreas=true)
        {
            if (string.IsNullOrWhiteSpace(routeUrl))
                routeUrl = "";
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";
            string[] arrParam= routeUrl.Split(new char[] { '/' });
            string controller = "";
            string action = "";
            string area ="";
            if (isAreas)
            {
                if (arrParam.Length >= 2)
                {
                    controller = arrParam[1];
                    area = arrParam[0];
                    action = "Index";
                }
                if (arrParam.Length >= 3)
                {
                    action = arrParam[2];
                }
            }
            else
               {
                controller = arrParam[0];
                action = "Index";
                if (arrParam.Length >1 )
                {
                    action = arrParam[1];
                }
            }
            
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            string currentArea = (string)html.ViewContext.RouteData.Values["area"];
            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            //if (String.IsNullOrEmpty(action))
            //    action = currentAction;
            //if (String.IsNullOrEmpty(area))
            //    area = currentArea;
            return controller.ToLower() == currentController.ToLower() && action.ToLower() == currentAction.ToLower() && ((!string.IsNullOrWhiteSpace(currentArea) && area.ToLower() == currentArea.ToLower()) || string.IsNullOrWhiteSpace(currentArea)) ?
                cssClass : String.Empty;
        }

        public static string IsSelectedMenu(this IHtmlHelper html, string routeUrl = null, string cssClass = null)
        {
            if (string.IsNullOrWhiteSpace(routeUrl))
                routeUrl = "";
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";
            string[] arrParam = routeUrl.Split(new char[] { '/' });
            string controller = "";
            string action = "";
            string area = "";
            if (arrParam.Length >= 2)
            {
                controller = arrParam[1];
                area = arrParam[0];
                action = "Index";
            }
            if (arrParam.Length >= 3)
            {
                action = arrParam[2];
            }
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            string currentArea = (string)html.ViewContext.RouteData.Values["area"];
            //if (String.IsNullOrEmpty(controller))
            //    controller = currentController;
            //if (String.IsNullOrEmpty(area))
            //    area = currentArea;
            return controller.ToLower() == currentController.ToLower()&& ((currentArea != null && area.ToLower() == currentArea.ToLower()) || currentArea == null) ?
                cssClass : String.Empty;
        }
        public static string PageClass(this IHtmlHelper htmlHelper)
        {
            string currentAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            return currentAction;
        }


    }
}
