﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;
namespace WHLRDF
{
  
    /// <summary>
    /// controller 对象说明 菜单对象
    /// </summary>
    public class MenuPageAttribute : Attribute, IAsyncAuthorizationFilter
    {
        /// <summary>
        /// 前缀
        /// </summary>
        public const string PAGEPREFIX = "P";
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orderNum">排序</param>
        /// <param name="pageid">页面id</param>
        /// <param name="localName">名称</param>
        /// <param name="routeUrl">路由</param>
        /// <param name="icon">图标</param>
        /// <param name="isDispay">是否显示</param>
        /// <param name="ismenu">是否菜单</param>
        public MenuPageAttribute(int orderNum,string pageid, string localName, string routeUrl, string icon, bool isDispay = true, bool ismenu = true)
        {
            PageId =pageid.PadLeft(4,'0');
            LocalName = localName;
            Icon = icon;
            IsMenu = ismenu;
            IsDispay = isDispay;
            RouteUrl = routeUrl;
           
            OrderNum = orderNum;
            IsGet = true;
            IsParent = false;
            IsPost = true;
            NoAccess= false;
            IsApi = false;
        }
        /// <summary>
        /// 页面或者按钮权限，当为权限时，必须是数字编码 2位
        /// </summary>
        public string PageId { get; set; }

        /// <summary>
        /// 本地名称
        /// </summary>
        public string LocalName { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// 是否是菜单
        /// </summary>
        public bool IsMenu { get; set; } = true;

        /// <summary>
        /// 是否需要Admin才能访问
        /// </summary>
        public bool IsAdmin { get; set; } = false;

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsDispay { get; set; } = true;

        /// <summary>
        /// 是否无效菜单
        /// </summary>
        public bool IsDeleted { get; set; } = false;

        public string RouteUrl { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 上级菜单
        /// </summary>
        public string ParentId { get; set; }

        public string ParentName { get; set; }

        public int OrderNum { get; set; }

        /// <summary>
        /// 是否是api接口
        /// </summary>
        public bool IsApi { get; set; } = false;

        /// <summary>
        /// get请求是否需要验证权限 默认是
        /// </summary>
        public bool IsGet { get; set; } = true;
        /// <summary>
        /// post请求是否需要验证权限 默认是
        /// </summary>
        public bool IsPost { get; set; } = true;

        /// <summary>
        /// 域名称
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// 控制器名称
        /// </summary>

        public string ControllerName { get; set; }

        /// <summary>
        /// 是否继承controller  Index访问权限
        /// </summary>
        public bool IsParent { get; set; } = false;

        /// <summary>
        /// 不允许访问
        /// </summary>
        public bool NoAccess { get; set; } = false;

        

        public MenuPageAttribute()
        {
            IsGet = true;
            IsPost = true;
            IsParent = false;
            LocalName = "";
            Icon = "";
            IsMenu = false;
            IsDispay = true;
            RouteUrl = "";
            OrderNum = 1;
        }
        public MenuPageAttribute(string code, string description)
        {
            LocalName = description;
            Icon = "";
            IsMenu = false;
            IsDispay = true;
            RouteUrl = "";
            OrderNum = 1;
            IsGet = true;
            IsPost = true;
            IsParent = false;
            PageId = code;
            
        }

        /// <summary>
        /// 输出错误信息
        /// </summary>
        /// <param name="filterContext"></param>
        /// <param name="strError"></param>
        /// <param name="noSign"></param>
        public void WriteResult(AuthorizationFilterContext filterContext, string strError,bool signIn = true)
        {
            if (signIn)
            {
                if (AppHttpContext.IsAjax)
                {
                    filterContext.HttpContext.Response.StatusCode = 200;
                    filterContext.Result = new JsonResult(AjaxResult.Error(strError, -100));
                }
                else {
                    filterContext.HttpContext.Response.StatusCode = 200;
                    filterContext.Result = new RedirectToActionResult("index", "account", null);
                }
            }
            else
            {
                filterContext.Result = new JsonResult(AjaxResult.Error(strError,500));

            }

        }

        public virtual Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
           return Task.CompletedTask;
        }
    }
}
