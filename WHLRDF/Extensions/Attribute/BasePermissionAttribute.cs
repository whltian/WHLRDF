﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;
namespace WHLRDF
{
    [System.AttributeUsage(AttributeTargets.Method)]
    public class BasePermissionAttribute :Attribute, IAsyncAuthorizationFilter 
    {
        /// <summary>
        /// get请求是否需要验证权限 默认是
        /// </summary>
        public bool IsGet { get; set; }
        /// <summary>
        /// post请求是否需要验证权限 默认是
        /// </summary>
        public bool IsPost { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 与其它ActionName权限一样
        /// </summary>
        public string ActionCode { get; set; }

        /// <summary>
        /// Action名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 域名称
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// 控制器名称
        /// </summary>

        public string ControllerName { get; set; }

        /// <summary>
        /// 是否继承controller  Index访问权限
        /// </summary>
        public bool IsParent { get; set; }

        /// <summary>
        /// 不允许访问
        /// </summary>
        public bool NoAccess { get; set; }

        /// <summary>
        /// 是否需要系统admin权限才能操作
        /// </summary>
        public bool IsAdmin { get; set; }

        //public PermissionAttribute()
        //{
        //    IsGet = true;
        //    IsPost = true;
        //}
        public BasePermissionAttribute() {
            IsGet = true;
            IsPost = true;
            IsParent = false;
        }
        public BasePermissionAttribute(string code,string description)
        {
            IsGet = true;
            IsPost = true;
            IsParent = false;
            ActionCode =  code;
            Description = description;
        }
       
        /// <summary>
        /// 输出错误信息
        /// </summary>
        /// <param name="filterContext"></param>
        /// <param name="strError"></param>
        public void WriteResult(AuthorizationFilterContext filterContext,  string strError)
        {
            var areaName = "";
            var actionName = filterContext.RouteData.Values["Action"].ToString();
            var controllerName = filterContext.RouteData.Values["controller"].ToString();
            if (filterContext.RouteData.DataTokens["area"] != null)
            {
                areaName = filterContext.RouteData.DataTokens["area"].ToString();
            }
            //new LogErrorService().Save((!string.IsNullOrEmpty(areaName)?areaName+"/"+controllerName:controllerName),actionName,strError,strError);
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.Result = new JsonResult(AjaxResult.Error(strError, -100));
            //if (AppHttpContext.IsPost && AppHttpContext.IsAjax)
            //{
            //    filterContext.HttpContext.Response.StatusCode = 500;
            //    filterContext.Result = new JsonResult(AjaxResult.Error(strError, -100));
            //}
            //else
            //{
            //    filterContext.HttpContext.Response.StatusCode = 500;
            //    filterContext.Result = new JsonResult(AjaxResult.Error(strError, -100));
            //    var view = new ViewResult();
            //    view.ViewName = "~/Views/Home/Error.cshtml";
            //    view.ViewData = new Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary(new BaseController().ViewData);
            //    view.ViewData["Message"] = strError;
            //    view.ViewData["Exception"] = new Exception(strError);
            //    filterContext.Result = view;

            //}

        }

        public virtual Task OnAuthorizationAsync(AuthorizationFilterContext filterContext)
        {
            return Task.CompletedTask;
        }
    }
}
