﻿

namespace WHLRDF
{
    /// <summary>
    /// 系统任务
    /// </summary>
    public interface ITaskService
    {
        bool InsertTask(TaskModel taskModel, ref string strError);
    }
}
