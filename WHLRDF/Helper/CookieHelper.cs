﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF
{
    public class CookieHelper
    {
        private static HttpResponse CurrentResponse
        {
            get {
                return AppHttpContext.Current.Response;
            }
        }
        private static HttpRequest CurrentRequest
        {
            get
            {
                return AppHttpContext.Current.Request;
            }
        }
        public static void Add(string key, string value, int minutes = 30)
        {
            
            CurrentResponse.Cookies.Append(key,value, new CookieOptions
            {
                Expires = DateTime.Now.AddMinutes(minutes)
            });
        }
        public static void Delete(string key)
        {
            CurrentResponse.Cookies.Delete(key);
        }
        /// <summary>
        /// 获取cookies
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>返回对应的值</returns>
        public static string GetCookies(string key)
        {
            CurrentRequest.Cookies.TryGetValue(key, out string value);
            if (string.IsNullOrEmpty(value))
                value = string.Empty;
            return value;
          
        }
    }
}
