﻿
using System.Collections;
using System.Collections.Generic;
using AutoMapper;

namespace WHLRDF
{
    /// <summary>
    /// AutoMapper扩展帮助类
    /// </summary>
    public static class AutoMapperHelper
    {

        public static TDestination Map<TDestination>(object source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<TDestination>(source);
        }

        public static TDestination Map<TSource, TDestination>(TSource source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TDestination>(this object source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<TDestination>(source);
        }

        public static List<TDestination> MapToList<TDestination>(this IEnumerable source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<List<TDestination>>(source);
        }


        public static List<TDestination> MapToList<TSource, TDestination>(this IEnumerable<TSource> source)
        {
            var mapper = AppHttpContext.GetSerivce<IMapper>();
            return mapper.Map<List<TDestination>>(source);
        }
    }
}
