﻿
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using NPOI.OpenXmlFormats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHLRDF
{
    /// <summary>
    /// 前台Ajax请求的统一返回结果类
    /// </summary>
    public class AjaxResult
    {
        public AjaxResult()
        {
            StatusCode = 200;
        }

        private bool _isSuccess = false;

        /// <summary>
        /// 是否产生错误
        /// </summary>
        public bool IsSuccess { get { return _isSuccess; } }

        /// <summary>
        /// 错误信息，或者成功信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 错误信息，或者成功信息
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// 成功可能时返回的数据
        /// </summary>
        public object Data { get; set; }

        #region Error
        public static AjaxResult Error()
        {
            return new AjaxResult()
            {
                _isSuccess = false
            };
        }
        public static AjaxResult Error(string message)
        {
            return new AjaxResult()
            {
                _isSuccess = false,
                StatusCode=-1,
                Message = message
            };
        }
        public static AjaxResult Error(string message,int statusCode)
        {
            return new AjaxResult()
            {
                _isSuccess = false,
                StatusCode=statusCode,
                Message = message
            };
        }

        /// <summary>
        /// 获取验证的错误信息
        /// </summary>
        /// <param name="modelState"></param>
        public static string GetModelError(ModelStateDictionary modelState)
        {
            string strError = "";
            foreach (string key in modelState.Keys)
            {
                foreach (var p in modelState[key].Errors)
                {
                    strError += "\n " + key + ":" + p.ErrorMessage;
                }
            }
            return strError;
        }

        /// <summary>
        /// 获取验证的错误信息
        /// </summary>
        /// <param name="modelState"></param>
        public static string GetModelError(Dictionary<string,string> dicError)
        {
            string strError = "";
            foreach (var item in dicError)
            {
                strError += "\n " + item.Key + ":" + item.Value;
            }
            return strError;
        }
        public static AjaxResult Error(Dictionary<string,string> modelState)
        {
            string strError = GetModelError(modelState);
            return new AjaxResult()
            {
                _isSuccess = false,
                Data = modelState,
                Message = strError
            };
        }
        public static AjaxResult Error(ModelStateDictionary modelState)
        {
            string strError = GetModelError(modelState);
            return new AjaxResult()
            {
                _isSuccess = false,
                Data = modelState,
                Message= strError
            };
        }
        #endregion

        #region Success
        public static AjaxResult Success()
        {
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode=0
            };
        }
        public static AjaxResult Success(string message)
        {
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode=0,
                Message = message
            };
        }
        public static AjaxResult Success(object data)
        {
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode=0,
                Data = data
            };
        }
        public static AjaxResult Success<source,to>(source data)
        {
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode = 0,
                Data = data.MapTo<to>()
            };
        }
        public static AjaxResult Success(object data, string message)
        {
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode=0,
                Data = data,
                Message = message
            };
        }
        public static AjaxResult Success<T1,T2>(T1 data, string message)
        {
         
            return new AjaxResult()
            {
                _isSuccess = true,
                StatusCode = 0,
                Data = data.MapTo<T2>(),
                Message = message
            };
        }
        #endregion

        #region 
        /// <summary>
        /// 返回基础信息
        /// </summary>
        /// <param name="status">成功返回true </param>
        /// <param name="message">消息</param>
        /// <returns></returns>
        public static AjaxResult Info(bool status=true, string message="")
        {
            return new AjaxResult()
            {
                _isSuccess = status,
                StatusCode = -1,
                Message = message
            };
        }
        #endregion

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}