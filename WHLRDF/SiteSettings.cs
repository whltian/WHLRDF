﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;

namespace WHLRDF
{
    public enum ProviderType
    {
        SqlServer = 1,
        Oracle = 2,
        MySql = 3,
        MsSqlCe = 4,
        SQLite = 5,
        PostgreSQL = 6,
        JetDriver = 7,
        DB2 = 8,
        Oledb = 9,
        ODBC = 10,
        Access = 11,
        SqlServer_2008 = 12,
        /// <summary>
        /// 其他类型
        /// </summary>
        Default = 99
    }

    /// <summary>
    /// 系统主要目录
    /// </summary>
    public enum ConfigPathKeyType { 
        /// <summary>
        /// 配置文件存放路径
        /// </summary>
        config=1,
        /// <summary>
        /// 系统上传文件
        /// </summary>
        upload=2,
        /// <summary>
        /// 静态文件
        /// </summary>
        statichtml=3,

      
}
    public class SiteSettings
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 数据驱动
        /// </summary>
        public ProviderType ProviderType
        {
            get
            {
                return GetProviderType(DBProviderType.ToString());
            }
        }

        /// <summary>
        /// 数据驱动
        /// </summary>
        public ProviderType DBProviderType { get; set; }

        /// <summary>
        /// 数据引擎版本号
        /// </summary>
        public string ProviderVersion { get {
               
                    return GetProviderVersion(this.DBProviderType.ToString());
          
            } }

        /// <summary>
        /// ORM类型
        /// </summary>
        public ORMType ORMType { get; set; }

        /// <summary>
        /// redis链接
        /// </summary>
        public string RedisConnection { get; set; }

        /// <summary>
        /// 应用程序头
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// 缓存类别
        /// </summary>
        public CacheType CacheType { get; set; }

        /// <summary>
        /// 当前版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 是否是Web应用
        /// </summary>
        public bool IsWeb { get; set; }

        /// <summary>
        /// 域
        /// </summary>
        public string Domain { get; set; }



        /// <summary>
        /// 邮件配置
        /// </summary>
        public SMTPConfig SMTP { get; set; }

        /// <summary>
        /// md5加密
        /// </summary>
        public string MD5key { get; set; }

        /// <summary>
        /// 向量加密
        /// </summary>
        public string AESKey { get; set; }

        public string AESIV { get; set; }

        /// <summary>
        /// 日志接口
        /// </summary>
        public string DBLog { get; set; }

        private int _sessionTimeout = 20;
        public int SessionTimeout { get => _sessionTimeout; set => _sessionTimeout = value; }

        /// <summary>
        /// 用户缓存键值
        /// </summary>
        public string UserCacheKey { get; set; }

        /// <summary>
        /// 系统用户处理dll
        /// </summary>
        public string UserServiceInstance { get; set; }

        /// <summary>
        /// 文件存储路径
        /// </summary>
        public string BaseDirectory { get; set; }

        /// <summary>
        /// 初始化数据dll路径
        /// </summary>
        public string InitData { get; set; }

        /// <summary>
        /// 默认密码
        /// </summary>
        public string DefaultPassword { get; set; }

        public List<ConfigPath> ConfigPath { get; set; }

        /// <summary>
        /// 默认静态文件路径
        /// </summary>
        public string DefaultDirectory { get; set; }

        /// <summary>
        /// 反射系统dll规则
        /// </summary>
        public AssemblyRule AssemblyRule { get; set; }

        public bool IsEntityCache { get; set; }

        /// <summary>
        /// 系统cookie配置
        /// </summary>
        public CookieSetting Cookie { get; set; }

        /// <summary>
        /// 系统管理员
        /// </summary>
        public string AdminUser { get; set; }

        public ApiConfig Api { get; set; }


        public SystemComponentConfig Components { get; set; }

        /// <summary>
        /// 服务器地址
        /// </summary>
        public string ServerAddress { get; set; }

        /// <summary>
        /// 获取无需读取Response contentType
        /// </summary>
        public List<string> NoGetResponse { get; set; }

        /// <summary>
        /// 是否启用SignalR
        /// </summary>
        public bool IsSignalR { get; set; }

        /// <summary>
        /// 发送消息的Url
        /// </summary>
        public string SendMessageUrl { get; set; }

        /// <summary>
        /// 日志微服务
        /// </summary>
        public string LogSiteService { get; set; }

        /// <summary>
        /// 系统是否进行初始化
        /// </summary>
        public bool Install { get; set; }
        /// <summary>
        /// 系统标识
        /// </summary>
        public string ApplicationIdentify { get; set; }
        /// <summary>
        /// Mongo连接字符串
        /// </summary>
        public string MongoConnection { get; set; }

        /// <summary>
        /// 是否记录sql日志
        /// </summary>
        public bool IsDBLog { get; set; }

        /// <summary>
        /// 获取数据引擎版本号
        /// </summary>
        /// <param name="providerTypeName"></param>
        /// <returns></returns>
        public string GetProviderVersion(string providerTypeName)
        {
            if (!string.IsNullOrWhiteSpace(providerTypeName)&& providerTypeName.IndexOf("_") > 0)
            {
                return providerTypeName.Substring(providerTypeName.IndexOf("_") + 1);
            }
            return "";
        }
        /// <summary>
        /// 获取数据库驱动类型
        /// </summary>
        /// <param name="providerTypeName"></param>
        /// <returns></returns>
        public ProviderType GetProviderType(string providerTypeName)
        {
            if (string.IsNullOrWhiteSpace(providerTypeName))
            {
                return ProviderType.SqlServer;
            }
            if ( providerTypeName.IndexOf("_") > 0)
            {
                providerTypeName = providerTypeName.Substring(0, providerTypeName.IndexOf("_"));
            }
            return Enum.Parse<ProviderType>(providerTypeName);
        }
    }

    /// <summary>
    /// 邮件配置
    /// </summary>
    public class SMTPConfig
    {

        public string Server { get; set; }

        public string  Account { get; set; }

        public string  Password { get; set; }

        public string SendAddress { get; set; }

        public int Port { get; set; }
    }

    /// <summary>
    /// 文件存放目录配置
    /// </summary>
    public class ConfigPath
    {
        public string Path { get; set; }

        public string HttpPath { get; set; }

        public ConfigPathKeyType RootKey { get; set; }

      

    }

    /// <summary>
    /// 反射系统dll规则
    /// </summary>
    public class AssemblyRule
    {
        public string ModelRule { get; set; }

        public string ServiceRule { get; set; }

        public string MvcRule { get; set; }

        public string ApiRule { get; set; }

        /// <summary>
        /// 其它实体model加载
        /// </summary>
        public string OtherModelRule { get; set; }
    }

    /// <summary>
    /// cookie配置
    /// </summary>
    public class CookieSetting
    {

        public string Domain{ get; set; }
        public string Name { get; set; }
        public string RootPath { get; set; }

        public string ProtectionProviderPath { get; set; }
    }

    public class ApiConfig
    {
        /// <summary>
        /// 获取命名空间规则
        /// </summary>
        public string NameRule { get; set; }

        /// <summary>
        /// api功能模块
        /// </summary>
        public List<ApiArea> Areas { get; set; }
       // public 
    }
    public class ApiArea
    {
        /// <summary>
        /// 模块编号
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 模块键值
        /// </summary>

        public string AreaKey { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        public string AreaValue { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int OrderNum { get; set; }
    }
   

    /// <summary>
    /// 系统组件
    /// </summary>
    public class SystemComponentConfig
    {
        /// <summary>
        /// 系统组件配置
        /// </summary>
        public SystemComponent System { get; set; }

        public List<SystemComponent> Plug { get; set; }
    }

    /// <summary>
    /// 单一组件对象
    /// </summary>
    public class SystemComponent
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 唯一值 Area值
        /// </summary>
        public string Unique { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 是否接口
        /// </summary>
        public bool IsApi { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool Opened { get; set; }

        /// <summary>
        /// 是否菜单
        /// </summary>
        public bool IsMenu { get; set; } = true;

        /// <summary>
        /// 排序
        /// </summary>
        public int OrderNum { get; set; }

        /// <summary>
        /// 模型
        /// </summary>
        public string[] Model { get; set; }

        /// <summary>
        /// 服务类
        /// </summary>
        public string[] Service { get; set; }

        /// <summary>
        /// mvc
        /// </summary>
        public string Mvc { get; set; }
        /// <summary>
        /// view
        /// </summary>
        public string Views { get; set; }

        /// <summary>
        /// api显示
        /// </summary>
        public string Api { get; set; }

        
    }
}
