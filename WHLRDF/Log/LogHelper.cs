﻿using System;
using System.Reflection;
using log4net;
using log4net.Layout;
using log4net.Layout.Pattern;
using log4net.Repository;
namespace WHLRDF.Log
{

    public class MyMessagePatternConverter : PatternLayoutConverter
    {
        /// <summary>
        /// 通过反射获取传入的日志对象的某个属性的值
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        protected override void Convert(System.IO.TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (!string.IsNullOrEmpty(Option))
            {
                object obj = loggingEvent.MessageObject;
                if (obj != null)
                {
                    PropertyInfo info = obj.GetType().GetProperty(Option);
                    if (info != null)
                    {
                        object cusMsg = info.GetValue(obj, null);
                        writer.Write(cusMsg);
                    }
                }
            }

        }

    }
    public class MyLayout : PatternLayout
    {
        public MyLayout()
        {
            this.AddConverter("property", typeof(MyMessagePatternConverter));
        }
    }

    public static class LogHelper
    {
 
        public static ILoggerRepository Repository { get; set; }

        public static void Init(ILoggerRepository repository)
        {
            Repository = repository;
            logerror = LogManager.GetLogger(Repository.Name, "logerror");
            logInfo=LogManager.GetLogger(Repository.Name, "loginfo");
        }

        private static ILog logerror { get; set; } 

        private static ILog logInfo { get; set; }

      
   

        /* Log a message object */

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Debug(object source, string message)
        {
            Debug(source.GetType(), message);
        }

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="ps">ps</param>
        public static void Debug(object source, string message, params object[] ps)
        {
            Debug(source.GetType(), string.Format(message, ps));
        }

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Debug(Type source, string message)
        {
       
            if (logInfo.IsDebugEnabled)
            {
                logInfo.Debug(message);
            }
        }

        /// <summary>
        /// 关键信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Info(string source, object message)
        {
            if (logInfo.IsInfoEnabled)
            {
                logInfo.Info(message);
            }
        }
    
        /// <summary>
        /// 关键信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Info(Type source, object message)
        {
            if (logInfo.IsInfoEnabled)
            {

                logInfo.Info(message);
            }
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Warn(object source, object message)
        {
            Warn(source.GetType(), message);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Warn(Type source, object message)
        {
          
            if (logInfo.IsWarnEnabled)
            {
                logInfo.Warn(message);
            }
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Error(object source, object message)
        {
            Error(source.GetType(), message);
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Error(Type source, object message)
        {
          
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(message);
            }
        }

        /// <summary>
        /// 失败信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Fatal(object source, object message)
        {
            Fatal(source.GetType(), message);
        }

        /// <summary>
        /// 失败信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        public static void Fatal(Type source, object message)
        {
        
            if (logerror.IsFatalEnabled)
            {
                logerror.Fatal(message);
            }
        }

        /* Log a message object and exception */

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Debug(object source, object message, Exception exception)
        {
            Debug(source.GetType(), message, exception);
        }

        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Debug(Type source, object message, Exception exception)
        {
            logInfo.Debug(message, exception);
        }

        /// <summary>
        /// 关键信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Info(object source, object message, Exception exception)
        {
            Info(source.GetType(), message, exception);
        }

        /// <summary>
        /// 关键信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Info(Type source, object message, Exception exception)
        {
            logInfo.Info(message, exception);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Warn(object source, object message, Exception exception)
        {
            Warn(source.GetType(), message, exception);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Warn(Type source, object message, Exception exception)
        {
            logInfo.Warn(message, exception);
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Error(object source, object message, Exception exception)
        {
            Error(source.GetType(), message, exception);
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Error(Type source, object message, Exception exception)
        {
            logerror.Error(message, exception);
        }

        /// <summary>
        /// 失败信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Fatal(object source, object message, Exception exception)
        {
            Fatal(source.GetType(), message, exception);
        }

        /// <summary>
        /// 失败信息
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="message">message</param>
        /// <param name="exception">ex</param>
        public static void Fatal(Type source, object message, Exception exception)
        {
            logerror.Fatal(message, exception);
        }
        /// <summary>
        /// 记录SQL日志
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="tickTimes"></param>
        /// <param name="sourceORM"></param>
        /// <param name="providerType"></param>
        public static void SQLInfo(string commandText, double tickTimes,string sourceORM, string providerType)
        {
            if (!ApplicationEnvironments.Site.IsDBLog)
            {
                return;
            }
            string requestIdentifier = "";
            if (AppHttpContext.Current != null)
            {
                requestIdentifier = AppHttpContext.Current.TraceIdentifier;
            }
            var obj = new { commandText = commandText, tickTimes = tickTimes, sourceCode = sourceORM, ProviderType = providerType, RequestIdentifier= requestIdentifier };
            if (!string.IsNullOrWhiteSpace(ApplicationEnvironments.Site.LogSiteService))
            {
                string strResult = "";
                SendHttpHelper.WriteLogAsync(ApplicationEnvironments.Site.LogSiteService + "/sqllog", obj.ToJson(), ref strResult);
            }
            Info("SQL-TEST", JSONHelper.ToJson(obj));
        }

        /// <summary>
        /// 记录请求日志
        /// </summary>
        /// <param name="httpContext">请求内容</param>
        /// <param name="isSendHttp">是否发送到服务器</param>
        public static void RequestInfo(HttpLoggingModel httpContext,bool isSendHttp=true)
        {
            if (isSendHttp && !string.IsNullOrWhiteSpace(ApplicationEnvironments.Site.LogSiteService))
            {
                string strResult = "";
                SendHttpHelper.WriteLogAsync(ApplicationEnvironments.Site.LogSiteService + "/requestlog", httpContext.ToJson(), ref strResult);
            }
            else
            {
                Info(httpContext.MenuPage, JSONHelper.ToJson(httpContext));
            }
        }

        public static void Error(Exception exception)
        {
            var httpContext = ApplicationEnvironments.DefaultSession.GetContextModel();
            httpContext.Result.EndDate = DateTime.Now;
            httpContext.Result.StatusCode = 500;
            httpContext.Result.Body = exception.StackTrace;
            httpContext.Result.IsError = true;
            if (!string.IsNullOrWhiteSpace(ApplicationEnvironments.Site.LogSiteService))
            {
                string strResult = "";
                SendHttpHelper.WriteLogAsync(ApplicationEnvironments.Site.LogSiteService + "/requestlog", httpContext.ToJson(), ref strResult);
            }
            Error(exception, JSONHelper.ToJson(httpContext));
        }
        public static void TaskInfo(TaskModel taskModel)
        {
            if (!string.IsNullOrWhiteSpace(ApplicationEnvironments.Site.LogSiteService))
            {
                string strResult = "";
                SendHttpHelper.WriteLogAsync(ApplicationEnvironments.Site.LogSiteService+"/tasklog", taskModel.ToJson(), ref strResult);
            }
            Info(taskModel.TaskName, taskModel.ToJson());
        }
    }
}