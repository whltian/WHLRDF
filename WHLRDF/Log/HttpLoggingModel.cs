﻿using Microsoft.AspNetCore.Http;
using System;
using System.Runtime.Serialization;

namespace WHLRDF.Log
{
    /// <summary>
    /// Http 请求日志 帮助信息类
    /// </summary>
    public class HttpLoggingModel
    {
        /// <summary>
        /// 请求头
        /// </summary>
        public string Headers { get; set; }

        /// <summary>
        /// 请求唯一id
        /// </summary>
        public string TraceIdentifier { get; set; }

        [DataMember]
        /// <summary>
        /// 请求相对路径
        /// </summary>
        public string MenuPage { get; set; }

        /// <summary>
        /// 主机头
        /// </summary>
        public string Host { get; set; }

        public string Query { get; set; }
        //
        // 摘要:
        //     Gets or sets the request body as a form.
        public string Form { get; set; }

        /// <summary>
        /// 获取请求 协议
        /// </summary>
        public string Protocol { get; set; }




        public string ContentType { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 请求内容
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public string UserId { get => ApplicationEnvironments.DefaultSession.UserId; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get => ApplicationEnvironments.DefaultSession.UserName; }

        /// <summary>
        /// 用户真实姓名
        /// </summary>
        public string RealName { get => ApplicationEnvironments.DefaultSession.UserName; }

        private string _ipAddress = "";
        /// <summary>
        /// 用户ip
        /// </summary>
        public string IpAddress
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_ipAddress))
                {
                    _ipAddress = AppHttpContext.Current.Request.Headers["X-Forwarded-For"].ToJson();
                    if (string.IsNullOrEmpty(_ipAddress))
                    {
                        _ipAddress = AppHttpContext.Current.Connection.RemoteIpAddress.ToString();
                    }
                }

                return _ipAddress;
            }
            set {
                _ipAddress = value;
            }
        }

        /// <summary>
        /// 请求方式  AppHttpContext.IsPost ? "POST" : "GET";
        /// </summary>
        public bool IsPost { get; set; }

        /// <summary>
        /// 请求平台
        /// </summary>
        public string SourcePlatform { get; set; }

        /// <summary>
        /// 当前服务器
        /// </summary>
        public string ServerPlatform { get => ApplicationEnvironments.Site.ApplicationIdentify; }

        /// <summary>
        /// AppHttpContext.Current.Request.Scheme.Equals("https");
        /// </summary>
        public bool IsHttps { get; set; }

        /// <summary>
        /// 从定向地址 AppHttpContext.Current.Request.Headers["Referer"].ToJson(); 
        /// </summary>
        public string Referer { get; set; }

        /// <summary>
        /// 请求用户头 AppHttpContext.Current.Request.Headers["User-Agent"].ToJson();
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 应用程序名称
        /// </summary>
        public string ApplicationName { get => ApplicationEnvironments.Site.ApplicationName; }


        /// <summary>
        /// 应用程序地址
        /// </summary>
        public string ServerAddress { get => ApplicationEnvironments.Site.ServerAddress; }

        /// <summary>
        /// 是否前端请求
        /// </summary>
        public bool IsAjax { get; set; }

        /// <summary>
        /// 请求结果
        /// </summary>
        public HttpLoggingResultModel Result { get; set; }

        /// <summary>
        /// 压缩方式
        /// </summary>
        public string AcceptEncoding{get;set;}

    }

    /// <summary>
    /// 请求结果
    /// </summary>
    public class HttpLoggingResultModel
    {
        /// <summary>
        /// 是否存在错误
        /// </summary>
        public bool IsError { get; set; } = false;

        /// <summary>
        /// 状态码
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        public string Body { get; set; }

        public string ContentType { get; set; }

        /// <summary>
        /// 请求耗时(单位毫秒)
        /// </summary>
        public double TotalMilliseconds { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 返回头部
        /// </summary>
        public string Headers { get; set; }
    }


}
