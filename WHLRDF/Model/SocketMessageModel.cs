﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Model
{
    /// <summary>
    /// 通讯消息
    /// </summary>
    public class SocketMessageModel
    {
        /// <summary>
        /// 发送者Id
        /// </summary>
        public string SenderId { get; set; }

        /// <summary>
        /// 发送者名称
        /// </summary>
        public string SenderName { get; set; }

        /// <summary>
        /// 接受者id
        /// </summary>
        public string ReceiverId { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public string MessageType { get; set; }

        public object Content { get; set; }

        public DateTime SenderDate {
            get {
                return DateTime.Now;
            }
        }
    }
}
