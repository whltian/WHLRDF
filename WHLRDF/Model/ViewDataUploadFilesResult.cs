﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHLRDF.ORM
{
    /// <summary>
    /// 附件对象
    /// </summary>
   public class ViewDataUploadFilesResult
    {
        public string name { get; set; }
        public decimal? size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string delete_url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_type { get; set; }
        private bool _IsError = false;
        public bool IsError { get { return _IsError; } set { _IsError = value; } }
        public string error { get; set; }
        public string path { get; set; }
        public string createUser = "";
        public string createDate = "";
        public string oldStepName { get; set; }
        public string StepName { get; set; }
        public string taskId = "";
    }
}
