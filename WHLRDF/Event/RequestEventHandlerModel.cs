﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.ORM;

namespace WHLRDF.Event
{
    /// <summary>
    /// 事件配置
    /// </summary>
    public class EventHandlerSetting
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string EventCode { get; set; }

        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 加密字符串
        /// </summary>
        public string MD5 { get; set; }
    }
    /// <summary>
    /// 事件请求实体
    /// </summary>
    public class RequestEventHandlerModel
    {
        /// <summary>
        /// 事件编码
        /// </summary>
        public string EventCode { get; set; }

        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string timestamp { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public object body { get; set; }
      
    }
}
