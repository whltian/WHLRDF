﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Cache
{
    public class BaseCacheService
    {
        /// <summary>
        /// 判断参数是否为空
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public void IsArgumentNull(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="field">主键名</param>
        /// <param name="key">主键值</param>
        public void IsDbArgumentNull(string tableName,string field, string key)
        {
            if (string.IsNullOrWhiteSpace(tableName) || string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(tableName) + " or " + nameof(key) + " is null");
            }
        }
    }
}
