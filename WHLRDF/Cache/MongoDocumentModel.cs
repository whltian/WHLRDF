﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Cache
{
    /// <summary>
    /// mongo 实体
    /// </summary>
    public class MongoDocumentModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
