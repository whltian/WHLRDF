﻿
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace WHLRDF.Cache
{
    public class MemoryService : BaseCacheService, ICacheService,IDisposable
    {
        public int CacheTimeout { get=> ApplicationEnvironments.Site.SessionTimeout; }
        private readonly static object locked = new object();
        protected IMemoryCache _cache;
        private static MemoryService _instance;
        public static MemoryService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (locked)
                    {
                        if (_instance == null)
                        {
                            //new MemoryCache(Options.Create(new MemoryCacheOptions()))
                            _instance = new MemoryService();
                        }
                    }
                   
                }
                return _instance;
            }
        }
        public TimeSpan ExpireTime
        {
            get
            {
                return TimeSpan.FromMinutes(CacheTimeout);
            }
        }
        /// <summary>
        /// 是否注册session缓存
        /// </summary>
        /// <param name="isSession">是否注册session缓存</param>
        public MemoryService()
        {
            _cache = new MemoryCache(Options.Create(new MemoryCacheOptions()));
            
        }
       
        #region add
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <returns></returns>
        public bool Add(string key, object value)
        {
           
            this.Add(key, value, ExpireTime);
            return true;
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresSliding">滑动过期时长（如果在过期时间内有操作，则以当前时间点延长过期时间）</param>
        /// <param name="expiressAbsoulte">绝对过期时长</param>
        /// <returns></returns>
        public bool Add(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (value == null)
            {
                return true;
            }
            _cache.Set(key,value,
                    new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(expiresSliding)
                    .SetAbsoluteExpiration(expiressAbsoulte)
                    );

            return true;
        }
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresIn">缓存时长</param>
        /// <param name="isSliding">是否滑动过期（如果在过期时间内有操作，则以当前时间点延长过期时间）</param>
        /// <returns></returns>
        public bool Add(string key, object value, TimeSpan expiresIn, bool isSliding = false)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            //if (value == null)
            //{

            //}
            if (value == null)
            {
                return true;
            }
            if (isSliding)
                _cache.Set(key, value,
                    new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(expiresIn)
                    );
            else
                _cache.Set(key, value,
                new MemoryCacheEntryOptions()
                .SetAbsoluteExpiration(expiresIn)
                );

            return true;
        }

        public Task<bool> AddAsync(string key, object value)
        {
            return Task.Run<bool>(() => { return this.Add(key, value); });
        }

        public Task<bool> AddAsync(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte)
        {
            return Task.Run<bool>(() => { return this.Add(key, value, expiresSliding, expiressAbsoulte); });
        }

        public Task<bool> AddAsync(string key, object value, TimeSpan expiresIn, bool isSliding = false)
        {
            return Task.Run<bool>(() => { return this.Add(key, value, expiresIn, isSliding); });
        }
        #endregion

        #region 验证是否存在
        /// <summary>
        /// 验证缓存项是否存在
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            object cached;
            return _cache.TryGetValue(key, out cached);
        }

        public Task<bool> ExistsAsync(string key)
        {
            //if (key == null)
            //{
            //    throw new ArgumentNullException(nameof(key));
            //}
            //object cached;
            // bool flag=_cache.TryGetValue(key, out cached);
            return Task.Run<bool>(() => { return this.Exists(key); });
        }
        #endregion

        #region Get
        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public T GetCache<T>(string key) where T : class, new()
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            object value = _cache.Get(key);
         
            return  (T)_cache.Get(key);
        }

        public T GetOrCreate<T>(string key, Func<T> acquire) where T : class, new()
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (this.Exists(key))
            {
                return (T)_cache.Get(key);
            }
            var result = acquire();
            this.Add(key, result);
            return result;
          
        }

        public T GetOrCreate<T>(string key, Func<T> acquire, bool isSliding) where T : class, new()
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (this.Exists(key))
            {
                return (T)_cache.Get(key);
            }
            var result = acquire();
            if (result == null)
            {
                return result;
            }
            this.Add(key, result,TimeSpan.FromMinutes(this.CacheTimeout),true);
            return result;

        }
        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public object GetCache(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var value= _cache.Get(key);
            //if (value == null)
            //{
            //    return null;
            //}
            return value;
        }
        /// <summary>
        /// 获取缓存集合
        /// </summary>
        /// <param name="keys">缓存Key集合</param>
        /// <returns></returns>
        public IDictionary<string, object> GetAll(List<string> keys)
        {
            if (keys == null)
            {
                throw new ArgumentNullException(nameof(keys));
            }

            var dict = new Dictionary<string, object>();

            keys.ForEach(item => dict.Add(item, _cache.Get(item)));

            return dict;
        }

        public Task<IDictionary<string, object>> GetAllAsync(List<string> keys)
        {
            return Task.Run<IDictionary<string, object>>(() => { return this.GetAll(keys); });
        }

        public Task<T> GetCacheAsync<T>(string key) where T : class, new()
        {
            return Task.Run<T>(()=> { return this.GetCache<T>(key); });
        }
        public Task<T> GetCacheAsync<T>(string key, Func<T> acquire) where T : class, new()
        {
            return Task.Run<T>(() => { return this.GetOrCreate<T>(key, acquire); });
        }
        public Task<object> GetCacheAsync(string key)
        {
            return Task.Run(() => { return this.GetCache(key); });
        }
        #endregion

        #region Remove
        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public bool RemoveCache(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            _cache.Remove(key);

            return !Exists(key);
        }
        /// <summary>
        /// 批量删除缓存
        /// </summary>
        /// <param name="key">缓存Key集合</param>
        /// <returns></returns>
        public void RemoveAll(List<string> keys)
        {
            if (keys == null)
            {
                throw new ArgumentNullException(nameof(keys));
            }

            keys.ForEach(item => _cache.Remove(item));
        }

        public Task RemoveAllAsync(List<string> keys)
        {
            return Task.Run(() => { this.RemoveAll(keys); });
        }

        public Task<bool> RemoveCacheAsync(string key)
        {
            return Task.Run<bool>(() => {return this.RemoveCache(key); });
        }
        #endregion


        /// <summary>
        /// 获取所有缓存键
        /// </summary>
        /// <returns></returns>
        public List<string> GetCacheKeys()
        {
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            var entries = _cache.GetType().GetField("_entries", flags).GetValue(_cache);
            var cacheItems = entries as  IDictionary;
            var keys = new List<string>();
            if (cacheItems == null) return keys;
          
            foreach (DictionaryEntry cacheItem in cacheItems)
            {
                keys.Add(cacheItem.Key.ToString());
            }
            return keys;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        public bool Clear()
        {
            const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            var entries = _cache.GetType().GetField("_entries", flags).GetValue(_cache);
            var cacheItems = entries as IDictionary;
            var keys = new List<string>();
            cacheItems.Clear();
            return true;
        }

        public void Dispose()
        {
            if (_cache != null)
                _cache.Dispose();
            GC.SuppressFinalize(this);
        }

        #region IDistributedCache
        public byte[] Get(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            var value = _cache.Get(key);
            if (value == null)
            {
                return null;
            }
            return (byte[])value;
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default(CancellationToken))
        {
           return Task.Run<byte[]>(()=> {
               
                return this.Get(key);
            });
        }
        public void Set(string key, byte[] value)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            _cache.Set(key, value, this.ExpireTime);
        }
        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            _cache.Set(key, value,this.ExpireTime);
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default(CancellationToken))
        {
            return Task.Run(() => {
            
                this.Set(key, value, options);
            });
        }

        public void Refresh(string key)
        {
            if (Exists(key))
            {
               var options= new MemoryCacheEntryOptions()
                   .SetSlidingExpiration(this.ExpireTime);
                this.Set(key,Get(key));
            }
        }

        public Task RefreshAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return Task.Run(() => {
                this.Add(key, GetCache(key));
            });
        }

        public void Remove(string key)
        {
            this.SessionRemove(key);
        }

        public Task RemoveAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return Task.Run(() => {
                this.SessionRemove(key);
            });
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public bool SessionRemove(string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                _cache.Remove(key);
            }
           
            return true;
        }

        public void HSet<T>(string tableName, string key, T value) where T : class, new()
        {
            this.Add(tableName+"-"+key,value);
        }

        public void HSetAsync<T>(string tableName, string key, T value) where T : class, new()
        {
            this.AddAsync(tableName + "-" + key, value);
        }
        public T HGet<T>(string tableName, string key) where T : class, new()
        {
            return this.GetCache<T>(tableName + "-" + key);
        }

        public Task<T> HGetAsync<T>(string tableName, string key) where T : class, new()
        {
           return this.GetCacheAsync<T>(tableName + "-" + key);
        }

        public T HGetOrCreate<T>(string tableName, string key, Func<T> acquire) where T : class, new()
        {
            return this.GetOrCreate<T>(tableName + "-" + key,acquire);
        }

        public Task<T> HGetOrCreateAsync<T>(string tableName, string key, Func<T> acquire) where T : class, new()
        {
            return this.GetCacheAsync<T>(tableName + "-" + key, acquire);
        }

        public bool HRemove<T>(string tableName, params string[] keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                keys[i] = tableName + "-" + keys[i];
            }
            this.RemoveAll( keys.ToList());
            return true;
        }

        public bool HRemoveAsync<T>(string tableName, params string[] keys)
        {
             Task.Run<bool>(() => { return this.HRemove<T>(tableName,keys); });
            return true;
        }
        #endregion
    }
}
