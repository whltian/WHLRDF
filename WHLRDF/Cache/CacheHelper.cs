﻿
using WHLRDF.Cache;

namespace WHLRDF
{
    /// <summary>
    /// 缓存枚举
    /// </summary>
    public enum CacheType { 
        Redis=1,
        Memory=2,
        Mongo=3
    }
    /// <summary>
    /// 缓存帮助类
    /// </summary>
    public class CacheHelper
    {
        private static ICacheService _cacheService;
        private static object _locker = new object();
        public static ICacheService CacheService
        {
            get
            {
                if (_cacheService == null)
                {
                    lock (_locker)
                    {
                        if (ApplicationEnvironments.IsWeb)
                        {
                            _cacheService = AppHttpContext.GetSerivce<ICacheService>();
                        }
                        if (_cacheService == null)
                        {
                            if (ApplicationEnvironments.Site.CacheType==CacheType.Redis)
                            {
                                _cacheService = RedisService.Instance;

                            }
                            else
                            {
                                _cacheService = MemoryService.Instance;
                            }
                        }
                    }
                }
                return _cacheService;
            }
            set
            {
                _cacheService = value;
            }
        }

    }
}
