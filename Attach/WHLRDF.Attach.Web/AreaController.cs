﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;

namespace WHLRDF.Attach.Web
{

    [MenuPage(1100, "500", "附件管理", "Attach", "fa-folder")]
    [Area("Attach")]
    [Route("Attach/{controller=Home}/{action=Index}/{id?}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "Attach";
            }
        }
    }
}
