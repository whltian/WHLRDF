﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.Attach.BLL;
using WHLRDF.Attach.Model;
using WHLRDF.ORM;
namespace WHLRDF.Attach.Web.Controllers
{
    [MenuPage(30, "030", "文件管理", "Attach/MFile", "")]
    public class MFileController : AreaController
    {
        #region Service注入

        public IAttachService mService { get; set; }
        public IApiService iService { get; set; }
        [Permission]
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region  扩展
        [Permission(IsParent = true)]
        public IActionResult GetFolders(string parent)
        {
            return Json(AjaxResult.Success(iService.GetFolders(parent)));
        }
        [Permission(IsParent =true)]
        public IActionResult GetFiles(string filePath)
        {
            return Json(AjaxResult.Success(iService.GetFiles(filePath)));
        }
        [Permission("02","文件删除")]
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public IActionResult FileDelete(string filePath)
        {
            if (iService.FileDelete(filePath))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error("文件不存在"));
        }
        [HttpPost]
        [Permission(ActionName = "FileDelete")]
        /// <summary>
        /// 文件夹删除
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public IActionResult FolderDelete(string folder)
        {
            string strError = "";
            if (iService.FolderDelete(folder, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
    }
}
