﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WHLRDF.Attach.BLL;

namespace WHLRDF.Attach.Web.Controllers
{
    [Authorize]
    public class ApiController : AreaController
    {
        public IApiService apiService { get; set; }

        public IActionResult Summernote(IFormFile file,string fileName,string typeno)
        {
            return Json(apiService.Upload(file, fileName, typeno));
        }

        public IActionResult Avatar(IFormFile file, string fileName, string typeno)
        {
            return Json(apiService.Upload(file, fileName, typeno));
        }
        
        public ActionResult KindUpload(string typeno, string userno, string token)
        {
          //  var result = apiService.Upload(ApplicationEnvironments.Current.Request.Files[0]);
            return Json(AjaxResult.Success());
        }
        public IActionResult GetFolders(string parent)
        {
            return Json(AjaxResult.Success(apiService.GetFolders(parent)));
        }

        public IActionResult GetFiles(string filePath)
        {
            return Json(AjaxResult.Success(apiService.GetFiles(filePath)));
        }
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public IActionResult FileDelete(string filePath)
        {
            if (apiService.FileDelete(filePath))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error("文件不存在"));
        }

        
    }
}
