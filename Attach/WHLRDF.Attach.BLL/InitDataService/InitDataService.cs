﻿using WHLRDF.Attach.Model;
using WHLRDF.ORM;
using System;
using System.Collections.Generic;
namespace WHLRDF.Attach.BLL
{

    public class InitializeService : SerivceBase ,IInitializeService{

        #region 私有变量
        private List<AttachTypeEntity> attachTypeEntities = null;
    
  
        #endregion
        public bool Read(IDbRepository dbRepository, ref Dictionary<string, object> dicResult, ref string strError)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            attachTypeEntities = dbRepository.Query<AttachTypeEntity>(criter);
            dicResult.Add(nameof(attachTypeEntities), attachTypeEntities);
          
            return true;
        }

        public bool Write(IDbRepository dbRepository, Dictionary<string, object> dicResult, ref string strError)
        {
            try
            {
                if (dicResult.ContainsKey(nameof(attachTypeEntities)) && dicResult[nameof(attachTypeEntities)] != null)
                {
                    attachTypeEntities = JSONHelper.FromJson<List<AttachTypeEntity>>(dicResult[nameof(attachTypeEntities)].ToJson());
                    if (attachTypeEntities != null && attachTypeEntities.Count > 0)
                    {
                        attachTypeEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }

            if (attachTypeEntities != null && attachTypeEntities.Count > 0)
            {
                dbRepository.BatchInsert<AttachTypeEntity>(attachTypeEntities, ref strError);
            }
            return true;
        }
    }
}
