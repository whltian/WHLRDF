﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Attach.BLL
{
    public class StaticFile
    {
        public string id { get; set; }

        public string text { get; set; }

        public string parent { get; set; }

        public string icon { get; set; }

        public bool children { get; set; }

        public DateTime EditTime { get; set; }

        public string FileType { get; set; }

        public bool IsImage { get; set; }

        public string HttpPath { get; set; }
    }
}
