﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace WHLRDF.Attach.BLL
{
    public interface IApiService
    {
        AjaxResult Upload(IFormFile file,string fileName, string typename);

        /// <summary>
        /// 获取头像
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        byte[] GetUserAvatar(string userid);

        /// <summary>
        /// 获取文件夹
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        List<StaticFile> GetFolders(string parent);
        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        List<StaticFile> GetFiles(string filePath);

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        bool FileDelete(string filePath);

        /// <summary>
        /// 删除文件夹
        /// </summary>
        /// <param name="folder">文件夹</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool FolderDelete(string folder, ref string strError);
    }
}
