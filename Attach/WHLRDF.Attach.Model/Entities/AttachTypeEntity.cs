﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Attach.Model
{
    /// <summary>
    /// 附件类别
    /// </summary>
    [Table("COMM_AttachType", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class AttachTypeEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public AttachTypeEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_AttachType";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "AttachTypeId";

        /// <summary>
        ///  类别名称
        /// </summary>
        public const string __AttachTypeName = "AttachTypeName";

        /// <summary>
        ///  文件夹名称
        /// </summary>
        public const string __DirName = "DirName";

        /// <summary>
        ///  启用用户
        /// </summary>
        public const string __IsUserNo = "IsUserNo";

        /// <summary>
        ///  类别限制扩展
        /// </summary>
        public const string __FileTypeExts = "FileTypeExts";

        /// <summary>
        /// 临时存储
        /// </summary>
        public const string __IsTemp = "IsTemp";

        /// <summary>
        ///  限制大小
        /// </summary>
        public const string __MaxSize = "MaxSize";

        /// <summary>
        ///  接口
        /// </summary>
        public const string __ThumbnailDLL = "ThumbnailDLL";

        #endregion
        #region 属性

        private string _AttachTypeId;
        ///<summary>
        ///  类别编号
        /// </summary>
        [DbColumn("AttachTypeId", IsPrimaryKey = true, LocalName = " 类别编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string AttachTypeId
        {
            get
            {
                return _AttachTypeId;
            }
            set
            {
                _AttachTypeId = value;
            }
        }


        private string _AttachTypeName;
        ///<summary>
        ///  类别名称
        /// </summary>
        [DbColumn("AttachTypeName", LocalName = " 类别名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 20, Min = 0)]
        public virtual string AttachTypeName
        {
            get
            {
                return _AttachTypeName;
            }
            set
            {
                SetData(__AttachTypeName,value);
                _AttachTypeName = value;
            }
        }


        private string _DirName;
        ///<summary>
        ///  文件夹名称
        /// </summary>
        [DbColumn("DirName", LocalName = " 文件夹名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DirName
        {
            get
            {
                return _DirName;
            }
            set
            {
                SetData(__DirName,value);
                _DirName = value;
            }
        }


        private bool _IsUserNo;
        ///<summary>
        ///  启用用户
        /// </summary>
        [DbColumn("IsUserNo", LocalName = " 启用用户", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsUserNo
        {
            get
            {
                return _IsUserNo;
            }
            set
            {
                SetData(__IsUserNo,value);
                _IsUserNo = value;
            }
        }


        private string _FileTypeExts;
        ///<summary>
        ///  类别限制扩展
        /// </summary>
        [DbColumn("FileTypeExts", LocalName = " 类别限制扩展", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 300, Min = 0)]
        public virtual string FileTypeExts
        {
            get
            {
                return _FileTypeExts;
            }
            set
            {
                SetData(__FileTypeExts,value);
                _FileTypeExts = value;
            }
        }


        private bool _IsTemp;
        ///<summary>
        /// 临时存储
        /// </summary>
        [DbColumn("IsTemp", LocalName = "临时存储", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsTemp
        {
            get
            {
                return _IsTemp;
            }
            set
            {
                SetData(__IsTemp,value);
                _IsTemp = value;
            }
        }


        private int _MaxSize;
        ///<summary>
        ///  限制大小
        /// </summary>
        [DbColumn("MaxSize", LocalName = " 限制大小", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int MaxSize
        {
            get
            {
                return _MaxSize;
            }
            set
            {
                SetData(__MaxSize,value);
                _MaxSize = value;
            }
        }


        private string _ThumbnailDLL;
        ///<summary>
        ///  接口
        /// </summary>
        [DbColumn("ThumbnailDLL", LocalName = " 接口", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ThumbnailDLL
        {
            get
            {
                return _ThumbnailDLL;
            }
            set
            {
                SetData(__ThumbnailDLL,value);
                _ThumbnailDLL = value;
            }
        }


        #endregion
    }
}