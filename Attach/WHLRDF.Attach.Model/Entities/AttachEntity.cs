﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Attach.Model
{
    /// <summary>
    /// 附件
    /// </summary>
    [Table("COMM_Attach", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class AttachEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public AttachEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Attach";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "AttachId";

        /// <summary>
        ///  类别编号
        /// </summary>
        public const string __AttachTypeId = "AttachTypeId";

        /// <summary>
        ///  文件名
        /// </summary>
        public const string __FileName = "FileName";

        /// <summary>
        ///  后缀
        /// </summary>
        public const string __AttachSuffix = "AttachSuffix";

        /// <summary>
        ///  路径
        /// </summary>
        public const string __AttachPath = "AttachPath";

        /// <summary>
        ///  大小
        /// </summary>
        public const string __AttachSize = "AttachSize";

        /// <summary>
        ///  表单
        /// </summary>
        public const string __FormId = "FormId";

        #endregion
        #region 属性

        private string _AttachId;
        ///<summary>
        ///  附件编号
        /// </summary>
        [DbColumn("AttachId", IsPrimaryKey = true, LocalName = " 附件编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string AttachId
        {
            get
            {
                return _AttachId;
            }
            set
            {
                _AttachId = value;
            }
        }


        private string _AttachTypeId;
        ///<summary>
        ///  类别编号
        /// </summary>
        [DbColumn("AttachTypeId", LocalName = " 类别编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 3, MaxLength = 10, Min = 0)]
        public virtual string AttachTypeId
        {
            get
            {
                return _AttachTypeId;
            }
            set
            {
                SetData(__AttachTypeId,value);
                _AttachTypeId = value;
            }
        }


        private string _FileName;
        ///<summary>
        ///  文件名
        /// </summary>
        [DbColumn("FileName", LocalName = " 文件名", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string FileName
        {
            get
            {
                return _FileName;
            }
            set
            {
                SetData(__FileName,value);
                _FileName = value;
            }
        }


        private string _AttachSuffix;
        ///<summary>
        ///  后缀
        /// </summary>
        [DbColumn("AttachSuffix", LocalName = " 后缀", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 10, Min = 0)]
        public virtual string AttachSuffix
        {
            get
            {
                return _AttachSuffix;
            }
            set
            {
                SetData(__AttachSuffix,value);
                _AttachSuffix = value;
            }
        }


        private string _AttachPath;
        ///<summary>
        ///  路径
        /// </summary>
        [DbColumn("AttachPath", LocalName = " 路径", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 300, Min = 0)]
        public virtual string AttachPath
        {
            get
            {
                return _AttachPath;
            }
            set
            {
                SetData(__AttachPath,value);
                _AttachPath = value;
            }
        }


        private int _AttachSize;
        ///<summary>
        ///  大小
        /// </summary>
        [DbColumn("AttachSize", LocalName = " 大小", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int AttachSize
        {
            get
            {
                return _AttachSize;
            }
            set
            {
                SetData(__AttachSize,value);
                _AttachSize = value;
            }
        }


        private string _FormId;
        ///<summary>
        ///  表单
        /// </summary>
        [DbColumn("FormId", LocalName = " 表单", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FormId
        {
            get
            {
                return _FormId;
            }
            set
            {
                SetData(__FormId,value);
                _FormId = value;
            }
        }


        #endregion
    }
}