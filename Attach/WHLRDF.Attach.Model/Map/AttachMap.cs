﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Attach.Model
{
    public partial class AttachMap : ClassMap<AttachEntity>
    {
        public override void Configure(EntityTypeBuilder<AttachEntity>
            builder)
        {
            builder.ToTable<AttachEntity>("COMM_Attach");
            

            /// <summary>
            ///  附件编号
            /// </summary>
            builder.HasKey(x => x.AttachId);

            builder.Property(x => x.AttachId).HasColumnName("AttachId").HasMaxLength(50);

            /// <summary>
            ///  类别编号
            /// </summary>
            builder.Property(x => x.AttachTypeId).HasColumnName("AttachTypeId").HasMaxLength(10).IsRequired(true);

            /// <summary>
            ///  文件名
            /// </summary>
            builder.Property(x => x.FileName).HasColumnName("FileName").HasMaxLength(100).IsRequired(false);

            /// <summary>
            ///  后缀
            /// </summary>
            builder.Property(x => x.AttachSuffix).HasColumnName("AttachSuffix").HasMaxLength(10).IsRequired(true);

            /// <summary>
            ///  路径
            /// </summary>
            builder.Property(x => x.AttachPath).HasColumnName("AttachPath").HasMaxLength(300).IsRequired(false);

            /// <summary>
            ///  大小
            /// </summary>
            builder.Property(x => x.AttachSize).HasColumnName("AttachSize");

            /// <summary>
            ///  表单
            /// </summary>
            builder.Property(x => x.FormId).HasColumnName("FormId").HasMaxLength(50).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}