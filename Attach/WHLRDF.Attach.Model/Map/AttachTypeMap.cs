﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Attach.Model
{
    public partial class AttachTypeMap : ClassMap<AttachTypeEntity>
    {
        public override void Configure(EntityTypeBuilder<AttachTypeEntity>
            builder)
        {
            builder.ToTable<AttachTypeEntity>("COMM_AttachType");
           

            /// <summary>
            ///  类别编号
            /// </summary>
            builder.HasKey(x => x.AttachTypeId);

            builder.Property(x => x.AttachTypeId).HasColumnName("AttachTypeId").HasMaxLength(50);

            /// <summary>
            ///  类别名称
            /// </summary>
            builder.Property(x => x.AttachTypeName).HasColumnName("AttachTypeName").HasMaxLength(20).IsRequired(false);

            /// <summary>
            ///  文件夹名称
            /// </summary>
            builder.Property(x => x.DirName).HasColumnName("DirName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  启用用户
            /// </summary>
            builder.Property(x => x.IsUserNo).HasColumnName("IsUserNo");

            /// <summary>
            ///  类别限制扩展
            /// </summary>
            builder.Property(x => x.FileTypeExts).HasColumnName("FileTypeExts").HasMaxLength(300).IsRequired(false);

            /// <summary>
            /// 临时存储
            /// </summary>
            builder.Property(x => x.IsTemp).HasColumnName("IsTemp");

            /// <summary>
            ///  限制大小
            /// </summary>
            builder.Property(x => x.MaxSize).HasColumnName("MaxSize");

            /// <summary>
            ///  接口
            /// </summary>
            builder.Property(x => x.ThumbnailDLL).HasColumnName("ThumbnailDLL").HasMaxLength(50).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}