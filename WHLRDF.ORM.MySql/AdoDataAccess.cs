﻿
using System;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Text;
using WHLRDF.ORM.Ado;

namespace WHLRDF.ORM.MySql
{
   
    public class AdoDataAccess : AbstractDatabase, IDataAccess
    {
        public MySqlConnection  connection   = new MySqlConnection();
        public MySqlCommand     command      = new MySqlCommand();
        public MySqlDataAdapter dataAdapter  = new MySqlDataAdapter();
        public MySqlTransaction transaction;
        public override DbProviderFactory DbFactory
        {
            get
            {
                return MySqlClientFactory.Instance;
            }
        }
        //public override DbProviderFactory GetInstance()
        //{
        //    return MySqlClientFactory.Instance;
        //}
       
        /// <summary>
        /// 当前数据库类型
        /// </summary>
        public override ProviderType DbProviderType
        {
            get
            {
                return ProviderType.MySql;
            }
        }

        #region public MySql()
        /// <summary>
        /// 构造方法
        /// </summary>
        public AdoDataAccess():base()
		{
           
		}
        public AdoDataAccess(string connstring,string version) : base(connstring,version)
        {

        }
        #endregion

        #region public MySql(string connectionString)
        /// <summary>
        /// 设定软件名称
        /// </summary>
        /// <param name="connectionString">数据连接</param>
        public AdoDataAccess(string connectionString) : this()
        {
            this.ConnectionString = connectionString;
		}
		#endregion

        #region public string SqlSafe(string value) 检查参数的安全性
        /// <summary>
        /// 检查参数的安全性
        /// </summary>
        /// <param name="value">参数</param>
        /// <returns>安全的参数</returns>
        public string SqlSafe(string value)
        {
          
            return DataAccess.Instance.SqlSafe(value);
        }
        #endregion

        #region public DbParameter MakeInParam(string targetFiled, object targetValue)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数</returns>
        public DbParameter MakeInParam(string targetFiled, object targetValue)
        {
            return DataAccess.Instance.MakeInParam(targetFiled, targetValue);
        }
        #endregion

        #region public DbParameter[] MakeParameters(string targetFiled, object targetValue)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数集</returns>
        public DbParameter[] MakeParameters(string targetFiled, object targetValue)
        {
            return DataAccess.Instance.MakeParameters(targetFiled, targetValue);
        }
        #endregion

        #region public DbParameter[] MakeParameters(string[] targetFileds, Object[] targetValues)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数集</returns>
        public DbParameter[] MakeParameters(string[] targetFileds, Object[] targetValues)
        {
            return DataAccess.Instance.MakeParameters(targetFileds, targetValues);
        }
        #endregion

        public override DbParameter MakeOutParam(string paramName, DbType DbType, int Size)
        {
            return DataAccess.Instance.MakeParam(paramName, DbType, Size, ParameterDirection.Output,null);
        }

        public override DbParameter MakeInParam(string paramName, DbType DbType, int Size, object Value)
        {
            return DataAccess.Instance.MakeParam(paramName, DbType, Size, ParameterDirection.Input, Value);
        }

        public override DbParameter MakeParam(string paramName, DbType DbType, Int32 Size, ParameterDirection Direction, object Value)
        {
            return DataAccess.Instance.MakeParam(paramName, DbType, Size, Direction, Value);
        }
       

        #region public string GetParameter(string parameter) 获得参数Sql表达式
        /// <summary>
        /// 获得参数Sql表达式
        /// </summary>
        /// <param name="parameter">参数名称</param>
        /// <returns>字符串</returns>
        public override string GetParameter(string parameter)
        {
            return DataAccess.Instance.GetParameter(parameter);
        }

        public override string GetLikeParameter(string parameter)
        {
            return DataAccess.Instance.GetLikeParameter(parameter);
        }
        #endregion

        #region string PlusSign(params string[] values)
        /// <summary>
        ///  获得Sql字符串相加符号
        /// </summary>
        /// <param name="values">参数值</param>
        /// <returns>字符加</returns>
        public string PlusSign(params string[] values)
        {
            return DataAccess.Instance.PlusSign(values);
        }
        #endregion

        #region
        public override DbConnection GetConnection(string connectionString)
        {
            return DataAccess.Instance.GetConnection(connectionString);
        }

        public override DbDataAdapter GetDataAdapter()
        {
            return DataAccess.Instance.GetDataAdapter();
        }

        public override string Now()
        {
            return DataAccess.Instance.Now();
        }

        public override DbParameter[] GetParameter(List<DataParameter> parameters)
        {
            return DataAccess.Instance.GetParameter(parameters);
        }

        public override DbParameter GetParameter(string name, object value)
        {
            return DataAccess.Instance.GetParameter(name, value);
        }
        public override string Identity(string primarykeyName)
        {
            return DataAccess.Instance.Identity(primarykeyName);
        }
        #endregion

        public override void SqlBulkCopyData(DataTable dataTable)
        {
            DataAccess.Instance.SqlBulkCopyData(dataTable);
        }
        /// <summary>
        /// 禁用自增
        /// </summary>
        /// <returns></returns>
        public override string Identity_Disabled(string tableName)
        {
           return DataAccess.Instance.Identity_Disabled(tableName);
        }
        /// <summary>
        /// 启用自增
        /// </summary>
        /// <returns></returns>
        public override string Identity_Enable(string tableName)
        {
            return DataAccess.Instance.Identity_Enable(tableName);
        }

        public override string QueryPage(string hql, Order[] orders,
            int pageIndex, int pageSize)
        {
            return DataAccess.Instance.QueryPage(hql, orders, pageIndex, pageSize);
        }
      

        public override string RecordCount(string hql, bool createSql = true)
        {
            return DataAccess.Instance.RecordCount(hql, createSql);
        }
        /// 获取统计查询总数Sql
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        public override string GetRecordCountSql(string tableName)
        {
            return DataAccess.Instance.GetRecordCountSql(tableName);
        }
        /// <summary>
        /// 获取查询表Sql
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="queryFieldBuilder"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public override string GetSelectSql(string tableName, string queryFieldBuilder, bool isDeleted)
        {
            return DataAccess.Instance.GetSelectSql(tableName, queryFieldBuilder, isDeleted);
        }

        #region idbtype

        public override string GetTable(string dbName)
        {
            return DataAccess.Instance.GetTableOrView(dbName,"U");
        }

        public override string GetView(string dbName)
        {
            return DataAccess.Instance.GetTableOrView(dbName,"V");
        }

        public override string GetDbColumn(string tableName, string dbServerName, bool IsTable)
        {
            return DataAccess.Instance.GetDbColumn(tableName, dbServerName);
        }
        public override string GetSource(string tableName)
        {
            return DataAccess.Instance.GetSource(tableName);
        }


        public override string CreateTable(string tableName, List<DbColumnAttribute> dbColumns)
        {
            return DataAccess.Instance.CreateTable(tableName, dbColumns);
        }

        public override string AddColumn(string tableName, DbColumnAttribute columnItem)
        {
            return DataAccess.Instance.AddColumn(tableName, columnItem);
        }

        public override string DeleteTable(string tableName)
        {
            return DataAccess.Instance.DeleteTable(tableName);
        }

        public override string DeleteColumn(string tableName, string columnName)
        {
            return DataAccess.Instance.DeleteColumn(tableName, columnName);
        }
        /// <summary>
        /// 更改字段描述
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="description"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        public override string UpdateDescriptionSql(string tableName, string columnName, string description, bool isAdd)
        {
            return DataAccess.Instance.UpdateDescriptionSql(tableName, columnName, description, isAdd);
        }

        /// <summary>
        /// 判断描述是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">字段名</param>
        /// <returns></returns>
        public override string ExistDescriptionSql(string tableName, string columnName)
        {
            return DataAccess.Instance.ExistDescriptionSql(tableName, columnName);
        }
        #endregion
    }
}