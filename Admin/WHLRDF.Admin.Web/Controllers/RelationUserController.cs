﻿
using Microsoft.AspNetCore.Mvc;
using WHLRDF;
using WHLRDF.Application.BLL;
using WHLRDF.Application.Model;
using WHLRDF.ORM;


namespace WHLRDF.Admin.Web.Controllers
{
    [MenuPage(81,"081", "用户关系管理", "Admin/RelationUser", "",false,false)]
    public class RelationUserController : AreaController
    {
        #region Service注入

        public IRelationUserService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent =true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
		[Permission("01","编辑")]
       
        public IActionResult Edit(string id, RelationUserEntity entity)
        {
            if (this.IsPost)
            {
              
                string strError = "";
               
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                }
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
		[HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion

        #region 
        [HttpPost]
        [Permission("05","用户关系设置")]
        public IActionResult RelationUserSave(string userid, string keys, int relationType=0)
        {
            string strError = "";
            if (mService.RelationUserSave(userid, keys, ref strError, (RelationUserType)relationType))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        /// <summary>
        /// 获取关联用户
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <param name="resourceId"></param>
        /// <param name="relationType"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission("06", "获取用户关系",IsParent =true)]
        public IActionResult ForGridByResourceId(LigerGrid ligerGrid,string resourceId,int relationType=0)
        {
            return Json(AjaxResult.Success(mService.ForGridByResourceId(ligerGrid, resourceId, relationType)));
        }

        /// <summary>
        /// 删除关联用户
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="itemKeys"></param>
        /// <param name="relationType"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission("", "删除用户关系",ActionName ="Delete")]
        public JsonResult RemoveRlUser(string resourceId, string itemKeys, int relationType = 0)
        {
            string strError = "";
            if (mService.RemoveRlUserSave(resourceId, itemKeys, ref strError, (RelationUserType)relationType))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        /// <summary>
        /// 添加关系用户
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="itemKeys"></param>
        /// <param name="relationType"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission("", "添加用户关系",ActionName ="Edit")]
        public JsonResult AddRlUser(string resourceId, string itemKeys, int relationType = 0)
        {
            string strError = "";
            if (mService.AddRlUserSave(resourceId, itemKeys, ref strError, (RelationUserType)relationType))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
    }
}
