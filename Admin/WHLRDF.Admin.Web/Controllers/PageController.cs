﻿

using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;

using System.Collections.Generic;
using WHLRDF.Application.Model;
using WHLRDF.ORM;
namespace WHLRDF.Admin.Web.Controllers
{
    [MenuPage(30, "030", "菜单管理", "Admin/Page", "fa-sitemap")]
    public class PageController : AreaController
    {
        #region Service注入

        public IPageService mService { get; set; }
        public IPermissionService perService{ get; set; }
        #endregion

        #region 模版生成
        [Permission]
        public IActionResult Index()
        {
            return View();
        }
        [Permission(IsParent = true)]
        public IActionResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }
        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01","编辑",IsGet = false)]
     
        public ActionResult Edit(string id, PageEntity entity)
        {
            if (this.IsPost)
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        return RedirectToAction("Index");
                    }
                }
                ModelState.AddModelError(string.Empty, strError);

            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
		[HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        #endregion
        #region
        public IActionResult PageTree(string resourceId, int resourceType=0) {
           
            ViewData["lstItem"] = perService.GetPermissionById(resourceId,(PermissionType) resourceType);
            List<PageEntity> pageEntities = mService.GetAll();
            return View(pageEntities);
        }
     
        [Permission("03", "初始化菜单",IsAdmin =true)]
        /// <summary>
        /// 初始化页面功能 
        /// </summary>
        /// <returns></returns>
        public IActionResult InitPage()
        {
            string strError = "";
            if (mService.InitMenu(ApplicationEnvironments.Site.Components.Plug, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        [HttpPost]
        [Permission(IsParent =true)]
        /// <summary>
        /// 初始化页面功能 
        /// </summary>
        /// <returns></returns>
        public IActionResult GetChildren(string parentid)
        {
                return Json(AjaxResult.Success(mService.GetChildTree(parentid)));
          
        }
        #endregion
    }
}