﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WHLRDF;
using WHLRDF.Application.BLL;
using WHLRDF.Application.Model;
using WHLRDF.ORM;
namespace WHLRDF.Admin.Web.Controllers
{
    [MenuPage(20, "020", "角色管理", "Admin/Role", "fa-user")]
    public class RoleController : AreaController
    {
        #region Service注入

        public IRoleService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent =true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
		[Permission("01", "编辑", IsGet = false)]
    
        public IActionResult Edit(string id, RoleEntity entity)
        {
            if (this.IsPost)
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            return Json(AjaxResult.Success());
                        }
                        else
                        {
                            return RedirectToAction("Index", "role");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, strError);
                    }
                }
                else
                {
                 
                    if (IsAjax)
                    {
                        return Json(AjaxResult.Error(ModelState));
                    }
                }
            }
            //else
            //{
            //    entity.RoleId = id;
            //    entity.Retrieve();
            //}
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
		[HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        [Permission("05", "角色分配")]
        public IActionResult RoleTree(string userid)
        {
            ViewData["UserId"] = userid;
           var lstRole= mService.GetAll();
            return View(lstRole);
        }
        #endregion
    }
}
