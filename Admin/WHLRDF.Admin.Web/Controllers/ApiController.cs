﻿
using Microsoft.AspNetCore.Mvc;

using WHLRDF.Socket;
using WHLRDF.Model;

namespace WHLRDF.Admin.Web.Controllers
{
    public class ApiController : AreaController
    {
        public ISocketService myHubContext { get; set; }
        public IActionResult CacheClear()
        {
            CacheHelper.CacheService.Clear();
            return Ok(AjaxResult.Success("成功"));
        }
       
        public IActionResult SendMessage([FromBody]SocketMessageModel socketModel)
        {
            if (myHubContext.Send(socketModel))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error());
        }
    }
}
