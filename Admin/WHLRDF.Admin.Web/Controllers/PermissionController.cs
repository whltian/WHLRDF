﻿
using Microsoft.AspNetCore.Mvc;
using WHLRDF;
using WHLRDF.Application.BLL;
using WHLRDF.Application.Model;
using WHLRDF.ORM;
namespace WHLRDF.Admin.Web.Controllers
{
    [MenuPage(83,"083","权限管理", "Admin/Permission", "", IsDispay =false)]
    public class PermissionController : AreaController
    {
        #region Service注入

        public IPermissionService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission( NoAccess =true)]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(NoAccess = true,IsParent =true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
		[Permission("01", "编辑", NoAccess = true)]
  
        public IActionResult Edit(string id, PermissionEntity entity)
        {
            if (this.IsPost)
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                }
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
		[HttpPost]
        [Permission("02", "删除",NoAccess =true)]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion

        #region 
        [HttpPost]
        [Permission("05", "权限设置")]
        public JsonResult PermissionSave(string resourceId,int resourceType,string permissions,string unpermissions)
        {
            string strError = "";
            if (mService.PermissionSave(resourceId,permissions, unpermissions, ref strError, (PermissionType)resourceType))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
    }
}
