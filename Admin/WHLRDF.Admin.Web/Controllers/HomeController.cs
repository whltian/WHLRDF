﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;

namespace WHLRDF.Admin.Web.Controllers
{
    public class HomeController : AreaController
    {
        [Permission(NoAccess =true)]
        public IActionResult Index()
        {
            return View();
        }



    }
}
