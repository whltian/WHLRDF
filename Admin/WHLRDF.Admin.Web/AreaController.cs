﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;
namespace WHLRDF.Admin.Web
{

    [MenuPage(1500,"001","系统管理", "Admin","fa-gear")]
    [Area("Admin")]
    [Route("Admin/{controller=Home}/{action=Index}/{id?}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }
    }
}
