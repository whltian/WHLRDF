﻿drop PROCEDURE  IF EXISTS PROC_GetRuleCode;
/*******Mysql********/
CREATE PROCEDURE PROC_GetRuleCode (
	IN RuleId VARCHAR(50),
	IN SerialNo VARCHAR(50),
	IN UserId VARCHAR (50),
	IN InitCount INT,
	OUT NewMaxCount INT
)
BEGIN
	DECLARE
		currentCount INT;
DECLARE
	NewCount INT;
DECLARE
	ErrorCount INT;
DECLARE
	RuleNumberId VARCHAR(50);
DECLARE
	CONTINUE HANDLER FOR SQLEXCEPTION
SET ErrorCount = 1;
SET currentCount = 0;
START TRANSACTION;
SELECT
	 MaxCount,SerialRuleNumberId INTO currentCount,RuleNumberId
FROM
	COMM_SerialRuleNumber
WHERE
	SerialRuleId = RuleId
AND SerialRuleNo = CONCAT(RuleId,SerialNo);
IF currentCount > 0 THEN
SET NewCount = currentCount;
END
IF;
IF NewCount <= 0 THEN

SET NewCount = 1;
ELSE
SET NewCount = NewCount+1;
END
IF;
IF currentCount <= 0 THEN
	INSERT INTO COMM_SerialRuleNumber (
		SerialRuleNumberId,
		SerialRuleNo,
		SerialRuleId,
		MaxCount,
		CreateBy,
		CreateDate,
		LastModifyUserId,
		LastModifyDate,
		IsDeleted
	)
VALUES
	(
		newid(),
		RuleId + SerialNo,
		RuleId,
		NewCount,
		UserId,
		now(),
		UserId,
		now(),
		0
	);
ELSE
	UPDATE COMM_SerialRuleNumber
	SET MaxCount= NewCount,LastModifyUserId= UserId ,LastModifyDate=now()
	WHERE
		SerialRuleId = RuleId
	AND SerialRuleNumberId = RuleNumberId;
END IF;
IF ErrorCount > 0
then
	ROLLBACK;
set
	NewMaxCount=-1;
ELSE
	COMMIT;
set
	NewMaxCount=NewCount;
END IF;

END