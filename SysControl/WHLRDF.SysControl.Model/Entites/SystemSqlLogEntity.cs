﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_SystemSqlLog
    /// </summary>
    [Table("LOG_SystemSqlLog", IsDeleted = false, IsCreated = true, IsLastModify = false, IsTable = true)]
    public partial class SystemSqlLogEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "LOG_SystemSqlLog";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "SystemSQLLogId";

        /// <summary>
        /// 请求id
        /// </summary>
        public const string __TraceIdentifier = "TraceIdentifier";

        /// <summary>
        /// 服务器IP
        /// </summary>
        public const string __ServerIP = "ServerIP";

        /// <summary>
        /// 服务器名称
        /// </summary>
        public const string __ServerName = "ServerName";

        /// <summary>
        /// 请求Url
        /// </summary>
        public const string __MenuPage = "MenuPage";

        /// <summary>
        /// 执行SQL
        /// </summary>
        public const string __CommandText = "CommandText";

        /// <summary>
        /// ORM类别
        /// </summary>
        public const string __SourceCode = "SourceCode";

        /// <summary>
        /// 数据库类型
        /// </summary>
        public const string __ProviderType = "ProviderType";

        /// <summary>
        /// 执行时间
        /// </summary>
        public const string __ExecuteTime = "ExecuteTime";

        /// <summary>
        /// 执行状态
        /// </summary>
        public const string __IsError = "IsError";
        #endregion
        #region 属性
        private string _SystemSQLLogId;
        /// <summary>
        /// 主键
        /// </summary>
        [DbColumn("SystemSQLLogId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string SystemSQLLogId
        {
            get
            {
                return _SystemSQLLogId;
            }
            set
            {
                _SystemSQLLogId = value;

            }
        }
        private string _TraceIdentifier;
        /// <summary>
        /// 请求id
        /// </summary>
        [DbColumn("TraceIdentifier", LocalName = "请求id", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string TraceIdentifier
        {
            get
            {
                return _TraceIdentifier;
            }
            set
            {
                SetData(__TraceIdentifier, value);
                _TraceIdentifier = value;

            }
        }
        private string _ServerIP;
        /// <summary>
        /// 服务器IP
        /// </summary>
        [DbColumn("ServerIP", LocalName = "服务器IP", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerIP
        {
            get
            {
                return _ServerIP;
            }
            set
            {
                SetData(__ServerIP, value);
                _ServerIP = value;

            }
        }
        private string _ServerName;
        /// <summary>
        /// 服务器名称
        /// </summary>
        [DbColumn("ServerName", LocalName = "服务器名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerName
        {
            get
            {
                return _ServerName;
            }
            set
            {
                SetData(__ServerName, value);
                _ServerName = value;

            }
        }
        private string _MenuPage;
        /// <summary>
        /// 请求Url
        /// </summary>
        [DbColumn("MenuPage", LocalName = "请求Url", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1)]
        public virtual string MenuPage
        {
            get
            {
                return _MenuPage;
            }
            set
            {
                SetData(__MenuPage, value);
                _MenuPage = value;

            }
        }
        private string _CommandText;
        /// <summary>
        /// 执行SQL
        /// </summary>
        [DbColumn("CommandText", LocalName = "执行SQL", ColumnType = "string", AllowDBNull = false, ControlType = 1)]
        public virtual string CommandText
        {
            get
            {
                return _CommandText;
            }
            set
            {
                SetData(__CommandText, value);
                _CommandText = value;

            }
        }
        private string _SourceCode;
        /// <summary>
        /// ORM类别
        /// </summary>
        [DbColumn("SourceCode", LocalName = "ORM类别", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string SourceCode
        {
            get
            {
                return _SourceCode;
            }
            set
            {
                SetData(__SourceCode, value);
                _SourceCode = value;

            }
        }
        private string _ProviderType;
        /// <summary>
        /// 数据库类型
        /// </summary>
        [DbColumn("ProviderType", LocalName = "数据库类型", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ProviderType
        {
            get
            {
                return _ProviderType;
            }
            set
            {
                SetData(__ProviderType, value);
                _ProviderType = value;

            }
        }
        private decimal _ExecuteTime;
        /// <summary>
        /// 执行时间
        /// </summary>
        [DbColumn("ExecuteTime", LocalName = "执行时间", ColumnType = "decimal", AllowDBNull = false, VaildKey = "IsDecimal", ControlType = 6)]
        public virtual decimal ExecuteTime
        {
            get
            {
                return _ExecuteTime;
            }
            set
            {
                SetData(__ExecuteTime, value);
                _ExecuteTime = value;

            }
        }
        private bool _IsError;
        /// <summary>
        /// 执行状态
        /// </summary>
        [DbColumn("IsError", LocalName = "执行状态", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsError
        {
            get
            {
                return _IsError;
            }
            set
            {
                SetData(__IsError, value);
                _IsError = value;

            }
        }
        #endregion
    }
}
