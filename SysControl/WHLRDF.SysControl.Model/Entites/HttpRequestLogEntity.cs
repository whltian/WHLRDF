﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_HttpRequestLog
    /// </summary>
    [Table("LOG_HttpRequestLog", IsDeleted = false, IsCreated = true, IsLastModify = false, IsTable = true)]
    public partial class HttpRequestLogEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "LOG_HttpRequestLog";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "HttpRequestLogId";

        /// <summary>
        /// 请求Id
        /// </summary>
        public const string __TraceIdentifier = "TraceIdentifier";

        /// <summary>
        /// 服务器地址
        /// </summary>
        public const string __ServerIP = "ServerIP";

        /// <summary>
        /// 服务器名称
        /// </summary>
        public const string __ServerName = "ServerName";

        /// <summary>
        /// 请求Url
        /// </summary>
        public const string __MenuPage = "MenuPage";

        /// <summary>
        /// 请求平台
        /// </summary>
        public const string __SourcePlatform = "SourcePlatform";

        /// <summary>
        /// 用户
        /// </summary>
        public const string __UserId = "UserId";

        /// <summary>
        /// 用户地址
        /// </summary>
        public const string __UserIpAddress = "UserIpAddress";

        /// <summary>
        /// 请求方式
        /// </summary>
        public const string __HttpMethod = "HttpMethod";

        /// <summary>
        /// IsHttps
        /// </summary>
        public const string __IsHttps = "IsHttps";

        /// <summary>
        /// 上一个请求
        /// </summary>
        public const string __Referer = "Referer";

        /// <summary>
        /// 请求头
        /// </summary>
        public const string __UserAgent = "UserAgent";

        /// <summary>
        /// 开始时间
        /// </summary>
        public const string __StartDate = "StartDate";

        /// <summary>
        /// 结束时间
        /// </summary>
        public const string __EndDate = "EndDate";

        /// <summary>
        /// 执行时间
        /// </summary>
        public const string __ExecuteTime = "ExecuteTime";

        /// <summary>
        /// Get 参数
        /// </summary>
        public const string __Query = "Query";

        /// <summary>
        /// Form参数
        /// </summary>
        public const string __Form = "Form";

        /// <summary>
        /// 请求正文
        /// </summary>
        public const string __body = "body";

        /// <summary>
        /// Ajax请求
        /// </summary>
        public const string __IsAjax = "IsAjax";

        /// <summary>
        /// 编码
        /// </summary>
        public const string __StatusCode = "StatusCode";

        /// <summary>
        /// 返回正文
        /// </summary>
        public const string __ResultBody = "ResultBody";

        /// <summary>
        /// 返回正文类别
        /// </summary>
        public const string __ContentType = "ContentType";

        /// <summary>
        /// 请求是否异常
        /// </summary>
        public const string __IsError = "IsError";
        #endregion
        #region 属性
        private string _HttpRequestLogId;
        /// <summary>
        /// 主键
        /// </summary>
        [DbColumn("HttpRequestLogId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string HttpRequestLogId
        {
            get
            {
                return _HttpRequestLogId;
            }
            set
            {
                _HttpRequestLogId = value;

            }
        }
        private string _TraceIdentifier;
        /// <summary>
        /// 请求Id
        /// </summary>
        [DbColumn("TraceIdentifier", LocalName = "请求Id", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string TraceIdentifier
        {
            get
            {
                return _TraceIdentifier;
            }
            set
            {
                SetData(__TraceIdentifier, value);
                _TraceIdentifier = value;

            }
        }
        private string _ServerIP;
        /// <summary>
        /// 服务器地址
        /// </summary>
        [DbColumn("ServerIP", LocalName = "服务器地址", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerIP
        {
            get
            {
                return _ServerIP;
            }
            set
            {
                SetData(__ServerIP, value);
                _ServerIP = value;

            }
        }
        private string _ServerName;
        /// <summary>
        /// 服务器名称
        /// </summary>
        [DbColumn("ServerName", LocalName = "服务器名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerName
        {
            get
            {
                return _ServerName;
            }
            set
            {
                SetData(__ServerName, value);
                _ServerName = value;

            }
        }
        private string _MenuPage;
        /// <summary>
        /// 请求Url
        /// </summary>
        [DbColumn("MenuPage", LocalName = "请求Url", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1)]
        public virtual string MenuPage
        {
            get
            {
                return _MenuPage;
            }
            set
            {
                SetData(__MenuPage, value);
                _MenuPage = value;

            }
        }
        private int _SourcePlatform;
        /// <summary>
        /// 请求平台
        /// </summary>
        [DbColumn("SourcePlatform", LocalName = "请求平台", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 6)]
        public virtual int SourcePlatform
        {
            get
            {
                return _SourcePlatform;
            }
            set
            {
                SetData(__SourcePlatform, value);
                _SourcePlatform = value;

            }
        }
        private string _UserId;
        /// <summary>
        /// 用户
        /// </summary>
        [DbColumn("UserId", LocalName = "用户", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                SetData(__UserId, value);
                _UserId = value;

            }
        }
        private string _UserIpAddress;
        /// <summary>
        /// 用户地址
        /// </summary>
        [DbColumn("UserIpAddress", LocalName = "用户地址", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string UserIpAddress
        {
            get
            {
                return _UserIpAddress;
            }
            set
            {
                SetData(__UserIpAddress, value);
                _UserIpAddress = value;

            }
        }
        private int _HttpMethod;
        /// <summary>
        /// 请求方式
        /// </summary>
        [DbColumn("HttpMethod", LocalName = "请求方式", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 6)]
        public virtual int HttpMethod
        {
            get
            {
                return _HttpMethod;
            }
            set
            {
                SetData(__HttpMethod, value);
                _HttpMethod = value;

            }
        }
        private bool _IsHttps;
        /// <summary>
        /// IsHttps
        /// </summary>
        [DbColumn("IsHttps", LocalName = "IsHttps", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsHttps
        {
            get
            {
                return _IsHttps;
            }
            set
            {
                SetData(__IsHttps, value);
                _IsHttps = value;

            }
        }
        private string _Referer;
        /// <summary>
        /// 上一个请求
        /// </summary>
        [DbColumn("Referer", LocalName = "上一个请求", ColumnType = "string", MaxLength = 200, ControlType = 1)]
        public virtual string Referer
        {
            get
            {
                return _Referer;
            }
            set
            {
                SetData(__Referer, value);
                _Referer = value;

            }
        }
        private string _UserAgent;
        /// <summary>
        /// 请求头
        /// </summary>
        [DbColumn("UserAgent", LocalName = "请求头", ColumnType = "string", MaxLength = 200, ControlType = 1)]
        public virtual string UserAgent
        {
            get
            {
                return _UserAgent;
            }
            set
            {
                SetData(__UserAgent, value);
                _UserAgent = value;

            }
        }
        private DateTime _StartDate;
        /// <summary>
        /// 开始时间
        /// </summary>
        [DbColumn("StartDate", LocalName = "开始时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                SetData(__StartDate, value);
                _StartDate = value;

            }
        }
        private DateTime _EndDate;
        /// <summary>
        /// 结束时间
        /// </summary>
        [DbColumn("EndDate", LocalName = "结束时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                SetData(__EndDate, value);
                _EndDate = value;

            }
        }
        private decimal _ExecuteTime;
        /// <summary>
        /// 执行时间
        /// </summary>
        [DbColumn("ExecuteTime", LocalName = "执行时间", ColumnType = "decimal", AllowDBNull = false, IsGridVisible = false, VaildKey = "IsDecimal", ControlType = 6)]
        public virtual decimal ExecuteTime
        {
            get
            {
                return _ExecuteTime;
            }
            set
            {
                SetData(__ExecuteTime, value);
                _ExecuteTime = value;

            }
        }
        private string _Query;
        /// <summary>
        /// Get 参数
        /// </summary>
        [DbColumn("Query", LocalName = "Get 参数", ColumnType = "string", IsGridVisible = false, ControlType = 1)]
        public virtual string Query
        {
            get
            {
                return _Query;
            }
            set
            {
                SetData(__Query, value);
                _Query = value;

            }
        }
        private string _Form;
        /// <summary>
        /// Form参数
        /// </summary>
        [DbColumn("Form", LocalName = "Form参数", ColumnType = "string", IsGridVisible = false, ControlType = 1)]
        public virtual string Form
        {
            get
            {
                return _Form;
            }
            set
            {
                SetData(__Form, value);
                _Form = value;

            }
        }
        private string _body;
        /// <summary>
        /// 请求正文
        /// </summary>
        [DbColumn("body", LocalName = "请求正文", ColumnType = "string", IsGridVisible = false, ControlType = 1)]
        public virtual string body
        {
            get
            {
                return _body;
            }
            set
            {
                SetData(__body, value);
                _body = value;

            }
        }
        private bool _IsAjax;
        /// <summary>
        /// Ajax请求
        /// </summary>
        [DbColumn("IsAjax", LocalName = "Ajax请求", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsAjax
        {
            get
            {
                return _IsAjax;
            }
            set
            {
                SetData(__IsAjax, value);
                _IsAjax = value;

            }
        }
        private int _StatusCode;
        /// <summary>
        /// 编码
        /// </summary>
        [DbColumn("StatusCode", LocalName = "编码", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 6)]
        public virtual int StatusCode
        {
            get
            {
                return _StatusCode;
            }
            set
            {
                SetData(__StatusCode, value);
                _StatusCode = value;

            }
        }
        private string _ResultBody;
        /// <summary>
        /// 返回正文
        /// </summary>
        [DbColumn("ResultBody", LocalName = "返回正文", ColumnType = "string", IsGridVisible = false, ControlType = 1)]
        public virtual string ResultBody
        {
            get
            {
                return _ResultBody;
            }
            set
            {
                SetData(__ResultBody, value);
                _ResultBody = value;

            }
        }
        private string _ContentType;
        /// <summary>
        /// 返回正文类别
        /// </summary>
        [DbColumn("ContentType", LocalName = "返回正文类别", ColumnType = "string", MaxLength = 200, ControlType = 1)]
        public virtual string ContentType
        {
            get
            {
                return _ContentType;
            }
            set
            {
                SetData(__ContentType, value);
                _ContentType = value;

            }
        }
        private bool _IsError;
        /// <summary>
        /// 请求是否异常
        /// </summary>
        [DbColumn("IsError", LocalName = "请求是否异常", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsError
        {
            get
            {
                return _IsError;
            }
            set
            {
                SetData(__IsError, value);
                _IsError = value;

            }
        }
        #endregion
    }
}
