﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_ThreadingTask
    /// </summary>
    [Table("LOG_ThreadingTask", IsDeleted = false, IsCreated = true, IsLastModify = false, IsTable = true)]
    public partial class ThreadingTaskEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "LOG_ThreadingTask";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "TaskId";

        /// <summary>
        /// 任务描述
        /// </summary>
        public const string __TaskName = "TaskName";

        /// <summary>
        /// 服务器ip
        /// </summary>
        public const string __ServerIP = "ServerIP";

        /// <summary>
        /// 服务器名称
        /// </summary>
        public const string __ServerName = "ServerName";

        /// <summary>
        /// 用户id
        /// </summary>
        public const string __UserId = "UserId";

        /// <summary>
        /// 启动的源
        /// </summary>
        public const string __StartupSource = "StartupSource";

        /// <summary>
        /// 是否运行
        /// </summary>
        public const string __IsOpen = "IsOpen";

        /// <summary>
        /// 是否发生错误
        /// </summary>
        public const string __IsError = "IsError";

        /// <summary>
        /// 开始时间
        /// </summary>
        public const string __StartDate = "StartDate";

        /// <summary>
        /// 结束时间
        /// </summary>
        public const string __EndDate = "EndDate";

        /// <summary>
        /// 执行时间(毫秒)
        /// </summary>
        public const string __ExecuteTime = "ExecuteTime";

        /// <summary>
        /// 返回结果
        /// </summary>
        public const string __ResultBody = "ResultBody";
        #endregion
        #region 属性
        private string _TaskId;
        /// <summary>
        /// 任务id
        /// </summary>
        [DbColumn("TaskId", LocalName = "任务id", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string TaskId
        {
            get
            {
                return _TaskId;
            }
            set
            {
                _TaskId = value;

            }
        }
        private string _TaskName;
        /// <summary>
        /// 任务描述
        /// </summary>
        [DbColumn("TaskName", LocalName = "任务描述", ColumnType = "string", AllowDBNull = false, MaxLength = 200, ControlType = 1)]
        public virtual string TaskName
        {
            get
            {
                return _TaskName;
            }
            set
            {
                SetData(__TaskName,value);
                _TaskName = value;

            }
        }
        private string _ServerIP;
        /// <summary>
        /// 服务器ip
        /// </summary>
        [DbColumn("ServerIP", LocalName = "服务器ip", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerIP
        {
            get
            {
                return _ServerIP;
            }
            set
            {
                SetData(__ServerIP, value);
                _ServerIP = value;

            }
        }
        private string _ServerName;
        /// <summary>
        /// 服务器名称
        /// </summary>
        [DbColumn("ServerName", LocalName = "服务器名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ServerName
        {
            get
            {
                return _ServerName;
            }
            set
            {
                SetData(__ServerName, value);
                _ServerName = value;

            }
        }
        private string _UserId;
        /// <summary>
        /// 用户id
        /// </summary>
        [DbColumn("UserId", LocalName = "用户id", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                SetData(__UserId, value);
                _UserId = value;

            }
        }
        private string _StartupSource;
        /// <summary>
        /// 启动的源
        /// </summary>
        [DbColumn("StartupSource", LocalName = "启动的源", ColumnType = "string", MaxLength = 500, ControlType = 1)]
        public virtual string StartupSource
        {
            get
            {
                return _StartupSource;
            }
            set
            {
                SetData(__StartupSource, value);
                _StartupSource = value;

            }
        }
        private bool _IsOpen;
        /// <summary>
        /// 是否运行
        /// </summary>
        [DbColumn("IsOpen", LocalName = "是否运行", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsOpen
        {
            get
            {
                return _IsOpen;
            }
            set
            {
                SetData(__IsOpen, value);
                _IsOpen = value;

            }
        }
        private bool _IsError;
        /// <summary>
        /// 是否发生错误
        /// </summary>
        [DbColumn("IsError", LocalName = "是否发生错误", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
        public virtual bool IsError
        {
            get
            {
                return _IsError;
            }
            set
            {
                SetData(__IsError, value);
                _IsError = value;

            }
        }
        private DateTime _StartDate;
        /// <summary>
        /// 开始时间
        /// </summary>
        [DbColumn("StartDate", LocalName = "开始时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                SetData(__StartDate, value);
                _StartDate = value;

            }
        }
        private DateTime _EndDate;
        /// <summary>
        /// 结束时间
        /// </summary>
        [DbColumn("EndDate", LocalName = "结束时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                SetData(__EndDate, value);
                _EndDate = value;

            }
        }
        private decimal _ExecuteTime;
        /// <summary>
        /// 执行时间(毫秒)
        /// </summary>
        [DbColumn("ExecuteTime", LocalName = "执行时间(毫秒)", ColumnType = "decimal", AllowDBNull = false, VaildKey = "IsDecimal", ControlType = 6)]
        public virtual decimal ExecuteTime
        {
            get
            {
                return _ExecuteTime;
            }
            set
            {
                SetData(__ExecuteTime, value);
                _ExecuteTime = value;

            }
        }
        private string _ResultBody;
        /// <summary>
        /// 返回结果
        /// </summary>
        [DbColumn("ResultBody", LocalName = "返回结果", ColumnType = "string", ControlType = 1)]
        public virtual string ResultBody
        {
            get
            {
                return _ResultBody;
            }
            set
            {
                SetData(__ResultBody, value);
                _ResultBody = value;

            }
        }
        #endregion
    }
}
