﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM.EF;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_SystemSqlLogEF字段映射
    /// </summary>
    public partial class SystemSqlLogMap : ClassMap<SystemSqlLogEntity>
    {
        public override void Configure(EntityTypeBuilder<SystemSqlLogEntity>
        builder)
        {
            builder.ToTable<SystemSqlLogEntity>("LOG_SystemSqlLog");

            builder.HasKey(x => x.SystemSQLLogId);
            builder.Property(x => x.SystemSQLLogId).HasColumnName("SystemSQLLogId").HasMaxLength(50);

            /// <summary>
            /// 请求id
            /// </summary>
            builder.Property(x => x.TraceIdentifier).HasColumnName("TraceIdentifier").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 服务器IP
            /// </summary>
            builder.Property(x => x.ServerIP).HasColumnName("ServerIP").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 服务器名称
            /// </summary>
            builder.Property(x => x.ServerName).HasColumnName("ServerName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 请求Url
            /// </summary>
            builder.Property(x => x.MenuPage).HasColumnName("MenuPage").HasMaxLength(200).IsRequired(true);

            /// <summary>
            /// 执行SQL
            /// </summary>
            builder.Property(x => x.CommandText).HasColumnName("CommandText").IsRequired(true);

            /// <summary>
            /// ORM类别
            /// </summary>
            builder.Property(x => x.SourceCode).HasColumnName("SourceCode").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 数据库类型
            /// </summary>
            builder.Property(x => x.ProviderType).HasColumnName("ProviderType").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 执行时间
            /// </summary>
            builder.Property(x => x.ExecuteTime).HasColumnName("ExecuteTime").HasColumnType("decimal(18,2)").IsRequired(true).HasDefaultValue(0);

            /// <summary>
            /// 执行状态
            /// </summary>
            builder.Property(x => x.IsError).HasColumnName("IsError").HasDefaultValue(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);
            base.Configure(builder);
        }
    }
}