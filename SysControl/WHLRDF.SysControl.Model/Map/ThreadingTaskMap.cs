﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM.EF;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_ThreadingTaskEF字段映射
    /// </summary>
    public partial class ThreadingTaskMap : ClassMap<ThreadingTaskEntity>
    {
        public override void Configure(EntityTypeBuilder<ThreadingTaskEntity>
        builder)
        {
            builder.ToTable<ThreadingTaskEntity>("LOG_ThreadingTask");

            builder.HasKey(x => x.TaskId);
            builder.Property(x => x.TaskId).HasColumnName("TaskId").HasMaxLength(50);

            /// <summary>
            /// 任务描述
            /// </summary>
            builder.Property(x => x.TaskName).HasColumnName("TaskName").HasMaxLength(200).IsRequired(true);

            /// <summary>
            /// 服务器ip
            /// </summary>
            builder.Property(x => x.ServerIP).HasColumnName("ServerIP").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 服务器名称
            /// </summary>
            builder.Property(x => x.ServerName).HasColumnName("ServerName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 用户id
            /// </summary>
            builder.Property(x => x.UserId).HasColumnName("UserId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 启动的源
            /// </summary>
            builder.Property(x => x.StartupSource).HasColumnName("StartupSource").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 是否运行
            /// </summary>
            builder.Property(x => x.IsOpen).HasColumnName("IsOpen").HasDefaultValue(false);

            /// <summary>
            /// 是否发生错误
            /// </summary>
            builder.Property(x => x.IsError).HasColumnName("IsError").HasDefaultValue(false);

            /// <summary>
            /// 开始时间
            /// </summary>
            builder.Property(x => x.StartDate).HasColumnName("StartDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 结束时间
            /// </summary>
            builder.Property(x => x.EndDate).HasColumnName("EndDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 执行时间(毫秒)
            /// </summary>
            builder.Property(x => x.ExecuteTime).HasColumnName("ExecuteTime").HasColumnType("decimal(18,2)").IsRequired(true).HasDefaultValue(0);

            /// <summary>
            /// 返回结果
            /// </summary>
            builder.Property(x => x.ResultBody).HasColumnName("ResultBody").IsRequired(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);
            base.Configure(builder);
        }
    }
}