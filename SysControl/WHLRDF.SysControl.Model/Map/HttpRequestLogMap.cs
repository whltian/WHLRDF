﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM.EF;
namespace WHLRDF.SysControl.Model
{
    /// <summary>
    /// LOG_HttpRequestLogEF字段映射
    /// </summary>
    public partial class HttpRequestLogMap : ClassMap<HttpRequestLogEntity>
    {
        public override void Configure(EntityTypeBuilder<HttpRequestLogEntity>
        builder)
        {
            builder.ToTable<HttpRequestLogEntity>("LOG_HttpRequestLog");

            builder.HasKey(x => x.HttpRequestLogId);
            builder.Property(x => x.HttpRequestLogId).HasColumnName("HttpRequestLogId").HasMaxLength(50);

            /// <summary>
            /// 请求Id
            /// </summary>
            builder.Property(x => x.TraceIdentifier).HasColumnName("TraceIdentifier").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 服务器地址
            /// </summary>
            builder.Property(x => x.ServerIP).HasColumnName("ServerIP").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 服务器名称
            /// </summary>
            builder.Property(x => x.ServerName).HasColumnName("ServerName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 请求Url
            /// </summary>
            builder.Property(x => x.MenuPage).HasColumnName("MenuPage").HasMaxLength(200).IsRequired(true);

            /// <summary>
            /// 请求平台
            /// </summary>
            builder.Property(x => x.SourcePlatform).HasColumnName("SourcePlatform").HasDefaultValue(0);

            /// <summary>
            /// 用户
            /// </summary>
            builder.Property(x => x.UserId).HasColumnName("UserId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 用户地址
            /// </summary>
            builder.Property(x => x.UserIpAddress).HasColumnName("UserIpAddress").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 请求方式
            /// </summary>
            builder.Property(x => x.HttpMethod).HasColumnName("HttpMethod").HasDefaultValue(0);

            /// <summary>
            /// IsHttps
            /// </summary>
            builder.Property(x => x.IsHttps).HasColumnName("IsHttps").HasDefaultValue(false);

            /// <summary>
            /// 上一个请求
            /// </summary>
            builder.Property(x => x.Referer).HasColumnName("Referer").HasMaxLength(200).IsRequired(false);

            /// <summary>
            /// 请求头
            /// </summary>
            builder.Property(x => x.UserAgent).HasColumnName("UserAgent").HasMaxLength(200).IsRequired(false);

            /// <summary>
            /// 开始时间
            /// </summary>
            builder.Property(x => x.StartDate).HasColumnName("StartDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 结束时间
            /// </summary>
            builder.Property(x => x.EndDate).HasColumnName("EndDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 执行时间
            /// </summary>
            builder.Property(x => x.ExecuteTime).HasColumnName("ExecuteTime").HasColumnType("decimal(18,2)").IsRequired(true).HasDefaultValue(0);

            /// <summary>
            /// Get 参数
            /// </summary>
            builder.Property(x => x.Query).HasColumnName("Query").IsRequired(false);

            /// <summary>
            /// Form参数
            /// </summary>
            builder.Property(x => x.Form).HasColumnName("Form").IsRequired(false);

            /// <summary>
            /// 请求正文
            /// </summary>
            builder.Property(x => x.body).HasColumnName("body").IsRequired(false);

            /// <summary>
            /// Ajax请求
            /// </summary>
            builder.Property(x => x.IsAjax).HasColumnName("IsAjax").HasDefaultValue(false);

            /// <summary>
            /// 编码
            /// </summary>
            builder.Property(x => x.StatusCode).HasColumnName("StatusCode").HasDefaultValue(0);

            /// <summary>
            /// 返回正文
            /// </summary>
            builder.Property(x => x.ResultBody).HasColumnName("ResultBody").IsRequired(false);

            /// <summary>
            /// 返回正文类别
            /// </summary>
            builder.Property(x => x.ContentType).HasColumnName("ContentType").HasMaxLength(200).IsRequired(false);

            /// <summary>
            /// 请求是否异常
            /// </summary>
            builder.Property(x => x.IsError).HasColumnName("IsError").HasDefaultValue(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);
            base.Configure(builder);
        }
    }
}