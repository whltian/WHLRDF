﻿
using System.Collections.Generic;
using WHLRDF.ORM;
using WHLRDF.SysControl.Model;

namespace WHLRDF.SysControl.BLL
{
    /// <summary>
    /// LOG_HttpRequestLog接口
    /// </summary>
    public partial interface IHttpRequestLogService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<HttpRequestLogEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        HttpRequestLogEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="domain">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(HttpRequestLogEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid ligerGrid);
        #endregion

        #region 扩展方法

        #endregion

    }
}
