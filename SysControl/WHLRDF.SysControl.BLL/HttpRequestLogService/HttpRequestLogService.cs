﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.ORM;
using WHLRDF.SysControl.Model;
namespace WHLRDF.SysControl.BLL
{

    /// <summary>
    /// LOG_HttpRequestLog实例
    /// </summary>
    public class HttpRequestLogService : SerivceBase, IHttpRequestLogService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<HttpRequestLogEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(HttpRequestLogEntity.__IsDeleted, false);
            return this.Query<HttpRequestLogEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public HttpRequestLogEntity GetById(string HttpRequestLogId)
        {
            if (string.IsNullOrWhiteSpace(HttpRequestLogId))
            {
                return null;
            }
            return this.GetById<HttpRequestLogEntity>(HttpRequestLogId);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(HttpRequestLogEntity entity, ref string strError)
        {
            if (string.IsNullOrWhiteSpace(entity.HttpRequestLogId))
            {
                entity.HttpRequestLogId = MyGenerateHelper.GenerateOrder();
            }
            return this.SaveOrUpdate<HttpRequestLogEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<HttpRequestLogEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                HttpRequestLogEntity._PrimaryKeyName ,
                                HttpRequestLogEntity.__MenuPage,
                                 HttpRequestLogEntity.__ServerName,
                                  HttpRequestLogEntity.__ServerIP,
                                  HttpRequestLogEntity.__SourcePlatform
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<HttpRequestLogEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
      
        #endregion

        #region 事件

        #endregion
    }
}
