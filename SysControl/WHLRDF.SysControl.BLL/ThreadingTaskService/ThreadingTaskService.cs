﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.ORM;
using WHLRDF.SysControl.Model;
namespace WHLRDF.SysControl.BLL
{

    /// <summary>
    /// LOG_ThreadingTask实例
    /// </summary>
    public class ThreadingTaskService : SerivceBase, IThreadingTaskService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<ThreadingTaskEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(ThreadingTaskEntity.__IsDeleted, false);
            return this.Query<ThreadingTaskEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public ThreadingTaskEntity GetById(string TaskId)
        {
            if (string.IsNullOrWhiteSpace(TaskId))
            {
                return null;
            }
            return this.GetById<ThreadingTaskEntity>(TaskId);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(ThreadingTaskEntity entity, ref string strError)
        {
          
            if (string.IsNullOrWhiteSpace(entity.TaskId))
            {
                entity.TaskId = MyGenerateHelper.GenerateOrder();
            }
           
            return this.SaveOrUpdate<ThreadingTaskEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<ThreadingTaskEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                         ThreadingTaskEntity.__TaskName,
                              ThreadingTaskEntity.__ServerName
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<ThreadingTaskEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法

        #endregion

        #region 事件

        #endregion
    }
}
