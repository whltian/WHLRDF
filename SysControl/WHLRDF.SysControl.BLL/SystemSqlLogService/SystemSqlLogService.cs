﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.ORM;
using WHLRDF.SysControl.Model;
namespace WHLRDF.SysControl.BLL
{

    /// <summary>
    /// LOG_SystemSqlLog实例
    /// </summary>
    public class SystemSqlLogService : SerivceBase, ISystemSqlLogService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<SystemSqlLogEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(SystemSqlLogEntity.__IsDeleted, false);
            return this.Query<SystemSqlLogEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public SystemSqlLogEntity GetById(string SystemSQLLogId)
        {
            if (string.IsNullOrWhiteSpace(SystemSQLLogId))
            {
                return null;
            }
            return this.GetById<SystemSqlLogEntity>(SystemSQLLogId);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(SystemSqlLogEntity entity, ref string strError)
        {
            if (string.IsNullOrWhiteSpace(entity.SystemSQLLogId))
            {
                entity.SystemSQLLogId = MyGenerateHelper.GenerateOrder();
            }
            return this.SaveOrUpdate<SystemSqlLogEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<SystemSqlLogEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                              SystemSqlLogEntity.__CommandText,
                              SystemSqlLogEntity.__ServerName,
                              SystemSqlLogEntity.__SourceCode
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<SystemSqlLogEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        
        #endregion

        #region 事件

        #endregion
    }
}
