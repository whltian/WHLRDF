﻿
using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;
using WHLRDF.Log;
using WHLRDF.SysControl.BLL;
using WHLRDF.SysControl.Model;

namespace WHLRDF.SysControl.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [MenuPage(2000, "010", "日志接口", "SysControlLog", "")]
    public class LogController : BaseController
    {
     
        public IHttpRequestLogService requestLogService { get; set; }

        public IThreadingTaskService taskLogService { get; set; }

        public ISystemSqlLogService sqlLogService { get; set; }
        [HttpPost]
        [Permission("01", "请求日志")]
        public JsonResult RequestLog(HttpRequestLogEntity logModel)
        {
            if (ModelState.IsValid)
            {
                string strError = "";
                if (requestLogService.Save(logModel,ref strError))
                {
                    return Json(AjaxResult.Success());
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }
        [HttpPost]
        [Permission("02", "SQL日志")]
        public JsonResult SQLLog(SystemSqlLogEntity logModel)
        {
            if (ModelState.IsValid)
            {
                string strError = "";
                if (sqlLogService.Save(logModel, ref strError))
                {
                    return Json(AjaxResult.Success());
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }
        [HttpPost]
        [Permission("03", "任务日志")]
        public JsonResult TaskLog(ThreadingTaskEntity logModel)
        {
            if (ModelState.IsValid)
            {
                string strError = "";
                if (taskLogService.Save(logModel, ref strError))
                {
                    return Json(AjaxResult.Success());
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }
    }
}
