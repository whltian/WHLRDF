﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程步骤
    /// </summary>
    [Table("WF_Activity", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class ActivityEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public ActivityEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_Activity";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ActivityId";

        /// <summary>
        ///  流程id
        /// </summary>
        public const string __FlowId = "FlowId";

        /// <summary>
        ///  步骤名称
        /// </summary>
        public const string __ActivityName = "ActivityName";

        /// <summary>
        ///  节点类别
        /// </summary>
        public const string __ActivityType = "ActivityType";

        /// <summary>
        ///  接收人类别
        /// </summary>
        public const string __RecipientType = "RecipientType";

        /// <summary>
        /// 组织机构Id
        /// </summary>
        public const string __OrgId = "OrgId";

        /// <summary>
        ///  接收人id
        /// </summary>
        public const string __RecipientId = "RecipientId";

        /// <summary>
        ///  接收人名称
        /// </summary>
        public const string __RecipientName = "RecipientName";

        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  最小请求数
        /// </summary>
        public const string __Mininum = "Mininum";

        /// <summary>
        ///  激活是否发送消息
        /// </summary>
        public const string __IsActive = "IsActive";

        /// <summary>
        ///  成功是否发送消息
        /// </summary>
        public const string __IsComplete = "IsComplete";

        /// <summary>
        ///  驳回是否发送消息
        /// </summary>
        public const string __IsReject = "IsReject";

        /// <summary>
        ///  
        /// </summary>
        public const string __IsListener = "IsListener";

        /// <summary>
        ///  
        /// </summary>
        public const string __HttpUrl = "HttpUrl";

        #endregion
        #region 属性

        private string _ActivityId;
        ///<summary>
        ///  步骤id
        /// </summary>
        [DbColumn("ActivityId", IsPrimaryKey = true, LocalName = " 步骤id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string ActivityId
        {
            get
            {
                return _ActivityId;
            }
            set
            {
                _ActivityId = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程id
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {
                SetData(__FlowId,value);
                _FlowId = value;
            }
        }


        private string _ActivityName;
        ///<summary>
        ///  步骤名称
        /// </summary>
        [DbColumn("ActivityName", LocalName = " 步骤名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActivityName
        {
            get
            {
                return _ActivityName;
            }
            set
            {
                SetData(__ActivityName,value);
                _ActivityName = value;
            }
        }


        private int _ActivityType;
        ///<summary>
        ///  节点类别
        /// </summary>
        [DbColumn("ActivityType", LocalName = " 节点类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 3, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ActivityType
        {
            get
            {
                return _ActivityType;
            }
            set
            {
                SetData(__ActivityType,value);
                _ActivityType = value;
            }
        }


        private int _RecipientType;
        ///<summary>
        ///  接收人类别
        /// </summary>
        [DbColumn("RecipientType", LocalName = " 接收人类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 3, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int RecipientType
        {
            get
            {
                return _RecipientType;
            }
            set
            {
                SetData(__RecipientType,value);
                _RecipientType = value;
            }
        }


        private string _OrgId;
        ///<summary>
        /// 组织机构Id
        /// </summary>
        [DbColumn("OrgId", LocalName = "组织机构Id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OrgId
        {
            get
            {
                return _OrgId;
            }
            set
            {
                SetData(__OrgId,value);
                _OrgId = value;
            }
        }


        private string _RecipientId;
        ///<summary>
        ///  接收人id
        /// </summary>
        [DbColumn("RecipientId", LocalName = " 接收人id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string RecipientId
        {
            get
            {
                return _RecipientId;
            }
            set
            {
                SetData(__RecipientId,value);
                _RecipientId = value;
            }
        }


        private string _RecipientName;
        ///<summary>
        ///  接收人名称
        /// </summary>
        [DbColumn("RecipientName", LocalName = " 接收人名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string RecipientName
        {
            get
            {
                return _RecipientName;
            }
            set
            {
                SetData(__RecipientName,value);
                _RecipientName = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {
                SetData(__OrderNum,value);
                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark,value);
                _Remark = value;
            }
        }


        private int _Mininum;
        ///<summary>
        ///  最小请求数
        /// </summary>
        [DbColumn("Mininum", LocalName = " 最小请求数", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Mininum
        {
            get
            {
                return _Mininum;
            }
            set
            {
                SetData(__Mininum,value);
                _Mininum = value;
            }
        }


        private bool _IsActive;
        ///<summary>
        ///  激活是否发送消息
        /// </summary>
        [DbColumn("IsActive", LocalName = " 激活是否发送消息", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                SetData(__IsActive,value);
                _IsActive = value;
            }
        }


        private bool _IsComplete;
        ///<summary>
        ///  成功是否发送消息
        /// </summary>
        [DbColumn("IsComplete", LocalName = " 成功是否发送消息", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsComplete
        {
            get
            {
                return _IsComplete;
            }
            set
            {
                SetData(__IsComplete,value);
                _IsComplete = value;
            }
        }


        private bool _IsReject;
        ///<summary>
        ///  驳回是否发送消息
        /// </summary>
        [DbColumn("IsReject", LocalName = " 驳回是否发送消息", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsReject
        {
            get
            {
                return _IsReject;
            }
            set
            {
                SetData(__IsReject,value);
                _IsReject = value;
            }
        }


        private bool _IsListener;
        ///<summary>
        ///  
        /// </summary>
        [DbColumn("IsListener", LocalName = " ", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsListener
        {
            get
            {
                return _IsListener;
            }
            set
            {
                SetData(__IsListener,value);
                _IsListener = value;
            }
        }


        private string _HttpUrl;
        ///<summary>
        ///  
        /// </summary>
        [DbColumn("HttpUrl", LocalName = " ", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string HttpUrl
        {
            get
            {
                return _HttpUrl;
            }
            set
            {
                SetData(__HttpUrl,value);
                _HttpUrl = value;
            }
        }


        #endregion
    }
}
