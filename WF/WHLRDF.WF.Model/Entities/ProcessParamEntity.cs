﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 实例参数
    /// </summary>
    [Table("WF_ProcessParam", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class ProcessParamEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProcessParamEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_ProcessParam";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ProcessParamId";


        /// <summary>
        ///  实例id
        /// </summary>
        public const string __ProcessId = "ProcessId";


        /// <summary>
        ///  任务id
        /// </summary>
        public const string __ProcessTaskId = "ProcessTaskId";


        /// <summary>
        ///  流程参数id
        /// </summary>
        public const string __FlowParamId = "FlowParamId";


        /// <summary>
        ///  参数名称
        /// </summary>
        public const string __ParamName = "ParamName";


        /// <summary>
        ///  值
        /// </summary>
        public const string __ParamValue = "ParamValue";


        /// <summary>
        ///  类别
        /// </summary>
        public const string __ParamType = "ParamType";


        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";


        #endregion
        #region 属性

        private int _ProcessParamId;
        ///<summary>
        ///  参数id
        /// </summary>
        [DbColumn("ProcessParamId", IsPrimaryKey = true,Identifier =true, LocalName = " 参数id", AllowDBNull = false, ColumnType = "int", SqlType = "int")]
        public virtual int ProcessParamId
        {
            get
            {
                return _ProcessParamId;
            }
            set
            {
                _ProcessParamId = value;
            }
        }


        private string _ProcessId;
        ///<summary>
        ///  实例id
        /// </summary>
        [DbColumn("ProcessId", LocalName = " 实例id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ProcessId
        {
            get
            {
                return _ProcessId;
            }
            set
            {

                SetData(__ProcessId,value);

                _ProcessId = value;
            }
        }


        private string _ProcessTaskId;
        ///<summary>
        ///  任务id
        /// </summary>
        [DbColumn("ProcessTaskId", LocalName = " 任务id", AllowDBNull = true, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ProcessTaskId
        {
            get
            {
                return _ProcessTaskId;
            }
            set
            {

                SetData(__ProcessTaskId,value);

                _ProcessTaskId = value;
            }
        }


        private string _FlowParamId;
        ///<summary>
        ///  流程参数id
        /// </summary>
        [DbColumn("FlowParamId", LocalName = " 流程参数id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowParamId
        {
            get
            {
                return _FlowParamId;
            }
            set
            {

                SetData(__FlowParamId,value);

                _FlowParamId = value;
            }
        }


        private string _ParamName;
        ///<summary>
        ///  参数名称
        /// </summary>
        [DbColumn("ParamName", LocalName = " 参数名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ParamName
        {
            get
            {
                return _ParamName;
            }
            set
            {

                SetData(__ParamName,value);

                _ParamName = value;
            }
        }


        private string _ParamValue;
        ///<summary>
        ///  值
        /// </summary>
        [DbColumn("ParamValue", LocalName = " 值", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ParamValue
        {
            get
            {
                return _ParamValue;
            }
            set
            {

                SetData(__ParamValue,value);

                _ParamValue = value;
            }
        }


        private int _ParamType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("ParamType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ParamType
        {
            get
            {
                return _ParamType;
            }
            set
            {

                SetData(__ParamType,value);

                _ParamType = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {

                SetData(__Status,value);

                _Status = value;
            }
        }


        #endregion
    }
}
