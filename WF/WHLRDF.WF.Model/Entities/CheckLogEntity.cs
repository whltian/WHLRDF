﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// WF_CheckLog
    /// </summary>
    [Table("WF_CheckLog", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class CheckLogEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public CheckLogEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_CheckLog";

        /// <summary>
        /// 日志id
        /// </summary>
        public const string _PrimaryKeyName = "CheckLogId";
       


        /// <summary>
        ///  流程id
        /// </summary>
        public const string __FlowId = "FlowId";


        /// <summary>
        ///  流程名称
        /// </summary>
        public const string __FlowName = "FlowName";


        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";


        /// <summary>
        ///  操作人
        /// </summary>
        public const string __ActionUserId = "ActionUserId";


        /// <summary>
        ///  流程图
        /// </summary>
        public const string __Chart = "Chart";


        #endregion
        #region 属性

        private string _CheckLogId;
        ///<summary>
        /// 日志id
        /// </summary>
        [DbColumn("CheckLogId",IsPrimaryKey =true, LocalName = "日志id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 36, Min = 0)]
        public virtual string CheckLogId
        {
            get
            {
                return _CheckLogId;
            }
            set
            {

               
                _CheckLogId = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程id
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {

                SetData(__FlowId,value);

                _FlowId = value;
            }
        }


        private string _FlowName;
        ///<summary>
        ///  流程名称
        /// </summary>
        [DbColumn("FlowName", LocalName = " 流程名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowName
        {
            get
            {
                return _FlowName;
            }
            set
            {

                SetData(__FlowName,value);

                _FlowName = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {

                SetData(__Status,value);

                _Status = value;
            }
        }


        private string _ActionUserId;
        ///<summary>
        ///  操作人
        /// </summary>
        [DbColumn("ActionUserId", LocalName = " 操作人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActionUserId
        {
            get
            {
                return _ActionUserId;
            }
            set
            {

                SetData(__ActionUserId,value);

                _ActionUserId = value;
            }
        }


        private string _Chart;
        ///<summary>
        ///  流程图
        /// </summary>
        [DbColumn("Chart", LocalName = " 流程图", ColumnType = "ntext", SqlType = "ntext", ControlType = 5, MaxLength = 1073741823, Min = 0)]
        public virtual string Chart
        {
            get
            {
                return _Chart;
            }
            set
            {

                SetData(__Chart,value);

                _Chart = value;
            }
        }


        #endregion
    }
}

