﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程实例
    /// </summary>
    [Table("WF_Process", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class ProcessEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProcessEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_Process";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ProcessId";

        /// <summary>
        ///  表单编号
        /// </summary>
        public const string __FromId = "FromId";

        /// <summary>
        ///  操作人
        /// </summary>
        public const string __OperatorId = "OperatorId";

        /// <summary>
        ///  所有者
        /// </summary>
        public const string __OwnerUserId = "OwnerUserId";

        /// <summary>
        ///  标题
        /// </summary>
        public const string __Title = "Title";

        /// <summary>
        ///  流程id
        /// </summary>
        public const string __FlowId = "FlowId";

        /// <summary>
        ///  版本号
        /// </summary>
        public const string __Version = "Version";

        /// <summary>
        ///  子流程
        /// </summary>
        public const string __IsSubProcess = "IsSubProcess";

        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";

        /// <summary>
        ///  
        /// </summary>
        public const string __StartDate = "StartDate";

        /// <summary>
        ///  
        /// </summary>
        public const string __EndDate = "EndDate";

        #endregion
        #region 属性

        private string _ProcessId;
        ///<summary>
        ///  实例id
        /// </summary>
        [DbColumn("ProcessId", IsPrimaryKey = true, LocalName = " 实例id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string ProcessId
        {
            get
            {
                return _ProcessId;
            }
            set
            {
                _ProcessId = value;
            }
        }


        private string _FromId;
        ///<summary>
        ///  表单编号
        /// </summary>
        [DbColumn("FromId", LocalName = " 表单编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 20, Min = 0)]
        public virtual string FromId
        {
            get
            {
                return _FromId;
            }
            set
            {
                SetData(__FromId,value);
                _FromId = value;
            }
        }


        private string _OperatorId;
        ///<summary>
        ///  操作人
        /// </summary>
        [DbColumn("OperatorId", LocalName = " 操作人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OperatorId
        {
            get
            {
                return _OperatorId;
            }
            set
            {
                SetData(__OperatorId,value);
                _OperatorId = value;
            }
        }


        private string _OwnerUserId;
        ///<summary>
        ///  所有者
        /// </summary>
        [DbColumn("OwnerUserId", LocalName = " 所有者", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OwnerUserId
        {
            get
            {
                return _OwnerUserId;
            }
            set
            {
                SetData(__OwnerUserId,value);
                _OwnerUserId = value;
            }
        }


        private string _Title;
        ///<summary>
        ///  标题
        /// </summary>
        [DbColumn("Title", LocalName = " 标题", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetData(__Title,value);
                _Title = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程id
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {
                SetData(__FlowId,value);
                _FlowId = value;
            }
        }


        private int _Version;
        ///<summary>
        ///  版本号
        /// </summary>
        [DbColumn("Version", LocalName = " 版本号", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Version
        {
            get
            {
                return _Version;
            }
            set
            {
                SetData(__Version,value);
                _Version = value;
            }
        }


        private bool _IsSubProcess;
        ///<summary>
        ///  子流程
        /// </summary>
        [DbColumn("IsSubProcess", LocalName = " 子流程", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsSubProcess
        {
            get
            {
                return _IsSubProcess;
            }
            set
            {
                SetData(__IsSubProcess,value);
                _IsSubProcess = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 4, MaxLength = 1, Min = 0)]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetData(__Status,value);
                _Status = value;
            }
        }


        private DateTime _StartDate;
        ///<summary>
        ///  
        /// </summary>
        [DbColumn("StartDate", LocalName = " ", AllowDBNull = false, ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                SetData(__StartDate,value);
                _StartDate = value;
            }
        }


        private DateTime? _EndDate;
        ///<summary>
        ///  
        /// </summary>
        [DbColumn("EndDate", LocalName = " ", ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime? EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                SetData(__EndDate,value);
                _EndDate = value;
            }
        }


        #endregion
    }
}
