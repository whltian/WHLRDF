﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程目录
    /// </summary>
    [Table("WF_FlowType", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class FlowTypeEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowTypeEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_FlowType";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "FlowTypeId";


        /// <summary>
        ///  名称
        /// </summary>
        public const string __FlowTypeName = "FlowTypeName";


        /// <summary>
        ///  上级
        /// </summary>
        public const string __ParentId = "ParentId";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _FlowTypeId;
        ///<summary>
        ///  编号
        /// </summary>
        [DbColumn("FlowTypeId", IsPrimaryKey = true, LocalName = " 编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 10, Min = 0)]
        public virtual string FlowTypeId
        {
            get
            {
                return _FlowTypeId;
            }
            set
            {
                _FlowTypeId = value;
            }
        }


        private string _FlowTypeName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("FlowTypeName", LocalName = " 名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowTypeName
        {
            get
            {
                return _FlowTypeName;
            }
            set
            {

                SetData(__FlowTypeName,value);

                _FlowTypeName = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  上级
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 10, Min = 0)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId,value);

                _ParentId = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 500, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
