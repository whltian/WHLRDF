﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程版本
    /// </summary>
    [Table("WF_FlowVersion", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class FlowVersionEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowVersionEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_FlowVersion";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "FlowVersionId";


        /// <summary>
        ///  流程id
        /// </summary>
        public const string __FlowId = "FlowId";


        /// <summary>
        ///  版本号
        /// </summary>
        public const string __Version = "Version";


        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";


        /// <summary>
        ///  dll注入
        /// </summary>
        public const string __ServiceClass = "ServiceClass";

        /// <summary>
        ///  流程图
        /// </summary>
        public const string __Chart = "Chart";


        #endregion
        #region 属性

        private string _FlowVersionId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("FlowVersionId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string FlowVersionId
        {
            get
            {
                return _FlowVersionId;
            }
            set
            {
                _FlowVersionId = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程id
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 20, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {

                SetData(__FlowId,value);

                _FlowId = value;
            }
        }


        private int _Version;
        ///<summary>
        ///  版本号
        /// </summary>
        [DbColumn("Version", LocalName = " 版本号", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Version
        {
            get
            {
                return _Version;
            }
            set
            {

                SetData(__Version,value);

                _Version = value;
            }
        }


        private bool _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool Status
        {
            get
            {
                return _Status;
            }
            set
            {

                SetData(__Status,value);

                _Status = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        private string _ServiceClass;
        ///<summary>
        ///  dll注入
        /// </summary>
        [DbColumn("ServiceClass", LocalName = " dll注入", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string ServiceClass
        {
            get
            {
                return _ServiceClass;
            }
            set
            {

                SetData(__ServiceClass,value);

                _ServiceClass = value;
            }
        }


        private string _Chart;
        ///<summary>
        ///  流程图
        /// </summary>
        [DbColumn("Chart", LocalName = " 流程图", ColumnType = "ntext", SqlType = "ntext", ControlType = 5, MaxLength = 1073741823, Min = 0)]
        public virtual string Chart
        {
            get
            {
                return _Chart;
            }
            set
            {

                SetData(__Chart,value);

                _Chart = value;
            }
        }


        #endregion
    }
}