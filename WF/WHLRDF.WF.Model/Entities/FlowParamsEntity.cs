﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程参数
    /// </summary>
    [Table("WF_FlowParams", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class FlowParamsEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowParamsEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_FlowParams";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "FlowParamId";


        /// <summary>
        ///  流程id
        /// </summary>
        public const string __FlowId = "FlowId";


        /// <summary>
        ///  参数名称
        /// </summary>
        public const string __FlowParamName = "FlowParamName";


        /// <summary>
        ///  参数类型
        /// </summary>
        public const string __ParamType = "ParamType";


        /// <summary>
        /// 源id
        /// </summary>
        public const string __ResourceId = "ResourceId";


        /// <summary>
        ///  值类别
        /// </summary>
        public const string __ParamValueType = "ParamValueType";


        /// <summary>
        ///  默认值
        /// </summary>
        public const string __DefaultValue = "DefaultValue";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _FlowParamId;
        ///<summary>
        ///  参数id
        /// </summary>
        [DbColumn("FlowParamId", IsPrimaryKey = true, LocalName = " 参数id", AllowDBNull = false, ColumnType = "string", SqlType = "string", ControlType = 2, MaxLength = 50 )]
        public virtual string FlowParamId
        {
            get
            {
                return _FlowParamId;
            }
            set
            {
                _FlowParamId = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程id
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {

                SetData(__FlowId,value);

                _FlowId = value;
            }
        }


        private string _FlowParamName;
        ///<summary>
        ///  参数名称
        /// </summary>
        [DbColumn("FlowParamName", LocalName = " 参数名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50 ,VaildKey = "stringEn")]
        public virtual string FlowParamName
        {
            get
            {
                return _FlowParamName;
            }
            set
            {

                SetData(__FlowParamName,value);

                _FlowParamName = value;
            }
        }


        private int _ParamType;
        ///<summary>
        ///  参数类型
        /// </summary>
        [DbColumn("ParamType", LocalName = " 参数类型", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 3, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ParamType
        {
            get
            {
                return _ParamType;
            }
            set
            {

                SetData(__ParamType,value);

                _ParamType = value;
            }
        }


        private string _ResourceId;
        ///<summary>
        /// 源id
        /// </summary>
        [DbColumn("ResourceId", LocalName = "源id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ResourceId
        {
            get
            {
                return _ResourceId;
            }
            set
            {

                SetData(__ResourceId,value);

                _ResourceId = value;
            }
        }


        private int _ParamValueType;
        ///<summary>
        ///  值类别
        /// </summary>
        [DbColumn("ParamValueType", LocalName = " 值类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 3, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ParamValueType
        {
            get
            {
                return _ParamValueType;
            }
            set
            {

                SetData(__ParamValueType,value);

                _ParamValueType = value;
            }
        }


        private string _DefaultValue;
        ///<summary>
        ///  默认值
        /// </summary>
        [DbColumn("DefaultValue", LocalName = " 默认值", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DefaultValue
        {
            get
            {
                return _DefaultValue;
            }
            set
            {

                SetData(__DefaultValue,value);

                _DefaultValue = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
