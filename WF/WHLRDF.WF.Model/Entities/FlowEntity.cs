﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程管理
    /// </summary>
    [Table("WF_Flow", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class FlowEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_Flow";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "FlowId";


        /// <summary>
        ///  类别
        /// </summary>
        public const string __FlowTypeId = "FlowTypeId";


        /// <summary>
        ///  名称
        /// </summary>
        public const string __FlowName = "FlowName";


        /// <summary>
        ///  发布状态
        /// </summary>
        public const string __Status = "Status";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";


        /// <summary>
        ///  迁出人
        /// </summary>
        public const string __CheckOutUserId = "CheckOutUserId";


        /// <summary>
        ///  迁出ip
        /// </summary>
        public const string __CheckOutIp = "CheckOutIp";


        /// <summary>
        ///  dll注入
        /// </summary>
        public const string __ServiceClass = "ServiceClass";


        /// <summary>
        /// 流程图
        /// </summary>
        public const string __Chart = "Chart";


        /// <summary>
        ///  是否迁出
        /// </summary>
        public const string __IsCheckOut = "IsCheckOut";

        #endregion
        #region 属性

        private string _FlowId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("FlowId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 20, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {
                _FlowId = value;
            }
        }


        private string _FlowTypeId;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("FlowTypeId", LocalName = " 类别", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 3, MaxLength = 20, Min = 0)]
        public virtual string FlowTypeId
        {
            get
            {
                return _FlowTypeId;
            }
            set
            {

                SetData(__FlowTypeId,value);

                _FlowTypeId = value;
            }
        }


        private string _FlowName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("FlowName", LocalName = " 名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowName
        {
            get
            {
                return _FlowName;
            }
            set
            {

                SetData(__FlowName,value);

                _FlowName = value;
            }
        }


        private bool _Status;
        ///<summary>
        ///  发布状态
        /// </summary>
        [DbColumn("Status", LocalName = " 发布状态", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool Status
        {
            get
            {
                return _Status;
            }
            set
            {

                SetData(__Status,value);

                _Status = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 500, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        private string _CheckOutUserId;
        ///<summary>
        ///  迁出人
        /// </summary>
        [DbColumn("CheckOutUserId", LocalName = " 迁出人", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string CheckOutUserId
        {
            get
            {
                return _CheckOutUserId;
            }
            set
            {

                SetData(__CheckOutUserId,value);

                _CheckOutUserId = value;
            }
        }


        private string _CheckOutIp;
        ///<summary>
        ///  迁出ip
        /// </summary>
        [DbColumn("CheckOutIp", LocalName = " 迁出ip", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string CheckOutIp
        {
            get
            {
                return _CheckOutIp;
            }
            set
            {

                SetData(__CheckOutIp,value);

                _CheckOutIp = value;
            }
        }


        private string _ServiceClass;
        ///<summary>
        ///  dll注入
        /// </summary>
        [DbColumn("ServiceClass", LocalName = " dll注入", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string ServiceClass
        {
            get
            {
                return _ServiceClass;
            }
            set
            {

                SetData(__ServiceClass,value);

                _ServiceClass = value;
            }
        }


        private string _Chart;
        ///<summary>
        /// 流程图
        /// </summary>
        [DbColumn("Chart", LocalName = "流程图", ColumnType = "ntext", SqlType = "ntext", ControlType = 5, MaxLength = 1073741823, Min = 0)]
        public virtual string Chart
        {
            get
            {
                return _Chart;
            }
            set
            {

                SetData(__Chart,value);

                _Chart = value;
            }
        }


        private bool _IsCheckOut;
        ///<summary>
        ///  是否迁出
        /// </summary>
        [DbColumn("IsCheckOut", LocalName = " 是否迁出", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsCheckOut
        {
            get
            {
                return _IsCheckOut;
            }
            set
            {

                SetData(__IsCheckOut,value);

                _IsCheckOut = value;
            }
        }


        #endregion
    }
}


