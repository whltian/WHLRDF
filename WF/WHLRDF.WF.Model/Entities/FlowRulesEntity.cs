﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// WF_FlowRules
    /// </summary>
    [Table("WF_FlowRules", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class FlowRulesEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowRulesEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_FlowRules";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "FlowRuleId";


        /// <summary>
        ///  流程
        /// </summary>
        public const string __FlowId = "FlowId";


        /// <summary>
        ///  规则类别
        /// </summary>
        public const string __FlowRuleType = "FlowRuleType";


        /// <summary>
        ///  节点id
        /// </summary>
        public const string __ActivityId = "ActivityId";


        /// <summary>
        ///  规则名称
        /// </summary>
        public const string __FlowRuleName = "FlowRuleName";


        /// <summary>
        ///  目标节点
        /// </summary>
        public const string __ToActivity = "ToActivity";


        /// <summary>
        ///  是否线
        /// </summary>
        public const string __IsPath = "IsPath";


        /// <summary>
        ///  目标类别
        /// </summary>
        public const string __ToType = "ToType";


        /// <summary>
        ///  规则
        /// </summary>
        public const string __Condition = "Condition";


        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _FlowRuleId;
        ///<summary>
        /// 主键 
        /// </summary>
        [DbColumn("FlowRuleId", IsPrimaryKey = true, LocalName = "主键 ", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string FlowRuleId
        {
            get
            {
                return _FlowRuleId;
            }
            set
            {
                _FlowRuleId = value;
            }
        }


        private string _FlowId;
        ///<summary>
        ///  流程
        /// </summary>
        [DbColumn("FlowId", LocalName = " 流程", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 20, Min = 0)]
        public virtual string FlowId
        {
            get
            {
                return _FlowId;
            }
            set
            {

                SetData(__FlowId,value);

                _FlowId = value;
            }
        }

        private int _FlowRuleType;
        ///<summary>
        ///  规则类别
        /// </summary>
        [DbColumn("FlowRuleType", LocalName = " 规则类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int FlowRuleType
        {
            get
            {
                return _FlowRuleType;
            }
            set
            {

                SetData(__FlowRuleType,value);

                _FlowRuleType = value;
            }
        }


        private string _ActivityId;
        ///<summary>
        ///  节点id
        /// </summary>
        [DbColumn("ActivityId", LocalName = " 节点id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActivityId
        {
            get
            {
                return _ActivityId;
            }
            set
            {

                SetData(__ActivityId,value);

                _ActivityId = value;
            }
        }


        private string _FlowRuleName;
        ///<summary>
        ///  规则名称
        /// </summary>
        [DbColumn("FlowRuleName", LocalName = " 规则名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FlowRuleName
        {
            get
            {
                return _FlowRuleName;
            }
            set
            {

                SetData(__FlowRuleName,value);

                _FlowRuleName = value;
            }
        }


        private string _ToActivity;
        ///<summary>
        ///  目标节点
        /// </summary>
        [DbColumn("ToActivity", LocalName = " 目标节点", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ToActivity
        {
            get
            {
                return _ToActivity;
            }
            set
            {

                SetData(__ToActivity,value);

                _ToActivity = value;
            }
        }


        private bool _IsPath;
        ///<summary>
        ///  是否线
        /// </summary>
        [DbColumn("IsPath", LocalName = " 是否线", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsPath
        {
            get
            {
                return _IsPath;
            }
            set
            {

                SetData(__IsPath,value);

                _IsPath = value;
            }
        }


        private int _ToType;
        ///<summary>
        ///  目标类别
        /// </summary>
        [DbColumn("ToType", LocalName = " 目标类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ToType
        {
            get
            {
                return _ToType;
            }
            set
            {

                SetData(__ToType,value);

                _ToType = value;
            }
        }


        private string _Condition;
        ///<summary>
        ///  规则
        /// </summary>
        [DbColumn("Condition", LocalName = " 规则", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 1000, Min = 0)]
        public virtual string Condition
        {
            get
            {
                return _Condition;
            }
            set
            {

                SetData(__Condition,value);

                _Condition = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
