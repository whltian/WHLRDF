﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 任务
    /// </summary>
    [Table("WF_ProcessTask", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class ProcessTaskEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProcessTaskEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "WF_ProcessTask";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ProcessTaskId";

        /// <summary>
        ///  实例id
        /// </summary>
        public const string __ProcessId = "ProcessId";

        /// <summary>
        ///  源任务id
        /// </summary>
        public const string __FromTaskId = "FromTaskId";

        /// <summary>
        /// 任务名称
        /// </summary>
        public const string __TaskLabel = "TaskLabel";

        /// <summary>
        ///  任务类别
        /// </summary>
        public const string __TaskType = "TaskType";

        /// <summary>
        ///  节点id
        /// </summary>
        public const string __ActivityId = "ActivityId";

        /// <summary>
        ///  轮数
        /// </summary>
        public const string __TaskRound = "TaskRound";

        /// <summary>
        ///  所有者
        /// </summary>
        public const string __OwnerUserId = "OwnerUserId";

        /// <summary>
        ///  发送人
        /// </summary>
        public const string __SenderUserId = "SenderUserId";

        /// <summary>
        ///  发送时间
        /// </summary>
        public const string __SendDate = "SendDate";

        /// <summary>
        ///  意见
        /// </summary>
        public const string __Description = "Description";

        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";

        /// <summary>
        ///  操作人
        /// </summary>
        public const string __AssignUser = "AssignUser";

        /// <summary>
        ///  执行时间
        /// </summary>
        public const string __EDate = "EDate";

        #endregion
        #region 属性

        private string _ProcessTaskId;
        ///<summary>
        ///  任务id
        /// </summary>
        [DbColumn("ProcessTaskId", IsPrimaryKey = true, LocalName = " 任务id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string ProcessTaskId
        {
            get
            {
                return _ProcessTaskId;
            }
            set
            {
                _ProcessTaskId = value;
            }
        }


        private string _ProcessId;
        ///<summary>
        ///  实例id
        /// </summary>
        [DbColumn("ProcessId", LocalName = " 实例id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ProcessId
        {
            get
            {
                return _ProcessId;
            }
            set
            {
                SetData(__ProcessId,value);
                _ProcessId = value;
            }
        }


        private string _FromTaskId;
        ///<summary>
        ///  源任务id
        /// </summary>
        [DbColumn("FromTaskId", LocalName = " 源任务id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FromTaskId
        {
            get
            {
                return _FromTaskId;
            }
            set
            {
                SetData(__FromTaskId,value);
                _FromTaskId = value;
            }
        }


        private string _TaskLabel;
        ///<summary>
        /// 任务名称
        /// </summary>
        [DbColumn("TaskLabel", LocalName = "任务名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string TaskLabel
        {
            get
            {
                return _TaskLabel;
            }
            set
            {
                SetData(__TaskLabel,value);
                _TaskLabel = value;
            }
        }


        private int _TaskType;
        ///<summary>
        ///  任务类别
        /// </summary>
        [DbColumn("TaskType", LocalName = " 任务类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int TaskType
        {
            get
            {
                return _TaskType;
            }
            set
            {
                SetData(__TaskType,value);
                _TaskType = value;
            }
        }


        private string _ActivityId;
        ///<summary>
        ///  节点id
        /// </summary>
        [DbColumn("ActivityId", LocalName = " 节点id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActivityId
        {
            get
            {
                return _ActivityId;
            }
            set
            {
                SetData(__ActivityId,value);
                _ActivityId = value;
            }
        }


        private int _TaskRound;
        ///<summary>
        ///  轮数
        /// </summary>
        [DbColumn("TaskRound", LocalName = " 轮数", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int TaskRound
        {
            get
            {
                return _TaskRound;
            }
            set
            {
                SetData(__TaskRound,value);
                _TaskRound = value;
            }
        }


        private string _OwnerUserId;
        ///<summary>
        ///  所有者
        /// </summary>
        [DbColumn("OwnerUserId", LocalName = " 所有者", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OwnerUserId
        {
            get
            {
                return _OwnerUserId;
            }
            set
            {
                SetData(__OwnerUserId,value);
                _OwnerUserId = value;
            }
        }


        private string _SenderUserId;
        ///<summary>
        ///  发送人
        /// </summary>
        [DbColumn("SenderUserId", LocalName = " 发送人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string SenderUserId
        {
            get
            {
                return _SenderUserId;
            }
            set
            {
                SetData(__SenderUserId,value);
                _SenderUserId = value;
            }
        }


        private DateTime _SendDate;
        ///<summary>
        ///  发送时间
        /// </summary>
        [DbColumn("SendDate", LocalName = " 发送时间", AllowDBNull = false, ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime SendDate
        {
            get
            {
                return _SendDate;
            }
            set
            {
                SetData(__SendDate,value);
                _SendDate = value;
            }
        }


        private string _Description;
        ///<summary>
        ///  意见
        /// </summary>
        [DbColumn("Description", LocalName = " 意见", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                SetData(__Description,value);
                _Description = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 4, MaxLength = 1, Min = 0)]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetData(__Status,value);
                _Status = value;
            }
        }


        private string _AssignUser;
        ///<summary>
        ///  操作人
        /// </summary>
        [DbColumn("AssignUser", LocalName = " 操作人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string AssignUser
        {
            get
            {
                return _AssignUser;
            }
            set
            {
                SetData(__AssignUser,value);
                _AssignUser = value;
            }
        }


        private DateTime? _EDate;
        ///<summary>
        ///  执行时间
        /// </summary>
        [DbColumn("EDate", LocalName = " 执行时间", ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime? EDate
        {
            get
            {
                return _EDate;
            }
            set
            {
                SetData(__EDate,value);
                _EDate = value;
            }
        }


        #endregion
    }
}