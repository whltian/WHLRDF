﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class CheckLogMap : ClassMap<CheckLogEntity>
    {
        public override void Configure(EntityTypeBuilder<CheckLogEntity>
            builder)
        {
            builder.ToTable<CheckLogEntity>("WF_CheckLog");
          
            /// <summary>
            /// 日志id
            /// </summary>
            builder.HasKey(x => x.CheckLogId);

            builder.Property(x => x.CheckLogId).HasColumnName("CheckLogId").HasMaxLength(36);

            /// <summary>
            ///  流程id
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  流程名称
            /// </summary>
            builder.Property(x => x.FlowName).HasColumnName("FlowName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status");

            /// <summary>
            ///  操作人
            /// </summary>
            builder.Property(x => x.ActionUserId).HasColumnName("ActionUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  流程图
            /// </summary>
            builder.Property(x => x.Chart).HasColumnName("Chart").IsRequired(false);



            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}
