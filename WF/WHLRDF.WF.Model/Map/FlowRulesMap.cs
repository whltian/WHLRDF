﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class FlowRulesMap : ClassMap<FlowRulesEntity>
    {
        public override void Configure(EntityTypeBuilder<FlowRulesEntity>
            builder)
        {
            builder.ToTable<FlowRulesEntity>("WF_FlowRules");
           
            /// <summary>
            /// 主键 
            /// </summary>
            builder.HasKey(x => x.FlowRuleId);


            builder.Property(x => x.FlowRuleId).HasColumnName("FlowRuleId").HasMaxLength(50);


            /// <summary>
            ///  流程
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(20);


            /// <summary>
            ///  规则类别
            /// </summary>
            builder.Property(x => x.FlowRuleType).HasColumnName("FlowRuleType").HasDefaultValue(0);



            /// <summary>
            ///  节点id
            /// </summary>
            builder.Property(x => x.ActivityId).HasColumnName("ActivityId").HasMaxLength(50);



            /// <summary>
            ///  规则名称
            /// </summary>
            builder.Property(x => x.FlowRuleName).HasColumnName("FlowRuleName").HasMaxLength(50);



            /// <summary>
            ///  目标节点
            /// </summary>
            builder.Property(x => x.ToActivity).HasColumnName("ToActivity").HasMaxLength(50);



            /// <summary>
            ///  是否线
            /// </summary>
            builder.Property(x => x.IsPath).HasColumnName("IsPath").HasDefaultValue(0);



            /// <summary>
            ///  目标类别
            /// </summary>
            builder.Property(x => x.ToType).HasColumnName("ToType").HasDefaultValue(0);



            /// <summary>
            ///  规则
            /// </summary>
            builder.Property(x => x.Condition).HasColumnName("Condition").HasMaxLength(1000);



            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500);


            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x=>x.id);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.IsNewAdd);
            builder.Ignore(x => x.group);
        }
    }
}
