﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class FlowMap : ClassMap<FlowEntity>
    {
        public override void Configure(EntityTypeBuilder<FlowEntity>
            builder)
        {
            builder.ToTable<FlowEntity>("WF_Flow");
           
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.FlowId);

            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.FlowTypeId).HasColumnName("FlowTypeId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.FlowName).HasColumnName("FlowName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  发布状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500).IsRequired(false);

            /// <summary>
            ///  迁出人
            /// </summary>
            builder.Property(x => x.CheckOutUserId).HasColumnName("CheckOutUserId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  迁出ip
            /// </summary>
            builder.Property(x => x.CheckOutIp).HasColumnName("CheckOutIp").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  dll注入
            /// </summary>
            builder.Property(x => x.ServiceClass).HasColumnName("ServiceClass").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 流程图
            /// </summary>
            builder.Property(x => x.Chart).HasColumnName("Chart").IsRequired(false);



            /// <summary>
            ///  是否迁出
            /// </summary>
            builder.Property(x => x.IsCheckOut).HasColumnName("IsCheckOut").HasDefaultValue(0);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x => x.IsNewAdd);
            builder.Ignore(x => x.Version);
            builder.Ignore(x => x.IsEdited);
        }
    }
}
