﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class FlowParamsMap : ClassMap<FlowParamsEntity>
    {
        public override void Configure(EntityTypeBuilder<FlowParamsEntity>
            builder)
        {
            builder.ToTable<FlowParamsEntity>("WF_FlowParams");
           
            /// <summary>
            ///  参数id
            /// </summary>
            builder.HasKey(x => x.FlowParamId);

            builder.Property(x => x.FlowParamId).HasColumnName("FlowParamId").ValueGeneratedOnAdd();

            /// <summary>
            ///  流程id
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  参数名称
            /// </summary>
            builder.Property(x => x.FlowParamName).HasColumnName("FlowParamName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  参数类型
            /// </summary>
            builder.Property(x => x.ParamType).HasColumnName("ParamType").HasDefaultValue(0);

            /// <summary>
            /// 源id
            /// </summary>
            builder.Property(x => x.ResourceId).HasColumnName("ResourceId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  值类别
            /// </summary>
            builder.Property(x => x.ParamValueType).HasColumnName("ParamValueType").HasDefaultValue(0);

            /// <summary>
            ///  默认值
            /// </summary>
            builder.Property(x => x.DefaultValue).HasColumnName("DefaultValue").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x => x.id);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.IsNewAdd);
        }
    }
}
