﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class ActivityMap : ClassMap<ActivityEntity>
    {
        public override void Configure(EntityTypeBuilder<ActivityEntity>
            builder)
        {
            builder.ToTable<ActivityEntity>("WF_Activity");
          
            /// <summary>
            ///  步骤id
            /// </summary>
            builder.HasKey(x => x.ActivityId);

            builder.Property(x => x.ActivityId).HasColumnName("ActivityId").HasMaxLength(50);

            /// <summary>
            ///  流程id
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  步骤名称
            /// </summary>
            builder.Property(x => x.ActivityName).HasColumnName("ActivityName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  节点类别
            /// </summary>
            builder.Property(x => x.ActivityType).HasColumnName("ActivityType").HasDefaultValue(0);

            /// <summary>
            ///  接收人类别
            /// </summary>
            builder.Property(x => x.RecipientType).HasColumnName("RecipientType").HasDefaultValue(0);

            /// <summary>
            /// 组织机构Id
            /// </summary>
            builder.Property(x => x.OrgId).HasColumnName("OrgId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  接收人id
            /// </summary>
            builder.Property(x => x.RecipientId).HasColumnName("RecipientId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  接收人名称
            /// </summary>
            builder.Property(x => x.RecipientName).HasColumnName("RecipientName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            /// <summary>
            ///  最小请求数
            /// </summary>
            builder.Property(x => x.Mininum).HasColumnName("Mininum").HasDefaultValue(0);

            /// <summary>
            ///  激活是否发送消息
            /// </summary>
            builder.Property(x => x.IsActive).HasColumnName("IsActive").HasDefaultValue(0);

            /// <summary>
            ///  成功是否发送消息
            /// </summary>
            builder.Property(x => x.IsComplete).HasColumnName("IsComplete").HasDefaultValue(0);

            /// <summary>
            ///  驳回是否发送消息
            /// </summary>
            builder.Property(x => x.IsReject).HasColumnName("IsReject").HasDefaultValue(0);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.IsListener).HasColumnName("IsListener").HasDefaultValue(0);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.HttpUrl).HasColumnName("HttpUrl").HasMaxLength(500).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x => x.IsNewAdd);
        }
    }
}

