﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class FlowVersionMap : ClassMap<FlowVersionEntity>
    {
        public override void Configure(EntityTypeBuilder<FlowVersionEntity>
            builder)
        {
            builder.ToTable<FlowVersionEntity>("WF_FlowVersion");
           
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.FlowVersionId);

            builder.Property(x => x.FlowVersionId).HasColumnName("FlowVersionId").HasMaxLength(50);

            /// <summary>
            ///  流程id
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  版本号
            /// </summary>
            builder.Property(x => x.Version).HasColumnName("Version").HasDefaultValue(0);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            ///  dll注入
            /// </summary>
            builder.Property(x => x.ServiceClass).HasColumnName("ServiceClass").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500).IsRequired(false);

            /// <summary>
            ///  流程图
            /// </summary>
            builder.Property(x => x.Chart).HasColumnName("Chart").IsRequired(false);



            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}