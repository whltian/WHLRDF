﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class ProcessParamMap : ClassMap<ProcessParamEntity>
    {
        public override void Configure(EntityTypeBuilder<ProcessParamEntity>
            builder)
        {
            builder.ToTable<ProcessParamEntity>("WF_ProcessParam");
          
            /// <summary>
            ///  参数id
            /// </summary>
            builder.HasKey(x => x.ProcessParamId);

            builder.Property(x => x.ProcessParamId).HasColumnName("ProcessParamId").ValueGeneratedOnAdd();

            /// <summary>
            ///  实例id
            /// </summary>
            builder.Property(x => x.ProcessId).HasColumnName("ProcessId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  任务id
            /// </summary>
            builder.Property(x => x.ProcessTaskId).HasColumnName("ProcessTaskId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  流程参数id
            /// </summary>
            builder.Property(x => x.FlowParamId).HasColumnName("FlowParamId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  参数名称
            /// </summary>
            builder.Property(x => x.ParamName).HasColumnName("ParamName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  值
            /// </summary>
            builder.Property(x => x.ParamValue).HasColumnName("ParamValue").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.ParamType).HasColumnName("ParamType").HasDefaultValue(0);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}