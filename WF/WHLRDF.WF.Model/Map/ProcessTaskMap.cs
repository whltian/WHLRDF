﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class ProcessTaskMap : ClassMap<ProcessTaskEntity>
    {
        public override void Configure(EntityTypeBuilder<ProcessTaskEntity>
            builder)
        {
            builder.ToTable<ProcessTaskEntity>("WF_ProcessTask");
           
            /// <summary>
            ///  任务id
            /// </summary>
            builder.HasKey(x => x.ProcessTaskId);

            builder.Property(x => x.ProcessTaskId).HasColumnName("ProcessTaskId").HasMaxLength(50);

            /// <summary>
            ///  实例id
            /// </summary>
            builder.Property(x => x.ProcessId).HasColumnName("ProcessId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  源任务id
            /// </summary>
            builder.Property(x => x.FromTaskId).HasColumnName("FromTaskId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 任务名称
            /// </summary>
            builder.Property(x => x.TaskLabel).HasColumnName("TaskLabel").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  任务类别
            /// </summary>
            builder.Property(x => x.TaskType).HasColumnName("TaskType").HasDefaultValue(0);

            /// <summary>
            ///  节点id
            /// </summary>
            builder.Property(x => x.ActivityId).HasColumnName("ActivityId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  轮数
            /// </summary>
            builder.Property(x => x.TaskRound).HasColumnName("TaskRound").HasDefaultValue(0);

            /// <summary>
            ///  所有者
            /// </summary>
            builder.Property(x => x.OwnerUserId).HasColumnName("OwnerUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  发送人
            /// </summary>
            builder.Property(x => x.SenderUserId).HasColumnName("SenderUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  发送时间
            /// </summary>
            builder.Property(x => x.SendDate).HasColumnName("SendDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            ///  意见
            /// </summary>
            builder.Property(x => x.Description).HasColumnName("Description").HasMaxLength(500).IsRequired(false);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  操作人
            /// </summary>
            builder.Property(x => x.AssignUser).HasColumnName("AssignUser").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  执行时间
            /// </summary>
            builder.Property(x => x.EDate).HasColumnName("EDate").IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}
