﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class FlowTypeMap : ClassMap<FlowTypeEntity>
    {
        public override void Configure(EntityTypeBuilder<FlowTypeEntity>
            builder)
        {
            builder.ToTable<FlowTypeEntity>("WF_FlowType");
            
            builder.HasKey(x => x.FlowTypeId);
            builder.Property(x => x.FlowTypeId).HasColumnName("FlowTypeId").HasMaxLength(50);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.FlowTypeName).HasColumnName("FlowTypeName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  上级
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(500).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x => x.IsType);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.id);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.children);
            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.IsEdited);
            builder.Ignore(x => x.IsCheckOut);

        }
    }
}