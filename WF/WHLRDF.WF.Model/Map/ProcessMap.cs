﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.WF.Model
{
    public partial class ProcessMap : ClassMap<ProcessEntity>
    {
        public override void Configure(EntityTypeBuilder<ProcessEntity>
            builder)
        {
            builder.ToTable<ProcessEntity>("WF_Process");
          
            /// <summary>
            ///  实例id
            /// </summary>
            builder.HasKey(x => x.ProcessId);

            builder.Property(x => x.ProcessId).HasColumnName("ProcessId").HasMaxLength(50);

            /// <summary>
            ///  表单编号
            /// </summary>
            builder.Property(x => x.FromId).HasColumnName("FromId").HasMaxLength(20).IsRequired(true);

            /// <summary>
            ///  操作人
            /// </summary>
            builder.Property(x => x.OperatorId).HasColumnName("OperatorId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  所有者
            /// </summary>
            builder.Property(x => x.OwnerUserId).HasColumnName("OwnerUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  标题
            /// </summary>
            builder.Property(x => x.Title).HasColumnName("Title").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  流程id
            /// </summary>
            builder.Property(x => x.FlowId).HasColumnName("FlowId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  版本号
            /// </summary>
            builder.Property(x => x.Version).HasColumnName("Version").HasDefaultValue(0);

            /// <summary>
            ///  子流程
            /// </summary>
            builder.Property(x => x.IsSubProcess).HasColumnName("IsSubProcess").HasDefaultValue(0);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.StartDate).HasColumnName("StartDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.EndDate).HasColumnName("EndDate").IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}
