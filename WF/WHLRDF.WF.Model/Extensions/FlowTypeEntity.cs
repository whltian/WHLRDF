﻿using System;
using System.Data;

namespace WHLRDF.WF.Model
{

    public partial class FlowTypeEntity 
    {
        public int IsType { get; set; }

        public string parent { get; set; }

        public string id { get => this.FlowTypeId; }
        public string text { get => this.FlowTypeName; }

        public bool children { get; set; }

        private string _icon = "fa fa-check";

        public virtual string icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
            }
        }
        public bool IsEdited { get; set; }

        public bool IsCheckOut { get; set; }
    }
}
