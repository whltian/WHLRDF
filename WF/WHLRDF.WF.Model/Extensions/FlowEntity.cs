﻿using System;
using System.Data;

namespace WHLRDF.WF.Model
{
    /// <summary>
    /// 流程管理
    /// </summary>
    public partial class FlowEntity 
    {
       public virtual bool IsNewAdd { get; set; }
        private  string _version = "0";
        public virtual string Version { get {
                return _version;
            } set {
                _version = value;
            }
        }

        public bool IsEdited { get; set; }

    }
}
