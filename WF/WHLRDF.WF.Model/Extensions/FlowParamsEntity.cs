﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.Model
{
    public partial class FlowParamsEntity
    {
        public virtual string id { get => this.FlowParamId; }

        public virtual string text { get => this.FlowParamName; }


        public virtual string icon { get => "fa fa-gear"; }

        public virtual bool IsNewAdd { get; set; }


    }
}
