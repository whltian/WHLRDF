﻿
using System;
using System.Data;
using WHLRDF.ORM;

namespace WHLRDF.WF.Model
{

    public partial class FlowRulesEntity 
    {
        public string id { get => this.FlowRuleId; }

        public string parent { get => "0"; }

        public string text { get => this.FlowRuleName; }
        private bool _isnewAdd { get; set; }
        public virtual bool IsNewAdd {
            get {
                return _isnewAdd;
            }
            set {
                _isnewAdd = value;
            }
        }

        public virtual GroupFilter group { get; set; }

    }
}
