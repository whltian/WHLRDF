﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL.Domain
{
    public class MyTaskEntity
    {
        public string ProcessId { get; set; }

        public string Title { get; set; }

        public int ProcessStatus { get; set; }

        public int TaskStatus { get; set; }

        public string ProcessTaskId { get; set; }

        public string TaskLabel { get; set; }

        public string OperatorId { get; set; }

        public string OwnerUserId { get; set; }

        public string ActivityId { get; set; }

        public string Description { get; set; }

        public DateTime LastModifyDate { get; set; }
        public DateTime SendDate { get; set; }

        public DateTime EDate { get; set; }
    }
}
