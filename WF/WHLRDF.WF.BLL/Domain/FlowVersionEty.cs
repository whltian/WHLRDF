﻿using WHLRDF.WF.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL.Domain
{
    public class FlowVersionEty
    {
        public FlowVersionEntity VersionEntity { get; set; }

        public FlowEntity flowEty { get; set; }

        public List<ActivityEntity> activities { get; set; }

        public List<FlowParamsEntity> flowParams { get; set; }

        public List<FlowRulesEntity> flowRules { get; set; }

        public IFlowEventListener EventListener
        {
            get
            {
                if (VersionEntity != null)
                {
                    var instance = ReflectionHelper.Create(VersionEntity.ServiceClass);
                    if (instance != null)
                    {
                        if (instance is IFlowEventListener)
                        {
                            return instance as IFlowEventListener;
                        }
                    }
                }
                return null;
            }
        }
    }
}
