﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace WHLRDF.WF.BLL
{


   
    public enum RebackTaskSubmitTypeEnum
    {
        /// <summary>
        /// 直接提交给回退节点
        /// </summary>
        ToRebackPhase = 1,
        /// <summary>
        /// 根据流程配置，提交给阶段的后续节点
        /// </summary>
        ToNextPhase = 2
    }
    public enum TaskTypeEnum
    {
        /// <summary>
        /// 普通任务
        /// </summary>
        NormalTask = 1,
        /// <summary>
        /// 传阅任务,无需办理
        /// </summary>
        MissionTask = 2,
        /// <summary>
        /// 队列
        /// </summary>
        Queue=3,

        /// <summary>
        /// 子流程
        /// </summary>
        SubProcessTask =4,
    }
    public enum TaskMakeTypeEnum
    {
        /// <summary>
        /// 提交
        /// </summary>
        Submit = 1,
        /// <summary>
        /// 回退
        /// </summary>
        Reback = 2,
        /// <summary>
        /// 取回
        /// </summary>
        Retrieve = 3
    }
    public enum TaskStateEnum
    {
        /// <summary>
        /// 已激活
        /// </summary>
        [Description("已激活")]
        Activated = 1,
        /// <summary>
        /// 未激活
        /// </summary>
        [Description("未激活")]
        NotActive = 0,
        /// <summary>
        /// 已执行
        /// </summary>
        [Description("已执行")]
        Executed =2,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        Paused = 5
    }
    public class FlowEnum
    {
        public static string GetInsStateString(InsStateEnum insState)
        {
            if (insState == InsStateEnum.Start)
                return "启动中";
            else if (insState == InsStateEnum.Normal)
                return "办理中";
            else if (insState == InsStateEnum.Pause)
                return "暂停";
            else if (insState == InsStateEnum.End)
                return "正常结束";
            else if (insState == InsStateEnum.Stop)
                return "终止";
            else
                return "";
        }
    }
    public enum InsStateEnum
    {
        /// <summary>
        /// 启动中
        /// </summary>
        [Description("启动中")]
        Start = 1,
        /// <summary>
        /// 办理中
        /// </summary>
        [Description("办理中")]
        Normal = 2,
        /// <summary>
        /// 暂停
        /// </summary>
        [Description("暂停")]
        Pause = 3,
        /// <summary>
        /// 正常结束
        /// </summary>
        [Description("正常结束")]
        End = 4,
        /// <summary>
        /// 终止
        /// </summary>
        [Description("终止")]
        Stop = 5
    }
    public enum FlowTypeEnum
    {
        /// <summary>
        /// 任意式工作流
        /// </summary>
        FreeFlow = 1,
        /// <summary>
        /// 顺序式工作流
        /// </summary>
        OrderFlow = 2
    }
    public enum FlowVerStateEnum
    {
        /// <summary>
        /// 修改中
        /// </summary>
        Editing = 0,
        /// <summary>
        /// 已发布
        /// </summary>
        Release = 1
    }
    public enum FlowRebackModelEnum
    {
        /// <summary>
        /// 任意节点回退均回退到发起节点
        /// </summary>
        ToFirst = 1,
        /// <summary>
        /// 回退到上一节点
        /// </summary>
        ToPre = 2
    }

    public enum PhaseTypeEnum
    {
        /// <summary>
        /// 开始节点
        /// </summary>
        [Description("Start")]
        start = 1,
        /// <summary>
        /// 普通节点
        /// </summary>
        [Description("普通")]
        normal = 2,
        /// <summary>
        /// 判断节点
        /// </summary>
        [Description("判断")]
        judge = 3,
      
        /// <summary>
        /// 队列
        /// </summary>
        [Description("队列")]
        queue = 4,
        /// <summary>
        /// 结束节点
        /// </summary>
        [Description("End")]
        end = 5,
       

        /// <summary>
        /// 子流程
        /// </summary>
        [Description("子流程")]
        subprocess = 11,

        /// <summary>
        /// 队列
        /// </summary>
        [Description("事件")]
        ServerTask = 12,

        /// <summary>
        /// 13
        /// </summary>
        [Description("邮件")]
        mail = 13,
    }
    public enum UserSelScaleEnum
    {
        /// <summary>
        /// 所有用户
        /// </summary>
        AllUser = 1,
        /// <summary>
        /// 指定 用户
        /// </summary>
        AppointUser = 2
    }

    public enum LinkConditionTypeEnum
    {
        /// <summary>
        /// 没有条件
        /// </summary>
        NoCondition = 0,
        /// <summary>
        /// 根据表达式判断
        /// </summary>
        ByExpression = 1,
        /// <summary>
        /// SQL语句判断，支持表达式替换
        /// </summary>
        BySql = 2,
        /// <summary>
        /// 3：扩展类
        /// </summary>
        ByClass = 3
    }


    public enum LinkTypeEnum
    {
        /// <summary>
        /// 提交任务
        /// </summary>
        Submit = 0,
        /// <summary>
        /// 回退任务
        /// </summary>
        Reject = 1
    }

    public enum RuleCompareTypeEnum
    {
        BySql = 1,
        ByClass = 2
    }



    public enum HandleApplyTypeEnum
    {
        /// <summary>
        /// 规则满足时
        /// </summary>
        RuleSatisfaction = 1,
        /// <summary>
        /// 规则不满足时
        /// </summary>
        RuleNotSatisfaction = 2
    }

    public enum HandleTypeEnum
    {
        /// <summary>
        /// 直接自动启动流程
        /// </summary>
        StartFlow = 1,
        /// <summary>
        /// 执行自定义扩展类
        /// </summary>
        ExecuteClass = 2,
        /// <summary>
        /// 给出提示信息，不允许用户提交或回退流程
        /// </summary>
        Alert = 3,
        /// <summary>
        /// 询问用户是否提交或回退流程
        /// </summary>
        Confirm = 4,
        /// <summary>
        /// 询问用户是否启动某个流程
        /// </summary>
        AskStartFlow = 5
    }

    public enum InitFormMethodTypeEnum
    {
        /// <summary>
        /// 通过SQL语句初始化
        /// </summary>
        BySql = 1,
        /// <summary>
        /// 通过扩展类初始化
        /// </summary>
        ByClass = 2
    }

    public enum WFParamType
    {
        /// <summary>
        /// 流程参数
        /// </summary>
        Flow = 0,
        /// <summary>
        /// 节点参数
        /// </summary>
        Activity = 1
    }

    public enum RecipientType
    {
        /// <summary>
        /// 用户
        /// </summary>
        [Description("单用户")]
        User = 1,
        /// <summary>
        /// 岗位
        /// </summary>
        [Description("岗位")]
        JobFunction = 2,

        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        Department = 3,

        /// <summary>
        /// 组
        /// </summary>
        [Description("组")]
        Group = 4,



        [Description("参数")]
        Dynamic_Recipient = 6,

        [Description("参数的上级")]
        Supervisor_Dynamic_Recipient = 7,

        [Description("参数的经理")]
        Manager_Dynamic_Recipient = 8,


        /// <summary>
        /// 上级
        /// </summary>
        [Description("步骤上级")]
        Supervisor = 21,
        /// <summary>
        /// 部门管理者
        /// </summary>
        [Description("步骤部门经理")]
        ManagerStep = 22,

        [Description("步骤接收人")]
        RecipientStep = 23,
        /// <summary>
        /// 上一个接收人
        /// </summary>
        [Description("上一步人员")]
        PreviousStep = 31,

        [Description("上一步上级")]
        Supervisor_Previous_Step = 32,

        [Description("上一步经理")]
        Manager_Previous_Step = 33,

        [Description("流程发起者")]
        SenderStep = 34,

        [Description("所有人")]
        Everyone = 41,
        [Description("匿名用户")]
        AllowAnonymous = 42
    }
}

