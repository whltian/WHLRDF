﻿using WHLRDF.WF.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL.Domain
{

    /// <summary>
    /// 流程图对象
    /// </summary>
    public class FlowDesigner
    {
        public Dictionary<string, FlowDesignerPhase> states { get; set; }
        public Dictionary<string, FlowDesignerPhaseLink> paths { get; set; }

        public Dictionary<string,ActivityEntity> activities { get; set; }

        public Dictionary<string,FlowParamsEntity> flowParams { get; set; }

        public FlowVerStateEnum FlowVerState { get; set; }

        public Dictionary<string, Dictionary<string,FlowRulesEntity>> flowRules { get; set; }
        public FlowEntity Flow { get; set; }
        private FlowMaxNumber _flowMax;
        public FlowMaxNumber flowMax { get => _flowMax; set => _flowMax = value; }
    }
    #region 阶段
    public class FlowDesignerPhaseText
    {
        public string text { get; set; }
    }
    public class FlowDesignerPhaseAttr
    {
        public double x { get; set; }
        public double y { get; set; }
        public double width { get; set; }
        public double height { get; set; }

    }
    public class FlowDesignerPhaseprops
    {
        public FlowDesignerPhasepropstemp1 temp1 { get; set; }

    }
    public class FlowDesignerPhasepropstemp1
    {
        public string value { get; set; }
    }
    public class FlowDesignerPhase
    {
        public FlowDesignerPhase()
        {
            this.IsActive = false;
        }
        public string type { get; set; }
        public static string ConvertToDesignerType(PhaseTypeEnum phaseType)
        {
            string stype = "";
            if (phaseType == PhaseTypeEnum.start)
            {
                stype = "start";
            }
            else if (phaseType == PhaseTypeEnum.normal)
            {
                stype = "normal";

            }
            else if (phaseType == PhaseTypeEnum.subprocess)
            {
                stype = "subprocess";
            }
            else if (phaseType == PhaseTypeEnum.judge)
            {
                stype = "judge";
            }
            else if (phaseType == PhaseTypeEnum.end)
            {
                stype = "end";
            }
            else
            {
                stype = "normal";
            }
            return stype;
        }
        public PhaseTypeEnum ConvertTypeToPhaseType()
        {
            if (type.Equals("start"))
                return PhaseTypeEnum.start;
            else if (type.Equals("task"))
            {
                return PhaseTypeEnum.normal;
            }
            else if (type.Equals("subprocess"))
            {
                return PhaseTypeEnum.subprocess;
            }
            else if (type.Equals("judge"))
            {
                return PhaseTypeEnum.judge;
            }
            else if (type.Equals("end"))
            {
                return PhaseTypeEnum.end;
            }
            else
            {
                return PhaseTypeEnum.normal;
            }

        }
        public FlowDesignerPhaseText text { get; set; }
        public FlowDesignerPhaseAttr attr { get; set; }
       // public FlowDesignerPhaseprops props { get; set; }
        /// <summary>
        /// 流程实例是否处于当前节点，主要为流程监控用
        /// </summary>
        public bool IsActive { get; set; }

    }
    #endregion

    #region 路由
    public class FlowDesignerPhaseLinkText
    {
        public string text { get; set; }
        public FlowDesignerPhaseLinkTextPos textPos { get; set; }

    }
    public class FlowDesignerPhaseLinkTextPos
    {
        public double x { get; set; }
        public double y { get; set; }
    }
    public class FlowDesignerPhaseLinkDot
    {
        public double x { get; set; }
        public double y { get; set; }
    }
    public class FlowDesignerPhaseLinkprops
    {
        public FlowDesignerPhasepropstemp1 temp1 { get; set; }

    }
    public class FlowDesignerPhaseLink
    {
        public FlowDesignerPhaseLink()
        {
            this.IsActive = false;
        }
        public string lineType { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public List<FlowDesignerPhaseLinkDot> dots { get; set; }

        public string path_p { get; set; }

        public string path_arr { get; set; }
        public FlowDesignerPhaseLinkText text { get; set; }

        public string pathKey { get; set; }
        //public FlowDesignerPhaseLinkprops props
        //{
        //    get; set;
        //}

        
        /// <summary>
        /// 路径是否经过，主要为流程监控用
        /// </summary>
        public bool IsActive { get; set; }

    }

    public class FlowMaxNumber {
        public int ActivityNum = 3;

        public int FlowRuleNum = 1;

        public int FlowParamNum = 1;

        public int PathNum = 1;


    }
    #endregion
}
