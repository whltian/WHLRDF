﻿using WHLRDF.WF.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL.Domain
{
    public class ProcessInstance
    {
        public ProcessEntity Process { get; set; }

        public List<ProcessParamEntity> lstParams { get; set; }

        public List<ProcessTaskEntity> taskEntities { get; set; }

        public List<ProcessParamEntity> taskParams { get; set; }

        public FlowParameterCollection flowParameters { get; set; }

        public FlowVersionEty Flow { get; set; }
    }
}
