﻿using System.Collections.Generic;
using WHLRDF.WF.BLL.Domain;
using WHLRDF.WF.Model;

namespace WHLRDF.WF.BLL
{
    public partial interface IFlowService
    {

        /// <summary>
        /// 获取所有的临时流程
        /// </summary>
        /// <returns></returns>
        List<FlowDesigner> GetTempAllFlow();

        /// <summary>
        /// 获取单个流程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        FlowDesigner GetFlowById(string id, int version = 0);

        /// <summary>
        /// 保存流程
        /// </summary>
        /// <param name="flowDesigner"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveWorkflow(FlowDesigner flowDesigner, ref string strError);

        /// <summary>
        /// 保存流程参数
        /// </summary>
        /// <param name="flowParamsEntity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveParams(FlowParamsEntity flowParamsEntity, ref string strError);

        /// <summary>
        /// 保存流程基本信息
        /// </summary>
        /// <param name="flowEntity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveFlow(FlowEntity flowEntity, ref string strError);

        /// <summary>
        /// 删除流程参数
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="paramId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool DeleteParams(string flowid, string paramId, ref string strError);

        /// <summary>
        /// 保存节点
        /// </summary>
        /// <param name="activityEntity">节点实体</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool SaveActivity(ActivityEntity activityEntity, ref string strError);

        /// <summary>
        /// 删除流程节点
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="activityId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool DeleteActivity(string flowid, string activityId, ref string strError);

        /// <summary>
        /// 保存流程节点规则
        /// </summary>
        /// <param name="flowRulesEntity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveRules(FlowRulesEntity flowRulesEntity, ref string strError);

        /// <summary>
        /// 删除流程节点规则
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="activityId"></param>
        /// <param name="ruleid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool DeleteRule(string flowid, string activityId, string ruleid, ref string strError);

        /// <summary>
        /// 迁出
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool CheckOut(string flowId, ref string strError);

        /// <summary>
        /// 迁出
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool CheckIn(string flowId, ref string strError);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="isType"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool Delete(string id, int isType,ref string strError);

        /// <summary>
        /// 验证workflow是否成功
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool CheckWorkflow(string flowid, ref string strError);

        /// <summary>
        /// 获取编号
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="isType"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        string GetMaxNumber(string flowId, int isType, ref string strError);

        /// <summary>
        /// 发布
        /// </summary>
        /// <param name="flowid">流程id</param>
        /// <param name="version">版本号</param>
        /// <param name="strError">错误信息</param>
        /// <param name="isCover">是否覆盖</param>
        /// <returns></returns>
        bool Publish(string flowid, string version, ref string strError, bool isCover = false);

        /// <summary>
        /// 撤销
        /// </summary>
        /// <param name="flowid">流程id</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool Revoke(string flowid, ref string strError);

    }
}