﻿using WHLRDF.ORG.Model;
using WHLRDF.ORG.BLL;
using System.Collections.Generic;
using WHLRDF.ORM;

namespace WHLRDF.WF.BLL
{
    public class OrgFlowService : SerivceBase,IORGService
    {
        public string OrgId { get; set; }

        private OrganizationsEntity _organizations;
        public OrganizationsEntity OrgEntity
        {
            get {
                if (_organizations == null)
                {
                    _organizations = this.GetById<OrganizationsEntity>(OrgId);
                }
                return _organizations;
            }
        }

        private IORGService _orgService;
        /// <summary>
        /// 获取接口类
        /// </summary>
        /// <returns></returns>
        public IORGService  OrgService
        {
            get {
                if (_orgService == null)
                {
                    if (OrgEntity != null)
                    {
                        if (OrgEntity.IsCustom)
                        {

                        }
                        else
                        {
                            _orgService = new ORGService();
                        }
                    }
                    else
                    {
                        _orgService = new ORGService();
                    }
                   
                }
                return _orgService;
            }
        }

        /// <summary>
        /// 验证组织机构用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool CheckOrgUser(string orgId,string userid)
        {
            if (OrgService != null)
            {
               return OrgService.CheckOrgUser(orgId,userid);
            }
            return false;
        }

        public bool CheckGroupUser(string orgId, string userid, string group)
        {
            if (OrgService != null)
            {
                return OrgService.CheckGroupUser(orgId,userid,group);
            }
            return false;
        }


        public bool CheckDepartmentUser(string orgId, string userid, string departmentId)
        {
            if (OrgService != null)
            {
                return OrgService.CheckDepartmentUser(orgId, userid, departmentId);
            }
            return false;
        }

        public bool CheckJobFunctionUser(string orgId, string userid, string jobFunctionId)
        {
            if (OrgService != null)
            {
                return OrgService.CheckJobFunctionUser(orgId, userid, jobFunctionId);
            }
            return false;
        }
        /// <summary>
        /// 获取组用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public List<string> GetUserByGroup(string orgId,string group)
        {
            if (OrgService != null)
            {
                return OrgService.GetUserByGroup(orgId,group);
            }
            return null;
        }

        /// <summary>
        /// 获取岗位
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserByJobFunction(string orgId, string job )
        {
            if (OrgService != null)
            {
                return OrgService.GetUserByJobFunction(orgId,job);
            }
            return null;
        }

        /// <summary>
        /// 获取部门
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public List<string> GetUserByDepartment(string orgId, string departmentId )
        {
            if (OrgService != null)
            {
                return OrgService.GetUserByDepartment(orgId,departmentId);
            }
            return null;
        }

        /// <summary>
        /// 获取上级
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserBySuperior(string orgId, string job )
        {
            if (OrgService != null)
            {
                return OrgService.GetUserBySuperior(orgId,job);
            }
            return null;
        }



        /// <summary>
        /// 获取部门经理
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserByManager(string orgId, string job )
        {
            if (OrgService != null)
            {
                return OrgService.GetUserByManager(orgId,job);
            }
            return null;
        }
        /// <summary>
        /// 获取用户岗位
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetJobFunctionByUser(string orgId, string userid )
        {
            if (OrgService != null)
            {
                return OrgService.GetJobFunctionByUser(orgId,userid);
            }
            return null;
        }


        /// <summary>
        /// 获取用户部门
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetDepartmentByUser(string orgId, string userid )
        {
            if (OrgService != null)
            {
                return OrgService.GetDepartmentByUser(orgId,userid);
            }
            return null;
        }


        /// <summary>
        /// 获取参数上级
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetSuperiorByUser(string orgId, string userid)
        {
            string job = OrgService.GetJobFunctionByUser(orgId, userid);
            if (!string.IsNullOrWhiteSpace(job))
            {
                string user = OrgService.GetUserBySuperior(orgId, job);
                return user;
            }
            return "";
        }

        /// <summary>
        /// 获取管理员
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetManagerByUser(string orgId, string userid)
        {
            string job = OrgService.GetJobFunctionByUser(orgId, userid);
            if (!string.IsNullOrWhiteSpace(job))
            {
                string user = OrgService.GetUserByManager(orgId, job);
                return user;
            }
            return "";
        }
    }
}
