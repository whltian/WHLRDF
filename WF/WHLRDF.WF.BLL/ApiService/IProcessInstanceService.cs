﻿using WHLRDF.WF.BLL.Domain;
using WHLRDF.WF.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL
{
    public interface IProcessInstanceService
    {

        /// <summary>
        /// 启动流程
        /// </summary>
        /// <param name="flowid">流程id</param>
        /// <param name="fromId">表单id</param>
        /// <param name="fromTitle">标题</param>
        /// <param name="senderUser">发送人</param>
        /// <param name="parameters">参数</param>
        /// <param name="instanceId">实例id</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool Start(string flowid, string fromId, string fromTitle, string senderUser,
          FlowParameterCollection parameters, ref string instanceId, ref string strError);

        /// <summary>
        /// 执行任务
        /// </summary>
        /// <param name="taskid">任务id</param>
        /// <param name="varParameters">参数</param>
        /// <param name="typeEnum">枚举</param>
        /// <param name="senderUserId">发送人</param>
        /// <param name="description">意见</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool RunTask(string taskid,
            FlowParameterCollection varParameters,
            LinkTypeEnum typeEnum,
            string senderUserId,
            string description,
            ref string strError );


        /// <summary>
        /// 实例终止
        /// </summary>
        /// <param name="instanceId">实例id</param>
        /// <param name="userid">用户id</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool Abort(string instanceId, string userid, ref string strError);

        /// <summary>
        /// 通过task获取实例
        /// </summary>
        /// <param name="taskid"></param>
        /// <returns></returns>
        ProcessTaskEntity GetTaskById(string taskid);

        /// <summary>
        /// 获取流程实例
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        ProcessInstance GetInstance(string instanceId);

        /// <summary>
        /// 根据版本号获取流程
        /// </summary>
        /// <param name="flowid">流程</param>
        /// <param name="version"></param>
        /// <returns></returns>
        FlowVersionEty GetFlow(string flowid, int version = 0);

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        FlowDesigner GetFlowDesignerByInstance(string instanceId);

        /// <summary>
        /// 获取待办事项
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        LigerGrid MyTask(LigerGrid ligerGrid, string userid);


        /// <summary>
        /// 获取所有任务
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        LigerGrid MyTodoList(LigerGrid ligerGrid, string userid);

        /// <summary>
        /// 任务指派
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool TaskAssignUser(string taskId, string userid, ref string strError);

        /// <summary>
        /// 激活队列任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool ActiveQueueTask(string taskId, string userid, ref string strError);

        /// <summary>
        /// 取消激活任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool UnActiveQueueTask(string taskId, string userid, ref string strError);


    }
}
