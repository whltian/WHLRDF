﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.WF.BLL.Domain;
using WHLRDF.WF.Model;

namespace WHLRDF.WF.BLL
{
    public class LeaveFlowEventListener : IFlowEventListener
    {
        public void ActivityListener(
            ProcessInstance instance,
            ActivityEntity activityEntity,
            ProcessTaskEntity taskEntity, 
            TaskStateEnum stateEnum)
        {
            string strError = stateEnum.GetHashCode().ToString();
        }

        public void ProcessListener(ProcessInstance instance, 
            InsStateEnum stateEnum)
        {
            string strError = stateEnum.GetHashCode().ToString();
        }
    }
}
