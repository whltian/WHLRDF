﻿using WHLRDF.WF.BLL.Domain;
using WHLRDF.WF.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL
{
    /// <summary>
    /// 流程实例事件接口
    /// </summary>
    public interface IFlowEventListener
    {
        /// <summary>
        /// 流程事件
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="stateEnum"></param>
        void ProcessListener(ProcessInstance instance, 
            InsStateEnum stateEnum);

        /// <summary>
        /// 节点事件执行
        /// </summary>
        /// <param name="instance">流程</param>
        /// <param name="activityEntity">任务节点</param>
        /// <param name="fromTask">来源task</param>
        /// <param name="stateEnum">状态</param>
        void ActivityListener(ProcessInstance instance,
            ActivityEntity activityEntity,
            ProcessTaskEntity taskEntity,
            TaskStateEnum stateEnum);
    }
}
