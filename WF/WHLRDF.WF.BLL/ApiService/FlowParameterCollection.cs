﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.WF.BLL
{
    public class FlowParameter
    {
        public FlowParameter()
        {
        }
        public FlowParameter(string name,string value)
        {
            this.ParameterName = name;
            this.ParameterValue = value;
        }
        public string ParameterName { get; set; }

        public string ParameterValue { get; set; }
    }

    public class FlowParameterCollection : List<FlowParameter>
    {
        public FlowParameterCollection() {

        }

        public void Add(string name,string value)
        {
            this.Add(new FlowParameter(name,value));
        }
    }
}
