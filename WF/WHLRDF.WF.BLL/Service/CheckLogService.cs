﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.WF.BLL.Domain;
using WHLRDF.WF.Model;
namespace WHLRDF.WF.BLL
{
    public class CheckLogService : SerivceBase, ICheckLogService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<CheckLogEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(CheckLogEntity.__IsDeleted, false);
            return this.Query<CheckLogEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public CheckLogEntity GetById(string id)
        {
            return this.GetById<CheckLogEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(CheckLogEntity entity, ref string strError)
        {
           
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<CheckLogEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                CheckLogEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return this.Query<CheckLogEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 获取log日志实体
        /// </summary>
        /// <param name="deptId">部门id</param>
        /// <param name="status">状态</param>
        /// <returns></returns>
        public CheckLogEntity GetLogEntity(string deptId, int status)
        {
            ICriterion criter = Expression.Eq(CheckLogEntity.__IsDeleted, false);
            criter = Expression.And(criter, Expression.And(Expression.Eq(CheckLogEntity.__Status, status), Expression.Eq(CheckLogEntity.__FlowId, deptId)));
            return this.Query<CheckLogEntity>(criter).FirstOrDefault();
        }

        public FlowDesigner GetFlowDesignerById(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            var entity = this.GetById(id);
            if (entity != null)
            {
                if (!string.IsNullOrWhiteSpace(entity.Chart))
                {
                    return JSONHelper.FromJson<FlowDesigner>(entity.Chart);
                }
               
            }
            return null;
        }

        /// <summary>
        /// 日志保存方法
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="localName"></param>
        /// <param name="status"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public CheckLogEntity Save(string flowid, string localName, int status, ref string strError)
        {
            CheckLogEntity entity = new CheckLogEntity()
            {
                FlowId = flowid,
                Status = status,
                LastModifyDate = DateTime.Now,
                ActionUserId = ApplicationEnvironments.DefaultSession.UserId,
                LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId,
                FlowName = localName

            };
            if (status > 0)
            {
                ICriterion criter = Expression.And(Expression.Eq(CheckLogEntity.__Status, 0), Expression.And(Expression.Eq(CheckLogEntity.__FlowId, flowid),
                    Expression.Eq(CheckLogEntity.__IsDeleted, false)));
                entity = this.Query<CheckLogEntity>(criter).FirstOrDefault();
                if (entity == null)
                {
                    strError = "数据异常，请联系管理员";
                    return null;
                }
            }
            else
            {
                entity.CreateBy = ApplicationEnvironments.DefaultSession.UserId;
                entity.CreateDate = DateTime.Now;

                entity.CheckLogId = Guid.NewGuid().ToString();
            }
            entity.Status = status;
            entity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
            entity.LastModifyDate = DateTime.Now;

            return entity;

        }
        #endregion

    }
}