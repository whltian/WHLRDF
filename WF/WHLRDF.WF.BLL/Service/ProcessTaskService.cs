﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.WF.Model;
namespace WHLRDF.WF.BLL
{
    public class ProcessTaskService : SerivceBase, IProcessTaskService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<ProcessTaskEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(ProcessTaskEntity.__IsDeleted, false);
            return this.Query<ProcessTaskEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public ProcessTaskEntity GetById(string id)
        {
            return this.GetById<ProcessTaskEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(ProcessTaskEntity entity, ref string strError)
        {
            //if (Exist(entity.Name, entity.Id))
            //{
            //    strError = entity.OrgName + "已存在，请不要重复输入";
            //    return false;
            //}
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<ProcessTaskEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                ProcessTaskEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return this.Query<ProcessTaskEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        ///// <summary>
        ///// 验证是否存在的方法
        ///// <param name="name">需要验证的值</param>
        ///// <param name="key">主键</param>
        ///// </summary>
        //public bool Exist(string name, string key)
        //{
        //    key = (string.IsNullOrWhiteSpace(key)) ? "" : key;

        //    ICriterion criter = Expression.Eq("IsDeleted", false);
        //    criter =
        //        Expression.And(criter, Expression.And(Expression.Eq(ProcessTaskEntity.__Name, name),
        //        Expression.Not(Expression.Eq(ProcessTaskEntity._PrimaryKeyName, key)))
        //        );
        //    var result = this.Query<ProcessTaskEntity>(criter).FirstOrDefault();
        //    return result != null;
        //}
        #endregion

    }
}