﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.WF.Model;
namespace WHLRDF.WF.BLL
{
    public class FlowVersionService : SerivceBase, IFlowVersionService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<FlowVersionEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(FlowVersionEntity.__IsDeleted, false);
            return this.Query<FlowVersionEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public FlowVersionEntity GetById(string id)
        {
            return this.GetById<FlowVersionEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(FlowVersionEntity entity, ref string strError)
        {
            if (entity.Version <= 0)
            {
                strError = entity.Version + "版本号值不能为负数";
                return false;
            }
            if (Exist(entity.Version, entity.FlowVersionId, entity.FlowId))
            {
                strError = entity.Version + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.FlowVersionId))
            {
                entity.FlowVersionId = entity.FlowId + "-" + entity.Version.ToString();
            }
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<FlowVersionEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                FlowVersionEntity._PrimaryKeyName 
                                
                           }, grid.keyWord)
                           );
            }
            return this.Query<FlowVersionEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(int fversion, string key,string flowid)
        {
         
          
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.And(Expression.Eq(FlowVersionEntity.__Version, fversion),Expression.Eq(FlowVersionEntity.__FlowId,flowid)),
                Expression.NotEq(FlowVersionEntity._PrimaryKeyName, key))
                );
            var result = this.Query<FlowVersionEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}