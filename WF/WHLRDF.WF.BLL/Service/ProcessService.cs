﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.WF.Model;
namespace WHLRDF.WF.BLL
{
    public class ProcessService : SerivceBase, IProcessService
    {
        public const string Process_Rule_No = "0013";
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<ProcessEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(ProcessEntity.__IsDeleted, false);
            return this.Query<ProcessEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public ProcessEntity GetById(string id)
        {
            return this.GetById<ProcessEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(ProcessEntity entity, ref string strError)
        {
            // entity.OrgId = new WHLRDF.BLL.SerialRuleService().CreateSerialNo(Organizations_Identity_Num, null, ref strError);// 主键生成值
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<ProcessEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                ProcessEntity._PrimaryKeyName ,
                                ProcessEntity.__Title,
                                ProcessEntity.__FlowId
                           }, ligerGrid.keyWord)
                           );
            }
            string flowId = ligerGrid.GetValue("FlowId");
            if (!string.IsNullOrWhiteSpace(flowId))
            {
                criter = Expression.And(criter, Expression.Eq( ProcessEntity.__FlowId, flowId));
            }
            string Status = ligerGrid.GetValue("Status");
            if (!string.IsNullOrWhiteSpace(Status))
            {
                criter = Expression.And(criter, Expression.Eq(ProcessEntity.__Status, Status));
            }
            return this.Query<ProcessEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        
        #endregion

    }
}