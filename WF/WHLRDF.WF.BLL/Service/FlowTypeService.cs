﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.WF.Model;
namespace WHLRDF.WF.BLL
{
    public class FlowTypeService : SerivceBase, IFlowTypeService
    {
        public const string FlowType_Identity_Num = "0011";

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<FlowTypeEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(FlowTypeEntity.__IsDeleted, false);
            return this.Query<FlowTypeEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public FlowTypeEntity GetById(string id)
        {
            return this.GetById<FlowTypeEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(FlowTypeEntity entity, ref string strError)
        {
            if (Exist(entity.FlowTypeName, entity.FlowTypeId))
            {
                strError = entity.FlowTypeName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.FlowTypeId))
            {
                entity.FlowTypeId = SerialService.CreateSerialNo(FlowType_Identity_Num, null, ref strError);// 主键生成值
            }
            
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<FlowTypeEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                FlowTypeEntity._PrimaryKeyName ,
                                FlowTypeEntity.__FlowTypeName
                           }, grid.keyWord)
                           );
            }
            return this.Query<FlowTypeEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string flowtypeName, string key)
        {
            key = (string.IsNullOrWhiteSpace(key)) ? "" : key;

            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(FlowTypeEntity.__FlowTypeName, flowtypeName),
                Expression.NotEq(FlowTypeEntity._PrimaryKeyName, key))
                );
            var result = this.Query<FlowTypeEntity>(criter).FirstOrDefault();
            return result != null;
        }

        public List<FlowTypeEntity> GetChildren(string parentId)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (string.IsNullOrWhiteSpace(parentId) || parentId.Equals("0"))
            {
                criter =
                    Expression.And(criter, Expression.Or(Expression.Eq(FlowTypeEntity.__ParentId, ""),
                   Expression.IsNull(FlowTypeEntity.__ParentId))
                    );
            }
            else
            {
                criter =  Expression.And(criter, Expression.Eq(FlowTypeEntity.__ParentId, parentId));
                   
            }
            return  this.Query<FlowTypeEntity>(criter);
        }
        #endregion



    }
}