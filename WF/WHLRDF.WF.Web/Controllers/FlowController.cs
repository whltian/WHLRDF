﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.WF.BLL;
using WHLRDF.WF.Model;
using WHLRDF.WF.BLL.Domain;
using WHLRDF.Application.BLL;

namespace WHLRDF.WF.Web.Controllers
{
    [MenuPage(10, "010", "流程管理", "WF/Flow", "")]
    public class FlowController : AreaController
    {
        #region Service注入

        public IFlowService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        ///</summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false)]
      
        public IActionResult Edit(string id, FlowEntity entity)
        {
            if (!this.IsPost)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    entity = mService.GetById(id);
                }
                if (entity == null)
                {
                    entity = new FlowEntity
                    {

                    };
                }
            }
            else
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.SaveFlow(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            return Json(AjaxResult.Success(new FlowTypeEntity {
                                FlowTypeId = entity.FlowId,
                                FlowTypeName = entity.FlowName,
                                icon = "fa fa-check",
                                ParentId=entity.FlowTypeId,
                                IsType=2,
                                IsCheckOut=entity.IsCheckOut,
                                IsEdited=(entity.IsCheckOut&&entity.CheckOutUserId.Equals(this.UserId))
                                
                            }));
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                    if (IsAjax)
                    {
                        return Json(AjaxResult.Error(strError));
                    }
                }
            }
            if (IsAjax)
            {
                return Json(AjaxResult.Error("参数错误"));
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
      
        #endregion
        #region 
        [HttpPost]
        [Permission(IsParent =true)]
        public JsonResult GetTree(string parentid,int IsType)
        {
            return Json(AjaxResult.Success(mService.GetTree(parentid, IsType)));
        }
        [HttpGet]
        /// <summary>
        /// 保存 编辑方法
        ///</summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
       // [Permission(IsParent =true,ActionName ="Edit")]
        public IActionResult ModalEdit(string id, FlowEntity entity)
        {
            if (!this.IsPost)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    FlowDesigner flowDesigner = mService.GetFlowById(id);
                    entity = flowDesigner != null? flowDesigner.Flow:null;
                }
                if (entity == null)
                {
                    entity = new FlowEntity
                    {

                    };
                }
            }
            
            return View(entity);
        }
        /// <summary>
        /// 保存流程
        /// </summary>
        /// <param name="flowEntity"></param>
        /// <returns></returns>
        [HttpPost]
     
        [Permission(ActionName = "CheckOut")]
        public IActionResult SaveFlow(FlowEntity flowEntity)
        {
            string strError = "";
            if (mService.SaveFlow(flowEntity, ref strError))
            {
                if (flowEntity.IsCheckOut && flowEntity.CheckOutUserId.Equals(this.UserId))
                {
                    flowEntity.IsEdited = true;
                }
                return Json(AjaxResult.Success(flowEntity));
            }
            return Json(AjaxResult.Error());
        }
        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult SaveWorkflow(FlowDesigner flowDesigner) {
            string strError = "";
            if (mService.SaveWorkflow(flowDesigner, ref strError))
            {
                return Json(AjaxResult.Success(flowDesigner));
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion

        /// <summary>
        /// 获取流程图
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="isType"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public IActionResult FlowDesigner(string flowid, int version)
        {
            return Json(AjaxResult.Success(mService.GetFlowById(flowid, version)));
        }

      
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
       
        [Permission(ActionName = "CheckOut")]
        public IActionResult SaveParams(FlowParamsEntity entity)
        {
            string strError = "";
            if (ModelState.IsValid)
            {
                if (mService.SaveParams(entity, ref strError))
                {
                    return Json(AjaxResult.Success(entity));
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }

        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult DeleteParams(string flowid, string id)
        {
            string strError = "";
            if (mService.DeleteParams(flowid, id, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        /// <summary>
        /// 保存节点
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
     
        [Permission(ActionName = "CheckOut")]
        public IActionResult SaveActivity(ActivityEntity entity)
        {
            string strError = "";
            if (ModelState.IsValid)
            {
                if (mService.SaveActivity(entity, ref strError))
                {
                    return Json(AjaxResult.Success(entity));
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult DeleteActivity(string flowid, string id)
        {
            string strError = "";
            if (mService.DeleteActivity(flowid, id, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        /// <summary>
        /// 保存节点
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult SaveRules(FlowRulesEntity entity)
        {
            string strError = "";
            if (ModelState.IsValid)
            {
                if (mService.SaveRules(entity, ref strError))
                {
                    return Json(AjaxResult.Success(entity));
                }
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Error(ModelState));
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="activityid"></param>
        /// <param name="ruleid"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult DeleteRule(string flowid, string activityid, string ruleid)
        {
            string strError = "";
            if (mService.DeleteRule(flowid, activityid, ruleid, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        [HttpPost]
        [Permission("03", "迁出")]
        public IActionResult CheckOut(string flowid)
        {
            string strError = "";
            if (mService.CheckOut(flowid, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        [Permission(ActionName = "CheckOut")]
        public IActionResult CheckIn(string flowid)
        {
            string strError = "";
            if (mService.CheckIn(flowid, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        [Permission("02", "删除")]
        public IActionResult Delete(string flowid, int isType)
        {
            string strError = "";
            if (mService.Delete(flowid, isType, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        public IActionResult CheckWorkflow(string flowid, int isType)
        {
            string strError = "";
            if (mService.CheckWorkflow(flowid, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        public IActionResult GetMaxNumber(string flowid, int isType)
        {
            string strError = "";
            string number = mService.GetMaxNumber(flowid, isType, ref strError);
            if (!string.IsNullOrWhiteSpace(number))
            {
                return Json(AjaxResult.Success(new { max = number }));
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        [Permission("04", "发布")]
        public IActionResult Publish(string flowid, string version,bool isCover=false)
        {
            string strError = "";

            if (mService.Publish(flowid, version, ref strError,isCover))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        /// <summary>
        /// 撤销
        /// </summary>
        /// <param name="flowid">流程id</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(ActionName="CheckOut")]
        public IActionResult Revoke(string flowid,bool isCover=false)
        {
            string strError = "";

            if (mService.Revoke(flowid, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        public IActionResult FlowVersion(string flowid)
        {
            ViewData["FlowId"] = flowid;
            var lst= mService.GetFlowVersions(flowid);
            return View(lst);
        }

    }
}
