﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.WF.BLL;
using WHLRDF.WF.Model;


namespace WHLRDF.WF.Web.Controllers
{
    [MenuPage(20, "020", "流程实例管理", "WF/Process", "")]
    public class ProcessController : AreaController
    {
        #region Service注入

        public IProcessService mService { get; set; }
        public IProcessInstanceService pService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        ///</summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false)]
     
        public IActionResult Edit(string id)
        {
            var entity = pService.GetFlowDesignerByInstance(id);
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
        [HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
    }
}
