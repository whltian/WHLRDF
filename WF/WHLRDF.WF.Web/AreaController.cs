﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;

namespace WHLRDF.WF.Web
{

    [MenuPage(1400, "100", "流程管理", "WF", "")]
    [Area("WF")]
    [Route("WF/{controller=Home}/{action=Index}/{id?}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "WF";
            }
        }
    }
}
