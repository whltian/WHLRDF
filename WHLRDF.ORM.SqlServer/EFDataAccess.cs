﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Extensions.Logging;
using WHLRDF.ORM.EF;
using Microsoft.Data.SqlClient;

namespace WHLRDF.ORM.SqlServer
{
    public class EFDataAccess : AbstractDbContext
    {
        public EFDataAccess():base()
        {

        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">链接字符串</param>
        public EFDataAccess(string connectionString,string version) : base(connectionString, version)
        {
        }
        public EFDataAccess(string connectionString, List<SystemComponent> dbModels, string version) : base(connectionString, dbModels, version)
        {
        }
        public override ProviderType DbProviderType { get => ProviderType.SqlServer; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            //var loggerFactory = new LoggerFactory();
            //loggerFactory.AddProvider(new EFLoggerProvider());
            //builder.UseLoggerFactory(loggerFactory);
            builder.UseSqlServer(ApplicationEnvironments.Site.ConnectionString);
        }



        #region public string SqlSafe(string value) 检查参数的安全性
        /// <summary>
        /// 检查参数的安全性
        /// </summary>
        /// <param name="value">参数</param>
        /// <returns>安全的参数</returns>
        public string SqlSafe(string value)
        {
            return DataAccess.Instance.SqlSafe(value);
        }
        #endregion

        #region string PlusSign(params string[] values)
        /// <summary>
        ///  获得Sql字符串相加符号
        /// </summary>
        /// <param name="values">参数值</param>
        /// <returns>字符加</returns>
        public string PlusSign(params string[] values)
        {
            return DataAccess.Instance.PlusSign(values);
        }
        #endregion


        #region public string GetParameter(string parameter) 获得参数Sql表达式
        /// <summary>
        /// 获得参数Sql表达式
        /// </summary>
        /// <param name="parameter">参数名称</param>
        /// <returns>字符串</returns>
        public override string GetParameter(string parameter)
        {
            return DataAccess.Instance.GetParameter(parameter);
        }

        /// <summary>
        /// 获得参数like Sql表达式
        /// </summary>
        /// <param name="parameter">参数名称</param>
        /// <returns>字符串</returns>
        public override string GetLikeParameter(string parameter)
        {
            return DataAccess.Instance.GetLikeParameter(parameter);
        }
        #endregion

        #region public DbParameter MakeInParam(string targetFiled, object targetValue)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数</returns>
        public DbParameter MakeInParam(string targetFiled, object targetValue)
        {
            return DataAccess.Instance.MakeInParam(targetFiled, targetValue);
        }
        #endregion

        #region public DbParameter[] MakeParameters(string targetFiled, object targetValue)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数集</returns>
        public DbParameter[] MakeParameters(string targetFiled, object targetValue)
        {
            return DataAccess.Instance.MakeParameters(targetFiled, targetValue);
        }
        #endregion

        #region public DbParameter[] MakeParameters(string[] targetFileds, Object[] targetValues)
        /// <summary>
        /// 获取参数
        /// </summary>
        /// <param name="targetFiled">目标字段</param>
        /// <param name="targetValue">值</param>
        /// <returns>参数集</returns>
        public DbParameter[] MakeParameters(string[] targetFileds, Object[] targetValues)
        {
            return DataAccess.Instance.MakeParameters(targetFileds, targetValues);
        }
        #endregion

        public override DbParameter MakeOutParam(string paramName, DbType dbType, int size)
        {
            return DataAccess.Instance.MakeParam(paramName, dbType, size, ParameterDirection.Output, null);
        }

        public override DbParameter MakeInParam(string paramName, DbType dbType, int size, object value)
        {
            return DataAccess.Instance.MakeParam(paramName, dbType, size, ParameterDirection.Input, value);
        }

        public override DbParameter MakeParam(string paramName, DbType dbType, Int32 size, ParameterDirection Direction, object value)
        {
            return DataAccess.Instance.MakeParam(paramName, dbType, size, Direction, value);
        }

        #region public void SqlBulkCopyData(DataTable dataTable)
        /// <summary>
        /// 利用Net SqlBulkCopy 批量导入数据库,速度超快
        /// </summary>
        /// <param name="dataTable">源内存数据表</param>
        public override void SqlBulkCopyData(DataTable dt)
        {
            DataAccess.Instance.SqlBulkCopyData(dt, this.ConnectionString);
        }
        #endregion

        #region

        public override string Now()
        {
            return DataAccess.Instance.Now();
        }
        public override DbParameter[] GetParameter(List<DataParameter> parameters)
        {
            return DataAccess.Instance.GetParameter(parameters);
        }

        public override DbParameter GetParameter(string name, object value)
        {
            return DataAccess.Instance.GetParameter(name, value);
        }


        public override string Identity(string primaryKeyName)
        {
            return DataAccess.Instance.Identity(primaryKeyName);
        }

        #endregion

        /// <summary>
        /// 禁用自增
        /// </summary>
        /// <returns></returns>
        public override string Identity_Disabled(string tableName)
        {
            return DataAccess.Instance.Identity_Disabled(tableName);
        }
        /// <summary>
        /// 启用自增
        /// </summary>
        /// <returns></returns>
        public override string Identity_Enable(string tableName)
        {
            return DataAccess.Instance.Identity_Enable(tableName);
        }


        public override string QueryPage(string hql, Order[] orders,
            int pageIndex, int pageSize)
        {
            return DataAccess.Instance.QueryPage(this.Version,hql, orders, pageIndex, pageSize);
        }
      
        public override string RecordCount(string hql, bool createSql = true)
        {
            return DataAccess.Instance.RecordCount(hql, createSql);
        }
        /// 获取统计查询总数Sql
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        public override string GetRecordCountSql(string tableName)
        {
            return DataAccess.Instance.GetRecordCountSql(tableName);
        }
        /// <summary>
        /// 获取查询表Sql
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="queryFieldBuilder"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public override string GetSelectSql(string tableName, string queryFieldBuilder, bool isDeleted)
        {
            return DataAccess.Instance.GetSelectSql(tableName, queryFieldBuilder,isDeleted);
        }
        #region idbtype

        public override string GetTable(string dbName)
        {
            return DataAccess.Instance.GetTableOrView("U");
        }

        public override string GetView(string dbName)
        {
            return DataAccess.Instance.GetTableOrView("V");
        }

        public override string GetDbColumn(string tableName, string serverName, bool IsTable)
        {
            return DataAccess.Instance.GetDbColumn(tableName,  IsTable);
        }
        public override string GetSource(string tableName)
        {
            return DataAccess.Instance.GetSource(tableName);
        }

        public override string CreateTable(string tableName, List<DbColumnAttribute> dbColumns)
        {
            return DataAccess.Instance.CreateTable(tableName, dbColumns);
        }

        public override string AddColumn(string tableName, DbColumnAttribute columnItem)
        {
            return DataAccess.Instance.AddColumn(tableName, columnItem);
        }

        public override string DeleteTable(string tableName)
        {
            return DataAccess.Instance.DeleteTable(tableName);
        }

        public override string DeleteColumn(string tableName, string columnName)
        {
            return DataAccess.Instance.DeleteColumn(tableName, columnName);
        }
        /// <summary>
        /// 更改字段描述
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="description"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        public override string UpdateDescriptionSql(string tableName, string columnName, string description, bool isAdd)
        {
            return DataAccess.Instance.UpdateDescriptionSql(tableName, columnName, description, isAdd);
        }

        /// <summary>
        /// 判断描述是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">字段名</param>
        /// <returns></returns>
        public override string ExistDescriptionSql(string tableName, string columnName)
        {
            return DataAccess.Instance.ExistDescriptionSql(tableName, columnName);
        }
        #endregion
    }
}
