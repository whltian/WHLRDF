# WHLRDF

 #### 介绍
 
1.该项目采用了 IOC DI DDD OOP AOP SOA 设计模式 以及autofac Log4net Signalr EF 等主流中间件进行开发

2.数据层支持ado.net 以及entityframework框架

3.缓存基于redis 支持MemoryCache 缓存。

4.数据查询参照了nhibernate查询方式 为了支持ado 以及enittyframework所有有所更改。

5.该框架采用autofac 包对service层进行属性注入。

6.日志基于log4net 进行写入。

7.该框架采用Areas分层模式。

8.EF 数据迁移

9.实现了Session共享

10.静态文件共享以及压缩

11.js css文件动态压缩合并

12.实现了T4模板的代码在线生成功能

13.实现了数据库之间的服务器同步。

14.实现了负载均衡设置。

15.实现单点登录功能

16.实现简易的webchat聊天室功能

17.数据库完全支持Mysql 以及SqlServer

18.实现业务与系统之间完整解耦

19.基于DDD软件设计模型进行开发

20.实现插件化 模块设计开发

21.实现了Signalr Web端实时通讯功能

22.实现了基于MyFlow的流程设计器功能

23.实现了基于go.js的组织架构图功能

24.实现了应用层的读写分离。

25.实现了基于Action 之间的权限控制，每个按钮菜单无需用户添加，由系统直接生成，超级管理员不受权限控制。

26.实现了基于AOP的日志监控功能，主要体现在对每个请求的时间、返回结果 以及每个Sql的执行时间进行监控。

27.实现了基于ModelBinder的数据绑定 以及验证 。

28.底层数据更新只会更新有修改的字段，并同步更新缓存，业务层无需再次更新缓存。

29.自动执行数据数据库脚本

30.支持AutoMapper注入

#### 开发工具

VS2017 Sql2017 软件架构 基于NetCore 3.1 EF core Ado GetChart js myflow js bootstrap

 #### 安装教程

1.下载源文件，使用cmd 命令进入whlrdf.web目录。

2.使用dotnet publish -c release 进行发布。

3.编辑 appsettings.json文件

4.将文件放入服务器指定目录。

5.在服务器中打开cmd,进入项目目录

6.使用dotnet whlrdf.web.dll启动项目

8.使用浏览器访问项目/Install,按照步骤执行即可安装系统以及数据库。

9.关于系统代码生成的相关配置数据，可以远程访问数据库进行直接获取，数据库地址：42.157.131.97，密码：123456。也可在代码生成-服务器管理中添加该数据库，并配置本地同步数据库，将该数据下载下来。

11.关于系统插件化配置目前没有做界面配置，如果想把所有功能参数化配置请将主项目中对应的引用全部去掉即可，插件化配置节点：“Components”


12.以下是appsettings.json 配置文件信息

13.支持的ORM有ADO,EFCore,
14.支持的数据库有SqlServer,MySql
15.支持数据库版本SqlServer_2008以及以上版本

```
{
    "Logging": {
        "LogLevel": {
            "Default": "Information",
            "Microsoft": "Warning",
            "Microsoft.Hosting.Lifetime": "Information"
        }
    },
    "AllowedHosts": "*",
    "Kestrel": {
        "EndPoints": {
            "Http": { "Url": "http://*:5418" },
            "Https": { "Url": "https://*:5419" }
        }
    },
    "SiteSettings": {
        "ConnectionString": "Server=.\\sql2017;User id=sa;Password=1;Database=WHLRDF;MultipleActiveResultSets=True",
        "MongoConnection": "mongodb://mongo-netcore:123456@10.10.10.101:27017/netcore?authSource=admin",
        "DBProviderType": "SqlServer",
        "ORMType": "EF",
        "ServerAddress": "localhost",
        "ApplicationName": "内部管理系统",
        "ApplicationIdentify": "WHLRDF",
        "IsEntityCache": false,
        "CacheType": "Redis",
        "IsSignalR": true,
        "RedisConnection": "127.0.0.1,poolsize=10,ssl=false,writeBuffer=10240,prefix=Redis_NetCore_",
        "Version": "1.2",
        "IsWeb": true,
        "Domain": ".netcore.com",
        "AESKey": "whladmin",
        "MD5key": "ae125efkk4454eeff444ferfkny6oxi8",
        "AESIV": "ae125efkk4454eeff444ferfkny6oxi8",
        "SessionTimeout": 30,
        "UserCacheKey": "CACHE_ENTITY_KEY_USERE_NTITY_INDETITY_",
        "UserServiceInstance": "WHLRDF.BLL.UserService",
        "InitData": "WHLRDF.BLL;WHLRDF.BLL.InitDataService",
        "DefaultDirectory": "g:/git/whlrdf/WHLRDF.Web/",
        "BaseDirectory": "g:/git/whlrdf/",
        "AdminUser": "1000000000",
        "DefaultPassword": "123456",
        "SendMessageUrl": "http://localhost:5411/admin/api/SendMessage",
        "LogSiteService": "",
        "IsDBLog": true,
        "Install": true,
        "SMTP": {
            "Server": "smtp.qq.com",
            "Port": 25,
            "Account": "403259156@qq.com",
            "Password": "dfvfzxsgacldcbad",
            "SendAddress": "403259156@qq.com"
        },
        "Components": {
            "System": {
                "Opened": true,
                "Name": "主系统",
                "Model": [ "WHLRDF.Application.Model.dll" ],
                "Service": [ "WHLRDF.Application.BLL.dll" ],
                "Mvc": "WHLRDF.Web.dll",
                "Api": ""
            },
          "Plug": [
            {
              "Id": "0001",
              "Name": "系统管理",
              "Unique": "Admin",
              "Remark": "Admin管理后台",
              "Opened": true,
              "OrderNum": 1500,
              "icon": "fa-wrench",
              "Model": [],
              "Service": [],
              "Mvc": "WHLRDF.Admin.Web.dll",
              "Views": ""
            },
            {
              "Id": "0099",
              "Name": "系统安装",
              "Unique": "Install",
              "Remark": "系统安装中心",
              "Opened": true,
              "OrderNum": 9900,
              "icon": "fa-wrench",
              "IsMenu": false,
              "Model": [],
              "Service": [ "WHLRDF.Install.BLL.dll" ],
              "Mvc": "WHLRDF.Install.Web.dll",
              "Views": "WHLRDF.Install.Web.Views.dll"
            },
            {
              "Id": "0002",
              "Name": "代码生成器",
              "Unique": "Code",
              "Remark": "",
              "Opened": true,
              "OrderNum": 1200,
              "icon": "fa-code",
              "Model": [ "WHLRDF.Code.Model.dll" ],
              "Service": [ "WHLRDF.Code.BLL.dll" ],
              "Mvc": "WHLRDF.Code.Web.dll",
              "Views": "WHLRDF.Code.Web.Views.dll"
            },
            {
              "Id": "0005",
              "Name": "附件管理",
              "Unique": "Attach",
              "Remark": "",
              "Opened": true,
              "OrderNum": 1100,
              "icon": "fa-folder",
              "Model": [ "WHLRDF.Attach.Model.dll" ],
              "Service": [ "WHLRDF.Attach.BLL.dll" ],
              "Mvc": "WHLRDF.Attach.Web.dll",
              "Views": "WHLRDF.Attach.Web.Views.dll"
            },
            {
              "Id": "0003",
              "Name": "组织架构管理",
              "Unique": "Org",
              "Remark": "组织架构管理",
              "Opened": true,
              "OrderNum": 1300,
              "icon": "fa-sitemap",
              "Model": [
                "WHLRDF.ORG.Model.dll"
              ],
              "Service": [
                "WHLRDF.ORG.BLL.dll"
              ],
              "Mvc": "WHLRDF.ORG.Web.dll",
              "Views": "WHLRDF.ORG.Web.Views.dll"
            },
            {
              "Id": "0004",
              "Name": "流程管理",
              "Unique": "WF",
              "Remark": "",
              "Opened": true,
              "OrderNum": 1400,
              "icon": "fa-cogs",
              "Model": [
                "WHLRDF.WF.Model.dll"
              ],
              "Service": [
                "WHLRDF.WF.BLL.dll"
              ],
              "Mvc": "WHLRDF.WF.Web.dll",
              "Views": "WHLRDF.WF.Web.Views.dll"
            }

          ]
        },
        "ConfigPath": [
            {
                "RootKey": "config",
                "Path": "cloud/config",
                "HttpPath": "/static/config"
            },
            {
                "RootKey": "statichtml",
                "Path": "cloud/html",
                "HttpPath": "/static/html"
            },
            {
                "RootKey": "upload",
                "Path": "cloud/upload",
                "HttpPath": "/static/resources"
            }
        ],
        "NoGetResponse": [ "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ],
        "Cookie": {
            "Domain": ".whlrdf.com",
            "Name": "sso",
            "RootPath": "/",
            "ProtectionProviderPath": "g:/git/whlrdf/cloud/config/shared-auth-ticket-keys"
        }
    }
}
```
