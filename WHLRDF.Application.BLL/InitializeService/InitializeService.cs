﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.Application.Model;
using WHLRDF.Log;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    /// <summary>
    /// 基础应用层数据初始化
    /// </summary>
    public class InitializeService : SerivceBase, IInitializeService
    {
        #region 私有变量
        private List<UserEntity> userEntities = null;
        private List<PermissionEntity> perEntities = null;
        private List<TemplateEntity> tempEntities = null;
        private List<DictionaryEntity> dicEntities = null;
        private List<SerialRuleNumberEntity> numerEntities = null;
        private List<RelationUserEntity> relEntities = null;
        private List<SerialRuleEntity> numEntities = null;
        private List<RegionEntity> regionEntities = null;
        #endregion
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public bool Read(IDbRepository dbRepository, ref Dictionary<string, object> dicResult, ref string strError)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            userEntities = dbRepository.Query<UserEntity>(criter);
            perEntities = dbRepository.Query<PermissionEntity>(criter);
            tempEntities = dbRepository.Query<TemplateEntity>(criter);
            dicEntities = dbRepository.Query<DictionaryEntity>(criter);
            numerEntities = dbRepository.Query<SerialRuleNumberEntity>(criter);
            relEntities = dbRepository.Query<RelationUserEntity>(criter);
            numEntities = dbRepository.Query<SerialRuleEntity>(criter);
            regionEntities = dbRepository.Query<RegionEntity>(criter);
            if (dicResult.ContainsKey(nameof(userEntities)))
            {
                strError = nameof(userEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(perEntities)))
            {
                strError = nameof(perEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(tempEntities)))
            {
                strError = nameof(tempEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(dicEntities)))
            {
                strError = nameof(dicEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(numerEntities)))
            {
                strError = nameof(numerEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(relEntities)))
            {
                strError = nameof(relEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(numEntities)))
            {
                strError = nameof(numEntities) + "已存在";
                return false;
            }
            if (dicResult.ContainsKey(nameof(regionEntities)))
            {
                strError = nameof(regionEntities) + "已存在";
                return false;
            }
            dicResult.Add(nameof(userEntities), userEntities);
            dicResult.Add(nameof(perEntities), perEntities);
            dicResult.Add(nameof(tempEntities), tempEntities);
            dicResult.Add(nameof(dicEntities), dicEntities);
            dicResult.Add(nameof(numerEntities), numerEntities);
            dicResult.Add(nameof(relEntities), relEntities);
            dicResult.Add(nameof(numEntities), numEntities);
            dicResult.Add(nameof(regionEntities), regionEntities);

            return true;
        }

        /// <summary>
        /// 同步数据
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="dicResult"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public bool Write(IDbRepository dbRepository, Dictionary<string, object> dicResult, ref string strError)
        {

            try
            {
                if (dicResult.ContainsKey(nameof(userEntities)) && dicResult[nameof(userEntities)] != null)
                {
                    userEntities = JSONHelper.FromJson<List<UserEntity>>(dicResult[nameof(userEntities)].ToJson());
                    if (userEntities != null && userEntities.Count > 0)
                    {
                        userEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(perEntities)) && dicResult[nameof(perEntities)] != null)
                {
                    perEntities = JSONHelper.FromJson<List<PermissionEntity>>(dicResult[nameof(perEntities)].ToJson());
                    if (perEntities != null && perEntities.Count > 0)
                    {
                        perEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(tempEntities)) && dicResult[nameof(tempEntities)] != null)
                {
                    tempEntities = JSONHelper.FromJson<List<TemplateEntity>>(dicResult[nameof(tempEntities)].ToJson());
                    if (tempEntities != null && tempEntities.Count > 0)
                    {
                        tempEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(dicEntities)) && dicResult[nameof(dicEntities)] != null)
                {
                    dicEntities = JSONHelper.FromJson<List<DictionaryEntity>>(dicResult[nameof(dicEntities)].ToJson());
                    if (dicEntities != null && dicEntities.Count > 0)
                    {
                        dicEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(numerEntities)) && dicResult[nameof(numerEntities)] != null)
                {
                    numerEntities = JSONHelper.FromJson<List<SerialRuleNumberEntity>>(dicResult[nameof(numerEntities)].ToJson());
                    if (numerEntities != null && numerEntities.Count > 0)
                    {
                        numerEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(relEntities)) && dicResult[nameof(relEntities)] != null)
                {
                    relEntities = JSONHelper.FromJson<List<RelationUserEntity>>(dicResult[nameof(relEntities)].ToJson());
                    if (relEntities != null && relEntities.Count > 0)
                    {
                        relEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(numEntities)) && dicResult[nameof(numEntities)] != null)
                {
                    numEntities = JSONHelper.FromJson<List<SerialRuleEntity>>(dicResult[nameof(numEntities)].ToJson());
                    if (numEntities != null && numEntities.Count > 0)
                    {
                        numEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
                if (dicResult.ContainsKey(nameof(regionEntities)) && dicResult[nameof(regionEntities)] != null)
                {
                    regionEntities = JSONHelper.FromJson<List<RegionEntity>>(dicResult[nameof(regionEntities)].ToJson());
                    if (regionEntities != null && regionEntities.Count > 0)
                    {
                        regionEntities.ForEach(x =>
                        {
                            x.State = EntityState.Add;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }

            if (userEntities != null && userEntities.Count > 0)
            {
                dbRepository.BatchInsert<UserEntity>(userEntities, ref strError);
            }
            if (perEntities != null && perEntities.Count > 0)
            {
                dbRepository.BatchInsert<PermissionEntity>(perEntities, ref strError);
            }
            if (tempEntities != null && tempEntities.Count > 0)
            {
                dbRepository.BatchInsert<TemplateEntity>(tempEntities, ref strError);
            }
            if (dicEntities != null && dicEntities.Count > 0)
            {
                dbRepository.BatchInsert<DictionaryEntity>(dicEntities, ref strError);
            }
            if (numerEntities != null && numerEntities.Count > 0)
            {
                dbRepository.BatchInsert<SerialRuleNumberEntity>(numerEntities, ref strError);
            }
            if (relEntities != null && relEntities.Count > 0)
            {
                dbRepository.BatchInsert<RelationUserEntity>(relEntities, ref strError);
            }
            if (numEntities != null && numEntities.Count > 0)
            {
                dbRepository.BatchInsert<SerialRuleEntity>(numEntities, ref strError);
            }
            if (regionEntities != null && regionEntities.Count > 0)
            {
                dbRepository.BatchInsert<RegionEntity>(regionEntities, ref strError);
            }
            return true;
        }
    }
}
