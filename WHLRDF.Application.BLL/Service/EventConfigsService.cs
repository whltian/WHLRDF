﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{

    /// <summary>
    /// COMM_EventConfigs实例
    /// </summary>
    public class EventConfigsService : SerivceBase, IEventConfigsService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<EventConfigsEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(EventConfigsEntity.__IsDeleted, false);
            return this.Query<EventConfigsEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public EventConfigsEntity GetById(string EventCode)
        {
            if (string.IsNullOrWhiteSpace(EventCode))
            {
                return null;
            }
            return this.GetById<EventConfigsEntity>(EventCode);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(EventConfigsEntity entity, ref string strError)
        {
            return this.SaveOrUpdate<EventConfigsEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<EventConfigsEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                EventConfigsEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<EventConfigsEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
       
        #endregion

        #region 事件

        #endregion
    }
}
