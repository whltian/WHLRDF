﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{
    public class TemplateService : SerivceBase, ITemplateService
    {
        public string Template_Identity_Num = "0015";
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<TemplateEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(TemplateEntity.__IsDeleted, false);
            return this.Query<TemplateEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public TemplateEntity GetById(string id)
        {
            return this.GetById<TemplateEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(TemplateEntity entity, ref string strError)
        {
            if (string.IsNullOrWhiteSpace(entity.TemplateId))
            {
                entity.TemplateId = SerialService.CreateSerialNo(Template_Identity_Num, null, ref strError);// 主键生成值
            }
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<TemplateEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                TemplateEntity._PrimaryKeyName ,
                                TemplateEntity.__Title
                           }, ligerGrid.keyWord)
                           );
            }
            string templateType = ligerGrid.GetValue("TemplateType");
            if (!string.IsNullOrWhiteSpace(templateType))
            {
                criter = Expression.And(Expression.Eq(TemplateEntity.__TemplateType,templateType), Expression.Eq("IsDeleted", false));
            }
            return this.Query<TemplateEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, string key)
        {
            key = (string.IsNullOrWhiteSpace(key)) ? "" : key;

            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(TemplateEntity.__Title, name),
                Expression.NotEq(TemplateEntity._PrimaryKeyName, key))
                );
            var result = this.Query<TemplateEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}