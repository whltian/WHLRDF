﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{

    /// <summary>
    /// COMM_SysTask实例
    /// </summary>
    public class SysTaskService : SerivceBase, ISysTaskService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<SysTaskEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(SysTaskEntity.__IsDeleted, false);
            return this.Query<SysTaskEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public SysTaskEntity GetById(string TaskId)
        {
            if (string.IsNullOrWhiteSpace(TaskId))
            {
                return null;
            }
            return this.GetById<SysTaskEntity>(TaskId);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(SysTaskEntity entity, ref string strError)
        {
            ////if (Exist(entity.Name, entity.TaskId))
            ////{
            ////    strError = entity.OrgName + "已存在，请不要重复输入";
            ////    return false;
            ////}
            //if (string.IsNullOrWhiteSpace(entity.TaskId))
            //{
            //    entity.TaskId = MyGenerateHelper.GenerateOrder();
            //}
            //// domain.TaskId = SerialService.CreateSerialNo(SysTask_Identity_Num, null, ref strError);// 序号

            //return this.SaveOrUpdate<SysTaskEntity>(entity);
            return true;
        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<SysTaskEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                SysTaskEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<SysTaskEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
        ///// <summary>
        ///// 验证是否存在的方法
        ///// <param name="name">需要验证的值</param>
        ///// <param name="key">主键</param>
        ///// </summary>
        //public bool Exist(string name, string TaskId)
        //{
        //    TaskId = (string.IsNullOrWhiteSpace(TaskId)) ? "" : TaskId;
        //    ICriterion criter = Expression.Eq("IsDeleted", false);
        //    criter =
        //        Expression.And(criter, Expression.And(Expression.Eq(SysTaskEntity.__Name, name),
        //        Expression.Not(Expression.Eq(SysTaskEntity._PrimaryKeyName, TaskId)))
        //        );
        //    var result = this.Query<SysTaskEntity>(criter).FirstOrDefault();
        //    return result != null;
        //}
        public bool InsertTask(TaskModel taskModel,ref string strError)
        {
            if (string.IsNullOrWhiteSpace(taskModel.TaskId))
            {
                return true;
            }
            SysTaskEntity taskEntity = GetById(taskModel.TaskId);
            if (taskEntity == null)
            {
                taskEntity = taskModel.MapTo<SysTaskEntity>();
                if (taskEntity != null)
                {
                    taskEntity.State = EntityState.Add;
                }
            }
            else
            {
                taskEntity.IsOpen = taskModel.IsOpen;
                taskEntity.IsSuccess = taskModel.IsSuccess;
                taskEntity.Message = taskModel.Message;
                taskEntity.ExcuteTime = taskModel.ExcuteTime.ToDecimal();
                taskEntity.EndDate = taskModel.EndDate;
                taskEntity.State = EntityState.Update;
            }
            if (taskEntity == null)
            {
                strError = "参数异常,无法保存";
                return false;
            }
            try
            {
                this.SaveOrUpdate<SysTaskEntity>(taskEntity);
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return false;
            }
            return true;
        }
        #endregion

        #region 事件

        #endregion
    }
}
