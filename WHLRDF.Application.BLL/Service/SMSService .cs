﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{
    public class SMSService : SerivceBase, ISMSService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<SMSEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(SMSEntity.__IsDeleted, false);
            return this.Query<SMSEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public SMSEntity GetById(string id)
        {
            return this.GetById<SMSEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(SMSEntity entity, ref string strError)
        {
        
            if (string.IsNullOrWhiteSpace(entity.SMSId))
            {
                entity.SMSId = MyGenerateHelper.GenerateOrder();
            }
          
            return this.SaveOrUpdate<SMSEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<SMSEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                SMSEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return this.Query<SMSEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool SendSMS(string tophone, string content, ref string strError)
        {
            return SendSMS(tophone, "", NotifyTypeEnum.Default, content, ref strError);
        }
        /// <summary>
        /// 短信发送
        /// </summary>
        /// <param name="tophone"></param>
        /// <param name="formid"></param>
        /// <param name="formType"></param>
        /// <param name="content"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public bool SendSMS(string tophone, string formid, NotifyTypeEnum formType,  string content, ref string strError)
        {
            bool flag = false;
            SMSEntity entity = null;
            if (!string.IsNullOrEmpty(formid) && formType != NotifyTypeEnum.Default)
            {
                ICriterion criter = Expression.And(Expression.Eq(SMSEntity.__FormType, formType.GetHashCode()),
                    Expression.And(Expression.Eq(SMSEntity.__FormId, formid),
                    Expression.Eq(SMSEntity.__IsDeleted, false)));
                entity = this.Query<SMSEntity>(criter).FirstOrDefault();
            }
            if (entity == null)
            {
                entity = new SMSEntity
                {
                    Content = content,
                    CreateBy = ApplicationEnvironments.DefaultSession.UserId,
                    CreateDate = DateTime.Now,
                    LastModifyDate = DateTime.Now,
                    LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId,
                    UserId = ApplicationEnvironments.DefaultSession.UserId,
                    SendRound = 0,
                    SendResult = "",
                    SendDate=DateTime.Now,
                    SMSId =MyGenerateHelper.GenerateOrder(),
                    FormId = formid,
                    FormType = formType.GetHashCode(),
                };
            }
            if (entity.Status == 0)
            {
                // flag = SmtpHelper.Send(ConfigHelper.SMTPSendAddress, toEmail, title, body);
                entity.SendRound += 1;
                entity.ExpireTime = DateTime.Now;
                entity.LastModifyDate = DateTime.Now;
                entity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
                entity.Status = flag ? 1 : 2;
                this.SaveOrUpdate<SMSEntity>(entity, ref strError);
            }

            return flag;
        }
        #endregion

    }
}