﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{
    public class DictionaryService : SerivceBase, IDictionaryService
    {
        public static string Dictionary_Identity_Num = "0014";
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DictionaryEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DictionaryEntity.__IsDeleted, false);
            return this.Query<DictionaryEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public DictionaryEntity GetById(string id)
        {
            return this.GetById<DictionaryEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DictionaryEntity entity, ref string strError)
        {
            if (Exist(entity.KeyName, entity.DictionaryId, entity.ParentId))
            {
                strError = entity.KeyName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.DictionaryId))
            {
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                keyValues.Add(DictionaryEntity.__ParentId, entity.ParentId);
                entity.DictionaryId = SerialService.CreateSerialNo(Dictionary_Identity_Num, keyValues, ref strError);// 主键生成值
            }
           
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<DictionaryEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DictionaryEntity._PrimaryKeyName ,
                                DictionaryEntity.__KeyName 
                           }, grid.keyWord)
                           );
            }
            return this.Query<DictionaryEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 验证是否存在的方法
        /// <param name="name">需要验证的值</param>
        /// <param name="key">主键</param>
        /// </summary>
        public bool Exist(string name, string key,string parentid)
        {
            key = (string.IsNullOrWhiteSpace(key)) ? "" : key;

            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(DictionaryEntity.__KeyName, name),
                Expression.NotEq(DictionaryEntity._PrimaryKeyName, key))
                );
            if (string.IsNullOrWhiteSpace(parentid))
            {
                criter = Expression.And(criter, Expression.IsNull(DictionaryEntity.__ParentId));
            }
            else
            {
                criter = Expression.And(criter, Expression.Eq(DictionaryEntity.__ParentId, parentid));
            }
            var result = this.Query<DictionaryEntity>(criter).FirstOrDefault();
            return result != null;
        }

        public List<DictionaryEntity> GetDicByParentId(string parentid)
        {
            ICriterion criter = Expression.Eq(DictionaryEntity.__IsDeleted, false);
            if (string.IsNullOrWhiteSpace(parentid))
            {
                criter = Expression.And(criter, Expression.IsNull(DictionaryEntity.__ParentId));
            }
            else
            {
                criter = Expression.And(criter, Expression.Eq(DictionaryEntity.__ParentId,parentid));
            }
           return this.Query<DictionaryEntity>(criter);
        }
        #endregion

    }
}