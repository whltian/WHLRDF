﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{
    public class RoleService : SerivceBase, IRoleService
    {
        public const string Role_No_Rule = "0005";
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<RoleEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(RoleEntity.__IsDeleted, false);
            return this.Query<RoleEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public RoleEntity GetById(string id)
        {
            return this.GetById<RoleEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(RoleEntity entity, ref string strError)
        {
            if (this.Exist(entity.RoleKey, entity.RoleId))
            {
                strError = "角色已存在，请重新输入！";
                return false;
            }
       
            if (string.IsNullOrWhiteSpace(entity.RoleId))
            {
                entity.RoleId = SerialService.CreateSerialNo(Role_No_Rule, null, ref strError);
            }
            if (entity.RoleId == entity.ParentId)
            {
                strError = "角色上级不能是当前角色！";
                return false;
            }
            return entity.Save(ref strError);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                RoleEntity entity = new RoleEntity();
                return entity.Delete(deleteKey, ref strError);
               
            }
            return false;
       
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                RoleEntity._PrimaryKeyName ,
                                 RoleEntity.__RoleKey,
                                  RoleEntity.__RoleName
                           }, grid.keyWord)
                           );
            }
            return this.Query<RoleEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool Exist(string roleKey, string roleId)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter = Expression.And( 
                Expression.And(criter, Expression.Eq(RoleEntity.__RoleKey, roleKey)),
                Expression.NotEq(RoleEntity._PrimaryKeyName,roleId)
                );
            var result=  this.Query<RoleEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}