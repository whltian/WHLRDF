﻿
using System;
using System.Collections.Generic;
using System.Linq;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{

    /// <summary>
    /// 流水号记录表实例
    /// </summary>
    public class SerialRuleNumberService : SerivceBase, ISerialRuleNumberService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<SerialRuleNumberEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(SerialRuleNumberEntity.__IsDeleted, false);
            return this.Query<SerialRuleNumberEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public SerialRuleNumberEntity GetById(string SerialRuleNumberId)
        {
            if (SerialRuleNumberId == null|| string.IsNullOrWhiteSpace(SerialRuleNumberId.ToString()))
            {
                return null;
            }
            return this.GetById<SerialRuleNumberEntity>(SerialRuleNumberId);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(SerialRuleNumberEntity entity, ref string strError)
        {
            //if (Exist(entity.Name, entity.SerialRuleNumberId))
            //{
            //    strError = entity.OrgName + "已存在，请不要重复输入";
            //    return false;
            //}
            if (string.IsNullOrWhiteSpace(entity.SerialRuleNumberId))
            {
                entity.SerialRuleNumberId = MyGenerateHelper.GenerateOrder();
            }
            // domain.SerialRuleNumberId = new WHLRDF.Application.BLL.SerialRuleService().CreateSerialNo(SerialRuleNumber_Identity_Num, null, ref strError);// 序号

            return this.SaveOrUpdate<SerialRuleNumberEntity>(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<SerialRuleNumberEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid ligerGrid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(ligerGrid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                SerialRuleNumberEntity._PrimaryKeyName ,
                           }, ligerGrid.keyWord)
                           );
            }
            return this.Query<SerialRuleNumberEntity>(ligerGrid, criter);

        }
        #endregion

        #region 扩展方法
       
        #endregion

        #region 事件

        #endregion
    }
}
