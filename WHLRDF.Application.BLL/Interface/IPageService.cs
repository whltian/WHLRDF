﻿
using System.Collections.Generic;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public interface IPageService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<PageEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        PageEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(PageEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        LigerGrid ForGrid(LigerGrid ligerGrid);
        #endregion

        #region 扩展方法
        List<PageEntity> GetMenuTree();

        List<PageEntity> GetChildTree(string parentid);
        List<PageEntity> GetChildTree(List<PageEntity> lstAll, string parentid,bool isMenu);
        /// <summary>
        /// 保存初始化功能菜单
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool InitBatchSave(List<PageEntity> entities,ref string strError);

        /// <summary>
        /// 初始化功能菜单
        /// </summary>
        /// <param name="lstComponent"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool InitMenu(List<SystemComponent> lstComponent, ref string strError);

        #endregion

    }
}
