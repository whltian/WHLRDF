﻿using System.Collections.Generic;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public interface INotifyService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<NotifyEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        NotifyEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(NotifyEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法
        /// <summary>
        /// 更新已读状态
        /// </summary>
        /// <param name="notifyid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool ReaderNotify(string notifyid, ref string strError);

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="users"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool Send(string senderUser,string users, string title, string content, ref string strError);

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="users"></param>
        /// <param name="formid"></param>
        /// <param name="formType"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool Send(string senderUser,string users, string formid,
            NotifyTypeEnum formType, string title, string content, ref string strError);

        /// <summary>
        /// 获取个人消息
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        LigerGrid MyNotify(LigerGrid ligerGrid,string userid);
        #endregion

    }
}