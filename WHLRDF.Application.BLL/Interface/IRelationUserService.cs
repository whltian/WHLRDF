﻿using System.Collections.Generic;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public interface IRelationUserService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<RelationUserEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        RelationUserEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(RelationUserEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法
        List<RelationUserEntity> GetRelationByUserId(string userid, int relationType = 0);

        bool RelationUserSave(string userid, string itemKeys, ref string strError,
            RelationUserType relationType = RelationUserType.Role);

        /// <summary>
        /// 获取已关联的用户
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <param name="resourceId"></param>
        /// <param name="relationType"></param>
        /// <returns></returns>
        LigerGrid ForGridByResourceId(LigerGrid ligerGrid, string resourceId, int relationType = 0);

        /// <summary>
        /// 添加关系用户
        /// </summary>
        /// <param name="resourceId">用户id</param>
        /// <param name="itemUsers">用户列表</param>
        /// <param name="strError"></param>
        /// <param name="relationType">关系类别</param>
        /// <returns></returns>
        bool AddRlUserSave(string resourceId, string itemUsers, ref string strError,
           RelationUserType relationType = RelationUserType.Role);
        /// <summary>
        /// 删除关系用户
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="itemUsers">用户列表</param>
        /// <param name="strError"></param>
        /// <param name="relationType">关系类别</param>
        /// <returns></returns>
        bool RemoveRlUserSave(string resourceId, string itemUsers, ref string strError,
          RelationUserType relationType = RelationUserType.Role);

        List<T> GetRelationByUserId<T>(string UserId, RelationUserType relationUserType) where T : class, new();
        #endregion

    }
}