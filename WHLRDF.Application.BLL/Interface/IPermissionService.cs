﻿using System.Collections.Generic;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public interface IPermissionService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<PermissionEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        PermissionEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(PermissionEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法
        /// <summary>
        /// 通过类别获取权限
        /// </summary>
        /// <param name="resourceid">数据源id</param>
        /// <param name="permissionType">类别</param>
        /// <returns></returns>
        List<PermissionEntity> GetPermissionById(string resourceid, PermissionType permissionType = PermissionType.Role);

        /// <summary>
        /// 权限保存
        /// </summary>
        /// <param name="resourceId">权限资源id</param>
        /// <param name="permissions"></param>
        /// <param name="unpermissions"></param>
        /// <param name="strError"></param>
        /// <param name="permissionType"></param>
        /// <returns></returns>
        bool PermissionSave(string resourceId,string permissions, string unpermissions, ref string strError, PermissionType permissionType = PermissionType.Role);

        /// <summary>
        /// 获取个人权限
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        List<PermissionEntity> GetMyPermission(UserEntity entity);
        #endregion

    }
}