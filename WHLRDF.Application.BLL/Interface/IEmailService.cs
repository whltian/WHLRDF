﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public interface IEmailService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<EmailEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        EmailEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(EmailEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="title"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        Task<bool> Send(string toEmail, string title, string body);

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="templateId"></param>
        /// <param name="valuePairs"></param>
        /// <returns></returns>
        Task<bool> Send(string toEmail, string templateId, Dictionary<string, string> valuePairs);

        Task<bool> Send(string sendUser, string formid, int formType,
            string toEmail, string ccuser, string title, string body);
        #endregion

    }
}