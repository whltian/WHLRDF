﻿
using System.Collections.Generic;
using WHLRDF.Application.Model;
using Microsoft.AspNetCore.Http;
namespace WHLRDF.Application.BLL
{
    public interface IUserService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<UserEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        UserEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(UserEntity entity, ref string strError);


        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法
        UserEntity QueryByUserName(string userName);

        UserEntity GetByInfoId(string id,string token);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="userAccount">账号/邮箱/手机号</param>
        /// <param name="Password">密码</param>
        /// <param name="strError"></param>
        /// <param name="token">令牌</param>
        /// <param name="oAuthCode"></param>
        /// <param name="oauthtoken"></param>
        /// <returns></returns>
        UserEntity Login(string userAccount, string Password, ref string strError,ref string token, string oAuthCode = "", string oauthtoken = "");

        /// <summary>
        /// 自动登录
        /// </summary>
        /// <param name="userno">用户编号</param>
        /// <param name="userkey">加密字符串</param>
        /// <returns></returns>
        UserEntity AutoLogin(string userno,string userkey, ref string strError);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldPwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        AjaxResult ChangePwd(string oldPwd, string newPwd);

        ///// <summary>
        ///// 保存方法
        ///// </summary>
        ///// <param name="entity"></param>
        ///// <param name="lstRole"></param>
        ///// <param name="lstGroup"></param>
        ///// <param name="strError"></param>
        ///// <returns></returns>
        //bool Save(UserEntity entity, List<RoleEntity> lstRole, List<GroupEntity> lstGroup, ref string strError);

        /// <summary>
        /// 验证用户名是否不存在
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <param name="userAccount">用户名</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool ChkUserName(string userid,string userAccount, ref string strError);

        /// <summary>
        /// 验证邮箱是否不存在
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <param name="email">邮箱</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool ChkUserEmail(string userid, string email, ref string strError);

        /// <summary>
        /// 验证手机是否不存在
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <param name="phone">手机</param>
        /// <param name="strError">错误信息</param>
        /// <returns></returns>
        bool ChkUserPhone(string userid, string phone, ref string strError);
        
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="entity">用户实体</param>
        /// <param name="strError">错误信息</param>
        /// <param name="isRegister">是否注册</param>
        /// <param name="verifycode">验证码</param>
        /// <param name="oAuthCode"></param>
        /// <param name="oauthtoken"></param>
        /// <returns></returns>
        bool Register(UserEntity entity, ref string strError, bool isRegister = true, string verifycode = "", string oAuthCode = "", string oauthtoken = "");

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="userids"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool ResetPwd(string userids, ref string strError);
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="pwd">新密码</param>
        /// <param name="repwd">确认密码</param>
        /// <param name="token">系统加密参数</param>
        /// <param name="vercode">验证码</param>
        /// <param name="mailvercode">邮箱验证码</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool ChangePwd(string userName, string pwd, string repwd, string token, string vercode, string mailvercode, ref string strError);

        LigerGrid GetUsersByName(LigerGrid ligerGrid);

        UserEntity GetUserByAccount(string account);

        bool Import(IFormFile file, ref string strError);

        byte[] Export(ref string strError);

        #endregion

    }
}