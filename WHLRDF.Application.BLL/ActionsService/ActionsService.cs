
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    public class ActionsService : SerivceBase, IActionsService
    {
        #region 属性
        public const string Action_No_Rule = "0003";
        #endregion
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<ActionsEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(ActionsEntity.__IsDeleted, false);
            return this.Query<ActionsEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public ActionsEntity GetById(string id)
        {
            return this.GetById<ActionsEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(ActionsEntity entity, ref string strError)
        {
            if (ChkActionName(entity.ActionId, entity.ActionName))
            {
                strError = "按钮已存在，请重新输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.ActionId))
            {
                entity.ActionId = SerialService.CreateSerialNo(Action_No_Rule, null, ref strError);
            }
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<ActionsEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                ActionsEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return this.Query<ActionsEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        /// <summary>
        /// 判断按钮是否不存在
        /// </summary>
        /// <param name="UserId">用户id</param>
        /// <param name="userAccount">用户账户</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public bool ChkActionName(string actionId, string actionName)
        {
            ICriterion criter = Expression.And(
            Expression.Not(Expression.Eq(ActionsEntity._PrimaryKeyName, actionId)),
            Expression.And(Expression.Eq(ActionsEntity.__ActionName, actionName),
             Expression.Eq(ActionsEntity.__IsDeleted, false))
             );
            var entity = this.Query<ActionsEntity>(criter).FirstOrDefault();
            return entity != null;
        }
        #endregion

    }
}