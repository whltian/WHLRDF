﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

namespace WHLRDF.Application.BLL
{
    /// <summary>
    /// 在线用户
    /// </summary>
    public class UserSocketModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public WebSocket Socket { get; set; }

        public string ConnectionId { get; set; }
    }
}
