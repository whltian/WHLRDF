using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.Application.Model;
namespace WHLRDF.Application.BLL
{
    public class PageActionService : SerivceBase, IPageActionService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<PageActionEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(PageActionEntity.__IsDeleted, false);
            return this.Query<PageActionEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public PageActionEntity GetById(string id)
        {
            return this.GetById<PageActionEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(PageActionEntity entity, ref string strError)
        {
            
            if (string.IsNullOrWhiteSpace(entity.ActionCode))
            {
                entity.ActionCode = entity.PageId + entity.ActionCode;
            }
            if (IsExist(entity))
            {
                strError = "按钮已存在，请不要重复添加";
                return false;
            }
            return this.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return this.Delete<PageActionEntity>(deleteKey,false);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                PageActionEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            string pageId = grid.GetValue("pageId");
            if (!string.IsNullOrWhiteSpace(pageId))
            {
                criter = Expression.And(criter, Expression.Eq(PageActionEntity.__PageId, pageId));
            }
            var result = this.Query<PageActionEntity>(criter);
           
            return this.Query<PageActionEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法

     

        /// <summary>
        /// 获取页面所有按钮
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="actionName"></param>
        /// <param name="localName"></param>
        /// <returns></returns>
        public List<PageActionEntity> ForGrid(string pageId,string actionName,string localName)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrWhiteSpace(actionName))
            {
                criter = Expression.And(criter,Expression.Like( PageActionEntity.__ActionName ,  actionName)  );
            }
            if (!string.IsNullOrWhiteSpace(localName))
            {
                criter = Expression.And(criter, Expression.Like(PageActionEntity.__LocalName, localName));
            }
            if (!string.IsNullOrWhiteSpace(pageId))
            {
                criter = Expression.And(criter, Expression.Eq(PageActionEntity.__PageId, pageId));
            }
           // var result = this.Query<PageActionEntity>(criter);

            return this.Query<PageActionEntity>(criter);
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool IsExist(PageActionEntity entity)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter = Expression.And(criter, Expression.Eq(PageActionEntity.__ActionName, entity.ActionName));
            criter = Expression.And(criter, Expression.Eq(PageActionEntity.__PageId, entity.PageId));
            criter = Expression.And(criter, Expression.NotEq(PageActionEntity._PrimaryKeyName, entity.ActionCode));
           
           return  this.Query<PageActionEntity>(criter).FirstOrDefault()!=null;
        }
        #endregion

    }
}