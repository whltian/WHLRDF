﻿
using WHLRDF.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WHLRDF.Application.BLL
{
    /// <summary>
    /// 获取系统菜单列表
    /// </summary>
    public class SystemMenuHelper
    {

        /// <summary>
        /// 初始化页面
        /// </summary>
        /// <param name="lstComponent">mvc dll引用路径</param>
        public static List<PageEntity> InitMenu(List<SystemComponent> lstComponent)
        {
            List<MenuPageAttribute> routePages = ControllerHelper.GetController(lstComponent);///获取所有页面列表
            List<PageEntity> entities = new List<PageEntity>();
            if (routePages != null && routePages.Count > 0)
            {
                foreach (var item in routePages)
                {
                    var page = new PageEntity
                    {
                        PageId = item.PageId,
                        LocalName = item.LocalName,
                        ActionName = item.ActionName,
                        RouteUrl = item.RouteUrl,
                        IsAdmin = item.IsAdmin,
                        IsDisplay = item.IsDispay,
                        IsMenu = item.IsMenu,
                        ParentId = item.ParentId,
                        OrderNum = item.OrderNum,
                        IsGet = item.IsGet,
                        IsPost = item.IsPost,
                        IsParent = item.IsParent,
                        NoAccess = item.NoAccess,
                        ControllerName = item.ControllerName,
                        AreaName = item.AreaName,
                        Icon = item.Icon,
                        IsDeleted=item.IsDeleted
                    };
                   
                    entities.Add(page);
                }
                //if (entities != null)
                //{
                //    //SystemPageEntities = entities.OrderBy(x => x.OrderNum).ToList();
                //}

                
            }
            return entities;
        }

        // <summary>
        /// 初始化页面
        /// </summary>
        /// <param name="lstMvc">mvc dll引用路径</param>
        public static List<PageEntity> InitApi(List<string> lstApi)
        {
            List<PageEntity> entities = new List<PageEntity>();
            List<MenuPageAttribute> routePages = ControllerHelper.GetApiController(lstApi);///获取所有页面列表
            if (routePages != null && routePages.Count > 0)
            {
                foreach (var item in routePages)
                {
                    var page = new PageEntity
                    {
                        PageId = item.PageId,
                        LocalName = item.LocalName,
                        ActionName = item.ActionName,
                        RouteUrl = item.RouteUrl,
                        IsAdmin = item.IsAdmin,
                        IsDisplay = item.IsDispay,
                        IsMenu = item.IsMenu,
                        ParentId = item.ParentId,
                        OrderNum = item.OrderNum,
                       
                        Icon = item.Icon,
                        IsApi = item.IsApi,
                        IsGet = item.IsGet,
                        IsPost = item.IsPost,
                        IsParent = item.IsParent,
                        NoAccess = item.NoAccess,
                        ControllerName = item.ControllerName,
                        AreaName = item.AreaName,
                    };
                    entities.Add(page);
                }
                //if (entities != null)
                //{
                //    SystemPageEntities = entities.OrderBy(x => x.OrderNum).ToList();
                //}


            }
            return entities;
        }
    }
}
