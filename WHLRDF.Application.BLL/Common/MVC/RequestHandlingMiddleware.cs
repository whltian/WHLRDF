﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;
using System;

using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq;
using WHLRDF.Log;

namespace WHLRDF.Application.BLL
{
    /// <summary>
    /// 请求中转中间件
    /// </summary>
    public class RequestHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public RequestHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        private string StaticFilesRootName
        {
            get
            {
                return ApplicationEnvironments.Site.ConfigPath.Where(x => x.RootKey == ConfigPathKeyType.statichtml).FirstOrDefault().Path + "/";
            }
        }
        public async Task Invoke(HttpContext context)
        {
            if (!context.Response.HasStarted)
            {
                DateTime startDate = DateTime.Now;
                context.Request.EnableBuffering();
                string responseBody = "";
                int statusCode = 200;
                int milliStart = Environment.TickCount;
                var httpContextModel = ApplicationEnvironments.DefaultSession.GetContextModel();
                bool IsStatic = false;//是否访问静态页面
                try
                {
                    if (File.Exists(StaticFilesRootName + context.Request.Path))
                    {
                        IsStatic = true;
                        await context.Response.WriteAsync(FileHelper.ReadLine(StaticFilesRootName + context.Request.Path));
                    }
                    else if (File.Exists(StaticFilesRootName + context.Request.Path + ".html"))
                    {
                        IsStatic = true;
                        await context.Response.WriteAsync(FileHelper.ReadLine(StaticFilesRootName + context.Request.Path+".html"));
                    }
                    else
                    {
                        httpContextModel.Result.ContentType = context.Response.ContentType;
                        httpContextModel.Result.Headers = context.Response.Headers.ToDictionary(x => x.Key, v => string.Join(";", v.Value.ToList())).ToJson();
                        if (AppHttpContext.IsPost || AppHttpContext.IsAjax)
                        {
                            var originalBodyStream = context.Response.Body;
                            using (var responseStream = new MemoryStream())
                            {
                                context.Response.Body = responseStream;
                                await next(context);
                                responseBody = await GetResponse(context.Request, context.Response);
                                await responseStream.CopyToAsync(originalBodyStream);
                            }
                        }
                        else
                        {
                            await next(context);
                        }
                        statusCode = context.Response.StatusCode;
                    }
                }
                catch (Exception ex)
                {
                    statusCode = 503;
                    responseBody = ex.Message;
                   
                  
                }
                finally
                {
                    if (statusCode == 401)
                    {
                        responseBody = "未授权";
                    }
                    else if (statusCode == 404)
                    {
                        responseBody = "未找到服务";
                    }
                    else if (statusCode == 502)
                    {
                        responseBody = "请求错误";
                    }

                    httpContextModel.Result.StatusCode = statusCode;

                    Regex regex = new Regex("^(2|3)\\d+$");
                    if (AppHttpContext.IsAjax && !regex.IsMatch(statusCode.ToString()) && !string.IsNullOrWhiteSpace(responseBody))
                    {
                        // LogHelper.RequestInfo(this.GetType(), httpContextModel);
                        await HandleExceptionAsync(context, statusCode, responseBody);
                    }
                    httpContextModel.Result.Body = responseBody;
                }
                int milliEnd = Environment.TickCount;
                var EnvSeconds = TimeSpan.FromMilliseconds(milliEnd - milliStart).TotalMilliseconds;
                if (!IsStatic)
                {
                    context.Response.OnCompleted(() =>
                    {
                        AddLogger(httpContextModel, EnvSeconds);
                        return Task.CompletedTask;
                    });
                }
              


            }
        }

        private async Task HandleExceptionAsync(HttpContext context, int statusCode, string msg)
        {
           var strError = JsonConvert.SerializeObject(AjaxResult.Error(msg, statusCode));
            context.Response.ContentType = "text/json;charset=utf-8";
            //  var data = new { code = statusCode.ToString(), is_success = false, msg = msg };

            await context.Response.WriteAsync(strError).ConfigureAwait(false);

        }
        /// <summary>
        /// 添加请求日志
        /// </summary>
        /// <param name="context">请求主体</param>
        /// <param name="totalseconds">执行时长</param>

        private static void AddLogger(HttpLoggingModel context,double totalseconds)
        {
            try
            {
                TimeSpan span = DateTime.Now - context.StartDate;
                context.Result.EndDate = DateTime.Now;
                context.Result.TotalMilliseconds = totalseconds;
                LogHelper.RequestInfo(context);
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Exception), ex);
            }
        }

        /// <summary>
        /// 获取响应内容
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public async Task<string> GetResponse(HttpRequest request, HttpResponse response)
        {
            string responseBody = "";
            response.Body.Seek(0, SeekOrigin.Begin);
            try
            {
                if (request.ContentType!=null&&(ApplicationEnvironments.Site.NoGetResponse == null || ApplicationEnvironments.Site.NoGetResponse.Where(x => x.ToLower().Equals(request.ContentType.ToLower())).FirstOrDefault() == null))
                {
                    var AcceptEncoding = request.Headers["Accept-Encoding"].ToArray();
                    if (request.Headers.ContainsKey("Accept-Encoding") && AcceptEncoding != null && AcceptEncoding.Length > 0)
                    {
                        AcceptEncoding = AcceptEncoding[0].Replace(" ", "").Split(',');
                        if (AcceptEncoding.Contains("br"))
                        {
                            using (var zipStream = new BrotliStream(response.Body, CompressionMode.Decompress, true))
                            {
                                responseBody =await new StreamReader(zipStream, Encoding.UTF8).ReadToEndAsync();
                            }
                        }
                        else if (AcceptEncoding.Contains("gzip"))
                        {
                            using (var zipStream = new GZipStream(response.Body, CompressionMode.Decompress, true))
                            {
                                responseBody = await new StreamReader(zipStream, Encoding.UTF8).ReadToEndAsync();
                            }
                        }
                        else if (AcceptEncoding.Contains("deflate"))
                        {
                            using (var zipStream = new DeflateStream(response.Body, CompressionMode.Decompress, true))
                            {
                                responseBody = await new StreamReader(zipStream, Encoding.UTF8).ReadToEndAsync();
                            }
                        }
                    }
                    else
                    {
                        responseBody = await new StreamReader(response.Body, Encoding.UTF8).ReadToEndAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                responseBody ="内容转义出错："+ ex.Message+ex.StackTrace;
            }
            response.Body.Seek(0, SeekOrigin.Begin);
            return responseBody;
        }

        /// <summary>
        /// 读取返回内容
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<string> ReadBodyAsync(HttpContext context)
        {
            string responseBody = "";
            using (var swapStream = new MemoryStream())
            {
                var originalResponseBody = context.Response.Body;
                context.Response.Body = swapStream;
                
                swapStream.Position = 0;
                //swapStream.Seek(0, SeekOrigin.Begin);
                try
                {
                    if (ApplicationEnvironments.Site.NoGetResponse == null || ApplicationEnvironments.Site.NoGetResponse.Where(x => x.ToLower().Equals(context.Response.ContentType.ToLower())).FirstOrDefault() == null)
                    {
                        var AcceptEncoding = context.Request.Headers["Accept-Encoding"].ToArray();
                        if (context.Request.Headers.ContainsKey("Accept-Encoding") && AcceptEncoding != null && AcceptEncoding.Length > 0)
                        {
                            AcceptEncoding = AcceptEncoding[0].Replace(" ", "").Split(',');
                            if (AcceptEncoding.Contains("br"))
                            {
                                using (var zipStream = new BrotliStream(swapStream, CompressionMode.Decompress, true))
                                {
                                    responseBody = new StreamReader(zipStream, Encoding.UTF8).ReadToEnd();
                                }
                            }
                            else if (AcceptEncoding.Contains("gzip"))
                            {
                                using (var zipStream = new GZipStream(swapStream, CompressionMode.Decompress, true))
                                {
                                    responseBody = new StreamReader(zipStream, Encoding.UTF8).ReadToEnd();
                                }
                            }
                            else if (AcceptEncoding.Contains("deflate"))
                            {
                                using (var zipStream = new DeflateStream(swapStream, CompressionMode.Decompress, true))
                                {
                                    responseBody = new StreamReader(zipStream, Encoding.UTF8).ReadToEnd();
                                }
                            }
                        }
                        else
                        {
                            responseBody = new StreamReader(swapStream, Encoding.UTF8).ReadToEnd();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseBody = ex.Message;
                }
                swapStream.Position = 0;
            //    swapStream.Seek(0, SeekOrigin.Begin);
                await swapStream.CopyToAsync(originalResponseBody);
                context.Response.Body = originalResponseBody;
            }
            return responseBody;
        }
    }

    public static class RequestHandlingExtensions
    {
        public static IApplicationBuilder UseRequestHandling(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestHandlingMiddleware>();
        }
    }

}
