﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using WHLRDF.Application.Model;
using Microsoft.AspNetCore.Http;
using WHLRDF.Log;
using WHLRDF.Application.BLL.WS;
using System.Web;
using Microsoft.AspNetCore.Authentication.Cookies;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{

    public class BaseController : Controller, IApplicationSession
    {

        public string UserId { get => (AppHttpContext.Current!=null&&AppHttpContext.Current.User.Identity.IsAuthenticated) ? AppHttpContext.Current.User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value : ""; }

        public string UserName { get => (AppHttpContext.Current != null && AppHttpContext.Current.User.Identity.IsAuthenticated) ? AppHttpContext.Current.User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault().Value : ""; }

        public string RealName { get => (AppHttpContext.Current != null && AppHttpContext.Current.User.Identity.IsAuthenticated) ? AppHttpContext.Current.User.Claims.Where(x => x.Type == ClaimTypes.GivenName).FirstOrDefault().Value : ""; }
        public string UserToken {
            get {
                string token = "";
                if (string.IsNullOrWhiteSpace(token))
                {
                    if (AppHttpContext.Current != null && AppHttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        token = AppHttpContext.Current.User.Claims.Where(x => x.Type == ClaimTypes.Authentication).FirstOrDefault().Value;
                    }
                    if (AppHttpContext.Current != null && AppHttpContext.Current.Request != null)
                    {
                        if (string.IsNullOrWhiteSpace(token))
                        {
                            token = AppHttpContext.Current.Request.Headers[AppHttpContext.TokenKeyName];
                        }
                        if (string.IsNullOrWhiteSpace(token))
                        {
                            token = CookieHelper.GetCookies(AppHttpContext.TokenKeyName);
                        }
                     
                        if (string.IsNullOrWhiteSpace(token))
                        {
                            if (AppHttpContext.Current.Request.HasFormContentType)
                            {
                                var formData =  AppHttpContext.Current.Request.Form;
                                if (formData != null && formData.Count > 0)
                                {
                                    token = formData[AppHttpContext.TokenKeyName];
                                }
                            }
                           
                        }
                    }
                }
               
                
                return token;
            }
        }

       

        public bool IsPost
        {
            get
            {
                if (this.Request.Method.Equals("POST", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsAjax
        {
            get
            {
                string sheader = Request.Headers["X-Requested-With"];
                return (sheader != null && sheader == "XMLHttpRequest") ? true : false;
               
            }
        }
     
        /// <summary>
        /// 获取当前用户
        /// </summary>
        public UserEntity CurrentUser {
            get {
                return this.GetUser<UserEntity>();
            }
        }
        /// <summary>
        /// 获取当前用户
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetUser<T>()where T:class,new()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(UserId) || !string.IsNullOrWhiteSpace(UserToken))
                {
                    T entity = CacheHelper.CacheService.GetCache<T>(ApplicationEnvironments.Site.UserCacheKey + UserToken);
                    if (entity == null )
                    {
                        if (!string.IsNullOrWhiteSpace(UserId))
                        {
                            object userEntity = AppHttpContext.GetSerivce<IUserService>().GetByInfoId(UserId.ToString(), UserToken);
                            entity = (T)userEntity;
                        }
                    }
                    //if (entity == null)
                    //{
                    //    AppHttpContext.Current.Response.Redirect("/home/logoff");
                    //}
                    return entity;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(this,ex);
            }
            return default(T);
        }
    

        /// <summary>
        /// ip地址
        /// </summary>
        public string IPAddress {
            get
            {
                var ip = AppHttpContext.Current.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                if (string.IsNullOrEmpty(ip))
                {
                    ip = AppHttpContext.Current.Connection.RemoteIpAddress.ToString();
                }
                return ip;
            }
        }
        public string FormAddress { get =>   new StringBuilder()
                .Append(AppHttpContext.Current.Request.Scheme)
                .Append("://")
                .Append(AppHttpContext.Current.Request.Host)
                .Append(AppHttpContext.Current.Request.PathBase)
                .Append(AppHttpContext.Current.Request.Path)
               //.Append(AppHttpContext.Current.Request.QueryString)
                .ToString();
    }
        public string ServerAddress { get => Directory.GetCurrentDirectory(); }

        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            return base.OnActionExecutionAsync(context, next);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userNo"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="rememberMe"></param>
        public async Task SignInAsync(List<Claim> claims,string userId,string token, bool rememberMe=false)
        {
            // CookieHelper.Add("UserNo", token, 30);
            CookieHelper.Add(AppHttpContext.TokenKeyName, token, 30);
            var claimUser = claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault();
            if (claimUser != null)
            {
                CookieHelper.Add(ConfigHelper.COOKIE_USER_NAME, claimUser.Value);
            }
            
            var Identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
     
            Identity.AddClaims(claims);
            var userPrincipal = new ClaimsPrincipal(Identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, userPrincipal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.Now.AddMinutes(ApplicationEnvironments.Site.SessionTimeout),
                IsPersistent = true,
                AllowRefresh = true
            });
            
            //AppHttpContext.CacheService.Add(ApplicationEnvironments.Site.UserCacheKey+ userId, user);
            //signin 
            //HttpContext.Authentication.SignInAsync("Cookie", userPrincipal, new AuthenticationProperties
            //{
            //    ExpiresUtc = DateTime.UtcNow.AddMinutes(30),
            //    IsPersistent = false,
            //    AllowRefresh = false
            //});
        }

       
        public async Task SignOutAsync()
        {
            CacheHelper.CacheService.Remove(ApplicationEnvironments.Site.UserCacheKey + UserToken);
            CookieHelper.Delete(AppHttpContext.TokenKeyName);
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        //public override JsonResult Json(object data)
        //{
        //    IsoDateTimeConverter convert = new IsoDateTimeConverter();
        //    convert.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        //    JsonSerializerSettings settings = new JsonSerializerSettings() {
              
        //    };
        //    settings.Converters.Add(convert);
       
        //    return new JsonResult(data, settings);
        //}

        public void SetToken(string value)
        {
            if (AppHttpContext.Current.User.Identity.IsAuthenticated)
            {
                var claim = AppHttpContext.Current.User.Claims.Where(x => x.Type == ClaimTypes.Authentication).FirstOrDefault();
                var identity = AppHttpContext.Current.User.Identity as ClaimsIdentity;
                if (identity.TryRemoveClaim(claim))
                {
                    AppHttpContext.Current.User.Claims.Append(new Claim(ClaimTypes.Authentication, value));
                }
                CookieHelper.Add(AppHttpContext.TokenKeyName, value);

            }
            
        }

        /// <summary>
        /// Excel导出
        /// </summary>
        /// <param name="buffer">字节流</param>
        /// <param name="fileName">文件名（ DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx"）</param>
        /// <returns></returns>
        public IActionResult ExportExcel(byte[] buffer, string fileName)
        {
            if (buffer == null || buffer.Length <= 0 || string.IsNullOrWhiteSpace(fileName))
            {
                return Json(AjaxResult.Error("导出文件异常或者文件名为空"));
            }

            return File(buffer, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        /// <summary>
        /// Excel导出
        /// </summary>
        /// <param name="buffer">字节流</param>
        /// <param name="fileName">文件名（ DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx"）</param>
        /// <returns></returns>
        public IActionResult ExportWord(byte[] buffer, string fileName)
        {
            if (buffer == null || buffer.Length <= 0 || string.IsNullOrWhiteSpace(fileName))
            {
                return Json(AjaxResult.Error("导出文件异常或者文件名为空"));
            }

            return File(buffer, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName);
        }
        /// <summary>
        /// 错误信息转换成系统错误信息
        /// </summary>
        /// <param name="dicError"></param>
        public void ConvertToModelState(EntityBase entityBase)
        {
            if (entityBase == null)
            {
                return;
            }
            Dictionary<string, string> dicError = entityBase.CheckValid();
            if (dicError != null && dicError.Count > 0)
            {
                foreach (var item in dicError)
                {
                    ModelState.AddModelError(item.Key, item.Value);
                }
            }
        }
        public HttpLoggingModel GetContextModel()
        {
            var context = AppHttpContext.Current;
            var user = GetUser<UserEntity>();
            Dictionary<string, string> frmData = new Dictionary<string, string>();
            if (context.Request.HasFormContentType)
            {
                foreach (var item in AppHttpContext.Current.Request.Form)
                {
                    frmData.Add(item.Key,item.Value);
                }
            }
            Dictionary<string, string> QueryData = new Dictionary<string, string>();
            foreach (var item in AppHttpContext.Current.Request.Query)
            {
                QueryData.Add(item.Key, item.Value);
            }
            HttpLoggingModel contextModel = new HttpLoggingModel
            {
                MenuPage = AppHttpContext.Current.Request.Path,
                Headers = context.Request.Headers.ToJson(),
                Host = context.Request.Host.ToJson(),
                Query = QueryData.ToJson(),
                Form = context.Request.HasFormContentType ? frmData.ToJson() : "",
                Body = ReadRequestBody(context.Request, Encoding.UTF8),
                TraceIdentifier = AppHttpContext.Current.TraceIdentifier,
                Protocol = AppHttpContext.Current.Request.Protocol,
                StartDate = DateTime.Now,
                ContentType = context.Request.ContentType,
                IsHttps = AppHttpContext.Current.Request.Scheme.ToLower().Equals("https"),
                IsPost = AppHttpContext.IsPost,
                IsAjax = AppHttpContext.IsAjax,
                AcceptEncoding = "",//string.Join(',', AppHttpContext.Current.Request.Headers["Accept-Encoding"]),
                Result = new HttpLoggingResultModel()
            };
            return contextModel;
        }


        /// <summary>
        /// 读取请求正文
        /// </summary>
        /// <param name="request"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        private string ReadRequestBody(HttpRequest request, Encoding encoding)
        {
            var body = "";
            request.EnableBuffering();
            if (request.ContentLength == null ||
                !(request.ContentLength > 0) ||
                !request.Body.CanSeek)
            {
                return body;
            }
            request.Body.Seek(0, SeekOrigin.Begin);

            using (var reader = new StreamReader(request.Body, encoding, true, 1024, true))
            {
                body = HttpUtility.UrlDecode( reader.ReadToEnd());
            }
            request.Body.Position = 0;
            return body;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="userno"></param>
        /// <param name="meesgeType"></param>
        /// <param name="message"></param>
        public void SendMessage(string userno,int meesgeType, string message)
        {
            if (ApplicationEnvironments.Site.IsSignalR)
            {
                SignalRHubHelper.Send(new SocketMessageModel
                {
                    SenderId = "Service",
                    SenderName = "系统",
                    ReceiverId = this.UserId,
                    Content = message
                });
            }
            else {
               // WebSocketHelper.SendAsync(userno, meesgeType, message);
            }
            
          
        }
        /// <summary>
        /// 获取用户头像地址
        /// </summary>
        /// <returns></returns>
        public string GetUserAvatarUrl(string userid)
        {
            if (string.IsNullOrWhiteSpace(userid))
            {
                userid = UserId;
            }
            string userFloder = ApplicationEnvironments.PublicPath + "/avatar";
            if (!string.IsNullOrWhiteSpace(userid))
            {
                if (FileHelper.Exist(ApplicationEnvironments.BaseDirectory+ userFloder + "/" + userid + "/avatar.jpg"))
                {
                    userFloder += "/" + userid;
                }
            }
            userFloder += "/avatar.jpg";

            return userFloder.Replace(ApplicationEnvironments.RootPath, ApplicationEnvironments.HttpRootPath) + "?d=" + DateTime.Now.ToString("yyyyMMddHHmm");
        }
    }
}
