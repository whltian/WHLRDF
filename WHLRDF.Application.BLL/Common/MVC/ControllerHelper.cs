﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Loader;
using WHLRDF.ORM;

namespace WHLRDF.Application.BLL
{
    /// <summary>
    /// 控制器操作类 
    /// </summary>
    public class ControllerHelper
    {
        #region 获取mvc权限
        /// <summary>
        /// 获取 mvc 页面权限
        /// </summary>
        /// <param name="mvcAssembliesPath"></param>
        /// <returns></returns>
        public static List<MenuPageAttribute> GetController(List<SystemComponent> lstComponent)
        {
            #region 通过反射读取Action方法写入对应权限至集合
            List<MenuPageAttribute> authAttributes = new List<MenuPageAttribute>();
            List<string> keys = new List<string>();
          
            //读取当前项目程序集中集成自AdminController的控制器
            if (lstComponent != null && lstComponent.Count > 0)
            {
                foreach (var component in lstComponent)
                {
                    if (keys.Contains(component.Id))
                        continue;
                    keys.Add(component.Id);
                    if (!component.IsMenu)
                    {
                        continue;
                    }
                    var result = GetController(component);
                    if (result != null && result.Count > 0)
                    {
                        authAttributes.AddRange(result);
                    }
                    
                }
            }
            return authAttributes;
            #endregion
        }

        /// <summary>
        /// 获取单个dll中的权限
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<MenuPageAttribute> GetController(SystemComponent component)
        {
            #region 通过反射读取Action方法写入对应权限至集合
            List<MenuPageAttribute> menuPages = new List<MenuPageAttribute>();
            List<string> keys = new List<string>();
            if (component==null||!component.Opened || string.IsNullOrWhiteSpace(component.Id))//已关闭的组件不在加载
            {
                return null;
            }
         
            var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(AppDomain.CurrentDomain.BaseDirectory + (!string.IsNullOrWhiteSpace(component.Api) ? component.Api : component.Mvc) );
            var areaController = assembly.GetTypes().Where(x => x.BaseType == (typeof(BaseAreaController))).FirstOrDefault();
            if (areaController != null)
            {
                string areaName = "";
                var areaAttr = areaController.GetCustomAttribute(typeof(AreaAttribute), true);
                if (areaAttr != null)
                {
                    areaName = (areaAttr as AreaAttribute).RouteValue;
                }
                var parentMenu = new MenuPageAttribute(component.OrderNum, MenuPageAttribute.PAGEPREFIX+ component.Id, component.Name, component.Unique, component.Icon, component.Opened);
                if (!keys.Contains(parentMenu.PageId))
                {
                    keys.Add(parentMenu.PageId);
                    menuPages.Add(parentMenu);
                }
                parentMenu.ActionName = areaName;
                parentMenu.ControllerName = areaName;
                parentMenu.AreaName = areaName;
                var lstPage = assembly.GetTypes().Where(x => x.IsSubclassOf(areaController)).ToList();
                if (lstPage != null && lstPage.Count > 0)
                {
                    foreach (var type in lstPage)
                    {
                        var pageType = type.GetCustomAttribute(typeof(MenuPageAttribute), false);
                        if (pageType != null)
                        {
                            var page = pageType as MenuPageAttribute;
                            if (!page.IsDeleted)
                            {
                                page.PageId = parentMenu.PageId + page.PageId;
                                if (keys.Contains(page.PageId))
                                {
                                    continue;
                                }
                                page.AreaName = areaName;
                                page.ControllerName = type.Name.Replace("Controller", "");
                                page.ParentId = parentMenu.PageId;
                                page.OrderNum = parentMenu.OrderNum + page.OrderNum;
                                GetActionMenus(areaName,type, page,ref menuPages);
                                page.ActionName = type.Name.Replace("Controller", "");
                                page.ParentName = component.Unique;
                                page.RouteUrl = ((!string.IsNullOrWhiteSpace(areaName) ?areaName+"/":"/") + page.ControllerName).ToLower();
                                if (!keys.Contains(page.PageId))
                                {
                                    keys.Add(page.PageId);
                                    menuPages.Add(page);
                                }
                            }
                        }
                    }
                }
            }
            return menuPages;
            #endregion
        }

        /// <summary>
        /// 获取单个控制器中的Actions
        /// </summary>
        /// <param name="controller"></param>
        public static void GetActionMenus(string areaName,Type controller, MenuPageAttribute parent,ref List<MenuPageAttribute> menuPages)
        {
            var members = controller.GetMethods().Where(e => e.GetCustomAttribute(typeof(MenuPageAttribute), true) != null);
            string[] systemAction = { "index", "forgrid" };
            foreach (var member in members)
            {
                if (systemAction.Contains(member.Name.ToLower()))
                {
                    continue;
                }
                //获取功能列表
                var attr = member.GetCustomAttribute(typeof(MenuPageAttribute), true);
                if (attr == null)
                    continue;
                var menuPage = attr as MenuPageAttribute;
             
                if (string.IsNullOrWhiteSpace(menuPage.PageId) || !string.IsNullOrWhiteSpace(menuPage.ActionName) || menuPage.IsParent || menuPage.NoAccess)
                {
                    continue;
                }
                menuPage.PageId = parent.PageId + menuPage.PageId;
                menuPage.ParentId = parent.PageId;
                menuPage.AreaName = areaName;
                menuPage.OrderNum = parent.OrderNum+ menuPage.PageId.ToInt();
                if (string.IsNullOrWhiteSpace(menuPage.ActionName))
                {
                    menuPage.ActionName = member.Name;
                }
                menuPage.ControllerName = controller.Name.Replace("Controller", "");
                menuPage.RouteUrl = (parent.RouteUrl + "/" + menuPage.ActionName).ToLower();
                //功能对应的二级菜单
                menuPages.Add(menuPage);
               
            }
           
        }

        #endregion

        #region 获取api
        /// <summary>
        /// 获取api 页面权限
        /// </summary>
        /// <param name="mvcAssembliesPath"></param>
        /// <returns></returns>
        public static List<MenuPageAttribute> GetApiController(List<string> mvcAssembliesPath)
        {
            #region 通过反射读取Action方法写入对应权限至集合
            List<MenuPageAttribute> menuPages = new List<MenuPageAttribute>();
            List<string> keys = new List<string>();
            //读取当前项目程序集中集成自AdminController的控制器
            if (mvcAssembliesPath != null && mvcAssembliesPath.Count > 0)
            {
                foreach (var file in mvcAssembliesPath)
                {
                    var assembly = Assembly.LoadFrom(file);
                    var lstPage = assembly.GetTypes().Where(x => x.IsSubclassOf(typeof(ControllerBase))).ToList();
                    if (lstPage != null && lstPage.Count > 0)
                    {
                        foreach (var type in lstPage)
                        {
                            var pageType = type.GetCustomAttribute(typeof(MenuPageAttribute), true);
                            if (pageType != null)
                            {
                                var page = pageType as MenuPageAttribute;
                                if (!page.IsDeleted)
                                {
                                    page.AreaName = "";
                                    page.ControllerName = type.Name.Replace("Controller", "");
                                    page.ParentId = "";
                                 
                                    GetActionMenus("", type, page, ref menuPages);
                                    page.ActionName = type.Name.Replace("Controller", "");
                                    page.ParentName = "";
                                    if (!keys.Contains(page.PageId))
                                    {
                                        keys.Add(page.PageId);
                                        menuPages.Add(page);
                                    }
                                }

                            }
                        }
                    }



                }
            }
            return menuPages;
            #endregion
        }

        /// <summary>
        /// 获取单个控制器中的Actions
        /// </summary>
        /// <param name="controller"></param>
        public static void GetApiMenuPages(Type controller, MenuPageAttribute parent, ref List<MenuPageAttribute> menuPages)
        {
            List<BasePermissionAttribute> authAttributes = new List<BasePermissionAttribute>();
            var areaAttr = controller.GetCustomAttribute(typeof(AreaAttribute), true);
            string areaName = "";
            if (areaAttr != null)
            {
                areaName = (areaAttr as AreaAttribute).RouteValue;
            }

            var members = controller.GetMethods().Where(e => e.GetCustomAttribute(typeof(MenuPageAttribute), true) != null);
            string[] systemAction = { "index", "forgrid" };
            foreach (var member in members)
            {
                if (systemAction.Contains(member.Name.ToLower()))
                {
                    continue;
                }
                //获取功能列表
                var menuPage = member.GetCustomAttribute(typeof(MenuPageAttribute), true);
                if (menuPage == null)
                    continue;
                var action = menuPage as MenuPageAttribute;
                if (string.IsNullOrWhiteSpace(action.PageId) || !string.IsNullOrWhiteSpace(action.ActionName) || action.IsParent || action.NoAccess)
                {
                    continue;
                }
                action.PageId = parent.PageId + action.PageId;
                action.ParentId = parent.PageId;
                action.AreaName = areaName;
                action.OrderNum = parent.OrderNum + action.PageId.ToInt();
                if (string.IsNullOrWhiteSpace(action.ActionName))
                {
                    action.ActionName = member.Name;
                }
                action.ControllerName = controller.Name.Replace("Controller", "");
                action.RouteUrl = (parent.RouteUrl + "/" + action.ActionName).ToLower();

                //功能对应的二级菜单
                menuPages.Add(action);

            }
            
        }

        #endregion
    }
}
