﻿using Microsoft.AspNetCore.Authorization;
namespace WHLRDF.Application.BLL
{
    public class BaseAreaController:BaseController
    {
        public virtual string AreaName { get; }
    }
}
