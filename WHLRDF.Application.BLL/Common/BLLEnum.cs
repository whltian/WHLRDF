﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace WHLRDF.Application.BLL
{
    public enum NotifyTypeEnum
    {
        /// <summary>
        /// 系统消息
        /// </summary>
        [Description("系统消息")]
        Default=2,
        /// <summary>
        /// 激活码
        /// </summary>
        [Description("激活码")]
        ActivationCode =1,
        /// <summary>
        /// 用户消息
        /// </summary>
        [Description("用户消息")]
        Users = 3
    }

    public enum NotifyStateEnum
    {
        /// <summary>
        /// 未读
        /// </summary>
        [Description("未读")]
        WaitRead = 0,

        /// <summary>
        /// 已读
        /// </summary>
        [Description("已读")]
        Read =1,
    }

    public enum UserType
    {
        /// <summary>
        /// 系统用户
        /// </summary>
        [Description("系统用户")]
        System=0,

        /// <summary>
        /// 用户类型
        /// </summary>
        [Description("注册用户")]
        Regiter=1
    }
}
