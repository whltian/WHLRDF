﻿$(function () {
    $(".jstree").jstree({
        "core": {
            "check_callback": true,//默认为flase，会导致无法使用修改树结构。
            data: function (node, callback) {
                var _self = this;

                Ajax({
                    url: BaseUrl + "/GetFolders",
                    data: { parent: node.id },
                    success: function (result) {
                        callback.call(_self, result);
                    }
                });

            }

        },
        "contextmenu": {
            select_node: true,
            show_at_node: true,
            show_Itemed: function (treeNode, items) {
                return items;
            },
            "items": {
                "delete": {
                    "label": "删除",
                    "icon": "fa fa-trash",
                    "action": function (treeNode) {
                        var tree = jQuery.jstree.reference(treeNode.reference);
                        var node = tree.get_node(treeNode.reference);
                        Notify.Confirm("确认要删除文件夹吗?", "确认删除", function () {
                            Ajax({
                                url: BaseUrl + "/FolderDelete",
                                data: { folder: node.id },
                                success: function (result) {
                                    tree.delete_node(treeNode.reference);
                                }
                            });
                        });
                    }
                },


            }
        },

        "plugins": ["wholerow", "contextmenu"]
    }).on('activate_node.jstree', function (obj, e) {
        if (e.node.id == "" || e.node.id == "#") {
            return false;
        }
        Ajax({
            url: BaseUrl + "/GetFiles",
            data: { filePath: e.node.id },
            success: function (result) {
                $(".file-container").html("");
                if (result != null && result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        var html = '<div class="file-box">';
                        html += ' <div class="file"><button class="btn btn-danger btn-circle" data-id="' + result[i].id + '" type="button" style="position:absolute;right: 0;width: 20px;height: 20px;padding:0;"> <i class="fa fa-close"></i></button>';
                        html += '<span class="corner"></span>';
                        if (result[i].IsImage) {
                            html += ' <div class="image">';
                            html += '<a href="' + result[i].HttpPath + '" target="_blank"><img alt="image" class="img-responsive" src="' + result[i].icon + '" /></a>';
                            html += '</div>';
                        }
                        else {
                            html += '<div class="icon">';
                            html += '<i class="fa ' + result[i].icon + '"></i>';
                            html += '</div>';
                        }
                        html += '<div class="file-name">';
                        html += '<a href="' + result[i].HttpPath + '" target="_blank">' + result[i].text + '</a>';
                        html += '<br>';
                        html += '<small>' + result[i].EditTime + '</small>';
                        html += '  </div>';
                        html += '  </div>';
                        html += '  </div>';
                        $(".file-container").append(html);
                    }
                }
                $(".file-container").find(".btn-circle").on("click", function () {
                    var _self = $(this);
                    Notify.Confirm("确认要删除文件吗，删除后无法恢复?", "确认删除", function () {
                        Ajax({
                            url: BaseUrl + "/FileDelete",
                            data: { filePath: _self.attr("data-id") },
                            success: function (result) {
                                _self.parent().parent().remove();
                            }
                        });
                    });
                    //alert($(this).attr("data-id"));
                });
                // $(".file-container").
            }
        });

    });

});