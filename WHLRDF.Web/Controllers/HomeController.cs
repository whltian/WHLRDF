﻿
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WHLRDF;
using WHLRDF.Application.BLL;
using WHLRDF.Application.Model;
using WHLRDF.Log;

namespace WHLRDF.Web.Controllers
{
   
    public class HomeController : BaseController
    {
        public IUserService mService { get; set; }



        // public  IUserService _service { get; set; }
        //public HomeController(IUserService service)
        //{
        //    _service = service;
        //}
        [HttpGet]
        [StaticHtml("/api/avatar",IsGet =true)]
        public IActionResult GetUser()
        {
            return View("");
        }
        public IActionResult Index() {

            //WHLRDF.Cache.MongoDBService.Instance.Add("abc",new UserEntity() { UserName = "Test1" });
            //WHLRDF.Cache.MongoDBService.Instance.Add("abc", new UserEntity() { UserName = "Test2" });
            //WHLRDF.Cache.MngoDBService.Instance.Add("abc", new UserEntity() { UserName="Test3"});
            //WHLRDF.Cache.MongoDBService.Instance.Add("ddd","aaaa");
            //mService.GetById("100000");
           
            //  _service.Add();
            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Stop (){
            ApplicationEnvironments.AppManagerService.Stop();
            return Json(AjaxResult.Success("停止成功"));
        }
        public IActionResult Restart()
        {
            ApplicationEnvironments.AppManagerService.Restart();
            return Json(AjaxResult.Success("重启成功"));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Error()
        {
            var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var error = feature?.Error;
            ViewData["Message"] = error?.Message;
            ViewData["Exception"] = error;
            ViewData["StatusCode"] =500;
            if (IsAjax)
            {
                return Json(AjaxResult.Error(error.Message, HttpContext.Response.StatusCode));
            }
            LogHelper.Error(feature.GetType(), error.Message);
            return View();
        }
       
        public async Task<IActionResult> LogOff()
        {
            await SignOutAsync();
            return RedirectToAction("Index", "account");
        }
    }
}
