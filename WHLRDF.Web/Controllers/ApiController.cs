﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;
using WHLRDF.Application.Model;
using WHLRDF.ORM;

namespace WHLRDF.Web.Controllers
{

    public class ApiController : BaseController
    {
        //public IApiService apiService { get; set; }


        public INotifyService nService { get; set; }

        public IUserService uService { get; set; }
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public FileResult VerificationCode(string type)
        {
            var strRandomCode = SecurityHelper.CreateRandomCode(6);
            var bytGraphic = SecurityHelper.CreateValidateGraphic(strRandomCode);
            HttpContext.Session.Set(type, strRandomCode);
            return File(bytGraphic, "image/jpeg");
        }
        //[ResponseCache(CacheProfileName = "default", VaryByQueryKeys = new string[] { "id" })]
        //public FileResult avatar(string id)
        //{
        //    //if (string.IsNullOrWhiteSpace(id))
        //    //{
        //    //    id = this.UserId;
        //    //}
        //    //var bytGraphic = apiService.GetUserAvatar(id);
        //    return File(bytGraphic, "image/jpeg");
        //}
        /// <summary>
        /// 代办事项
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <returns></returns>
        [HttpPost]
        public  IActionResult MyToDoList(LigerGrid ligerGrid)
        {
            //return await Task.Factory.StartNew<IActionResult>(() =>
            //{
            //    ligerGrid.pageSize = 10;
            //    ligerGrid.pageIndex = 1;
            //    ligerGrid = pService.MyTodoList(ligerGrid, this.UserId);
            //    return Json(AjaxResult.Success(ligerGrid));
            //});
            return Json(AjaxResult.Success(ligerGrid));
        }
        /// <summary>
        /// 个人消息
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult MyNotify(LigerGrid ligerGrid)
        {
            ligerGrid.pageSize = 10;
            ligerGrid.pageIndex = 1;
            ligerGrid = nService.MyNotify(ligerGrid, this.UserId);
            return Json(AjaxResult.Success(ligerGrid));
        }
        [ResponseCache(Duration = 60, VaryByQueryKeys = new string[] { "keyWord" })]
        [Authorize]
        [HttpPost]
        public IActionResult GetUsers(LigerGrid ligerGrid)
        {

            var lst = uService.GetUsersByName(ligerGrid);
            return Json(AjaxResult.Success(lst));
        }

        [Authorize]
        [HttpPost]
        public IActionResult ReaderNotify(string notifyid)
        {
            string strError = "";
            if (!nService.ReaderNotify(notifyid, ref strError))
            {
                return Json(AjaxResult.Error(strError));
            }
            return Json(AjaxResult.Success());
        }
      
    }
}