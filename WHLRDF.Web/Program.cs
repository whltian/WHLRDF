using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WHLRDF.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //ApplicationEnvironments.AppManagerService = ApplicationManagerService.Instance;
            //try
            //{
            //    var appManager = ApplicationEnvironments.AppManagerService;

            //    do
            //    {
            //        ApplicationEnvironments.AppManagerService.Start();
            //    } while (appManager.Restarting);


            //}
            //catch (Exception ex)
            //{

            //}
           
          
           CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(serverOptions =>
                    {
                        // Set properties and call methods on options
                    })
                    .UseContentRoot(Directory.GetCurrentDirectory())
               .UseStartup<Startup>();
                  
                });
    }
}
