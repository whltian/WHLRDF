using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using WHLRDF.Application.BLL;
using WHLRDF.Cache;
using WHLRDF.Log;
using WHLRDF.ORM;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WHLRDF.Application.BLL.WS;
using WHLRDF.Socket;
using AutoMapper;

namespace WHLRDF.Web
{
    public class AutofacDefaultModule : Autofac.Module
    {
        /// <summary>
        /// 重写注入
        /// </summary>
        /// <param name="containerBuilder"></param>
        protected override void Load(ContainerBuilder containerBuilder)
        {
        
            //获取所有控制器类型并使用属性注入
            var controllerBaseType = typeof(ControllerBase);
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (ApplicationEnvironments.Site.Components.Plug != null && ApplicationEnvironments.Site.Components.Plug.Count > 0)
            {
                List<Assembly> assemblies = new List<Assembly>();
                foreach (var components in ApplicationEnvironments.Site.Components.Plug)
                {
                    if (!components.Opened)
                    {
                        continue;
                    }
           
                    if (!string.IsNullOrWhiteSpace(components.Mvc))
                    {
                        var controllerAssembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(baseDirectory + components.Mvc);
                        assemblies.Add(controllerAssembly);
                    }
                }
                containerBuilder.RegisterAssemblyTypes(assemblies.ToArray())
                            .Where(t => controllerBaseType.IsAssignableFrom(t) && t != controllerBaseType)
                            .PropertiesAutowired();

            }
          
           
            //containerBuilder.RegisterType<XiaoMi>().As<IPhone>().PropertiesAutowired();
        }
    }
    public class Startup
    {
        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
       .SetBasePath(env.ContentRootPath)
       .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
       //.AddJsonFile($"appsettings.Development.json",optional:true)
       .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; private set; }

        public ILifetimeScope AutofacContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //负载均衡中间件
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            //系统配置
            ApplicationEnvironments.Site = Configuration.GetSection("SiteSettings").Get<SiteSettings>();
            ApplicationEnvironments.ProviderType = ApplicationEnvironments.Site.ProviderType;

            ///log4日志记录
            var logRepository = LogManager.CreateRepository("NETCORERepository");

            XmlConfigurator.Configure(logRepository, new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "/log4net.config"));
            LogHelper.Init(logRepository);
            //httpcontext 如果在dll层需要用到httpContext则需要使用方法
            services.AddHttpContextAccessor();
            //属性注入
            services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
            services.AddResponseCompression();//压缩文件
            services.AddResponseCaching();
          
            var mvcBuilders = services.AddControllersWithViews(config =>
            {
                config.Filters.Add<CustomExceptionFilter>();
                config.ModelBinderProviders.Insert(0, new CustomModelBinderProvider());
                config.CacheProfiles.Add("default", new Microsoft.AspNetCore.Mvc.CacheProfile
                {
                    Duration = 600 /*10分钟*/
                });
            }) 
            .AddRazorRuntimeCompilation()//razer页面不编译
                //全局配置Json序列化处理
            .AddNewtonsoftJson(options => {
                //忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //不使用驼峰样式的key
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            })
     
        .AddControllersAsServices()
        .AddSessionStateTempDataProvider();
           
            services.AddSignalR(options => options.EnableDetailedErrors = true)
             
                .AddJsonProtocol(options => {
                //忽略循环引用
                options.PayloadSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
                options.PayloadSerializerOptions.PropertyNamingPolicy = null;
                  
            });

            #region  //身份认证时需要使用的方法 缓存
            ICacheService cacheService = null;
            //缓存注入
            switch (ApplicationEnvironments.Site.CacheType )
            {
                case CacheType.Redis:
                    cacheService = RedisService.Instance;
                    break;
                case CacheType.Mongo:
                    cacheService = MongoDBService.Instance;
                    break;
                default:
                    cacheService = MemoryService.Instance;
                    break;
            }
          
            services.AddSingleton<ICacheService>(cacheService);
            services.AddSingleton<IDistributedCache>(cacheService);

            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = false;
                //options.Cookie.Domain = ApplicationEnvironments.Site.Cookie.Domain;
                //options.Cookie.Path = ApplicationEnvironments.Site.Cookie.RootPath;
                //options.Cookie.Name = ApplicationEnvironments.Site.Cookie.Name;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;

                //使会话cookie成为必不可少的
                options.Cookie.IsEssential = true;
                options.IdleTimeout = TimeSpan.FromMinutes(ApplicationEnvironments.Site.SessionTimeout);
            });
            services.AddDirectoryBrowser();
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                //options.Cookie.Domain = ApplicationEnvironments.Site.Cookie.Domain;
                options.DataProtectionProvider = DataProtectionProvider.Create(ApplicationEnvironments.Site.Cookie.ProtectionProviderPath);
                options.Cookie.Name = ApplicationEnvironments.Site.Cookie.Name;
                options.Cookie.Path = ApplicationEnvironments.Site.Cookie.RootPath;
                options.LoginPath = new PathString("/account");
                options.AccessDeniedPath = new PathString("/Forbidden"); //禁止访问路径：当用户试图访问资源时，但未通过该资源的任何授权策略，请求将被重定向到这个相对路径。
                options.SlidingExpiration = true;  //Cookie可以分为永久性的和临时性的。 临时性的是指只在当前浏览器进程里有效，浏览器一旦关闭就失效（被浏览器删除）。 永久性的是指Cookie指定了一个过期时间，在这个时间到达之前，此cookie一直有效（浏览器一直记录着此cookie的存在）。 slidingExpriation的作用是，指示浏览器把cookie作为永久性cookie存储，但是会自动更改过期时间，以使用户不会在登录后并一直活动，但是一段时间后却自动注销。也就是说，你10点登录了，服务器端设置的TimeOut为30分钟，如果slidingExpriation为false,那么10:30以后，你就必须重新登录。如果为true的话，你10:16分时打开了一个新页面，服务器就会通知浏览器，把过期时间修改为10:46。 更详细的说明还是参考MSDN的文档。
            });
            #endregion
            ApplicationEnvironments.DefaultSession = new BaseController();
            //数据库驱动注入
            if (ApplicationEnvironments.Site.ORMType== ORMType.EF)
            {
                services.AddScoped<IDbRepository, EFRepository>();
            }
            else
            {
                services.AddScoped<IDbRepository, AdoRepository>();
            }
            //系统编号生成
            services.AddScoped<ISerialService, SerialRuleService>();
            services.AddScoped<ISocketService, SignalRService>();
            services.AddScoped<ITaskService, SysTaskService>();
            //   services.AddSingleton<IApplicationManagerService>(ApplicationManagerService.Instance);
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            if (ApplicationEnvironments.Site.Components.Plug == null)
            {
                ApplicationEnvironments.Site.Components.Plug = new List<SystemComponent>();
            }
            ApplicationEnvironments.Site.Components.Plug.Insert(0, ApplicationEnvironments.Site.Components.System);
            InitAutoFac(baseDirectory, services, mvcBuilders);
         // CommonHelper.InitPages();


        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutofacDefaultModule>();
        }

      
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            ///负载均衡中间件
            app.UseForwardedHeaders();

            //请求中间件处理
            app.UseRequestHandling();
            //app.UseExceptionHandler("/Home/Error");//错误处理
            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                //app.UseDeveloperExceptionPage();
                ApplicationEnvironments.IsDevelopment = true;
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                // app.UseExceptionHandler("/Home/Error");
              //  app.UseExceptionHandler("/Home/Error");
                ApplicationEnvironments.IsDevelopment = false;
            }
            applicationLifetime.ApplicationStarted.Register(OnStart);//1:应用启动时加载配置,2:应用启动后注册服务中心
            applicationLifetime.ApplicationStopped.Register(OnStop);//应用停止后从服务中心注销
           //app.UseHttpsRedirection(); //强制https跳转

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(ApplicationEnvironments.DefaultDirectory, @"wwwroot")
            ),
                OnPrepareResponse = ctx =>
                {
                    ctx.Context.Response.Headers.Append("Cache-Control", "public,max-age=6000");
                }
            });
            if (ApplicationEnvironments.Site.ConfigPath != null && ApplicationEnvironments.Site.ConfigPath.Count > 0)
            {
                foreach (var item in ApplicationEnvironments.Site.ConfigPath)
                {
                    if (string.IsNullOrWhiteSpace(item.HttpPath) || string.IsNullOrWhiteSpace(item.Path))
                    {
                        continue;
                    }
                    app.UseStaticFiles(new StaticFileOptions()
                    {
                        FileProvider = new PhysicalFileProvider(
                        Path.Combine(ApplicationEnvironments.BaseDirectory, item.Path)
                        ),
                        RequestPath = new PathString(item.HttpPath),
                        OnPrepareResponse = ctx =>
                        {
                            ctx.Context.Response.Headers.Append("Cache-Control", "public,max-age=6000");
                        }

                    });
                }
            }
         
            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();

            app.UseResponseCompression();//文件压缩
            app.UseResponseCaching();
          //  app.Map("/socket/connect", WebSocketHelper.Map);
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SignalRHubHelper>("/mysignalr");
             
                endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapAreaControllerRoute(
                name: "areas", "areas",
                pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");//控制器分层
            });
         
            //添加httpcontext类
            AppHttpContext.Configure(app.ApplicationServices.GetRequiredService<Microsoft.AspNetCore.Http.IHttpContextAccessor>());
           
            //app.ApplicationServices.GetService<PageService>().InitMenu(ApplicationEnvironments.Site.Components.Plug, ref strError);
            //      
            //       CacheHelper.CacheService = app.ApplicationServices.GetService<ICacheService>();
            //       using (EFRepository eFRepository = new EFRepository())
            //       {
            //           try
            //           {
            //               if (eFRepository.DbEnsureCreated())
            //               {
            //                   //InitDataService initDataService = new InitDataService();
            //                   //initDataService.InsertData(eFRepository);
            //                   //initDataService.DeleteInitConfig();
            //               }
            //           }
            //           catch (Exception ex)
            //           {
            //                LogHelper.Info(ex.Message, this);
            //           }
            //       }
            // ApplicationEnvironments.DefaultSession= 
            //跨域访问
            // app.UseCors("AllowSameDomain");
            //app.UseSpa(spa =>
            //{
            //    // To learn more about options for serving an Angular SPA from ASP.NET Core,
            //    // see https://go.microsoft.com/fwlink/?linkid=864501

            //    spa.Options.SourcePath = "ClientApp";

            //    if (env.IsDevelopment())
            //    {
            //        spa.UseAngularCliServer(npmScript: "start");
            //    }
            //});
            //nginx反向代理
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }
        private void OnStart()
        {
            var log = new HttpLoggingModel();
            log.Body = "系统启动";
            log.SourcePlatform= log.ServerPlatform;
            log.IpAddress = log.ServerAddress;
            log.Result = new HttpLoggingResultModel();
            log.Result.StatusCode = 100000;
            log.Result.Body = "成功启动";
            LogHelper.RequestInfo(log);
        }

        private void OnStop()
        {
            var log = new HttpLoggingModel();
            log.Body = "系统关闭";
            log.SourcePlatform = log.ServerPlatform;
            log.IpAddress = log.ServerAddress;
            log.Result = new HttpLoggingResultModel();
            log.Result.StatusCode = 100000;
            log.Result.Body = "成功关闭";
            //this code is called when the application stops
            LogHelper.RequestInfo(log);
        }

        /// <summary>
        /// 初始化Service 层注入 mvc model注入
        /// </summary>
        /// <param name="baseDirectory"></param>
        /// <param name="services"></param>
        /// <param name="mvcBuilder"></param>
        private void InitAutoFac(string baseDirectory,IServiceCollection services,IMvcBuilder mvcBuilder)
        {
            #region AutoFac
            #region  mvc 区域分项目时调用
            var manager = new ApplicationPartManager();
            var builder = new Autofac.ContainerBuilder();
            #region Service 层注入
            List<Type> lstMappingTypes = new List<Type>();
            if (ApplicationEnvironments.Site.Components.Plug != null && ApplicationEnvironments.Site.Components.Plug.Count > 0)
            {
                foreach (var components in ApplicationEnvironments.Site.Components.Plug)
                {
                    if (!components.Opened)
                    {
                        continue;
                    }
                    if (components.Service != null && components.Service.Length > 0)
                    {
                        foreach (var item in components.Service)
                        {
                            foreach (var interfaceObj in ReflectionHelper.GetInterface(baseDirectory + item))
                            {
                                services.AddScoped(interfaceObj.Value, interfaceObj.Key);
                            }
                        }

                    }
                    if (components.Model != null && components.Model.Length > 0)
                    {
                        foreach (var item in components.Model)
                        {
                            ReflectionHelper.GetMappingProfiles(baseDirectory + item, ref lstMappingTypes);
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(components.Mvc))
                    {
                        var controllerObj = new AssemblyPart(AssemblyLoadContext.Default.LoadFromAssemblyPath(baseDirectory + components.Mvc));
                        if (!string.IsNullOrWhiteSpace(components.Id))
                        {
                            mvcBuilder.ConfigureApplicationPartManager(controller =>
                            {
                                controller.ApplicationParts.Add(controllerObj);
                                if (!string.IsNullOrWhiteSpace(components.Views))
                                {
                                    controller.ApplicationParts.Add(new CompiledRazorAssemblyPart(AssemblyLoadContext.Default.LoadFromAssemblyPath(baseDirectory + components.Views)));
                                }

                            });
                        }

                        manager.ApplicationParts.Add(controllerObj);
                    }
                }

            }
            if (lstMappingTypes != null)
            {
                services.AddAutoMapper(lstMappingTypes.ToArray());
            }




            builder.Populate(services);
            #endregion

            //manager.FeatureProviders.Add(new ControllerFeatureProvider());
            //var feature = new ControllerFeature();
            //manager.PopulateFeature(feature);
            #endregion
            //  return new AutofacServiceProvider(this.ApplicationContainer);

            #endregion
        }
    }

}
