﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WHLRDF.Web.Components
{
    public class CropperViewComponent: ViewComponent
    {
        /*
        如果需要构造函数的话，你就创建
            */
        /// <summary>
        /// 构造函数
        /// </summary>
        public CropperViewComponent()
        {

        }
        /// <summary>
        /// 异步调用
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.Factory.StartNew<IViewComponentResult>(()=> {
                 return View();
            });
        }

    }
}
