﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// 用户组
    /// </summary>
    [Table("ORG_Group", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class GroupEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public GroupEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Group";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "GroupId";


        /// <summary>
        ///  唯一标识
        /// </summary>
        public const string __GroupKey = "GroupKey";


        /// <summary>
        ///  名称
        /// </summary>
        public const string __GroupName = "GroupName";


        /// <summary>
        ///  是否系统
        /// </summary>
        public const string __IsSystem = "IsSystem";


        /// <summary>
        ///  上级Id
        /// </summary>
        public const string __ParentId = "ParentId";


        /// <summary>
        ///  备注
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _GroupId;
        ///<summary>
        ///  组编号
        /// </summary>
        [DbColumn("GroupId", IsPrimaryKey = true, LocalName = " 组编号", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string GroupId
        {
            get
            {
                return _GroupId;
            }
            set
            {
                _GroupId = value;
            }
        }


        private string _GroupKey;
        ///<summary>
        ///  唯一标识
        /// </summary>
        [DbColumn("GroupKey", LocalName = " 唯一标识", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string GroupKey
        {
            get
            {
                return _GroupKey;
            }
            set
            {

                SetData(__GroupKey,value);

                _GroupKey = value;
            }
        }


        private string _GroupName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("GroupName", LocalName = " 名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string GroupName
        {
            get
            {
                return _GroupName;
            }
            set
            {

                SetData(__GroupName,value);

                _GroupName = value;
            }
        }


        private bool _IsSystem;
        ///<summary>
        ///  是否系统
        /// </summary>
        [DbColumn("IsSystem", LocalName = " 是否系统", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsSystem
        {
            get
            {
                return _IsSystem;
            }
            set
            {

                SetData(__IsSystem,value);

                _IsSystem = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  上级Id
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级Id", ColumnType = "string", SqlType = "varchar", ControlType = 1, MaxLength = 50)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId,value);

                _ParentId = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  备注
        /// </summary>
        [DbColumn("Remark", LocalName = " 备注", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
