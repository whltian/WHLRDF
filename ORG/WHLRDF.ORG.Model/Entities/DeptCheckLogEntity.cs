﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// ORG_DeptCheckLog
    /// </summary>
    [Table("ORG_DeptCheckLog", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DeptCheckLogEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public DeptCheckLogEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_DeptCheckLog";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DeptCheckLogId";


        /// <summary>
        ///  部门id
        /// </summary>
        public const string __DepartmentId = "DepartmentId";


        /// <summary>
        ///  名称
        /// </summary>
        public const string __DepartmentName = "DepartmentName";


        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";


        /// <summary>
        ///  路径
        /// </summary>
        public const string __Chart = "Chart";


        #endregion
        #region 属性

        private string _DeptCheckLogId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("DeptCheckLogId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 36, Min = 0)]
        public virtual string DeptCheckLogId
        {
            get
            {
                return _DeptCheckLogId;
            }
            set
            {
                _DeptCheckLogId = value;
            }
        }


        private string _DepartmentId;
        ///<summary>
        ///  部门id
        /// </summary>
        [DbColumn("DepartmentId", LocalName = " 部门id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DepartmentId
        {
            get
            {
                return _DepartmentId;
            }
            set
            {

                SetData(__DepartmentId,value);

                _DepartmentId = value;
            }
        }


        private string _DepartmentName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("DepartmentName", LocalName = " 名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }
            set
            {

                SetData(__DepartmentName,value);

                _DepartmentName = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {

                SetData(__Status,value);

                _Status = value;
            }
        }


        private string _Chart;
        ///<summary>
        ///  结构图
        /// </summary>
        [DbColumn("Chart", LocalName = " 结构图", ColumnType = "string", SqlType = "ntext", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Chart
        {
            get
            {
                return _Chart;
            }
            set
            {

                SetData(__Chart,value);

                _Chart = value;
            }
        }


        #endregion
    }
}

