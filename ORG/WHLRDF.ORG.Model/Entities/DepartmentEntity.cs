﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// 部门
    /// </summary>
    [Table("ORG_Department", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DepartmentEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public DepartmentEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_Department";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DepartmentId";


        /// <summary>
        ///  上级部门
        /// </summary>
        public const string __ParentId = "ParentId";


        /// <summary>
        ///  部门名称
        /// </summary>
        public const string __DepartmentName = "DepartmentName";


        /// <summary>
        ///  唯一标识
        /// </summary>
        public const string __DepartmentCode = "DepartmentCode";


        /// <summary>
        ///  级别
        /// </summary>
        public const string __DepartmentLevel = "DepartmentLevel";


        /// <summary>
        ///  深度
        /// </summary>
        public const string __DepartmentPath = "DepartmentPath";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        ///  部门机构路径
        /// </summary>
        public const string __Chart = "Chart";


        /// <summary>
        ///  机构id
        /// </summary>
        public const string __OrgId = "OrgId";


        /// <summary>
        ///  迁出人
        /// </summary>
        public const string __CheckOutUserId = "CheckOutUserId";


        /// <summary>
        ///  迁出时间
        /// </summary>
        public const string __CheckOutIp = "CheckOutIp";


        /// <summary>
        ///  是否迁出
        /// </summary>
        public const string __IsCheckOut = "IsCheckOut";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _DepartmentId;
        ///<summary>
        ///  部门id
        /// </summary>
        [DbColumn("DepartmentId", IsPrimaryKey = true, LocalName = " 部门id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string DepartmentId
        {
            get
            {
                return _DepartmentId;
            }
            set
            {
                _DepartmentId = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  上级部门
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级部门", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId,value);

                _ParentId = value;
            }
        }


        private string _DepartmentName;
        ///<summary>
        ///  部门名称
        /// </summary>
        [DbColumn("DepartmentName", LocalName = " 部门名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }
            set
            {

                SetData(__DepartmentName,value);

                _DepartmentName = value;
            }
        }


        private string _DepartmentCode;
        ///<summary>
        ///  唯一标识
        /// </summary>
        [DbColumn("DepartmentCode", LocalName = " 唯一标识", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DepartmentCode
        {
            get
            {
                return _DepartmentCode;
            }
            set
            {

                SetData(__DepartmentCode,value);

                _DepartmentCode = value;
            }
        }


        private int _DepartmentLevel;
        ///<summary>
        ///  级别
        /// </summary>
        [DbColumn("DepartmentLevel", LocalName = " 级别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int DepartmentLevel
        {
            get
            {
                return _DepartmentLevel;
            }
            set
            {

                SetData(__DepartmentLevel,value);

                _DepartmentLevel = value;
            }
        }


        private string _DepartmentPath;
        ///<summary>
        ///  深度
        /// </summary>
        [DbColumn("DepartmentPath", LocalName = " 深度", AllowDBNull = true, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string DepartmentPath
        {
            get
            {
                return _DepartmentPath;
            }
            set
            {

                SetData(__DepartmentPath,value);

                _DepartmentPath = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Chart;
        ///<summary>
        ///  部门机构路径
        /// </summary>
        [DbColumn("Chart", LocalName = " 部门机构路径", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Chart
        {
            get
            {
                return _Chart;
            }
            set
            {

                SetData(__Chart,value);

                _Chart = value;
            }
        }


        private string _OrgId;
        ///<summary>
        ///  机构id
        /// </summary>
        [DbColumn("OrgId", LocalName = " 机构id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OrgId
        {
            get
            {
                return _OrgId;
            }
            set
            {

                SetData(__OrgId,value);

                _OrgId = value;
            }
        }


        private string _CheckOutUserId;
        ///<summary>
        ///  迁出人
        /// </summary>
        [DbColumn("CheckOutUserId", LocalName = " 迁出人", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string CheckOutUserId
        {
            get
            {
                return _CheckOutUserId;
            }
            set
            {

                SetData(__CheckOutUserId,value);

                _CheckOutUserId = value;
            }
        }


        private string _CheckOutIp;
        ///<summary>
        ///  迁出时间
        /// </summary>
        [DbColumn("CheckOutIp", LocalName = " 迁出时间", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string CheckOutIp
        {
            get
            {
                return _CheckOutIp;
            }
            set
            {

                SetData(__CheckOutIp,value);

                _CheckOutIp = value;
            }
        }


        private bool _IsCheckOut;
        ///<summary>
        ///  是否迁出
        /// </summary>
        [DbColumn("IsCheckOut", LocalName = " 是否迁出", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsCheckOut
        {
            get
            {
                return _IsCheckOut;
            }
            set
            {

                SetData(__IsCheckOut,value);

                _IsCheckOut = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("Remark", LocalName = " 描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 2000, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
