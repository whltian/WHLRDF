﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// 组织机构
    /// </summary>
    [Table("ORG_Organizations", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class OrganizationsEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public OrganizationsEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_Organizations";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "OrgId";


        /// <summary>
        ///  名称
        /// </summary>
        public const string __OrgName = "OrgName";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __Comments = "Comments";


        /// <summary>
        ///  系统
        /// </summary>
        public const string __IsSystem = "IsSystem";


        /// <summary>
        ///  是否自定义
        /// </summary>
        public const string __IsCustom = "IsCustom";


        /// <summary>
        ///  接口
        /// </summary>
        public const string __EventHandler = "EventHandler";


        /// <summary>
        ///  注入的dll
        /// </summary>
        public const string __ReisterDLL = "ReisterDLL";


        #endregion
        #region 属性

        private string _OrgId;
        ///<summary>
        /// 主键
        /// </summary>
        [DbColumn("OrgId", IsPrimaryKey = true, LocalName = "主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string OrgId
        {
            get
            {
                return _OrgId;
            }
            set
            {
                _OrgId = value;
            }
        }


        private string _OrgName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("OrgName", LocalName = " 名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OrgName
        {
            get
            {
                return _OrgName;
            }
            set
            {

                SetData(__OrgName,value);

                _OrgName = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Comments;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("Comments", LocalName = " 描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 50, Min = 0)]
        public virtual string Comments
        {
            get
            {
                return _Comments;
            }
            set
            {

                SetData(__Comments,value);

                _Comments = value;
            }
        }


        private bool _IsSystem;
        ///<summary>
        ///  系统
        /// </summary>
        [DbColumn("IsSystem", LocalName = " 系统", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsSystem
        {
            get
            {
                return _IsSystem;
            }
            set
            {

                SetData(__IsSystem,value);

                _IsSystem = value;
            }
        }


        private bool _IsCustom;
        ///<summary>
        ///  是否自定义
        /// </summary>
        [DbColumn("IsCustom", LocalName = " 是否自定义", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsCustom
        {
            get
            {
                return _IsCustom;
            }
            set
            {

                SetData(__IsCustom,value);

                _IsCustom = value;
            }
        }


        private string _EventHandler;
        ///<summary>
        ///  接口
        /// </summary>
        [DbColumn("EventHandler", LocalName = " 接口", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string EventHandler
        {
            get
            {
                return _EventHandler;
            }
            set
            {

                SetData(__EventHandler,value);

                _EventHandler = value;
            }
        }


        private string _ReisterDLL;
        ///<summary>
        ///  注入的dll
        /// </summary>
        [DbColumn("ReisterDLL", LocalName = " 注入的dll", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string ReisterDLL
        {
            get
            {
                return _ReisterDLL;
            }
            set
            {

                SetData(__ReisterDLL,value);

                _ReisterDLL = value;
            }
        }


        #endregion
    }
}