﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// 职级
    /// </summary>
    [Table("ORG_JobGrade", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class JobGradeEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public JobGradeEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_JobGrade";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "JobGradeId";


        /// <summary>
        ///  职级名称
        /// </summary>
        public const string __JobGradeName = "JobGradeName";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __Comments = "Comments";


        #endregion
        #region 属性

        private string _JobGradeId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("JobGradeId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string JobGradeId
        {
            get
            {
                return _JobGradeId;
            }
            set
            {
                _JobGradeId = value;
            }
        }


        private string _JobGradeName;
        ///<summary>
        ///  职级名称
        /// </summary>
        [DbColumn("JobGradeName", LocalName = " 职级名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string JobGradeName
        {
            get
            {
                return _JobGradeName;
            }
            set
            {

                SetData(__JobGradeName,value);

                _JobGradeName = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Comments;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("Comments", LocalName = " 描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 50, Min = 0)]
        public virtual string Comments
        {
            get
            {
                return _Comments;
            }
            set
            {

                SetData(__Comments,value);

                _Comments = value;
            }
        }


        #endregion
    }
}
