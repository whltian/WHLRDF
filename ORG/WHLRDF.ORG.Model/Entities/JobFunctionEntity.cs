﻿
using WHLRDF.ORM;
using System;
using System.Data;
namespace WHLRDF.ORG.Model
{
    /// <summary>
    /// 岗位
    /// </summary>
    [Table("ORG_JobFunction", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class JobFunctionEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public JobFunctionEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_JobFunction";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "JobFunctionId";


        /// <summary>
        ///  上级Id
        /// </summary>
        public const string __ParentId = "ParentId";


        /// <summary>
        ///  类别
        /// </summary>
        public const string __JobFunctionType = "JobFunctionType";


        /// <summary>
        ///  部门Id
        /// </summary>
        public const string __DepartmentId = "DepartmentId";


        /// <summary>
        ///  是否主岗
        /// </summary>
        public const string __IsPrimary = "IsPrimary";


        /// <summary>
        ///  机构Id
        /// </summary>
        public const string __OrgId = "OrgId";


        /// <summary>
        ///  职级Id
        /// </summary>
        public const string __JobGradeId = "JobGradeId";


        /// <summary>
        ///  职级名称
        /// </summary>
        public const string __JobFunctionName = "JobFunctionName";


        /// <summary>
        /// 资源id
        /// </summary>
        public const string __SourceId = "SourceId";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __SourceName = "SourceName";


        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";


        /// <summary>
        ///  备注
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _JobFunctionId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("JobFunctionId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 36, Min = 0)]
        public virtual string JobFunctionId
        {
            get
            {
                return _JobFunctionId;
            }
            set
            {
                _JobFunctionId = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  上级Id
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级Id", ColumnType = "string", SqlType = "varchar", ControlType = 1, MaxLength = 36, Min = 0)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId,value);

                _ParentId = value;
            }
        }


        private int _JobFunctionType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("JobFunctionType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "smallint", ControlType = 6, MaxLength = 5, Min = 0, VaildKey = "IsInt")]
        public virtual int JobFunctionType
        {
            get
            {
                return _JobFunctionType;
            }
            set
            {

                SetData(__JobFunctionType,value);

                _JobFunctionType = value;
            }
        }


        private string _DepartmentId;
        ///<summary>
        ///  部门Id
        /// </summary>
        [DbColumn("DepartmentId", LocalName = " 部门Id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string DepartmentId
        {
            get
            {
                return _DepartmentId;
            }
            set
            {

                SetData(__DepartmentId,value);

                _DepartmentId = value;
            }
        }


        private bool _IsPrimary;
        ///<summary>
        ///  是否主岗
        /// </summary>
        [DbColumn("IsPrimary", LocalName = " 是否主岗", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsPrimary
        {
            get
            {
                return _IsPrimary;
            }
            set
            {

                SetData(__IsPrimary,value);

                _IsPrimary = value;
            }
        }


        private string _OrgId;
        ///<summary>
        ///  机构Id
        /// </summary>
        [DbColumn("OrgId", LocalName = " 机构Id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string OrgId
        {
            get
            {
                return _OrgId;
            }
            set
            {

                SetData(__OrgId,value);

                _OrgId = value;
            }
        }


        private string _JobGradeId;
        ///<summary>
        ///  职级Id
        /// </summary>
        [DbColumn("JobGradeId", LocalName = " 职级Id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string JobGradeId
        {
            get
            {
                return _JobGradeId;
            }
            set
            {

                SetData(__JobGradeId,value);

                _JobGradeId = value;
            }
        }


        private string _JobFunctionName;
        ///<summary>
        ///  职级名称
        /// </summary>
        [DbColumn("JobFunctionName", LocalName = " 职级名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string JobFunctionName
        {
            get
            {
                return _JobFunctionName;
            }
            set
            {

                SetData(__JobFunctionName,value);

                _JobFunctionName = value;
            }
        }


        private string _SourceId;
        ///<summary>
        /// 资源id
        /// </summary>
        [DbColumn("SourceId", LocalName = "资源id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string SourceId
        {
            get
            {
                return _SourceId;
            }
            set
            {

                SetData(__SourceId,value);

                _SourceId = value;
            }
        }


        private string _SourceName;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("SourceName", LocalName = " 描述", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string SourceName
        {
            get
            {
                return _SourceName;
            }
            set
            {

                SetData(__SourceName,value);

                _SourceName = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum,value);

                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  备注
        /// </summary>
        [DbColumn("Remark", LocalName = " 备注", ColumnType = "string", SqlType = "nvarchar", ControlType = 5, MaxLength = 2000, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark,value);

                _Remark = value;
            }
        }


        #endregion
    }
}
