﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class JobFunctionMap : ClassMap<JobFunctionEntity>
    {
        public override void Configure(EntityTypeBuilder<JobFunctionEntity>
            builder)
        {
            builder.ToTable<JobFunctionEntity>("ORG_JobFunction");
          
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.JobFunctionId);

            builder.Property(x => x.JobFunctionId).HasColumnName("JobFunctionId").HasMaxLength(50);

            /// <summary>
            ///  上级Id
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.JobFunctionType).HasColumnName("JobFunctionType").HasDefaultValue(0);

            /// <summary>
            ///  部门Id
            /// </summary>
            builder.Property(x => x.DepartmentId).HasColumnName("DepartmentId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  是否主岗
            /// </summary>
            builder.Property(x => x.IsPrimary).HasColumnName("IsPrimary").HasDefaultValue(0);

            /// <summary>
            ///  机构Id
            /// </summary>
            builder.Property(x => x.OrgId).HasColumnName("OrgId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  职级Id
            /// </summary>
            builder.Property(x => x.JobGradeId).HasColumnName("JobGradeId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  职级名称
            /// </summary>
            builder.Property(x => x.JobFunctionName).HasColumnName("JobFunctionName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 资源id
            /// </summary>
            builder.Property(x => x.SourceId).HasColumnName("SourceId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.SourceName).HasColumnName("SourceName").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            ///  备注
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(2000).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x => x.Parent);
            builder.Ignore(x => x.IsAdd);
        }

    }
}
