﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class DepartmentMap : ClassMap<DepartmentEntity>
    {
        public override void Configure(EntityTypeBuilder<DepartmentEntity>
            builder)
        {
            builder.ToTable<DepartmentEntity>("ORG_Department");

            /// <summary>
            ///  部门id
            /// </summary>
            builder.HasKey(x => x.DepartmentId);

            builder.Property(x => x.DepartmentId).HasColumnName("DepartmentId").HasMaxLength(50);

            /// <summary>
            ///  上级部门
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  部门名称
            /// </summary>
            builder.Property(x => x.DepartmentName).HasColumnName("DepartmentName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  唯一标识
            /// </summary>
            builder.Property(x => x.DepartmentCode).HasColumnName("DepartmentCode").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  级别
            /// </summary>
            builder.Property(x => x.DepartmentLevel).HasColumnName("DepartmentLevel");

            /// <summary>
            ///  深度
            /// </summary>
            builder.Property(x => x.DepartmentPath).HasColumnName("DepartmentPath").HasMaxLength(500).IsRequired(false);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.Chart).HasColumnName("Chart").IsRequired(false);



            /// <summary>
            ///  机构id
            /// </summary>
            builder.Property(x => x.OrgId).HasColumnName("OrgId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  迁出人
            /// </summary>
            builder.Property(x => x.CheckOutUserId).HasColumnName("CheckOutUserId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  迁出时间
            /// </summary>
            builder.Property(x => x.CheckOutIp).HasColumnName("CheckOutIp").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  是否迁出
            /// </summary>
            builder.Property(x => x.IsCheckOut).HasColumnName("IsCheckOut").HasDefaultValue(0);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(2000).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x=>x.id);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.children);
            
            builder.Ignore(x => x.LstChildren);
            builder.Ignore(x => x.LstJobFunctions);
            builder.Ignore(x => x.ParentDepartment);
            builder.Ignore(x => x.DeptCheckLogEntity);
            builder.Ignore(x => x.IsOrg);
            builder.Ignore(x => x.IsEdited);
            builder.Ignore(x => x.IsAdded);
            builder.Ignore(x => x.IsNewAdd);
        }
    }
}
