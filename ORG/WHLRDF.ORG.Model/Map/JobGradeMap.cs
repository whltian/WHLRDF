﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class JobGradeMap : ClassMap<JobGradeEntity>
    {
        public override void Configure(EntityTypeBuilder<JobGradeEntity>
            builder)
        {
            builder.ToTable<JobGradeEntity>("ORG_JobGrade");
          
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.JobGradeId);

            builder.Property(x => x.JobGradeId).HasColumnName("JobGradeId").HasMaxLength(50);

            /// <summary>
            ///  职级名称
            /// </summary>
            builder.Property(x => x.JobGradeName).HasColumnName("JobGradeName").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum");

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.Comments).HasColumnName("Comments").HasMaxLength(50).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}