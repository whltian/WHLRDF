﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class OrganizationsMap : ClassMap<OrganizationsEntity>
    {
        public override void Configure(EntityTypeBuilder<OrganizationsEntity>
            builder)
        {
            builder.ToTable<OrganizationsEntity>("ORG_Organizations");
            
            /// <summary>
            /// 主键
            /// </summary>
            builder.HasKey(x => x.OrgId);

            builder.Property(x => x.OrgId).HasColumnName("OrgId").HasMaxLength(50);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.OrgName).HasColumnName("OrgName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(1);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.Comments).HasColumnName("Comments").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  系统
            /// </summary>
            builder.Property(x => x.IsSystem).HasColumnName("IsSystem").HasDefaultValue(false);

            /// <summary>
            ///  是否自定义
            /// </summary>
            builder.Property(x => x.IsCustom).HasColumnName("IsCustom").HasDefaultValue(0);

            /// <summary>
            ///  接口
            /// </summary>
            builder.Property(x => x.EventHandler).HasColumnName("EventHandler").HasMaxLength(100).IsRequired(false);

            /// <summary>
            ///  注入的dll
            /// </summary>
            builder.Property(x => x.ReisterDLL).HasColumnName("ReisterDLL").HasMaxLength(100).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

            builder.Ignore(x => x.id);
            builder.Ignore(x => x.text);
            builder.Ignore(x => x.icon);
            builder.Ignore(x => x.parent);
            builder.Ignore(x => x.children);
            builder.Ignore(x => x.IsOrg);
            builder.Ignore(x => x.IsEdited);
            builder.Ignore(x => x.IsAdded);
            builder.Ignore(x => x.CheckStatus);
        }
    }
}
