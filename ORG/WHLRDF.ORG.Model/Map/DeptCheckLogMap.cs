﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class DeptCheckLogMap : ClassMap<DeptCheckLogEntity>
    {
        public override void Configure(EntityTypeBuilder<DeptCheckLogEntity>
            builder)
        {
            builder.ToTable<DeptCheckLogEntity>("ORG_DeptCheckLog");
           

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.DeptCheckLogId);

            builder.Property(x => x.DeptCheckLogId).HasColumnName("DeptCheckLogId").HasMaxLength(36);

            /// <summary>
            ///  部门id
            /// </summary>
            builder.Property(x => x.DepartmentId).HasColumnName("DepartmentId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.DepartmentName).HasColumnName("DepartmentName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status");

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.Chart).HasColumnName("Chart").IsRequired(false);



            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            base.Configure(builder);

        }
    }
}