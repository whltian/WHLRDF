﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.ORG.Model
{
    public partial class GroupMap : ClassMap<GroupEntity>
    {
        public override void Configure(EntityTypeBuilder<GroupEntity>
            builder)
        {
            builder.ToTable<GroupEntity>("ORG_Group");
           
            /// <summary>
            ///  组编号
            /// </summary>
            builder.HasKey(x => x.GroupId);

            builder.Property(x => x.GroupId).HasColumnName("GroupId").HasMaxLength(50);

            /// <summary>
            ///  唯一标识
            /// </summary>
            builder.Property(x => x.GroupKey).HasColumnName("GroupKey").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.GroupName).HasColumnName("GroupName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  是否系统
            /// </summary>
            builder.Property(x => x.IsSystem).HasColumnName("IsSystem").HasDefaultValue(1);

            /// <summary>
            ///  上级Id
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  备注
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}
