﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WHLRDF.ORG.Model
{
    public partial class DepartmentEntity
    {
        public virtual string id {
            get {
                return this.DepartmentId;
            }
        }
        public virtual string text {
            get {
                return this.DepartmentName;
            }
        }
        private string _icon = "fa fa-check";
        public virtual string icon
        {
            get
            {
                return _icon;
            }
            set {
                _icon = value;
            }
        }

        public virtual string parent { get; set; }
        public virtual List<DepartmentEntity> LstChildren { get; set; }

        public virtual bool children { get; set; }
        public virtual List<JobFunctionEntity> LstJobFunctions { get; set; }
        [JsonIgnore]
        public virtual DepartmentEntity ParentDepartment { get; set; }


        /// <summary>
        /// 迁入迁出
        /// </summary>
        public virtual DeptCheckLogEntity DeptCheckLogEntity { get; set; }

        //public virtual bool IsManager { get; set; }

        //public virtual bool IsHRManager { get; set; }

        //public virtual bool IsSearch { get; set; }

        //public virtual string ReadDBData { get; set; }

           public virtual bool IsOrg { get => false; }

        public virtual bool IsEdited { get; set; }

        /// <summary>
        /// 是否允许新增
        /// </summary>
        public virtual bool IsAdded
        {
            get
            {
                return false;
            }
           
        }

        /// <summary>
        /// 是否新增
        /// </summary>
        public virtual bool IsNewAdd { get; set; }
    }
}