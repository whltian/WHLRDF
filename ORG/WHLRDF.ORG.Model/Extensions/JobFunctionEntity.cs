﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WHLRDF.ORG.Model
{
    public partial class JobFunctionEntity
    {
        [JsonIgnore]
        public virtual JobFunctionEntity Parent { get; set; }

        public virtual bool IsAdd { get; set; }
    }
}