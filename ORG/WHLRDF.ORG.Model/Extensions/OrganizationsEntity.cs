﻿using System;
using System.Collections.Generic;

namespace WHLRDF.ORG.Model
{
    public partial class OrganizationsEntity
    {

        public virtual string id
        {
            get {
                return this.OrgId;
            }
        }
        public virtual string text
        {
            get
            {
                return this.OrgName;
            }
        }
        public virtual string parent { get; set; } = "000";
        public virtual string icon { get {
                return "fa fa-sitemap";
            } }


        public virtual object children
        {
            get; set;

        } = true;

        //public virtual bool IsManager { get; set; }

        //public virtual bool IsHRManager { get; set; }

        //public virtual bool IsSearch { get; set; }

        /// <summary>
        /// 是否允许编辑
        /// </summary>
        public virtual bool IsEdited {
            get {
                return true;
            }
        }
        bool _isAdded=true;
        /// <summary>
        /// 是否允许新增
        /// </summary>
        public virtual bool IsAdded
        {
            get
            {
                return _isAdded;
            }
            set {
                _isAdded = value;
            }
        }
        public virtual bool IsOrg { get => true; }
        public virtual bool CheckStatus { get; set; }

     


    }
}