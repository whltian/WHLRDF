﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.ORG.Model;
namespace WHLRDF.ORG.BLL
{
    public class JobGradeService : SerivceBase, IJobGradeService
    {
        #region 
        /// <summary>
        /// 职级
        /// </summary>
        public const string JobGrade_Identity_Num = "0010";
        #endregion
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<JobGradeEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(JobGradeEntity.__IsDeleted, false);
            return DbRepository.Query<JobGradeEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public JobGradeEntity GetById(string id)
        {
            return DbRepository.GetById<JobGradeEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(JobGradeEntity entity, ref string strError)
        {
            if (Exist(entity.JobGradeName, entity.JobGradeId))
            {
                strError = entity.JobGradeName + "已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.JobGradeId))
            {
                entity.JobGradeId = SerialService.CreateSerialNo(JobGrade_Identity_Num, null, ref strError);
            }
            return DbRepository.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return DbRepository.Delete<JobGradeEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                JobGradeEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return DbRepository.Query<JobGradeEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool Exist(string jobName, string jobId)
        {
            jobId = (string.IsNullOrWhiteSpace(jobId)) ? "" : jobId;
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter = Expression.And(
                Expression.And(criter, Expression.Eq(JobGradeEntity.__JobGradeName, jobName)),
                Expression.NotEq(JobGradeEntity._PrimaryKeyName, jobId)
                );
            var result = DbRepository.Query<JobGradeEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}