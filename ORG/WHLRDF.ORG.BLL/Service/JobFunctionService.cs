﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.ORG.Model;
namespace WHLRDF.ORG.BLL
{
    public class JobFunctionService : SerivceBase, IJobFunctionService
    {
        #region 
        /// <summary>
        /// 岗位
        /// </summary>
        public const string JobFunction_Identity_Num = "0009";
        #endregion

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<JobFunctionEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(JobFunctionEntity.__IsDeleted, false);
            return DbRepository.Query<JobFunctionEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public JobFunctionEntity GetById(string id)
        {
            return DbRepository.GetById<JobFunctionEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(JobFunctionEntity entity, ref string strError)
        {
          
            if (string.IsNullOrWhiteSpace(entity.JobFunctionId))
            {
                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                keyValuePairs.Add("DEPTID",entity.DepartmentId);
                entity.JobFunctionId = SerialService.CreateSerialNo(JobFunction_Identity_Num, keyValuePairs, ref strError);
            }
            return DbRepository.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return DbRepository.Delete<JobFunctionEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                JobFunctionEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return DbRepository.Query<JobFunctionEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool Exist(string jobName, string jobId)
        {
            jobId = (string.IsNullOrWhiteSpace(jobId)) ? "" : jobId;
         
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter =
                Expression.And(criter, Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionName, jobName),
                Expression.NotEq(JobFunctionEntity._PrimaryKeyName, jobId))
                );
            var result = DbRepository.Query<JobFunctionEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

        #region 扩展方法

        /// <summary>
        /// 生成岗位编号
        /// </summary>
        /// <param name="deptid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        /// 
        public string CreateJobFunctionIdByDeptId(string deptid, ref string strError)
        {
            Dictionary<string, string> dicParam = new Dictionary<string, string>();
            dicParam.Add("DEPTID", deptid);
            return SerialService.CreateSerialNo(JobFunction_Identity_Num, dicParam, ref strError);
        }

        /// <summary>
        /// 获取部门下所有岗位
        /// </summary>
        /// <param name="deptid">部门Id</param>
        /// <returns></returns>
        public List<JobFunctionEntity> GetAllByDeptId(string deptid)
        {
            ICriterion criter = Expression.And(Expression.Eq(JobFunctionEntity.__DepartmentId, deptid), Expression.Eq(JobFunctionEntity.__IsDeleted, false));
            return DbRepository.Query<JobFunctionEntity>(criter, new Order(JobFunctionEntity.__OrderNum, true)).ToList();
        }



        /// <summary>
        /// 获取非本部门下的所有岗位
        /// </summary>
        /// <param name="deptid"></param>
        /// <returns></returns>
        public List<JobFunctionEntity> GetAllByNoDeptId(string deptid)
        {
            ICriterion criter = Expression.And(Expression.NotEq(JobFunctionEntity.__DepartmentId, deptid), Expression.Eq(JobFunctionEntity.__IsDeleted, false));
            return DbRepository.Query<JobFunctionEntity>(criter, new Order(JobFunctionEntity.__OrderNum, true)).ToList();

        }
        /// <summary>
        /// 获取部门下一级岗位
        /// </summary>
        /// <param name="deptid">部门Id</param>
        /// <returns></returns>
        public List<JobFunctionEntity> GetJobFunctionByParent(string deptid,string parentid)
        {
            ICriterion criter = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),Expression.Eq(JobFunctionEntity.__JobFunctionType,0));
            if (!string.IsNullOrWhiteSpace(parentid))
            {
                criter= Expression.And(criter, Expression.Eq(JobFunctionEntity.__ParentId, parentid));
            }
            else
            {
                criter =Expression.And(Expression.Eq(JobFunctionEntity.__DepartmentId, deptid), Expression.And(criter,Expression.IsNull(JobFunctionEntity.__ParentId)));
            }
            return DbRepository.Query<JobFunctionEntity>(criter, new Order(JobFunctionEntity.__OrderNum, true)).ToList();

        }

        #endregion
        #region Event
        /// <summary>
        /// 保存成功事件
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool OnSavedEventHandler(JobFunctionEntity entity, ref string strError)
        {

            return true;
        }

        /// <summary>
        /// 删除后执行的事件
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool OnDeletedEventHandler(JobFunctionEntity entity)
        {

            return true;
        }
        #endregion 

    }
}