﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.ORG.Model;
namespace WHLRDF.ORG.BLL
{
    public class GroupService : SerivceBase, IGroupService
    {
        #region 
        public const string Group_No_Rule = "0006";
        #endregion
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<GroupEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(GroupEntity.__IsDeleted, false);
            return DbRepository.Query<GroupEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public GroupEntity GetById(string id)
        {
            return DbRepository.GetById<GroupEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(GroupEntity entity, ref string strError)
        {
            if (string.IsNullOrWhiteSpace(entity.GroupId))
            {
                entity.GroupId = SerialService.CreateSerialNo(Group_No_Rule,null,ref strError);
            }
            if (Exist(entity.GroupKey, entity.GroupId))
            {
                strError = "组的唯一标识已存在，请不要重复添加";
                return false;
            }
            return DbRepository.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return DbRepository.Delete<GroupEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                GroupEntity._PrimaryKeyName ,
                                GroupEntity.__GroupName,
                                GroupEntity.__GroupKey
                           }, grid.keyWord)
                           );
            }
            return DbRepository.Query<GroupEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool Exist(string groupKey, string groupId)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter = Expression.And(
                Expression.And(criter, Expression.Eq(GroupEntity.__GroupKey, groupKey)),
                Expression.NotEq(GroupEntity._PrimaryKeyName, groupId)
                );
            var result = DbRepository.Query<GroupEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}