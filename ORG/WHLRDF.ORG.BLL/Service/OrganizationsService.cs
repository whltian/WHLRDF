﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.ORG.Model;
namespace WHLRDF.ORG.BLL
{
    public class OrganizationsService : SerivceBase, IOrganizationsService
    {
        #region 
        /// <summary>
        /// 组织机构编码规则
        /// </summary>
        public const string Organizations_Identity_Num = "0007";
        #endregion

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<OrganizationsEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(OrganizationsEntity.__IsDeleted, false);
            return DbRepository.Query<OrganizationsEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public OrganizationsEntity GetById(string id)
        {
            return DbRepository.GetById<OrganizationsEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(OrganizationsEntity entity, ref string strError)
        {
          
            if (Exist(entity.OrgName, entity.OrgId))
            {
                strError = entity.OrgName+"已存在，请不要重复输入";
                return false;
            }
            if (string.IsNullOrWhiteSpace(entity.OrgId))
            {
                entity.OrgId = SerialService.CreateSerialNo(Organizations_Identity_Num, null, ref strError);
            }
            return DbRepository.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return DbRepository.Delete<OrganizationsEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                OrganizationsEntity._PrimaryKeyName ,
                                OrganizationsEntity.__OrgName
                           }, grid.keyWord)
                           );
            }
            return DbRepository.Query<OrganizationsEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        public bool Exist(string orgName, string orgId)
        {
            orgId = (string.IsNullOrWhiteSpace(orgId)) ? "" : orgId;
            ICriterion criter = Expression.Eq("IsDeleted", false);
            criter = Expression.And(
                Expression.And(criter, Expression.Eq(OrganizationsEntity.__OrgName, orgName)),
                Expression.NotEq(OrganizationsEntity._PrimaryKeyName, orgId)
                );
            var result = DbRepository.Query<OrganizationsEntity>(criter).FirstOrDefault();
            return result != null;
        }
        #endregion

    }
}