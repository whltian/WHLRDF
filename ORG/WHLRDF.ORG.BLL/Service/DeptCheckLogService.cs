﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WHLRDF.ORM;
using WHLRDF.ORG.Model;
namespace WHLRDF.ORG.BLL
{
    public class DeptCheckLogService : SerivceBase, IDeptCheckLogService
    {

        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        public List<DeptCheckLogEntity> GetAll()
        {
            ICriterion criter = Expression.Eq(DeptCheckLogEntity.__IsDeleted, false);
            return DbRepository.Query<DeptCheckLogEntity>(criter).ToList();

        }
        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        public DeptCheckLogEntity GetById(string id)
        {
            return DbRepository.GetById<DeptCheckLogEntity>(id);

        }

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Save(DeptCheckLogEntity entity, ref string strError)
        {
            return DbRepository.SaveOrUpdate(entity);

        }
        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        public bool Delete(string deleteKey, ref string strError)
        {
            if (!string.IsNullOrEmpty(deleteKey))
            {
                return DbRepository.Delete<DeptCheckLogEntity>(deleteKey);
            }
            return true;
        }
        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        public LigerGrid ForGrid(LigerGrid grid)
        {
            ICriterion criter = Expression.Eq("IsDeleted", false);
            if (!string.IsNullOrEmpty(grid.keyWord))
            {
                criter = Expression.And(criter,

                           Expression.Like(new string[] {
                                DeptCheckLogEntity._PrimaryKeyName ,
                           }, grid.keyWord)
                           );
            }
            return DbRepository.Query<DeptCheckLogEntity>(grid, criter);

        }
        #endregion

        #region 扩展方法
        #region 扩展方法
        /// <summary>
        /// 日志保存方法
        /// </summary>
        /// <param name="deptId"></param>
        /// <param name="localName"></param>
        /// <param name="status"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public DeptCheckLogEntity Save(string deptId, string localName, int status, ref string strError)
        {
            DeptCheckLogEntity entity = new DeptCheckLogEntity()
            {
                DepartmentId = deptId,
                Status = status,
                LastModifyDate = DateTime.Now,
                LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId,
                DepartmentName = localName

            };
            if (status > 0)
            {
                ICriterion criter = Expression.And(Expression.Eq(DeptCheckLogEntity.__Status, 0), Expression.And(Expression.Eq(DeptCheckLogEntity.__DepartmentId, deptId),
                    Expression.Eq(DeptCheckLogEntity.__IsDeleted, false)));
                entity = DbRepository.Query<DeptCheckLogEntity>(criter).FirstOrDefault();
                if (entity == null)
                {
                    strError = "数据异常，请联系管理员";
                    return null;
                }
            }
            else
            {
                entity.CreateBy = ApplicationEnvironments.DefaultSession.UserId;
                entity.CreateDate = DateTime.Now;
             
                entity.DeptCheckLogId = Guid.NewGuid().ToString();
            }
            entity.Status = status;
            entity.LastModifyUserId = ApplicationEnvironments.DefaultSession.UserId;
            entity.LastModifyDate = DateTime.Now;

            return entity;

        }

        /// <summary>
        /// 获取log日志实体
        /// </summary>
        /// <param name="deptId">部门id</param>
        /// <param name="status">状态</param>
        /// <returns></returns>
        public DeptCheckLogEntity GetLogEntity(string deptId, int status)
        {
            ICriterion criter = Expression.Eq(DeptCheckLogEntity.__IsDeleted, false);
            criter = Expression.And(criter, Expression.And(Expression.Eq(DeptCheckLogEntity.__Status, status), Expression.Eq(DeptCheckLogEntity.__DepartmentId, deptId)));
            return DbRepository.Query<DeptCheckLogEntity>(criter).FirstOrDefault();
        }

        public OrgChartEntity GetOrgChartByLogId(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            var entity = this.GetById(id);
            if (entity != null)
            {
                if (!string.IsNullOrWhiteSpace(entity.Chart))
                {
                    return JSONHelper.FromJson<OrgChartEntity>(entity.Chart);
                }
            }
            return null;
        }
        #endregion
        #endregion

    }
}