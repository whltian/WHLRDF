﻿
using WHLRDF.ORG.Model;
using WHLRDF.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WHLRDF.Application.Model;
using WHLRDF.Application.BLL;

namespace WHLRDF.ORG.BLL
{
    public class ORGService : SerivceBase,IORGService
    {
       

        //private OrganizationsEntity _organizationsEntity;
        ///// <summary>
        ///// 组织机构
        ///// </summary>
        //public OrganizationsEntity OrgEntity {
        //    get {
        //        if (string.IsNullOrWhiteSpace(OrgId))
        //        {
        //            return null;
        //        }
        //        if (_organizationsEntity == null)
        //        {
        //            _organizationsEntity = DbRepository.GetById<OrganizationsEntity>(OrgId);
        //        }
        //        return _organizationsEntity;
        //    }
        //}
        /// <summary>
        /// 验证用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool CheckOrgUser(string orgId,string userid)
        {
            var criterion = Expression.And(Expression.Eq( JobFunctionEntity.__IsDeleted, false),
            Expression.And(Expression.Eq( JobFunctionEntity.__SourceId, userid),
             Expression.And(Expression.Eq( JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
            Expression.Eq( JobFunctionEntity.__OrgId, orgId))));
            var job = DbRepository.Select<JobFunctionEntity>(criterion);
            if (job != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 获取组用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool CheckGroupUser(string orgId, string userid,string group)
        {
            var criterion = Expression.And(Expression.Eq(RelationUserEntity.__IsDeleted, false),
                Expression.And(Expression.Eq(RelationUserEntity.__RelationType, RelationUserType.Group.GetHashCode()),
                Expression.Eq(JobFunctionEntity.__SourceId, group)));
            var entities = DbRepository.Query<RelationUserEntity>(criterion);
            if (entities != null && entities.Count > 0)
            {
                var check = entities.Where(x => x.UserId.Equals(userid)).FirstOrDefault();
                return check != null;
            }
            return false;
        }

        /// <summary>
        /// 验证是否部门用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public bool CheckDepartmentUser(string orgId, string userid, string departmentId)
        {
            var criterion = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),
            Expression.And(Expression.Eq(JobFunctionEntity.__DepartmentId, departmentId),
            Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
            Expression.Eq(JobFunctionEntity.__OrgId, orgId))
            ));
            var entities = DbRepository.Query<JobFunctionEntity>(criterion);
            if (entities != null && entities.Count > 0)
            {
                var check = entities.Where(x => x.SourceId.Equals(userid)).FirstOrDefault();
                return check != null;
            }
            return false;
        }

        /// <summary>
        /// 验证岗位用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public bool CheckJobFunctionUser(string orgId, string userid, string job)
        {
            var criterion = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),
               Expression.And(Expression.Eq(JobFunctionEntity._PrimaryKeyName, job),
               Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
               Expression.Eq(JobFunctionEntity.__OrgId, orgId))
               ));
            var jobEntity = DbRepository.Select<JobFunctionEntity>(criterion);
            if (jobEntity != null)
            {
                return jobEntity.SourceId.Equals(userid);
            }
            return false;
        }

        /// <summary>
        /// 获取岗位用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserByJobFunction(  string orgId,string job)
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return "";
            }
            var criterion = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),
                Expression.And(Expression.Eq(JobFunctionEntity._PrimaryKeyName, job),
                Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
                Expression.Eq(JobFunctionEntity.__OrgId, orgId))
                ));
            var jobEntity = DbRepository.Select<JobFunctionEntity>(criterion);
            if (jobEntity != null)
            {
                return jobEntity.SourceId;
            }
            return "";
        }

        /// <summary>
        /// 获取部门下所有用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public List<string> GetUserByDepartment(string orgId, string departmentId)
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return null;
            }
            var criterion = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),
            Expression.And(Expression.Eq(JobFunctionEntity.__DepartmentId, departmentId),
            Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
            Expression.Eq(JobFunctionEntity.__OrgId, orgId))
            ));
            var entities = DbRepository.Query<JobFunctionEntity>(criterion);
            if (entities != null && entities.Count > 0)
            {
                return (from x in entities select x.SourceId).ToList();
            }

            return null;
        }

        /// <summary>
        /// 获取组用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public List<string> GetUserByGroup(  string orgId,string group)
        {
            var criterion = Expression.And(Expression.Eq(RelationUserEntity.__IsDeleted, false),
                 Expression.And(Expression.Eq(RelationUserEntity.__RelationType, RelationUserType.Group.GetHashCode()),
                 Expression.Eq(JobFunctionEntity.__SourceId, group)) );
            var entities = DbRepository.Query<RelationUserEntity>(criterion);
            if (entities != null && entities.Count > 0)
            {
                return (from x in entities  select x.UserId).ToList();
            }
            return null;
        }


        /// <summary>
        /// 获取部门管理者
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserByManager(string orgId, string job )
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return "";
            }
            var criterion = Expression.And(Expression.Eq("b." + JobFunctionEntity.__IsDeleted, false),
                Expression.And(Expression.Eq("b." + JobFunctionEntity._PrimaryKeyName, job),
              //  Expression.And(Expression.Eq("b." + JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
                Expression.Eq("b." + JobFunctionEntity.__OrgId, orgId)));
            StringBuilder strsql = new StringBuilder(@"select a.SourceId from ORG_JobFunction as a 
                            inner join ORG_JobFunction as b on a.DepartmentId=b.DepartmentId
                            where ( a.ParentId is null or a.ParentId='' ) and a.IsDeleted=0 and {0}");
            object userid = DbRepository.Scalar(strsql.ToString(), criterion);
            if (userid != null)
            {
                return userid.ToString();
            }
            return null;
        }

        /// <summary>
        /// 获取上级用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        public string GetUserBySuperior(string orgId, string job)
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return "";
            }
            var criterion = Expression.And(Expression.Eq("b." + JobFunctionEntity.__IsDeleted, false),
                Expression.And(Expression.Eq("b." + JobFunctionEntity._PrimaryKeyName, job),
               // Expression.And(Expression.Eq("b." + JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
                Expression.Eq("b." + JobFunctionEntity.__OrgId, orgId)));
            StringBuilder strsql = new StringBuilder(@"select a.* from ORG_JobFunction as a 
                inner join ORG_JobFunction as b on a.JobFunctionId = b.ParentId where {0}");
            JobFunctionEntity jobFunction = DbRepository.Select<JobFunctionEntity>(strsql.ToString(), criterion);
            if (jobFunction != null)
            {
                if (jobFunction.JobFunctionType == JobFunctionType.SubChart.GetHashCode())
                {
                    return this.GetUserByJobFunction(jobFunction.JobFunctionId, orgId);
                }
                return jobFunction.SourceId;
            }

            return null;
        }

        /// <summary>
        /// 获取用户岗位
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetJobFunctionByUser(string orgId, string userid )
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return "";
            }
            var criterion = Expression.And(Expression.Eq( JobFunctionEntity.__IsDeleted, false),
                Expression.And(Expression.Eq( JobFunctionEntity.__SourceId, userid),
                 Expression.And(Expression.Eq(JobFunctionEntity.__IsPrimary, true),
                 Expression.And(Expression.Eq( JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
                Expression.Eq( JobFunctionEntity.__OrgId, orgId)))));
            var job = DbRepository.Select<JobFunctionEntity>(criterion);
            if (job != null)
            {
                return job.JobFunctionId;
            }
            return "";
        }

        /// <summary>
        /// 获取用户部门
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public string GetDepartmentByUser(string orgId, string userid )
        {
            if (string.IsNullOrWhiteSpace(orgId))
            {
                return "";
            }
            var criterion = Expression.And(Expression.Eq(JobFunctionEntity.__IsDeleted, false),
                Expression.And(Expression.Eq(JobFunctionEntity.__SourceId, userid),
                 Expression.And(Expression.Eq(JobFunctionEntity.__IsPrimary, true),
                 Expression.And(Expression.Eq(JobFunctionEntity.__JobFunctionType, JobFunctionType.JobFunction.GetHashCode()),
                Expression.Eq(JobFunctionEntity.__OrgId, orgId)))));
            var job = DbRepository.Select<JobFunctionEntity>(criterion);
            if (job != null)
            {
                return job.DepartmentId;
            }
            return "";
        }
    }
}
