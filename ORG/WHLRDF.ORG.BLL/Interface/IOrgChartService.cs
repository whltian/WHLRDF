﻿using WHLRDF.ORG.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WHLRDF.ORM;

namespace WHLRDF.ORG.BLL
{
    public interface IOrgChartService
    {
        /// <summary>
        /// 获取组织机构图
        /// </summary>
        /// <param name="deptid"></param>
        /// <returns></returns>
        OrgChartEntity GetOrgById(string deptid);

        /// <summary>
        /// 获取岗位信息
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        JobFunctionEntity GetJobById(JobFunctionEntity entity, ref string strError);

        /// <summary>
        /// 保存岗位信息
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool SaveJobFunction(ref JobFunctionEntity entity, ref string strError);

        /// <summary>
        /// 部门保存
        /// </summary>
        /// <param name="deptEntity"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool DeptSave(ref DepartmentEntity deptEntity, ref string strError);

        /// <summary>
        /// 岗位删除
        /// </summary>
        /// <param name="jobFuntionId"></param>
        /// <param name="deptId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool JobDelete(string jobFuntionId, string deptId, ref string strError);

        /// <summary>
        /// 获取组织架构图
        /// </summary>
        /// <param name="deptid">部门id</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        OrgChartEntity GetOrgById(string deptid, ref string strError);

        /// <summary>
        /// 迁出
        /// </summary>
        /// <param name="deptid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool CheckOut(string deptid, ref string strError);

        /// <summary>
        /// 迁入操作
        /// </summary>
        /// <param name="deptid"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        bool CheckIn(string deptid, ref string strError);

        bool Revoke(string deptid, ref string strError);

        /// <summary>
        /// 获取岗位下的用户
        /// </summary>
        /// <param name="ligerGrid"></param>
        /// <returns></returns>
        LigerGrid GetUsers(LigerGrid ligerGrid);


        /// <summary>
        /// 获取部门岗位
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="orgId"></param>
        /// <param name="orgId">是否加载岗位</param>
        /// <returns></returns>
        List<DepartmentEntity> GetDeptByOrgId(string parentId, string orgId, bool iscurrentDept, int isloadjob);
    }
}
