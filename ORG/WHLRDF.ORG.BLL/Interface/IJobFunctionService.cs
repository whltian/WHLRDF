﻿using System.Collections.Generic;
using WHLRDF.ORG.Model;
using WHLRDF.ORM;

namespace WHLRDF.ORG.BLL
{
    public interface IJobFunctionService
    {
        #region 基本方法
        /// <summary>
        /// 查询所有
        /// </summary>
        List<JobFunctionEntity> GetAll();

        /// <summary>
        /// 获取单个实例的方法
        /// <param name="id">object类型</param>
        /// </summary>
        JobFunctionEntity GetById(string id);

        /// <summary>
        /// 保存方法
        /// <param name="entity">对象</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Save(JobFunctionEntity entity, ref string strError);

        /// <summary>
        /// 删除方法
        /// <param name="deleteKey">主键，如果字符串请将字符串列表用单引号将字符串用上,如（'aaa','bbb'）</param>
        /// <param name="strError">错误信息</param>
        /// </summary>
        bool Delete(string deleteKey, ref string strError);

        /// <summary>
        /// 分页查询方法
        /// <param name="grid">分页查询的相关参数</param>
        /// </summary>
        LigerGrid ForGrid(LigerGrid grid);
        #endregion

        #region 扩展方法

        #endregion

    }
}
