﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.ORG.BLL
{
    public interface IORGService
    {
        /// <summary>
        /// 验证组织机构用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        bool CheckOrgUser(string orgId, string userid);

        /// <summary>
        /// 验证部门用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        bool CheckDepartmentUser(string orgId, string userid, string departmentId);

        /// <summary>
        /// 验证岗位用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        bool CheckJobFunctionUser(string orgId, string userid, string job);

        /// <summary>
        /// 验证组用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        bool CheckGroupUser(string orgId, string userid, string group);

        #region 获取用户
        /// <summary>
        /// 获取组用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        List<string> GetUserByGroup(string orgId, string group);

        /// <summary>
        /// 获取岗位用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        string GetUserByJobFunction(string orgId,string job);

        /// <summary>
        /// 获取部门用户
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        List<string> GetUserByDepartment(string orgId,string departmentId);

        /// <summary>
        /// 获取上级
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        string GetUserBySuperior(string orgId,string job);


        /// <summary>
        /// 获取部门经理
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        string GetUserByManager(string orgId,string job);

        /// <summary>
        /// 通过用户获取岗位
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        string GetJobFunctionByUser(string orgId,string userid);

        /// <summary>
        /// 获取用户部门
        /// </summary>
        /// <param name="orgId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        string GetDepartmentByUser(string orgId,string userid);
        #endregion
    }
}
