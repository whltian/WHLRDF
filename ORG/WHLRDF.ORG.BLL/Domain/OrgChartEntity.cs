﻿using WHLRDF.ORG.Model;
using System.Collections.Generic;
namespace WHLRDF.ORG.BLL
{
    /// <summary>
    /// 组织架构实体
    /// </summary>
    public class OrgChartEntity
    {
        /// <summary>
        /// 部门信息
        /// </summary>
        public DepartmentEntity Department { get; set; }

        /// <summary>
        /// 岗位信息
        /// </summary>
        public List<JobFunctionEntity> JobData { get; set; }

        public OrganizationsEntity OrgEntity{get; set; }
    }

   
}
