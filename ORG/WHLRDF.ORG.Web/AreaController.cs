﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF;
using WHLRDF.Application.BLL;

namespace WHLRDF.ORG.Web
{
  
    [MenuPage(1300, "300", "组织结构管理", "ORG", "fa-sitemap")]
    [Area("ORG")]
    [Route("org/{controller=Home}/{action=Index}/{id?}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "ORG";
            }
        }
    }
}
