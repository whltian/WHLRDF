﻿


using Microsoft.AspNetCore.Mvc;
using WHLRDF.ORG.BLL;
using WHLRDF.Application.BLL;
using WHLRDF.ORM;
using WHLRDF;

namespace WHLRDF.ORG.Web.Controllers
{
    public class ApiController :AreaController
    {
        #region Service注入
     
        public IDepartmentService mService { get; set; }
        public IJobFunctionService mJobService { get; set; }
        public IOrgChartService mOrgService { get; set; }

        public IUserService muService { get; set; }
        [HttpGet]
        public IActionResult UserView() {
            return View();
        }

        [HttpPost]
        public IActionResult GetUsers(LigerGrid ligerGrid)
        {
            return Json(AjaxResult.Success(mOrgService.GetUsers(ligerGrid)));
        }
        #endregion

    }
}
