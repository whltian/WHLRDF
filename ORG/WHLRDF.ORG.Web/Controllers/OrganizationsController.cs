﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.ORG.BLL;
using WHLRDF.ORG.Model;
using WHLRDF.ORM;

namespace WHLRDF.ORG.Web.Controllers
{
    [MenuPage(20, "020", "组织机构管理", "ORG/Organizations", "fa-sitemap",IsDispay =false,IsDeleted =true)]
    public class OrganizationsController : AreaController
    {
        #region Service注入

        public IOrganizationsService mService { get; set; }

        public IDepartmentService dService { get; set; }
        public IOrgChartService oService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        ///</summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false)]
   
        public IActionResult Edit(string id, OrganizationsEntity entity)
        {
            string strError = "";
            if (this.IsPost)
            {
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            return Json(AjaxResult.Success(entity));
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                }
            }
            if (IsAjax)
            {
                return Json(AjaxResult.Error(!string.IsNullOrWhiteSpace(strError) ? strError : "数据格式错误"));
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        ///</summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
        [HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion

        #region GetTree
        [Permission(IsParent =true)]
        public JsonResult GetTree(string id,string orgId)
        {
            if (!string.IsNullOrWhiteSpace(id) && id.Equals("#"))
            {
                OrganizationsEntity organizations = new OrganizationsEntity()
                {
                    OrgId = "000",
                    parent = "#",
                    OrgName = "组织架构管理",
                    CheckStatus = false,
                    IsAdded = true,
                    
                };
                return Json(AjaxResult.Success(new object[] { organizations }));
            }
            else if (!string.IsNullOrWhiteSpace(orgId) && orgId.Equals("000"))
            {
                return Json(AjaxResult.Success(mService.GetAll()));
            }
            else
            {
                return Json(AjaxResult.Success(dService.GetDeptByOrgId(id, orgId)));
            }
        }
       [HttpPost]
        public JsonResult GetOrgTree(string id, string orgId,bool iscurrentDept, int isloadjob)
        {
             return Json(AjaxResult.Success(oService.GetDeptByOrgId(id, orgId, iscurrentDept, isloadjob)));
        }
        [HttpGet]
        [Permission(IsParent = true)]
        public IActionResult ModalEdit(string id, OrganizationsEntity entity)
        {
            if (string.IsNullOrEmpty(entity.OrgId)|| entity.OrgId=="000")
            {
                entity.OrgId = "";
            }
            return View(entity);
        }
        #endregion
    }
}
