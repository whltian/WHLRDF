﻿

using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;
using WHLRDF.ORG.BLL;
using WHLRDF.ORG.Model;
using WHLRDF.ORM;

namespace WHLRDF.ORG.Web.Controllers
{
    [MenuPage(50, "050", "组织架构管理", "Org/OrgChart", "fa-sitemap",IsDispay =false)]
    public class OrgChartController :AreaController
    {
        #region Service注入
     
        public IDepartmentService mService { get; set; }
        public IJobFunctionService mJobService { get; set; }
        public IOrgChartService mOrgService { get; set; }

        public IUserService muService { get; set; }
        #endregion
        [Permission]
        public IActionResult Index(string id)
        {
            var dept = mOrgService.GetOrgById(id);
            return Json(AjaxResult.Success(dept));
        }
         [HttpPost]
        [Permission("05","迁出")]
        /// <summary>
        /// 迁出
        /// </summary>
        /// <param name="deptid"></param>
        /// <returns></returns>
        public ActionResult CheckOut(string departmentId)
        {
            string strError = "";
            if (mOrgService.CheckOut(departmentId, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        [HttpPost]
        [Permission(ActionName="CheckOut")]
        public ActionResult CheckIn(string departmentId)
        {
            string strError = "";
            if (mOrgService.CheckIn(departmentId, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        [Permission(ActionName="CheckOut")]
        public ActionResult Revoke(string departmentId)
        {
            string strError = "";
            if (mOrgService.Revoke(departmentId, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpPost]
        [Permission(NoAccess =true)]
        public JsonResult Delete(string deleteKeys)
        {
            return Json(AjaxResult.Error());
        }
         [HttpPost]
        [Permission(NoAccess = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Error());
        }
        [Permission(IsParent = true, ActionName = "CheckOut")]
        /// <summary>
        /// 岗位编辑
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public IActionResult JobEdit(string id,JobFunctionEntity entity)
        {
            string strError = "";
            if (!this.IsPost)
            {
                entity = mOrgService.GetJobById(entity, ref strError);
            }
            else
            {
               
                if (ModelState.IsValid)
                {
                    if (mOrgService.SaveJobFunction(ref entity, ref strError))
                    {
                        return Json(AjaxResult.Success(entity));
                    }
                }
                else
                {

                    foreach (string key in ModelState.Keys)
                    {
                        foreach (var p in ModelState[key].Errors)
                        {
                            strError += "\n" + p.ErrorMessage;
                        }
                    }
                }
                return Json(AjaxResult.Error(strError));
            }
            return View(entity);
        }
        [Permission(IsParent = true,ActionName = "CheckOut")]
        /// <summary>
        /// 部门编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ActionResult DeptEdit(string id, DepartmentEntity entity)
         {
             string strError = "";
             if (!this.IsPost)
             {
                 if (!string.IsNullOrEmpty(id))
                 {
                     var orgEntity= mOrgService.GetOrgById(id);
                     entity = orgEntity != null ? orgEntity.Department : entity;
                 }
                
             }
             else
             {
                
                 if (ModelState.IsValid)
                 {
                     if (mOrgService.DeptSave(ref entity, ref strError))
                     {
                         return Json(AjaxResult.Success(entity));
                     }
                 }
                 else
                 {
                     //this.GetModelError(ref strError);
                 }
                 return Json(AjaxResult.Error(strError));
             }
             return View(entity);
         }
        [HttpPost]
        [Permission(IsParent = true, ActionName = "CheckOut")]
        /// <summary>
        /// 岗位删除
        /// </summary>
        /// <param name="jobid"></param>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public JsonResult JobDelete(string jobid, string deptId)
         {
             string strError = "";
             if( mOrgService.JobDelete(jobid,deptId,ref strError))
             {
                 return Json(AjaxResult.Success());
             }
             return Json(AjaxResult.Error(strError));
         }
        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]

        public JsonResult GetResource(string keyword, string qtype, string orgId)
        {
            if (qtype.Equals("1"))
            {
                return Json(AjaxResult.Success(mService.GetDepartmentByName(keyword,orgId)));
            }
            else
            {
                LigerGrid ligerGrid = new LigerGrid();
                ligerGrid.keyWord = keyword;
                ligerGrid.sortName = EntityBase.__LastModifyDate;
                ligerGrid.sortOrder = "desc";
                ligerGrid.pageSize = 0;
                ligerGrid = muService.GetUsersByName(ligerGrid);
                return Json(AjaxResult.Success(ligerGrid.Rows));
            }
          
        }
    }
}
