﻿
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.ORG.BLL;
using WHLRDF.ORG.Model;
using WHLRDF.ORM;

namespace WHLRDF.ORG.Web.Controllers
{
    [MenuPage(40, "040", "职级管理", "ORG/JobFunction", "",IsDispay =false)]
    public class JobFunctionController : AreaController
    {
        #region Service注入

        public IJobFunctionService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        ///</summary>
        /// <returns></returns>
        [Permission()]
        public IActionResult Index(LigerGrid grid)
        {
            return View();
        }

        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false, NoAccess = true)]
        public IActionResult Edit(string id, JobFunctionEntity entity)
        {
            if (!this.IsPost)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    entity = mService.GetById(id);
                }
                if (entity == null)
                {
                    entity = new JobFunctionEntity
                    {

                    };
                }
            }
            else
            {
                string strError = "";
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            return Json(AjaxResult.Success(entity));
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, strError);
                    if (IsAjax)
                    {
                        return Json(AjaxResult.Error(strError));
                    }
                }
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        ///</summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
        [HttpPost]
        [Permission("02", "删除",NoAccess =true)]
        public JsonResult Delete(string deleteKeys)
        {
            string strError = "";
            if (mService.Delete(deleteKeys, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }

        [HttpGet]
        [Permission(IsParent = true)]
        public IActionResult ModalEdit(string id, JobFunctionEntity entity)
        {
            if (!string.IsNullOrEmpty(id))
            {
                entity = mService.GetById(id);
            }
            if (entity == null)
            {
                entity = new JobFunctionEntity
                {

                };
            }
            return View(entity);
        }
        #endregion
    }
}

