﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WHLRDF.Application.BLL;
using WHLRDF.ORG.BLL;
using WHLRDF.ORG.Model;
using WHLRDF.ORM;

namespace WHLRDF.ORG.Web.Controllers
{
    [MenuPage(10, "010", "组织架构", "ORG/Department", "fa-sitemap")]
    public class DepartmentController : AreaController
    {
        #region Service注入

        public IDepartmentService mService { get; set; }
        #endregion

        #region 模版生成
        /// <summary>
        /// 首页
        ///</summary>
        /// <returns></returns>
        [Permission]
        public IActionResult Index()
        {
            return View();
        }
       
        /// <summary>
        /// Grid 分页查询方法
        /// </summary>
        /// <param name="grid">分页参数</param>
        /// <param name="data">其他参数</param>
        /// <returns></returns>
        [HttpPost]
        [Permission(IsParent = true)]
        public JsonResult ForGrid(LigerGrid grid)
        {
            return Json(AjaxResult.Success(mService.ForGrid(grid)));
        }

        /// <summary>
        /// 保存 编辑方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Permission("01", "编辑", IsGet = false,NoAccess =true)]
        public IActionResult Edit(string id, DepartmentEntity entity)
        {
            string strError = "";
            if (this.IsPost)
            {
                if (ModelState.IsValid)
                {
                    if (mService.Save(entity, ref strError))
                    {
                        if (IsAjax)
                        {
                            return Json(AjaxResult.Success(entity));
                        }
                        return RedirectToAction("List");
                    }
                    ModelState.AddModelError(string.Empty, strError);

                }
            }
            if (IsAjax)
            {
                return Json(AjaxResult.Error(!string.IsNullOrWhiteSpace(strError)?strError:"数据格式错误"));
            }
            return View(entity);
        }

        /// <summary>
        /// 批量删除方法
        ///</summary>
        /// <param name="deleteKeys">主键列表</param>
        /// <returns></returns>
        [HttpPost]
        [Permission("02", "删除")]
        public JsonResult Delete(string departmentId)
        {
            string strError = "";
            if (mService.SingleDelete(departmentId, ref strError))
            {
                return Json(AjaxResult.Success());
            }
            return Json(AjaxResult.Error(strError));
        }
        #endregion
        #region 
        [Permission(IsParent =true, NoAccess = true)]
        public IActionResult List()
        {
            return View();
        }
        [HttpGet]
        [Permission(IsParent = true)]
        public IActionResult ModalEdit(string id, DepartmentEntity entity)
        {
            if (!string.IsNullOrEmpty(id))
            {
                entity = mService.GetById(id);
            }
            return View(entity);
        }
        #endregion
    }
}

