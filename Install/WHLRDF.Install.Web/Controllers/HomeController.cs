﻿
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WHLRDF.Install.BLL;

namespace WHLRDF.Install.Web.Controllers
{
    public class HomeController : AreaController
    {
        public IInstallService mService { get; set; }
   

        public IActionResult Index() {
            return View();
        }
        /// <summary>
        /// 检测安装环境
        /// </summary>
        /// <param name="site"></param>
        /// <param name="smtp"></param>
        /// <returns></returns>
        public IActionResult CheckInstallEnv(SiteSettings site,SMTPConfig smtp)
        {
            return Json(AjaxResult.Success(mService.CheckInstallEnv(site, smtp)));
        }

        /// <summary>
        /// 获取线程状态
        /// </summary>
        /// <param name="taskId">线程Id</param>
        /// <returns></returns>
        public IActionResult CheckTaskStatus(string taskId)
        {
            string strError = "";
            return Json(AjaxResult.Success(mService.CheckTaskStatus(taskId, ref strError)));
        }


        /// <summary>
        /// 获取线程状态
        /// </summary>
        /// <param name="taskId">线程Id</param>
        /// <returns></returns>
        public IActionResult TaskRemove(string taskId)
        {
            string strError = "";
            return Json(AjaxResult.Success(mService.TaskRemove(taskId,ref strError)));
        }
        /// <summary>
        /// 安装数据库
        /// </summary>
        /// <param name="site"></param>
        /// <param name="smtp"></param>
        /// <returns></returns>
        public IActionResult InstallDbEnv(SiteSettings site, SMTPConfig smtp)
        {
            return Json(AjaxResult.Success(mService.InstallDbEnv(site, smtp)));
        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="site"></param>
        /// <param name="smtp"></param>
        /// <returns></returns>
        public IActionResult InitialEnv(SiteSettings site, SMTPConfig smtp)
        {
            return Json(AjaxResult.Success(mService.InitialEnv(site, smtp)));
        }


    }
}
