﻿
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WHLRDF.Install.BLL;

namespace WHLRDF.Install.Web.Controllers
{
    public class InitDataController : AreaController
    {
        public IInstallService mService { get; set; }
   

        public IActionResult Index() {
            return View();
        }

        /// <summary>
        /// 生成初始化数据文件
        /// </summary>
        /// <param name="plug"></param>
        /// <returns></returns>
        public IActionResult CreateData(SiteSettings site,string plugs)
        {
            return Json(AjaxResult.Success(mService.CreateIntallData(site,plugs)));

        }
        /// <summary>
        /// 获取线程状态
        /// </summary>
        /// <param name="taskId">线程Id</param>
        /// <returns></returns>
        public IActionResult CheckTaskStatus(string taskId)
        {
            string strError = "";
            return Json(AjaxResult.Success(mService.CheckTaskStatus(taskId, ref strError)));
        }
    }
}
