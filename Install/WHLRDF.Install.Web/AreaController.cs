﻿using Microsoft.AspNetCore.Mvc;
using WHLRDF.Application.BLL;

namespace WHLRDF.Install.Web
{
    [Area("Install")]
    [Route("Install/{controller=Home}/{action=Index}")]
    public class AreaController: BaseAreaController
    {
        public override string AreaName
        {
            get
            {
                return "Install";
            }
        }
    }
}
