﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Loader;
using System.Data;
using System.Data.Common;
namespace WHLRDF.Install.BLL
{
    /// <summary>
    /// 数据库安装
    /// </summary>
    public class InstallDbContext : DbContext
    {
        #region 

        protected string ConnectionString { get; set; }

        public DbProviderFactory DbFactory { get; set; }

        private List<SystemComponent> DbModels { get; set; }

        public  ProviderType ProviderType { get; set; }

        public InstallDbContext(ProviderType providerType,string connectionString, List<SystemComponent> dbModels)
        {
            ProviderType = providerType;
            ConnectionString = connectionString;
            DbModels = dbModels;
        }

        // public DbSet<User> Users { get; set; }
        public InstallDbContext(DbContextOptions<InstallDbContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            switch (ProviderType)
            {
                case ProviderType.SqlServer:
                    builder.UseSqlServer(this.ConnectionString);
                    break;
                case ProviderType.MySql:
                    builder.UseMySql(this.ConnectionString);
                    break;
                case ProviderType.Oracle:
                    break;
            }
           
        }
        /// <summary>
        /// 创建映射关系
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region 动态加载模型实体
            base.OnModelCreating(builder);
            List<dynamic> dynamics = null;
            if (dynamics == null)
            {
                dynamics = new List<dynamic>();
                if (DbModels != null && DbModels.Count > 0)
                {
                    var basePath = AppDomain.CurrentDomain.BaseDirectory;
                    foreach (var component in DbModels)
                    {
                        if (!component.Opened || component.Model == null || component.Model.Length <= 0)
                        {
                            continue;
                        }
                        foreach (var dll in component.Model)
                        {
                            var typesToRegister = AssemblyLoadContext.Default.LoadFromAssemblyPath(basePath + dll).GetTypes().Where(q => q.GetInterface(typeof(IEntityTypeConfiguration<>).FullName) != null);
                            foreach (var type in typesToRegister)
                            {
                                dynamic configurationInstance = Activator.CreateInstance(type);
                                dynamics.Add(configurationInstance);
                                builder.ApplyConfiguration(configurationInstance);
                            }
                        }

                    }
                }

                return;
            }
            if (dynamics == null || dynamics.Count <= 0)
            {
                return;
            }
            foreach (var type in dynamics)
            {
                builder.ApplyConfiguration(type);
            }
            #endregion
        }


        /// <summary>
        /// 初始化数据库
        /// </summary>
        /// <returns></returns>
        public bool DbEnsureCreated()
        {
            if (this.Database != null && this.Database.EnsureCreated())
            {
                return true;
             
            }
            return false;

        }
        #endregion


    }
}