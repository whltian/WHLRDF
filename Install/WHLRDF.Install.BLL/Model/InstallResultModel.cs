﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Install.Model
{
    /// <summary>
    /// 安装输出文件
    /// </summary>
    public class InstallResultModel
    {
        public string Name { get; set; }

        public bool ResultStatus { get; set; }

        public string Result { get; set; }

        public string TaskNo { get; set; }
    }
}
