﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.Install.Model;

namespace WHLRDF.Install.BLL
{
    public interface IInstallService
    {
        /// <summary>
        /// 环境检测
        /// </summary>
        /// <param name="site">系统配置</param>
        /// <param name="smtp">邮件配置</param>
        /// <returns></returns>
        List<TaskModel> CheckInstallEnv(SiteSettings site, SMTPConfig smtp);

        /// <summary>
        /// 安装数据库
        /// </summary>
        /// <param name="site"></param>
        /// <param name="smtp"></param>
        /// <returns></returns>
        TaskModel InstallDbEnv(SiteSettings site, SMTPConfig smtp);

        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="site"></param>
        /// <param name="smtp"></param>
        /// <returns></returns>
        List<TaskModel> InitialEnv(SiteSettings site, SMTPConfig smtp);

        /// <summary>
        /// 创建初始化数据
        /// </summary>
        /// <param name="site">系统配置</param>
        /// <param name="plugs">系统插件列表</param>
        /// <param name="strError"></param>
        /// <returns></returns>
        TaskModel CreateIntallData(SiteSettings site, string plugs);

        /// <summary>
        /// 验证线程执行情况
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        TaskModel CheckTaskStatus(string taskId, ref string strError);

        /// <summary>
        /// 删除任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        bool TaskRemove(string taskId, ref string strError);
    }
}
