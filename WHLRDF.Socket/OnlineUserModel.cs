﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Socket
{
    /// <summary>
    /// 在线用户集合
    /// </summary>
    public class SocketUsers
    {
        private static List<OnlineUserModel> _socketConnectUsers;
        /// <summary>
        /// 用于存储在线的websocket用户
        /// </summary>
        public static List<OnlineUserModel> SocketConnectUsers
        {
            get
            {
                if (_socketConnectUsers == null)
                {
                    _socketConnectUsers = new List<OnlineUserModel>();
                }
                return _socketConnectUsers;
            }
        }
    }
    /// <summary>
    /// 在线用户
    /// </summary>
    public class OnlineUserModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }


        public string ConnectionId { get; set; }
    }
}
