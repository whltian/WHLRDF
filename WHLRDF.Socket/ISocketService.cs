﻿using System;
using System.Collections.Generic;
using System.Text;
using WHLRDF.Model;

namespace WHLRDF.Socket
{
    public interface ISocketService
    {
        bool Send(SocketMessageModel message);
    }
}
