﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;

namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 流水号记录表EF 字段映射
    /// </summary>
    public partial class SerialRuleNumberMap : ClassMap<SerialRuleNumberEntity>
    {
        public override void Configure(EntityTypeBuilder<SerialRuleNumberEntity>
        builder)
        {
            builder.ToTable<SerialRuleNumberEntity>("COMM_SerialRuleNumber");
            
            builder.HasKey(x => x.SerialRuleNumberId);
            builder.Property(x => x.SerialRuleNumberId).HasColumnName("SerialRuleNumberId").HasMaxLength(50);

            /// <summary>
            /// 规则编号
            /// </summary>
            builder.Property(x => x.SerialRuleId).HasColumnName("SerialRuleId").HasMaxLength(100).IsRequired(true);

            /// <summary>
            /// 规则流水号
            /// </summary>
            builder.Property(x => x.SerialRuleNo).HasColumnName("SerialRuleNo").HasMaxLength(100).IsRequired(true);

            /// <summary>
            /// 最大记录数
            /// </summary>
            builder.Property(x => x.MaxCount).HasColumnName("MaxCount");

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);
            base.Configure(builder);
        }
    }
}
