using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class TemplateMap : ClassMap<TemplateEntity>
    {
        public override void Configure(EntityTypeBuilder<TemplateEntity>
            builder)
        {
            builder.ToTable<TemplateEntity>("COMM_Template");
            
            /// <summary>
            ///  编号
            /// </summary>
            builder.HasKey(x => x.TemplateId);

            builder.Property(x => x.TemplateId).HasColumnName("TemplateId").HasMaxLength(50);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.TemplateType).HasColumnName("TemplateType");

            /// <summary>
            ///  标题
            /// </summary>
            builder.Property(x => x.Title).HasColumnName("Title").HasMaxLength(100).IsRequired(true);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            /// <summary>
            ///  内容
            /// </summary>
            builder.Property(x => x.Content).HasColumnName("Content").IsRequired(true);



            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}