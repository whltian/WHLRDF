﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class DictionaryMap : ClassMap<DictionaryEntity>
    {
        public override void Configure(EntityTypeBuilder<DictionaryEntity>
            builder)
        {
            builder.ToTable<DictionaryEntity>("COMM_Dictionary");
           

            /// <summary>
            ///  编号
            /// </summary>
            builder.HasKey(x => x.DictionaryId);

            builder.Property(x => x.DictionaryId).HasColumnName("DictionaryId").HasMaxLength(50);

            /// <summary>
            ///  主键
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.KeyName).HasColumnName("KeyName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  值
            /// </summary>
            builder.Property(x => x.KeyValue).HasColumnName("KeyValue").HasDefaultValue(0);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(0);

            /// <summary>
            /// 描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(2000).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}