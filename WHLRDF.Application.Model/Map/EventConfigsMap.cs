﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// COMM_EventConfigsEF字段映射
    /// </summary>
    public partial class EventConfigsMap : ClassMap<EventConfigsEntity>
    {
        public override void Configure(EntityTypeBuilder<EventConfigsEntity>
        builder)
        {
            builder.ToTable<EventConfigsEntity>("COMM_EventConfigs");

            builder.HasKey(x => x.EventCode);
            builder.Property(x => x.EventCode).HasColumnName("EventCode").HasMaxLength(50);

            /// <summary>
            /// 事件名称
            /// </summary>
            builder.Property(x => x.EventName).HasColumnName("EventName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 请求地址
            /// </summary>
            builder.Property(x => x.Url).HasColumnName("Url").HasMaxLength(500).IsRequired(true);

            /// <summary>
            /// 密钥
            /// </summary>
            builder.Property(x => x.MD5).HasColumnName("MD5").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}