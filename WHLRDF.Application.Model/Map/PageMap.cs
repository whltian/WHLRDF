﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class PageMap : ClassMap<PageEntity>
    {
        public override void Configure(EntityTypeBuilder<PageEntity>
            builder)
        {
            builder.ToTable<PageEntity>("COMM_Page");
            
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.PageId);

            builder.Property(x => x.PageId).HasColumnName("PageId").ValueGeneratedOnAdd().HasMaxLength(36);

            /// <summary>
            ///  上级id
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(36);

            /// <summary>
            ///  页面名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 是否支持Get请求
            /// </summary>
            builder.Property(x => x.IsGet).HasColumnName("IsGet").HasDefaultValue(1);

            /// <summary>
            /// 是否支持POST请求
            /// </summary>
            builder.Property(x => x.IsPost).HasColumnName("IsPost").HasDefaultValue(1);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.AreaName).HasColumnName("AreaName").HasMaxLength(50);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.ControllerName).HasColumnName("ControllerName").HasMaxLength(50);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.IsParent).HasColumnName("IsParent").HasDefaultValue(0);

            builder.Property(x => x.NoAccess).HasColumnName("NoAccess").HasDefaultValue(0);

            /// <summary>
            ///  Action
            /// </summary>
            builder.Property(x => x.ActionName).HasColumnName("ActionName").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  全路径
            /// </summary>
            builder.Property(x => x.RouteUrl).HasColumnName("RouteUrl").HasMaxLength(200).IsRequired(false);

            /// <summary>
            ///  图标
            /// </summary>
            builder.Property(x => x.Icon).HasColumnName("Icon").HasMaxLength(100).IsRequired(false);


            /// <summary>
            ///  是否菜单
            /// </summary>
            builder.Property(x => x.IsMenu).HasColumnName("IsMenu").HasDefaultValue(0);

            /// <summary>
            ///  是否系统
            /// </summary>
            builder.Property(x => x.IsAdmin).HasColumnName("IsAdmin").HasDefaultValue(0);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.IsDisplay).HasColumnName("IsDisplay").HasDefaultValue(1);

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.OrderNum).HasColumnName("OrderNum").HasDefaultValue(1);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x=>x.IsApi);
        }
    }
}