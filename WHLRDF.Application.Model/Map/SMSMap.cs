﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class SMSMap : ClassMap<SMSEntity>
    {
        public override void Configure(EntityTypeBuilder<SMSEntity>
            builder)
        {
            builder.ToTable<SMSEntity>("COMM_SMS");
          

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.SMSId);

            builder.Property(x => x.SMSId).HasColumnName("SMSId").HasMaxLength(50);

            /// <summary>
            ///  表单id
            /// </summary>
            builder.Property(x => x.FormId).HasColumnName("FormId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.FormType).HasColumnName("FormType");

            /// <summary>
            ///  用户
            /// </summary>
            builder.Property(x => x.UserId).HasColumnName("UserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  接收号码
            /// </summary>
            builder.Property(x => x.ToPhone).HasColumnName("ToPhone").HasMaxLength(3000).IsRequired(true);

            /// <summary>
            ///  发送时间
            /// </summary>
            builder.Property(x => x.SendDate).HasColumnName("SendDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            ///  内容
            /// </summary>
            builder.Property(x => x.Content).HasColumnName("Content").HasMaxLength(200).IsRequired(true);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  轮数
            /// </summary>
            builder.Property(x => x.SendRound).HasColumnName("SendRound").HasDefaultValue(0);

            /// <summary>
            ///  回执内容
            /// </summary>
            builder.Property(x => x.SendResult).HasColumnName("SendResult").HasMaxLength(200).IsRequired(false);

            /// <summary>
            ///  过期时间
            /// </summary>
            builder.Property(x => x.ExpireTime).HasColumnName("ExpireTime").HasColumnType("datetime").IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);

        }
    }
}
