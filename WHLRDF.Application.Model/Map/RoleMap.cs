﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class RoleMap : ClassMap<RoleEntity>
    {
        public override void Configure(EntityTypeBuilder<RoleEntity>
            builder)
        {
            builder.ToTable<RoleEntity>("COMM_Role");
           

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.RoleId);

            builder.Property(x => x.RoleId).HasColumnName("RoleId").HasMaxLength(50);

            /// <summary>
            ///  角色标识
            /// </summary>
            builder.Property(x => x.RoleName).HasColumnName("RoleName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.RoleKey).HasColumnName("RoleKey").HasMaxLength(50).IsRequired(true);


            /// <summary>
            ///  是否系统
            /// </summary>
            builder.Property(x => x.IsSystem).HasColumnName("IsSystem").HasDefaultValue(0);

            /// <summary>
            ///  上级角色
            /// </summary>
            builder.Property(x => x.ParentId).HasColumnName("ParentId").HasMaxLength(50);

            /// <summary>
            ///  备注
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(200).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}