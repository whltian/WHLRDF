﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 消息管理EF 字段映射
    /// </summary>
    public partial class NotifyMap : ClassMap<NotifyEntity>
    {
        public override void Configure(EntityTypeBuilder<NotifyEntity>
        builder)
        {
            builder.ToTable<NotifyEntity>("COMM_Notify");
           
            builder.HasKey(x => x.NotifyId);
            builder.Property(x => x.NotifyId).HasColumnName("NotifyId").HasMaxLength(50);

            /// <summary>
            ///  表单id
            /// </summary>
            builder.Property(x => x.FormId).HasColumnName("FormId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.FormType).HasColumnName("FormType");

            /// <summary>
            ///  接收人
            /// </summary>
            builder.Property(x => x.ReceiveUserId).HasColumnName("ReceiveUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  发送人
            /// </summary>
            builder.Property(x => x.SenderUserId).HasColumnName("SenderUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  标题
            /// </summary>
            builder.Property(x => x.Title).HasColumnName("Title").HasMaxLength(3000).IsRequired(true);

            /// <summary>
            ///  发送时间
            /// </summary>
            builder.Property(x => x.SendDate).HasColumnName("SendDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            ///  内容
            /// </summary>
            builder.Property(x => x.Content).HasColumnName("Content").IsRequired(true);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  已读时间
            /// </summary>
            builder.Property(x => x.ExpireTime).HasColumnName("ExpireTime").IsRequired(false);

            /// <summary>
            ///  目标url
            /// </summary>
            builder.Property(x => x.TargetUrl).HasColumnName("TargetUrl").HasMaxLength(500).IsRequired(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x=>x.SenderName) ;
        }
    }
}