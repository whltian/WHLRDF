﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM.EF;
namespace WHLRDF.Application.Model
{
    public partial class ActionsMap : ClassMap<ActionsEntity>
    {
        public override void Configure(EntityTypeBuilder<ActionsEntity>
            builder)
        {
            builder.ToTable<ActionsEntity>("COMM_Actions");
           

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.ActionId);

            builder.Property(x => x.ActionId).HasColumnName("ActionId").HasMaxLength(36);

            /// <summary>
            /// ActionName
            /// </summary>
            builder.Property(x => x.ActionName).HasColumnName("ActionName").HasMaxLength(30).IsRequired(true);

            /// <summary>
            ///  按钮名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  描述
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(100).IsRequired(false);

            /// <summary>
            ///  是否系统
            /// </summary>
            builder.Property(x => x.IsSystem).HasColumnName("IsSystem").HasDefaultValue(1);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}