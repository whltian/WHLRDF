﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// COMM_SysTaskEF字段映射
    /// </summary>
    public partial class SysTaskMap : ClassMap<SysTaskEntity>
    {
        public override void Configure(EntityTypeBuilder<SysTaskEntity>
        builder)
        {
            builder.ToTable<SysTaskEntity>("COMM_SysTask");

            builder.HasKey(x => x.TaskId);
            builder.Property(x => x.TaskId).HasColumnName("TaskId").HasMaxLength(50);

            /// <summary>
            /// 任务名称
            /// </summary>
            builder.Property(x => x.TaskName).HasColumnName("TaskName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 执行时间
            /// </summary>
            builder.Property(x => x.ExcuteTime).HasColumnName("ExcuteTime").HasColumnType("decimal(18,2)").HasDefaultValue(0).IsRequired(true);

            /// <summary>
            /// 开始时间
            /// </summary>
            builder.Property(x => x.StartDate).HasColumnName("StartDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 结束时间
            /// </summary>
            builder.Property(x => x.EndDate).HasColumnName("EndDate").IsRequired(false);

            /// <summary>
            /// 是否开启
            /// </summary>
            builder.Property(x => x.IsOpen).HasColumnName("IsOpen").HasDefaultValue(false);

            /// <summary>
            /// 是否成功
            /// </summary>
            builder.Property(x => x.IsSuccess).HasColumnName("IsSuccess").HasDefaultValue(false);

            /// <summary>
            /// 结果
            /// </summary>
            builder.Property(x => x.Message).HasColumnName("Message").IsRequired(false);

            /// <summary>
            /// 所属用户
            /// </summary>
            builder.Property(x => x.UserNo).HasColumnName("UserNo").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 会话
            /// </summary>
            builder.Property(x => x.UserToken).HasColumnName("UserToken").HasMaxLength(50).IsRequired(false);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}