﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class EmailMap : ClassMap<EmailEntity>
    {
        public override void Configure(EntityTypeBuilder<EmailEntity>
            builder)
        {
            builder.ToTable<EmailEntity>("COMM_Email");
            

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.EmailId);

            builder.Property(x => x.EmailId).HasColumnName("EmailId").HasMaxLength(50);

            /// <summary>
            ///  表单id
            /// </summary>
            builder.Property(x => x.FormId).HasColumnName("FormId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.FormType).HasColumnName("FormType");

            /// <summary>
            ///  用户
            /// </summary>
            builder.Property(x => x.UserId).HasColumnName("UserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  发送人
            /// </summary>
            builder.Property(x => x.SenderUser).HasColumnName("SenderUser").HasMaxLength(200).IsRequired(true);

            /// <summary>
            ///  发送时间
            /// </summary>
            builder.Property(x => x.SendDate).HasColumnName("SendDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            ///  接收人
            /// </summary>
            builder.Property(x => x.ToUser).HasColumnName("ToUser").HasMaxLength(3000).IsRequired(true);

            /// <summary>
            ///  抄送人
            /// </summary>
            builder.Property(x => x.CCUser).HasColumnName("CCUser").HasMaxLength(3000).IsRequired(false);

            /// <summary>
            ///  标题
            /// </summary>
            builder.Property(x => x.Title).HasColumnName("Title").HasMaxLength(3000).IsRequired(true);

            /// <summary>
            ///  内容
            /// </summary>
            builder.Property(x => x.Content).HasColumnName("Content").IsRequired(true);

            /// <summary>
            ///  状态
            /// </summary>
            builder.Property(x => x.Status).HasColumnName("Status").HasDefaultValue(0);

            /// <summary>
            ///  发送次数
            /// </summary>
            builder.Property(x => x.SendRound).HasColumnName("SendRound").HasDefaultValue(0);

            /// <summary>
            /// 执行时间
            /// </summary>
            builder.Property(x => x.ExpireTime).HasColumnName("ExpireTime").HasColumnType("datetime").IsRequired(false);

            /// <summary>
            ///  反馈结果
            /// </summary>
            builder.Property(x => x.SendResult).HasColumnName("SendResult").HasMaxLength(500).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}
