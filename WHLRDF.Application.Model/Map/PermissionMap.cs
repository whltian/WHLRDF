﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class PermissionMap : ClassMap<PermissionEntity>
    {
        public override void Configure(EntityTypeBuilder<PermissionEntity>
            builder)
        {
            builder.ToTable<PermissionEntity>("COMM_Permission");
            
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.PermissionId);

            builder.Property(x => x.PermissionId).HasColumnName("PermissionId").HasMaxLength(36);

            /// <summary>
            ///  源Id
            /// </summary>
            builder.Property(x => x.ResourceId).HasColumnName("ResourceId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.ResourceType).HasColumnName("ResourceType").HasDefaultValue(0);

            /// <summary>
            ///  是否启用
            /// </summary>
            builder.Property(x => x.IsDisabled).HasColumnName("IsDisabled").HasDefaultValue(0);

            /// <summary>
            ///  页面或按钮Id
            /// </summary>
            builder.Property(x => x.PageId).HasColumnName("PageId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  路径
            /// </summary>
            builder.Property(x => x.RouteUrl).HasColumnName("RouteUrl").HasMaxLength(200).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}