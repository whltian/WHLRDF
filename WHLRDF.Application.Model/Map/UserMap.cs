﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class UserMap : ClassMap<UserEntity>
    {
        public override void Configure(EntityTypeBuilder<UserEntity>
            builder)
        {
            builder.ToTable<UserEntity>("ORG_User");

            /// <summary>
            ///  用户编号
            /// </summary>
            builder.HasKey(x => x.UserId);

            builder.Property(x => x.UserId).HasColumnName("UserId").HasDefaultValue("").HasMaxLength(50);

            /// <summary>
            ///  用户名称
            /// </summary>
            builder.Property(x => x.UserName).HasColumnName("UserName").HasMaxLength(16).IsRequired(true);

            /// <summary>
            ///  电话
            /// </summary>
            builder.Property(x => x.Phone).HasColumnName("Phone").HasMaxLength(20).IsRequired(false);

            /// <summary>
            ///  邮箱
            /// </summary>
            builder.Property(x => x.Email).HasColumnName("Email").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  用户类型
            /// </summary>
            builder.Property(x => x.UserType).HasColumnName("UserType");

            /// <summary>
            ///  真实姓名
            /// </summary>
            builder.Property(x => x.RealName).HasColumnName("RealName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  密码
            /// </summary>
            builder.Property(x => x.Password).HasColumnName("Password").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  性别
            /// </summary>
            builder.Property(x => x.Sex).HasColumnName("Sex").HasDefaultValue(0);

            /// <summary>
            ///  出生日期
            /// </summary>
            builder.Property(x => x.Birthday).HasColumnName("Birthday").HasColumnType("datetime").IsRequired(false);

            /// <summary>
            /// 地址
            /// </summary>
            builder.Property(x => x.Address).HasColumnName("Address").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  证件号码
            /// </summary>
            builder.Property(x => x.CardNo).HasColumnName("CardNo").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  审核状态
            /// </summary>
            builder.Property(x => x.ApproveState).HasColumnName("ApproveState").HasMaxLength(10).IsRequired(true).HasDefaultValue(1);

            /// <summary>
            ///  家庭电话
            /// </summary>
            builder.Property(x => x.TelPhone).HasColumnName("TelPhone").HasMaxLength(30).IsRequired(false);

            /// <summary>
            /// QQ
            /// </summary>
            builder.Property(x => x.QQ).HasColumnName("QQ").HasMaxLength(30).IsRequired(false);

            /// <summary>
            ///  微信
            /// </summary>
            builder.Property(x => x.WebChat).HasColumnName("WebChat").HasMaxLength(30).IsRequired(false);

            /// <summary>
            ///  区域
            /// </summary>
            builder.Property(x => x.RegionId).HasColumnName("RegionId").HasMaxLength(50).IsRequired(false);

            /// <summary>
            ///  学历
            /// </summary>
            builder.Property(x => x.Education).HasColumnName("Education").IsRequired(false);

            /// <summary>
            ///  是否锁定
            /// </summary>
            builder.Property(x => x.IsLocked).HasColumnName("IsLocked").HasDefaultValue(0);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x => x.Permission);
            builder.Ignore(x => x.UnPermission);
            builder.Ignore(x => x.ErrorMsg);
            builder.Ignore(x => x.IsError);
            builder.Ignore(x => x.MyPageEntities);
            builder.Ignore(x => x.Roles);
            builder.Ignore(x => x.RememberMe);
            builder.Ignore(x => x.IsOnline);
            builder.Ignore(x => x.Token);
        }
    }
}
