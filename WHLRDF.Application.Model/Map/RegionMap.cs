﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 区域EF 字段映射
    /// </summary>
    public partial class RegionMap : ClassMap<RegionEntity>
    {
        public override void Configure(EntityTypeBuilder<RegionEntity>
        builder)
        {
            builder.ToTable<RegionEntity>("COMM_Region");
          
            builder.HasKey(x => x.RegionId);
            builder.Property(x => x.RegionId).HasColumnName("RegionId").ValueGeneratedOnAdd();

            /// <summary>
            ///  编码
            /// </summary>
            builder.Property(x => x.RegionCode).HasColumnName("RegionCode").HasMaxLength(100).IsRequired(true);

            /// <summary>
            ///  名称
            /// </summary>
            builder.Property(x => x.RegionName).HasColumnName("RegionName").HasMaxLength(100).IsRequired(true);

          

            /// <summary>
            ///  上级Code
            /// </summary>
            builder.Property(x => x.ParentCode).HasColumnName("ParentCode");

            /// <summary>
            ///  级别
            /// </summary>
            builder.Property(x => x.RegionLevel).HasColumnName("RegionLevel");

            /// <summary>
            ///  排序
            /// </summary>
            builder.Property(x => x.RegionOrder).HasColumnName("RegionOrder");

            /// <summary>
            ///  英文名称
            /// </summary>
            builder.Property(x => x.RegionNameEn).HasColumnName("RegionNameEn").HasMaxLength(100).IsRequired(true);

            /// <summary>
            /// 简称
            /// </summary>
            builder.Property(x => x.RegionShortNameEn).HasColumnName("RegionShortNameEn").HasMaxLength(10).IsRequired(true);

            /// <summary>
            ///  深度
            /// </summary>
            builder.Property(x => x.RegionPath).HasColumnName("RegionPath").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建人
            /// </summary>
            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 创建时间
            /// </summary>
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 修改人
            /// </summary>
            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            /// 修改时间
            /// </summary>
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            /// <summary>
            /// 是否删除
            /// </summary>
            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
            builder.Ignore(x=>x.ChildEntities);
        }
    }
}