﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class RelationUserMap : ClassMap<RelationUserEntity>
    {
        public override void Configure(EntityTypeBuilder<RelationUserEntity>
            builder)
        {
            builder.ToTable<RelationUserEntity>("COMM_RelationUser");
           

            /// <summary>
            ///  关系id
            /// </summary>
            builder.HasKey(x => x.RelationUserId);

            builder.Property(x => x.RelationUserId).HasColumnName("RelationUserId").HasMaxLength(36);

            /// <summary>
            ///  类别
            /// </summary>
            builder.Property(x => x.RelationType).HasColumnName("RelationType").HasDefaultValue(0);

            /// <summary>
            ///  用户id
            /// </summary>
            builder.Property(x => x.UserId).HasColumnName("UserId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  源id
            /// </summary>
            builder.Property(x => x.ResourceId).HasColumnName("ResourceId").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  
            /// </summary>
            builder.Property(x => x.IsPrimary).HasColumnName("IsPrimary").HasDefaultValue(0);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}
