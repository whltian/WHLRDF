﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class SerialRuleMap : ClassMap<SerialRuleEntity>
    {
        public override void Configure(EntityTypeBuilder<SerialRuleEntity>
            builder)
        {
            builder.ToTable<SerialRuleEntity>("COMM_SerialRule");
            

            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.SerialRuleId);

            builder.Property(x => x.SerialRuleId).HasColumnName("SerialRuleId").HasMaxLength(20);

            /// <summary>
            ///  前缀
            /// </summary>
            builder.Property(x => x.SerialPrefix).HasColumnName("SerialPrefix").HasMaxLength(100).IsRequired(false);

            /// <summary>
            ///  后缀
            /// </summary>
            builder.Property(x => x.SerialSuffix).HasColumnName("SerialSuffix").HasMaxLength(100).IsRequired(false);

            /// <summary>
            ///  长度
            /// </summary>
            builder.Property(x => x.SerialLength).HasColumnName("SerialLength").HasDefaultValue(4);

            /// <summary>
            ///  启用年
            /// </summary>
            builder.Property(x => x.IsYear).HasColumnName("IsYear").HasDefaultValue(0);

            /// <summary>
            ///  启用月
            /// </summary>
            builder.Property(x => x.IsMonth).HasColumnName("IsMonth").HasDefaultValue(0);

            /// <summary>
            ///  启用天
            /// </summary>
            builder.Property(x => x.IsDay).HasColumnName("IsDay").HasDefaultValue(0);

            /// <summary>
            ///  启用时间
            /// </summary>
            builder.Property(x => x.IsTime).HasColumnName("IsTime").HasDefaultValue(0);

            /// <summary>
            ///  计数器
            /// </summary>
            builder.Property(x => x.SerialCount).HasColumnName("SerialCount").HasDefaultValue(0);

            /// <summary>
            ///  备注
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(300).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}