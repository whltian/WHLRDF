﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WHLRDF.ORM.EF;
namespace WHLRDF.Application.Model
{
    public partial class PageActionMap : ClassMap<PageActionEntity>
    {
        public override void Configure(EntityTypeBuilder<PageActionEntity>
            builder)
        {
            builder.ToTable<PageActionEntity>("COMM_PageAction");
          
            /// <summary>
            ///  主键
            /// </summary>
            builder.HasKey(x => x.ActionCode);

            builder.Property(x => x.ActionCode).HasColumnName("ActionCode").HasMaxLength(50);

            /// <summary>
            ///  页面Id
            /// </summary>
            builder.Property(x => x.PageId).HasColumnName("PageId").HasMaxLength(50).IsRequired();

            /// <summary>
            /// 是否支持Get请求
            /// </summary>
            builder.Property(x => x.IsGet).HasColumnName("IsGet").HasDefaultValue(1);

            /// <summary>
            /// 是否支持POST请求
            /// </summary>
            builder.Property(x => x.IsPost).HasColumnName("IsPost").HasDefaultValue(1);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.AreaName).HasColumnName("AreaName").HasMaxLength(50);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.ControllerName).HasColumnName("ControllerName").HasMaxLength(50);

            /// <summary>
            /// 
            /// </summary>
            builder.Property(x => x.IsParent).HasColumnName("IsParent").HasDefaultValue(0);

            builder.Property(x => x.NoAccess).HasColumnName("NoAccess").HasDefaultValue(0);

            /// <summary>
            /// ActionName
            /// </summary>
            builder.Property(x => x.ActionName).HasColumnName("ActionName").HasMaxLength(50).IsRequired(true).IsRequired();

            /// <summary>
            ///  按钮名称
            /// </summary>
            builder.Property(x => x.LocalName).HasColumnName("LocalName").HasMaxLength(50).IsRequired(true);

            /// <summary>
            ///  备注
            /// </summary>
            builder.Property(x => x.Remark).HasColumnName("Remark").HasMaxLength(100).IsRequired(false);

            builder.Property(x => x.CreateBy).HasColumnName("CreateBy").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate").HasColumnType("datetime").IsRequired(true);



            builder.Property(x => x.LastModifyUserId).HasColumnName("LastModifyUserId").HasMaxLength(50).IsRequired(true);
            builder.Property(x => x.LastModifyDate).HasColumnName("LastModifyDate").HasColumnType("datetime").IsRequired(true);

            builder.Property(x => x.IsDeleted).HasColumnName("IsDeleted").HasDefaultValue(false);
            base.Configure(builder);
        }
    }
}