﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Application.Model
{
    public class UserDto
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string RealName { get; set; }

        public string Sex { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string token { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar {
            get {
                return ApplicationEnvironments.DefaultSession.GetUserAvatarUrl();
            }
        }
    }
}
