﻿using System;
using System.Collections.Generic;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    public partial class RegionDto 
    {
       
        #region 属性
      
        public virtual string RegionCode
        {
            get; set;
        }
        
        public virtual string RegionName
        {
            get; set;
        }
        
        public virtual int RegionOrder
        {
            get; set;
        }
 
        public virtual string RegionPath
        {
            get; set;
        }
        public virtual string ParentCode
        {
            get; set;
        }
        public string id { get => this.RegionCode.ToString(); }

        public string text { get => this.RegionName; }
        public List<RegionDto> ChildEntities { get; set; }
        #endregion
    }
}
