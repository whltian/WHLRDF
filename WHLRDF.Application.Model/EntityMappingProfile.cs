﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Application.Model
{
    public class EntityMappingProfile : MappingProfileBase
    {
        public EntityMappingProfile()
        {
            CreateMap<UserEntity, UserDto>();
            CreateMap<RegionEntity, RegionDto>();
            CreateMap<TaskModel, SysTaskEntity>();
        }
    }
}
