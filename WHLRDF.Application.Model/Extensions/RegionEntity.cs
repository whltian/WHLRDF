﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WHLRDF.Application.Model
{
    public partial class RegionEntity
    {
        public string id { get => this.RegionId.ToString(); }

        public string text { get => this.RegionName; }
        public List<RegionEntity> ChildEntities { get; set; }
    }
}
