﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace WHLRDF.Application.Model
{
    public partial class UserEntity
    {

       //public virtual IList<GroupEntity> Groups { get; set; }

        public IList<RoleEntity> Roles { get; set; }
       
        /// <summary>
        /// 启用权限
        /// </summary>
        public List<PermissionEntity> Permission { get; set; }

        /// <summary>
        /// 禁用权限
        /// </summary>
        public List<PermissionEntity> UnPermission { get; set; }

        public List<PageEntity> MyPageEntities { get; set; }

        public List<RelationUserEntity> RelationUserEntities { get; set; }


        /// <summary>
        /// 登录返回是否有错
        /// </summary>
        public bool IsError { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public  string ErrorMsg { get; set; }

        //public virtual List<UserPermissionEntity> UserMenu { get; set; }

        /// <summary>
        /// 学历类别编号
        /// </summary>
        public const string EducationDicNo = "0002";

        /// <summary>
        /// 证件类型编号
        /// </summary>
        public const string CardTypeDicNo = "0003";

        public bool RememberMe{get;set;}

        public bool IsOnline { get; set; }

        public string Token{get;set;}


    }
}