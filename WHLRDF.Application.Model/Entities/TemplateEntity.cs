
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 模板管理
    /// </summary>
    [Table("COMM_Template", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class TemplateEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public TemplateEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Template";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "TemplateId";

        /// <summary>
        ///  类别
        /// </summary>
        public const string __TemplateType = "TemplateType";

        /// <summary>
        ///  标题
        /// </summary>
        public const string __Title = "Title";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        /// <summary>
        ///  内容
        /// </summary>
        public const string __Content = "Content";

        #endregion
        #region 属性

        private string _TemplateId;
        ///<summary>
        ///  编号
        /// </summary>
        [DbColumn("TemplateId", IsPrimaryKey = true, LocalName = " 编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string TemplateId
        {
            get
            {
                return _TemplateId;
            }
            set
            {
                _TemplateId = value;
            }
        }


        private int _TemplateType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("TemplateType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 3, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int TemplateType
        {
            get
            {
                return _TemplateType;
            }
            set
            {
                SetData(__TemplateType, value);
                _TemplateType = value;
            }
        }


        private string _Title;
        ///<summary>
        ///  标题
        /// </summary>
        [DbColumn("Title", LocalName = " 标题", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetData(__Title, value);
                _Title = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark, value);
                _Remark = value;
            }
        }


        private string _Content;
        ///<summary>
        ///  内容
        /// </summary>
        [DbColumn("Content", LocalName = " 内容", AllowDBNull = false, ColumnType = "ntext", SqlType = "ntext", ControlType = 5, MaxLength = 1073741823, Min = 0)]
        public virtual string Content
        {
            get
            {
                return _Content;
            }
            set
            {
                SetData(__Content, value);
                _Content = value;
            }
        }


        #endregion
    }
}