﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 页面权限
    /// </summary>
    [Table("COMM_PageAction", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class PageActionEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public PageActionEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_PageAction";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ActionCode";


        /// <summary>
        /// ActionCode 编码
        /// </summary>
        public const string __ActionCode = "ActionCode";

        /// <summary>
        ///  页面Id
        /// </summary>
        public const string __PageId = "PageId";


        /// <summary>
        /// 页面按钮Id
        /// </summary>
        public const string __IsGet = "IsGet";

        /// <summary>
        /// 页面按钮Id
        /// </summary>
        public const string __IsPost = "IsPost";


        /// <summary>
        /// 域名称
        /// </summary>
        public const string __AreaName = "AreaName";

        /// <summary>
        /// 控制器名称
        /// </summary>
        public const string __ControllerName = "ControllerName";

        /// <summary>
        /// 是否继承controller  Index访问权限
        /// </summary>
        public const string __IsParent = "IsParent";

        /// <summary>
        /// 不允许访问
        /// </summary>
        public const string __NoAccess = "NoAccess";


        /// <summary>
        /// ActionName
        /// </summary>
        public const string __ActionName = "ActionName";


        /// <summary>
        ///  按钮名称
        /// </summary>
        public const string __LocalName = "LocalName";




        /// <summary>
        ///  备注
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("ActionCode", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 36, Min = 0)]
        public virtual string ActionCode { get; set; }


        private string _PageId;
        ///<summary>
        ///  页面Id
        /// </summary>
        [DbColumn("PageId", LocalName = " 页面Id", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 6, MaxLength = 50, Min = 0)]
        public virtual string PageId
        {
            get
            {
                return _PageId;
            }
            set
            {

                SetData(__PageId, value);
                _PageId = value;
            }
        }

        private bool _IsGet;
        ///<summary>
        ///  是否支持Get请求
        /// </summary>
        [DbColumn("IsGet", LocalName = "支持Get请求", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsGet
        {
            get
            {
                return _IsGet;
            }
            set
            {
                SetData(__IsGet, value);
                _IsGet = value;
            }
        }

        private bool _IsPost;
        ///<summary>
        ///  是否支持POST请求
        /// </summary>
        [DbColumn("IsPost", LocalName = "支持POST请求", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsPost
        {
            get
            {
                return _IsPost;
            }
            set
            {
                SetData(__IsPost, value);
                _IsPost = value;
            }
        }

        private string _AreaName;
        ///<summary>
        ///  AreaName域名
        /// </summary>
        [DbColumn("AreaName", LocalName = "域名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 6, MaxLength = 50, Min = 0)]
        public virtual string AreaName
        {
            get
            {
                return _AreaName;
            }
            set
            {
                SetData(__AreaName, value);
                _AreaName = value;
            }
        }
        private string _ControllerName;
        ///<summary>
        ///  AreaName域名
        /// </summary>
        [DbColumn("ControllerName", LocalName = "控制器名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 6, MaxLength = 50, Min = 0)]
        public virtual string ControllerName
        {
            get
            {
                return _ControllerName;
            }
            set
            {
                SetData(__ControllerName, value);
                _ControllerName = value;
            }
        }
        private bool _IsParent;
        ///<summary>
        ///  是否继承上级
        /// </summary>
        [DbColumn("IsParent", LocalName = "是否继承上级", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsParent
        {
            get
            {
                return _IsParent;
            }
            set
            {
                SetData(__IsParent, value);
                _IsParent = value;
            }
        }

        private bool _NoAccess;
        ///<summary>
        ///  是否继承上级
        /// </summary>
        [DbColumn("NoAccess", LocalName = "是否继承上级", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool NoAccess
        {
            get
            {
                return _NoAccess;
            }
            set
            {
                SetData(__NoAccess, value);
                _NoAccess = value;
            }
        }
        private string _ActionName;
        ///<summary>
        /// ActionName
        /// </summary>
        [DbColumn("ActionName", LocalName = "ActionName", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActionName
        {
            get
            {
                return _ActionName;
            }
            set
            {

                SetData(__ActionName, value);
                _ActionName = value;
            }
        }




        private string _LocalName;
        ///<summary>
        ///  按钮名称
        /// </summary>
        [DbColumn("LocalName", LocalName = " 按钮名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {

                SetData(__LocalName, value);

                _LocalName = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  备注
        /// </summary>
        [DbColumn("Remark", LocalName = " 备注", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark, value);

                _Remark = value;
            }
        }


        #endregion
    }
}
