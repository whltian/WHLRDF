﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 权限表
    /// </summary>
    [Table("COMM_Permission", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class PermissionEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public PermissionEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Permission";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "PermissionId";


        /// <summary>
        ///  源Id
        /// </summary>
        public const string __ResourceId = "ResourceId";


        /// <summary>
        ///  类别
        /// </summary>
        public const string __ResourceType = "ResourceType";


        /// <summary>
        ///  是否启用
        /// </summary>
        public const string __IsDisabled = "IsDisabled";


        /// <summary>
        ///  页面或按钮Id
        /// </summary>
        public const string __PageId = "PageId";


        /// <summary>
        ///  路径
        /// </summary>
        public const string __RouteUrl = "RouteUrl";


        #endregion
        #region 属性

        private string _PermissionId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("PermissionId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 36, Min = 0)]
        public virtual string PermissionId
        {
            get
            {
                return _PermissionId;
            }
            set
            {
                _PermissionId = value;
            }
        }


        private string _ResourceId;
        ///<summary>
        ///  源Id
        /// </summary>
        [DbColumn("ResourceId", LocalName = " 源Id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ResourceId
        {
            get
            {
                return _ResourceId;
            }
            set
            {

                SetData(__ResourceId, value);

                _ResourceId = value;
            }
        }


        private int _ResourceType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("ResourceType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int ResourceType
        {
            get
            {
                return _ResourceType;
            }
            set
            {

                SetData(__ResourceType, value);

                _ResourceType = value;
            }
        }


        private bool _IsDisabled;
        ///<summary>
        ///  是否启用
        /// </summary>
        [DbColumn("IsDisabled", LocalName = " 是否启用", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsDisabled
        {
            get
            {
                return _IsDisabled;
            }
            set
            {

                SetData(__IsDisabled, value);

                _IsDisabled = value;
            }
        }


        private string _PageId;
        ///<summary>
        ///  页面或按钮Id
        /// </summary>
        [DbColumn("PageId", LocalName = " 页面或按钮Id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string PageId
        {
            get
            {
                return _PageId;
            }
            set
            {

                SetData(__PageId, value);

                _PageId = value;
            }
        }


        private string _RouteUrl;
        ///<summary>
        ///  路径
        /// </summary>
        [DbColumn("RouteUrl", LocalName = " 路径", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string RouteUrl
        {
            get
            {
                return _RouteUrl;
            }
            set
            {

                SetData(__RouteUrl, value);

                _RouteUrl = value;
            }
        }


        #endregion
    }
}
