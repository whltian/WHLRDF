﻿

using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
	/// <summary>
	/// COMM_EventConfigs
	/// </summary>
	[Table("COMM_EventConfigs", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
	public partial class EventConfigsEntity : EntityBase
	{
		#region 常量
		public const string _TableName = "COMM_EventConfigs";

		/// <summary>
		/// 主键字段名
		/// </summary>
		public const string _PrimaryKeyName = "EventCode";

		/// <summary>
		/// 事件名称
		/// </summary>
		public const string __EventName = "EventName";

		/// <summary>
		/// 请求地址
		/// </summary>
		public const string __Url = "Url";

		/// <summary>
		/// 密钥
		/// </summary>
		public const string __MD5 = "MD5";
		#endregion
		#region 属性
		private string _EventCode;
		/// <summary>
		/// 主键
		/// </summary>
		[DbColumn("EventCode", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
		public virtual string EventCode
		{
			get
			{
				return _EventCode;
			}
			set
			{
				_EventCode = value;

			}
		}
		private string _EventName;
		/// <summary>
		/// 事件名称
		/// </summary>
		[DbColumn("EventName", LocalName = "事件名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
		public virtual string EventName
		{
			get
			{
				return _EventName;
			}
			set
			{
				SetData(__EventName, value);
				_EventName = value;

			}
		}
		private string _Url;
		/// <summary>
		/// 请求地址
		/// </summary>
		[DbColumn("Url", LocalName = "请求地址", ColumnType = "string", AllowDBNull = false, MaxLength = 500, ControlType = 1)]
		public virtual string Url
		{
			get
			{
				return _Url;
			}
			set
			{
				SetData(__Url, value);
				_Url = value;

			}
		}
		private string _MD5;
		/// <summary>
		/// 密钥
		/// </summary>
		[DbColumn("MD5", LocalName = "密钥", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
		public virtual string MD5
		{
			get
			{
				return _MD5;
			}
			set
			{
				SetData(__MD5, value);
				_MD5 = value;

			}
		}
		#endregion
	}
}