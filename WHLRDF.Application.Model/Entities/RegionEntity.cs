﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 区域
    /// </summary>
    [Table("COMM_Region", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class RegionEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "COMM_Region";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "RegionId";

        /// <summary>
        ///  编码
        /// </summary>
        public const string __RegionCode = "RegionCode";

        /// <summary>
        ///  名称
        /// </summary>
        public const string __RegionName = "RegionName";

       
        /// <summary>
        ///  级别
        /// </summary>
        public const string __RegionLevel = "RegionLevel";

        /// <summary>
        ///  排序
        /// </summary>
        public const string __RegionOrder = "RegionOrder";

        /// <summary>
        ///  英文名称
        /// </summary>
        public const string __RegionNameEn = "RegionNameEn";

        /// <summary>
        /// 简称
        /// </summary>
        public const string __RegionShortNameEn = "RegionShortNameEn";

        /// <summary>
        ///  深度
        /// </summary>
        public const string __RegionPath = "RegionPath";

        public const string __ParentCode = "ParentCode";

        #endregion
        #region 属性
        private int _RegionId;
        /// <summary>
        ///  区域id
        /// </summary>
        [DbColumn("RegionId", LocalName = "区域id", ColumnType = "int", Identifier = true, IsPrimaryKey = true, AllowDBNull = false, VaildKey = "IsInt", ControlType = 2)]
        public virtual int RegionId
        {
            get
            {
                return _RegionId;
            }
            set
            {
                _RegionId = value;

            }
        }
        private string _RegionCode;
        /// <summary>
        ///  编码
        /// </summary>
        [DbColumn("RegionCode", LocalName = "编码", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string RegionCode
        {
            get
            {
                return _RegionCode;
            }
            set
            {
                SetData(__RegionCode, value);
                _RegionCode = value;

            }
        }
        private string _RegionName;
        /// <summary>
        ///  名称
        /// </summary>
        [DbColumn("RegionName", LocalName = "名称", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string RegionName
        {
            get
            {
                return _RegionName;
            }
            set
            {
                SetData(__RegionName, value);
                _RegionName = value;

            }
        }
       
        private int _RegionLevel;
        /// <summary>
        ///  级别
        /// </summary>
        [DbColumn("RegionLevel", LocalName = "级别", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int RegionLevel
        {
            get
            {
                return _RegionLevel;
            }
            set
            {
                SetData(__RegionLevel, value);
                _RegionLevel = value;

            }
        }
        private int _RegionOrder;
        /// <summary>
        ///  排序
        /// </summary>
        [DbColumn("RegionOrder", LocalName = "排序", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int RegionOrder
        {
            get
            {
                return _RegionOrder;
            }
            set
            {
                SetData(__RegionOrder, value);
                _RegionOrder = value;

            }
        }
        private string _RegionNameEn;
        /// <summary>
        ///  英文名称
        /// </summary>
        [DbColumn("RegionNameEn", LocalName = "英文名称", ColumnType = "string", AllowDBNull = false, MaxLength = 100, ControlType = 1)]
        public virtual string RegionNameEn
        {
            get
            {
                return _RegionNameEn;
            }
            set
            {
                SetData(__RegionNameEn, value);
                _RegionNameEn = value;

            }
        }
        private string _RegionShortNameEn;
        /// <summary>
        /// 简称
        /// </summary>
        [DbColumn("RegionShortNameEn", LocalName = "简称", ColumnType = "string", AllowDBNull = false, MaxLength = 10, ControlType = 1)]
        public virtual string RegionShortNameEn
        {
            get
            {
                return _RegionShortNameEn;
            }
            set
            {
                SetData(__RegionShortNameEn, value);
                _RegionShortNameEn = value;

            }
        }
        private string _RegionPath;
        /// <summary>
        ///  深度
        /// </summary>
        [DbColumn("RegionPath", LocalName = "深度", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string RegionPath
        {
            get
            {
                return _RegionPath;
            }
            set
            {
                SetData(__RegionPath, value);
                _RegionPath = value;

            }
        }

        private string _ParentCode;
        /// <summary>
        ///  上级Code
        /// </summary>
        [DbColumn("ParentCode", LocalName = "上级Code", ColumnType = "string", AllowDBNull = true)]
        public virtual string ParentCode
        {
            get
            {
                return _ParentCode;
            }
            set
            {
                SetData(__ParentCode, value);
                _ParentCode = value;

            }
        }
        #endregion
    }
}
