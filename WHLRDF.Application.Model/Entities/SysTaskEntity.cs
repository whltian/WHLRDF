﻿
using System;
using System.Data;
using WHLRDF;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
	/// <summary>
	/// COMM_SysTask
	/// </summary>
	[Table("COMM_SysTask", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
	public partial class SysTaskEntity : EntityBase
	{
		public SysTaskEntity()
		{

		}
		#region 常量
		public const string _TableName = "COMM_SysTask";

		/// <summary>
		/// 主键字段名
		/// </summary>
		public const string _PrimaryKeyName = "TaskId";

		/// <summary>
		/// 任务名称
		/// </summary>
		public const string __TaskName = "TaskName";

		/// <summary>
		/// 执行时间
		/// </summary>
		public const string __ExcuteTime = "ExcuteTime";

		/// <summary>
		/// 开始时间
		/// </summary>
		public const string __StartDate = "StartDate";

		/// <summary>
		/// 结束时间
		/// </summary>
		public const string __EndDate = "EndDate";

		/// <summary>
		/// 是否开启
		/// </summary>
		public const string __IsOpen = "IsOpen";

		/// <summary>
		/// 是否成功
		/// </summary>
		public const string __IsSuccess = "IsSuccess";

		/// <summary>
		/// 结果
		/// </summary>
		public const string __Message = "Message";

		/// <summary>
		/// 所属用户
		/// </summary>
		public const string __UserNo = "UserNo";

		/// <summary>
		/// 会话
		/// </summary>
		public const string __UserToken = "UserToken";
		#endregion
		#region 属性
		private string _TaskId;
		/// <summary>
		/// 任务Id
		/// </summary>
		[DbColumn("TaskId", LocalName = "任务Id", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
		public virtual string TaskId
		{
			get
			{
				return _TaskId;
			}
			set
			{
				_TaskId = value;

			}
		}
		private string _TaskName;
		/// <summary>
		/// 任务名称
		/// </summary>
		[DbColumn("TaskName", LocalName = "任务名称", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
		public virtual string TaskName
		{
			get
			{
				return _TaskName;
			}
			set
			{
				SetData(__TaskName, value);
				_TaskName = value;

			}
		}
		private decimal _ExcuteTime;
		/// <summary>
		/// 执行时间
		/// </summary>
		[DbColumn("ExcuteTime", LocalName = "执行时间", ColumnType = "decimal", AllowDBNull = false, VaildKey = "IsDecimal", ControlType = 6)]
		public virtual decimal ExcuteTime
		{
			get
			{
				return _ExcuteTime;
			}
			set
			{
				SetData(__ExcuteTime, value);
				_ExcuteTime = value;

			}
		}
		private DateTime _StartDate;
		/// <summary>
		/// 开始时间
		/// </summary>
		[DbColumn("StartDate", LocalName = "开始时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
		public virtual DateTime StartDate
		{
			get
			{
				return _StartDate;
			}
			set
			{
				SetData(__StartDate, value);
				_StartDate = value;

			}
		}
		private DateTime? _EndDate;
		/// <summary>
		/// 结束时间
		/// </summary>
		[DbColumn("EndDate", LocalName = "结束时间", ColumnType = "datetime", VaildKey = "IsDate", ControlType = 7)]
		public virtual DateTime? EndDate
		{
			get
			{
				return _EndDate;
			}
			set
			{
				SetData(__EndDate, value);
				_EndDate = value;

			}
		}
		private bool _IsOpen;
		/// <summary>
		/// 是否开启
		/// </summary>
		[DbColumn("IsOpen", LocalName = "是否开启", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
		public virtual bool IsOpen
		{
			get
			{
				return _IsOpen;
			}
			set
			{
				SetData(__IsOpen, value);
				_IsOpen = value;

			}
		}
		private bool _IsSuccess;
		/// <summary>
		/// 是否成功
		/// </summary>
		[DbColumn("IsSuccess", LocalName = "是否成功", ColumnType = "bool", AllowDBNull = false, ControlType = 4)]
		public virtual bool IsSuccess
		{
			get
			{
				return _IsSuccess;
			}
			set
			{
				SetData(__IsSuccess, value);
				_IsSuccess = value;

			}
		}
		private string _Message;
		/// <summary>
		/// 结果
		/// </summary>
		[DbColumn("Message", LocalName = "结果", ColumnType = "string", ControlType = 1)]
		public virtual string Message
		{
			get
			{
				return _Message;
			}
			set
			{
				SetData(__Message, value);
				_Message = value;

			}
		}
		private string _UserNo;
		/// <summary>
		/// 所属用户
		/// </summary>
		[DbColumn("UserNo", LocalName = "所属用户", ColumnType = "string", MaxLength = 50, ControlType = 1)]
		public virtual string UserNo
		{
			get
			{
				return _UserNo;
			}
			set
			{
				SetData(__UserNo, value);
				_UserNo = value;

			}
		}
		private string _UserToken;
		/// <summary>
		/// 会话
		/// </summary>
		[DbColumn("UserToken", LocalName = "会话", ColumnType = "string", MaxLength = 50, ControlType = 1)]
		public virtual string UserToken
		{
			get
			{
				return _UserToken;
			}
			set
			{
				SetData(__UserToken, value);
				_UserToken = value;

			}
		}
		#endregion
	}
}