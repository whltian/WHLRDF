﻿
using System;
using Microsoft.AspNetCore.Mvc;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    [Table("ORG_User", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class UserEntity : EntityBase
    {
        #region 基类属性 
        /// <summary>
        /// 构造函数
        /// </summary>
        public UserEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "ORG_User";


        /// <summary>
        /// 用户编号
        /// </summary>
        public const string _PrimaryKeyName = "UserId";


        /// <summary>
        /// 用户类别 
        /// </summary>
        public const string __UserType = "UserType";




        /// <summary>
        /// 用户帐号 
        /// </summary>
        public const string __UserName = "UserName";


        /// <summary>
        /// 用户名称 
        /// </summary>
        public const string __RealName = "RealName";


        /// <summary>
        /// 密码 
        /// </summary>
        public const string __Password = "Password";




        /// <summary>
        /// 手机号码 
        /// </summary>
        public const string __Phone = "Phone";


        /// <summary>
        /// 邮箱 
        /// </summary>
        public const string __Email = "Email";


        /// <summary>
        /// 性别 
        /// </summary>
        public const string __Sex = "Sex";


        /// <summary>
        /// 生日日期 
        /// </summary>
        public const string __Birthday = "Birthday";


        /// <summary>
        /// 地址 
        /// </summary>
        public const string __Address = "Address";


        /// <summary>
        /// 证件号码 
        /// </summary>
        public const string __CardNo = "CardNo";


        /// <summary>
        /// 审核状态 
        /// </summary>
        public const string __ApproveState = "ApproveState";


        /// <summary>
        /// 是否锁定 
        /// </summary>
        public const string __IsLocked = "IsLocked";


        /// <summary>
        /// 家庭电话 
        /// </summary>
        public const string __TelPhone = "TelPhone";


        /// <summary>
        /// QQ 
        /// </summary>
        public const string __QQ = "QQ";


        /// <summary>
        /// 微信号 
        /// </summary>
        public const string __WebChat = "WebChat";


        /// <summary>
        /// 所在区域 
        /// </summary>
        public const string __RegionId = "RegionId";


        /// <summary>
        /// 学历 
        /// </summary>
        public const string __Education = "Education";


        #endregion
        #region 属性

        private string _UserId;
        ///<summary>
        /// 
        ///</summary>
        [DbColumn("UserId", Identifier = false, IsPrimaryKey = true, LocalName = "用户编号", AllowDBNull = true, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength =50, Min = 0)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
              // SetData(_PrimaryKeyName);
                _UserId = value;
            }
        }


        private int _UserType;
        ///<summary>
        /// 用户类别
        ///</summary>
        [DbColumn("UserType", LocalName = "用户类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 1, MaxLength = 5, Min = 0)]
        public virtual int UserType
        {
            get
            {
                return _UserType;
            }
            set
            {
                SetData(__UserType, value);
                _UserType = value;
            }
        }


        private string _UserName;
        ///<summary>
        /// 用户帐号
        ///</summary>
        [DbColumn("UserName", LocalName = "用户名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 16, Min = 0, RegValidate = "^[a-zA-Z][a-zA-Z0-9_-]{4,15}$", VaildKey = "Vaild_IsValidUser")]
        public virtual string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                SetData(__UserName, value);
                _UserName = value;
            }
        }


        private string _RealName;
        ///<summary>
        /// 真实姓名
        ///</summary>
        [DbColumn("RealName", LocalName = "本地名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string RealName
        {
            get
            {
                return _RealName;
            }
            set
            {
                SetData(__RealName, value);
                _RealName = value;
            }
        }

        private string _Password;
        ///<summary>
        /// 密码
        ///</summary>
        [DbColumn("Password", LocalName = "密码", ReourceKeyName = "lbl_Comm_User_PassWord", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                SetData(__Password, value);
                _Password = value;
            }
        }

        private string _Phone;
        ///<summary>
        /// 手机号码
        ///</summary>
        [DbColumn("Phone", LocalName = "手机号码", ReourceKeyName = "lbl_Comm_User_Phone", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 11, Min = 0)]
        public virtual string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                SetData(__Phone, value);
                _Phone = value;
            }
        }


        private string _Email;
        ///<summary>
        /// 邮箱
        ///</summary>
        [DbColumn("Email", LocalName = "邮箱", ReourceKeyName = "lbl_Comm_User_Email", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                SetData(__Email, value);
                _Email = value;
            }
        }


        private int _Sex;
        ///<summary>
        /// 性别
        ///</summary>
        [DbColumn("Sex", LocalName = "性别", ReourceKeyName = "lbl_Comm_User_Sex", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 1, MaxLength = 5, Min = 0)]
        public virtual int Sex
        {
            get
            {
                return _Sex;
            }
            set
            {
                SetData(__Sex, value);
                _Sex = value;
            }
        }


        private DateTime? _Birthday;
        ///<summary>
        /// 生日日期
        ///</summary>
        [DbColumn("Birthday", LocalName = "生日日期", ReourceKeyName = "lbl_Comm_User_Birthday", ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0,VaildKey ="IsTime")]
        public virtual DateTime? Birthday
        {
            get
            {
                return _Birthday;
            }
            set
            {
                SetData(__Birthday, value);
                _Birthday = value;
            }
        }


        private string _Address;
        ///<summary>
        /// 地址
        ///</summary>
        [DbColumn("Address", LocalName = "地址", ReourceKeyName = "lbl_Comm_User_Address", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                SetData(__Address, value);
                _Address = value;
            }
        }


        private string _CardNo;
        ///<summary>
        /// 证件号码
        ///</summary>
        [DbColumn("CardNo", LocalName = "证件号码", ReourceKeyName = "lbl_Comm_User_CardNo", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string CardNo
        {
            get
            {
                return _CardNo;
            }
            set
            {
                SetData(__CardNo, value);
                _CardNo = value;
            }
        }


        private int _ApproveState;
        ///<summary>
        /// 审核状态
        ///</summary>
        [DbColumn("ApproveState", LocalName = "审核状态", ReourceKeyName = "lbl_ORG_User_ApproveState", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 4, Min = 0, VaildKey = "IsInt")]
        public virtual int ApproveState
        {
            get
            {
                return _ApproveState;
            }
            set
            {
                SetData(__ApproveState, value);
                _ApproveState = value;
            }
        }


        private bool _IsLocked;
        ///<summary>
        /// 是否锁定
        ///</summary>
        [DbColumn("IsLocked", LocalName = "是否锁定", ReourceKeyName = "lbl_ORG_User_IsLocked", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsLocked
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                SetData(__IsLocked, value);
                _IsLocked = value;
            }
        }


        private string _TelPhone;
        ///<summary>
        /// 家庭电话
        ///</summary>
        [DbColumn("TelPhone", LocalName = "家庭电话", ReourceKeyName = "lbl_ORG_User_TelPhone", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 60, Min = 0)]
        public virtual string TelPhone
        {
            get
            {
                return _TelPhone;
            }
            set
            {
                SetData(__TelPhone, value);
                _TelPhone = value;
            }
        }


        private string _QQ;
        ///<summary>
        /// QQ
        ///</summary>
        [DbColumn("QQ", LocalName = "QQ", ReourceKeyName = "lbl_ORG_User_QQ", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 60, Min = 0)]
        public virtual string QQ
        {
            get
            {
                return _QQ;
            }
            set
            {
                SetData(__QQ, value);
                _QQ = value;
            }
        }


        private string _WebChat;
        ///<summary>
        /// 微信号
        ///</summary>
        [DbColumn("WebChat", LocalName = "微信号", ReourceKeyName = "lbl_ORG_User_WebChat", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 60, Min = 0)]
        public virtual string WebChat
        {
            get
            {
                return _WebChat;
            }
            set
            {
                SetData(__WebChat, value);
                _WebChat = value;
            }
        }


        private string _RegionId;
        ///<summary>
        /// 所在区域
        ///</summary>
        [DbColumn("RegionId", LocalName = "所在区域", ReourceKeyName = "lbl_ORG_User_RegionId", ColumnType = "nvarchar", SqlType = "string", ControlType = 6)]
        public virtual string RegionId
        {
            get
            {
                return _RegionId;
            }
            set
            {
                SetData(__RegionId, value);
                _RegionId = value;
            }
        }


        private int? _Education;
        ///<summary>
        /// 学历
        ///</summary>
        [DbColumn("Education", LocalName = "学历", ReourceKeyName = "lbl_ORG_User_Education", ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 4, Min = 0, VaildKey = "IsInt")]
        public virtual int? Education
        {
            get
            {
                return _Education;
            }
            set
            {
                SetData(__Education, value);
                _Education = value;
            }
        }


        #endregion
    }
}