﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
 
    /// <summary>
    /// COMM_Role
    /// </summary>
    [Table("COMM_Role", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class RoleEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public RoleEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Role";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "RoleId";


        /// <summary>
        ///  角色标识
        /// </summary>
        public const string __RoleKey = "RoleKey";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __RoleName = "RoleName";


        /// <summary>
        ///  是否系统
        /// </summary>
        public const string __IsSystem = "IsSystem";


        /// <summary>
        ///  上级角色
        /// </summary>
        public const string __ParentId = "ParentId";


        /// <summary>
        ///  备注
        /// </summary>
        public const string __Remark = "Remark";


        #endregion
        #region 属性

        private string _RoleId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("RoleId",  IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual string RoleId
        {
            get
            {
                return _RoleId;
            }
            set
            {
                _RoleId = value;
            }
        }


        private string _RoleKey;
        ///<summary>
        ///  角色标识
        /// </summary>
        [DbColumn("RoleKey", LocalName = " 角色标识", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string RoleKey
        {
            get
            {
                return _RoleKey;
            }
            set
            {

                SetData(__RoleKey, value);

                _RoleKey = value;
            }
        }


        private string _RoleName;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("RoleName", LocalName = " 角色名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string RoleName
        {
            get
            {
                return _RoleName;
            }
            set
            {

                SetData(__RoleName, value);

                _RoleName = value;
            }
        }


        private bool _IsSystem;
        ///<summary>
        ///  是否系统
        /// </summary>
        [DbColumn("IsSystem", LocalName = " 是否系统", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsSystem
        {
            get
            {
                return _IsSystem;
            }
            set
            {

                SetData(__IsSystem, value);

                _IsSystem = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  上级角色
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级角色", ColumnType = "string", SqlType = "varchar", ControlType = 6, MaxLength = 10, Min = 0)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId, value);

                _ParentId = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  备注
        /// </summary>
        [DbColumn("Remark", LocalName = " 备注", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark, value);

                _Remark = value;
            }
        }


        #endregion
    }
}
