﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// COMM_Page
    /// </summary>
    [Table("COMM_Page", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class PageEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public PageEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion
        #region 常量
        public const string _TableName = "COMM_Page";
        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "PageId";

        /// <summary>
        /// 是否需要admin权限
        /// </summary>
        public const string __IsAdmin = "IsAdmin";
        /// <summary>
        ///  上级id
        /// </summary>
        public const string __ParentId = "ParentId";
        /// <summary>
        /// 页面按钮Id
        /// </summary>
        public const string __IsGet = "IsGet";

        /// <summary>
        /// 页面按钮Id
        /// </summary>
        public const string __IsPost = "IsPost";
        /// <summary>
        /// 控制器名称
        /// </summary>
        public const string __ControllerName = "ControllerName";

        /// <summary>
        /// 是否继承controller  Index访问权限
        /// </summary>
        public const string __IsParent = "IsParent";

        /// <summary>
        /// 不允许访问
        /// </summary>
        public const string __NoAccess = "NoAccess";

        /// <summary>
        /// 域名称
        /// </summary>
        public const string __AreaName = "AreaName";
        /// <summary>
        ///  页面名称
        /// </summary>
        public const string __LocalName = "LocalName";
        /// <summary>
        ///  Action
        /// </summary>
        public const string __ActionName = "ActionName";
        /// <summary>
        ///  全路径
        /// </summary>
        public const string __RouteUrl = "RouteUrl";
        /// <summary>
        ///  图标
        /// </summary>
        public const string __Icon = "Icon";
        /// <summary>
        ///  是否菜单
        /// </summary>
        public const string __IsMenu = "IsMenu";

        /// <summary>
        ///  是否菜单
        /// </summary>
        public const string __IsDisplay = "IsDisplay";
     
        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";
        /// <summary>
        ///  描述
        /// </summary>
        public const string __Remark = "Remark";
        #endregion
        #region 属性
        private string _PageId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("PageId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 10, Min = 0)]
        public virtual string PageId
        {
            get
            {
                return _PageId;
            }
            set
            {
                _PageId = value;
            }
        }
        private string _ParentId ="";
        ///<summary>
        ///  上级id
        /// </summary>
        [DbColumn("ParentId", LocalName = " 上级id", ColumnType = "string", SqlType = "varchar", ControlType = 6)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {

                SetData(__ParentId, value);

                _ParentId = value;
            }
        }
        private string _LocalName;
        ///<summary>
        ///  页面名称
        /// </summary>
        [DbColumn("LocalName", LocalName = " 页面名称", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {

                SetData(__LocalName, value);

                _LocalName = value;
            }
        }

        private bool _IsGet;
        ///<summary>
        ///  是否支持Get请求
        /// </summary>
        [DbColumn("IsGet", LocalName = "支持Get请求", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsGet
        {
            get
            {
                return _IsGet;
            }
            set
            {
                SetData(__IsGet, value);
                _IsGet = value;
            }
        }

        private bool _IsPost;
        ///<summary>
        ///  是否支持POST请求
        /// </summary>
        [DbColumn("IsPost", LocalName = "支持POST请求", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsPost
        {
            get
            {
                return _IsPost;
            }
            set
            {
                SetData(__IsPost, value);
                _IsPost = value;
            }
        }

        private string _AreaName;
        ///<summary>
        ///  AreaName域名
        /// </summary>
        [DbColumn("AreaName", LocalName = "域名称", AllowDBNull = true, ColumnType = "string", SqlType = "nvarchar", ControlType = 6, MaxLength = 50, Min = 0)]
        public virtual string AreaName
        {
            get
            {
                return _AreaName;
            }
            set
            {
                SetData(__AreaName, value);
                _AreaName = value;
            }
        }
        private string _ControllerName;
        ///<summary>
        ///  AreaName域名
        /// </summary>
        [DbColumn("ControllerName", LocalName = "控制器名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 6, MaxLength = 50, Min = 0)]
        public virtual string ControllerName
        {
            get
            {
                return _ControllerName;
            }
            set
            {
                SetData(__ControllerName, value);
                _ControllerName = value;
            }
        }
        private bool _IsParent;
        ///<summary>
        ///  是否继承上级
        /// </summary>
        [DbColumn("IsParent", LocalName = "是否继承上级", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsParent
        {
            get
            {
                return _IsParent;
            }
            set
            {
                SetData(__IsParent, value);
                _IsParent = value;
            }
        }

        private bool _NoAccess;
        ///<summary>
        ///  是否继承上级
        /// </summary>
        [DbColumn("NoAccess", LocalName = "是否继承上级", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool NoAccess
        {
            get
            {
                return _NoAccess;
            }
            set
            {
                SetData(__NoAccess, value);
                _NoAccess = value;
            }
        }
        private string _ActionName;
        ///<summary>
        ///  Action
        /// </summary>
        [DbColumn("ActionName", LocalName = " Action", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ActionName
        {
            get
            {
                return _ActionName;
            }
            set
            {

                SetData(__ActionName, value);

                _ActionName = value;
            }
        }
        private string _RouteUrl;
        ///<summary>
        ///  全路径
        /// </summary>
        [DbColumn("RouteUrl", LocalName = " 全路径", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string RouteUrl
        {
            get
            {
                return _RouteUrl;
            }
            set
            {

                SetData(__RouteUrl, value);

                _RouteUrl = value;
            }
        }
        private string _Icon;
        ///<summary>
        ///  图标
        /// </summary>
        [DbColumn("Icon", LocalName = " 图标", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string Icon
        {
            get
            {
                return _Icon;
            }
            set
            {

                SetData(__Icon, value);

                _Icon = value;
            }
        }
        private bool _IsMenu;
        ///<summary>
        ///  是否菜单
        /// </summary>
        [DbColumn("IsMenu", LocalName = " 是否菜单", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsMenu
        {
            get
            {
                return _IsMenu;
            }
            set
            {

                SetData(__IsMenu, value);

                _IsMenu = value;
            }
        }

        private bool _IsDisplay;
        ///<summary>
        ///  是否显示
        /// </summary>
        [DbColumn("IsDisplay", LocalName = " 是否显示", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsDisplay
        {
            get
            {
                return _IsDisplay;
            }
            set
            {

                SetData(__IsDisplay, value);

                _IsDisplay = value;
            }
        }
        private bool _IsAdmin;
        ///<summary>
        ///  是否系统
        /// </summary>
        [DbColumn("IsAdmin", LocalName = " 是否系统", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsAdmin
        {
            get
            {
                return _IsAdmin;
            }
            set
            {

                SetData(__IsAdmin, value);

                _IsAdmin = value;
            }
        }
        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {

                SetData(__OrderNum, value);

                _OrderNum = value;
            }
        }
        private string _Remark;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("Remark", LocalName = " 描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark, value);

                _Remark = value;
            }
        }
        #endregion
    }
}