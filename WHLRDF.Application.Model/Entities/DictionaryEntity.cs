﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 数据字典
    /// </summary>
    [Table("COMM_Dictionary", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class DictionaryEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public DictionaryEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Dictionary";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "DictionaryId";

        /// <summary>
        ///  主键
        /// </summary>
        public const string __ParentId = "ParentId";

        /// <summary>
        ///  名称
        /// </summary>
        public const string __KeyName = "KeyName";

        /// <summary>
        ///  值
        /// </summary>
        public const string __KeyValue = "KeyValue";

        /// <summary>
        ///  排序
        /// </summary>
        public const string __OrderNum = "OrderNum";

        /// <summary>
        /// 描述
        /// </summary>
        public const string __Remark = "Remark";

        #endregion
        #region 属性

        private string _DictionaryId;
        ///<summary>
        ///  编号
        /// </summary>
        [DbColumn("DictionaryId", IsPrimaryKey = true, LocalName = " 编号", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string DictionaryId
        {
            get
            {
                return _DictionaryId;
            }
            set
            {
                _DictionaryId = value;
            }
        }


        private string _ParentId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("ParentId", LocalName = " 主键", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ParentId
        {
            get
            {
                return _ParentId;
            }
            set
            {
                SetData(__ParentId, value);
                _ParentId = value;
            }
        }


        private string _KeyName;
        ///<summary>
        ///  名称
        /// </summary>
        [DbColumn("KeyName", LocalName = " 名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string KeyName
        {
            get
            {
                return _KeyName;
            }
            set
            {
                SetData(__KeyName, value);
                _KeyName = value;
            }
        }


        private int _KeyValue;
        ///<summary>
        ///  值
        /// </summary>
        [DbColumn("KeyValue", LocalName = " 值", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int KeyValue
        {
            get
            {
                return _KeyValue;
            }
            set
            {
                SetData(__KeyValue, value);
                _KeyValue = value;
            }
        }


        private int _OrderNum;
        ///<summary>
        ///  排序
        /// </summary>
        [DbColumn("OrderNum", LocalName = " 排序", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int OrderNum
        {
            get
            {
                return _OrderNum;
            }
            set
            {
                SetData(__OrderNum, value);
                _OrderNum = value;
            }
        }


        private string _Remark;
        ///<summary>
        /// 描述
        /// </summary>
        [DbColumn("Remark", LocalName = "描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 2000, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {
                SetData(__Remark, value);
                _Remark = value;
            }
        }


        #endregion
    }
}