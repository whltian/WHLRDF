﻿
using WHLRDF.ORM;
using System;
using System.Data;

namespace WHLRDF.Application.Model
{
    /// <summary>
    /// Action管理
    /// </summary>
    [Table("COMM_Actions", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class ActionsEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public ActionsEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Actions";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "ActionId";


        /// <summary>
        /// ActionName
        /// </summary>
        public const string __ActionName = "ActionName";


        /// <summary>
        ///  按钮名称
        /// </summary>
        public const string __LocalName = "LocalName";


        /// <summary>
        ///  描述
        /// </summary>
        public const string __Remark = "Remark";


        /// <summary>
        ///  是否系统
        /// </summary>
        public const string __IsSystem = "IsSystem";


        #endregion
        #region 属性

        private string _ActionId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("ActionId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 36)]
        public virtual string ActionId
        {
            get
            {
                return _ActionId;
            }
            set
            {
                _ActionId = value;
            }
        }


        private string _ActionName;
        ///<summary>
        /// ActionName
        /// </summary>
        [DbColumn("ActionName", LocalName = "ActionName", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 30)]
        public virtual string ActionName
        {
            get
            {
                return _ActionName;
            }
            set
            {

                SetData(__ActionName,value);

                _ActionName = value;
            }
        }


        private string _LocalName;
        ///<summary>
        ///  按钮名称
        /// </summary>
        [DbColumn("LocalName", LocalName = " 按钮名称", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string LocalName
        {
            get
            {
                return _LocalName;
            }
            set
            {

                SetData(__LocalName, value);

                _LocalName = value;
            }
        }


        private string _Remark;
        ///<summary>
        ///  描述
        /// </summary>
        [DbColumn("Remark", LocalName = " 描述", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark, value);

                _Remark = value;
            }
        }


        private bool _IsSystem;
        ///<summary>
        ///  是否系统
        /// </summary>
        [DbColumn("IsSystem", LocalName = " 是否系统", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsSystem
        {
            get
            {
                return _IsSystem;
            }
            set
            {

                SetData(__IsSystem, value);

                _IsSystem = value;
            }
        }


        #endregion
    }
}

