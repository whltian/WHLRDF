﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 邮件管理
    /// </summary>
    [Table("COMM_Email", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class EmailEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public EmailEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_Email";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "EmailId";

        /// <summary>
        ///  表单id
        /// </summary>
        public const string __FormId = "FormId";

        /// <summary>
        ///  类别
        /// </summary>
        public const string __FormType = "FormType";

        /// <summary>
        ///  用户
        /// </summary>
        public const string __UserId = "UserId";

        /// <summary>
        ///  发送人
        /// </summary>
        public const string __SenderUser = "SenderUser";

        /// <summary>
        ///  发送时间
        /// </summary>
        public const string __SendDate = "SendDate";

        /// <summary>
        ///  接收人
        /// </summary>
        public const string __ToUser = "ToUser";

        /// <summary>
        ///  抄送人
        /// </summary>
        public const string __CCUser = "CCUser";

        /// <summary>
        ///  标题
        /// </summary>
        public const string __Title = "Title";

        /// <summary>
        ///  内容
        /// </summary>
        public const string __Content = "Content";

        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";

        /// <summary>
        ///  发送次数
        /// </summary>
        public const string __SendRound = "SendRound";

        /// <summary>
        /// 执行时间
        /// </summary>
        public const string __ExpireTime = "ExpireTime";

        /// <summary>
        ///  反馈结果
        /// </summary>
        public const string __SendResult = "SendResult";

        #endregion
        #region 属性

        private string _EmailId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("EmailId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string EmailId
        {
            get
            {
                return _EmailId;
            }
            set
            {
                _EmailId = value;
            }
        }


        private string _FormId;
        ///<summary>
        ///  表单id
        /// </summary>
        [DbColumn("FormId", LocalName = " 表单id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FormId
        {
            get
            {
                return _FormId;
            }
            set
            {
                SetData(__FormId, value);
                _FormId = value;
            }
        }


        private int _FormType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("FormType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int FormType
        {
            get
            {
                return _FormType;
            }
            set
            {
                SetData(__FormType, value);
                _FormType = value;
            }
        }


        private string _UserId;
        ///<summary>
        ///  用户
        /// </summary>
        [DbColumn("UserId", LocalName = " 用户", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                SetData(__UserId, value);
                _UserId = value;
            }
        }


        private string _SenderUser;
        ///<summary>
        ///  发送人
        /// </summary>
        [DbColumn("SenderUser", LocalName = " 发送人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string SenderUser
        {
            get
            {
                return _SenderUser;
            }
            set
            {
                SetData(__SenderUser, value);
                _SenderUser = value;
            }
        }


        private DateTime _SendDate;
        ///<summary>
        ///  发送时间
        /// </summary>
        [DbColumn("SendDate", LocalName = " 发送时间", AllowDBNull = false, ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime SendDate
        {
            get
            {
                return _SendDate;
            }
            set
            {
                SetData(__SendDate, value);
                _SendDate = value;
            }
        }


        private string _ToUser;
        ///<summary>
        ///  接收人
        /// </summary>
        [DbColumn("ToUser", LocalName = " 接收人", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 3000, Min = 0)]
        public virtual string ToUser
        {
            get
            {
                return _ToUser;
            }
            set
            {
                SetData(__ToUser, value);
                _ToUser = value;
            }
        }


        private string _CCUser;
        ///<summary>
        ///  抄送人
        /// </summary>
        [DbColumn("CCUser", LocalName = " 抄送人", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 3000, Min = 0)]
        public virtual string CCUser
        {
            get
            {
                return _CCUser;
            }
            set
            {
                SetData(__CCUser, value);
                _CCUser = value;
            }
        }


        private string _Title;
        ///<summary>
        ///  标题
        /// </summary>
        [DbColumn("Title", LocalName = " 标题", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 3000, Min = 0)]
        public virtual string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetData(__Title, value);
                _Title = value;
            }
        }


        private string _Content;
        ///<summary>
        ///  内容
        /// </summary>
        [DbColumn("Content", LocalName = " 内容", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = -1, Min = 0)]
        public virtual string Content
        {
            get
            {
                return _Content;
            }
            set
            {
                SetData(__Content, value);
                _Content = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetData(__Status, value);
                _Status = value;
            }
        }


        private int _SendRound;
        ///<summary>
        ///  发送次数
        /// </summary>
        [DbColumn("SendRound", LocalName = " 发送次数", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int SendRound
        {
            get
            {
                return _SendRound;
            }
            set
            {
                SetData(__SendRound, value);
                _SendRound = value;
            }
        }


        private DateTime? _ExpireTime;
        ///<summary>
        /// 执行时间
        /// </summary>
        [DbColumn("ExpireTime", LocalName = "执行时间", ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime? ExpireTime
        {
            get
            {
                return _ExpireTime;
            }
            set
            {
                SetData(__ExpireTime, value);
                _ExpireTime = value;
            }
        }


        private string _SendResult;
        ///<summary>
        ///  反馈结果
        /// </summary>
        [DbColumn("SendResult", LocalName = " 反馈结果", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 500, Min = 0)]
        public virtual string SendResult
        {
            get
            {
                return _SendResult;
            }
            set
            {
                SetData(__SendResult, value);
                _SendResult = value;
            }
        }


        #endregion
    }
}