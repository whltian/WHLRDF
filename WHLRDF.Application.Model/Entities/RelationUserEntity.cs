﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 用户关系表
    /// </summary>
    [Table("COMM_RelationUser", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class RelationUserEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public RelationUserEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_RelationUser";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "RelationUserId";


        /// <summary>
        ///  类别
        /// </summary>
        public const string __RelationType = "RelationType";


        /// <summary>
        ///  用户id
        /// </summary>
        public const string __UserId = "UserId";


        /// <summary>
        ///  源id
        /// </summary>
        public const string __ResourceId = "ResourceId";

        public const string __IsPrimary = "IsPrimary";


        #endregion
        #region 属性

        private string _RelationUserId;
        ///<summary>
        ///  关系id
        /// </summary>
        [DbColumn("RelationUserId", IsPrimaryKey = true, LocalName = " 关系id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 36, Min = 0)]
        public virtual string RelationUserId
        {
            get
            {
                return _RelationUserId;
            }
            set
            {
                _RelationUserId = value;
            }
        }


        private int _RelationType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("RelationType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int RelationType
        {
            get
            {
                return _RelationType;
            }
            set
            {

                SetData(__RelationType, value);

                _RelationType = value;
            }
        }


        private string _UserId;
        ///<summary>
        ///  用户id
        /// </summary>
        [DbColumn("UserId", LocalName = " 用户id", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {

                SetData(__UserId, value);

                _UserId = value;
            }
        }


        private string _ResourceId;
        ///<summary>
        ///  源id
        /// </summary>
        [DbColumn("ResourceId", LocalName = " 源id", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string ResourceId
        {
            get
            {
                return _ResourceId;
            }
            set
            {

                SetData(__ResourceId, value);

                _ResourceId = value;
            }
        }
        private bool _IsPrimary;
        ///<summary>
        ///  是否主岗
        /// </summary>
        [DbColumn("IsPrimary", LocalName = " 是否主岗", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 4, MaxLength = 6, Min = 0)]
        public virtual bool IsPrimary
        {
            get
            {
                return _IsPrimary;
            }
            set
            {

                SetData(__IsPrimary, value);

                _IsPrimary = value;
            }
        }

        #endregion
    }
}
