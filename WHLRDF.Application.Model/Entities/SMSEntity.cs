﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 短信管理
    /// </summary>
    [Table("COMM_SMS", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class SMSEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        /// </summary>
        public SMSEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion

        #region 常量
        public const string _TableName = "COMM_SMS";


        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "SMSId";

        /// <summary>
        ///  表单id
        /// </summary>
        public const string __FormId = "FormId";

        /// <summary>
        ///  类别
        /// </summary>
        public const string __FormType = "FormType";

        /// <summary>
        ///  用户
        /// </summary>
        public const string __UserId = "UserId";

        /// <summary>
        ///  接收号码
        /// </summary>
        public const string __ToPhone = "ToPhone";

        /// <summary>
        ///  发送时间
        /// </summary>
        public const string __SendDate = "SendDate";

        /// <summary>
        ///  内容
        /// </summary>
        public const string __Content = "Content";

        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";

        /// <summary>
        ///  轮数
        /// </summary>
        public const string __SendRound = "SendRound";

        /// <summary>
        ///  回执内容
        /// </summary>
        public const string __SendResult = "SendResult";

        /// <summary>
        ///  过期时间
        /// </summary>
        public const string __ExpireTime = "ExpireTime";

        #endregion
        #region 属性

        private string _SMSId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("SMSId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 2, MaxLength = 50, Min = 0)]
        public virtual string SMSId
        {
            get
            {
                return _SMSId;
            }
            set
            {
                _SMSId = value;
            }
        }


        private string _FormId;
        ///<summary>
        ///  表单id
        /// </summary>
        [DbColumn("FormId", LocalName = " 表单id", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string FormId
        {
            get
            {
                return _FormId;
            }
            set
            {
                SetData(__FormId, value);
                _FormId = value;
            }
        }


        private int _FormType;
        ///<summary>
        ///  类别
        /// </summary>
        [DbColumn("FormType", LocalName = " 类别", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int FormType
        {
            get
            {
                return _FormType;
            }
            set
            {
                SetData(__FormType, value);
                _FormType = value;
            }
        }


        private string _UserId;
        ///<summary>
        ///  用户
        /// </summary>
        [DbColumn("UserId", LocalName = " 用户", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 50, Min = 0)]
        public virtual string UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                SetData(__UserId, value);
                _UserId = value;
            }
        }


        private string _ToPhone;
        ///<summary>
        ///  接收号码
        /// </summary>
        [DbColumn("ToPhone", LocalName = " 接收号码", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 3000, Min = 0)]
        public virtual string ToPhone
        {
            get
            {
                return _ToPhone;
            }
            set
            {
                SetData(__ToPhone, value);
                _ToPhone = value;
            }
        }


        private DateTime _SendDate;
        ///<summary>
        ///  发送时间
        /// </summary>
        [DbColumn("SendDate", LocalName = " 发送时间", AllowDBNull = false, ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime SendDate
        {
            get
            {
                return _SendDate;
            }
            set
            {
                SetData(__SendDate, value);
                _SendDate = value;
            }
        }


        private string _Content;
        ///<summary>
        ///  内容
        /// </summary>
        [DbColumn("Content", LocalName = " 内容", AllowDBNull = false, ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string Content
        {
            get
            {
                return _Content;
            }
            set
            {
                SetData(__Content, value);
                _Content = value;
            }
        }


        private int _Status;
        ///<summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = " 状态", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetData(__Status, value);
                _Status = value;
            }
        }


        private int _SendRound;
        ///<summary>
        ///  轮数
        /// </summary>
        [DbColumn("SendRound", LocalName = " 轮数", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 6, MaxLength = 10, Min = 0, VaildKey = "IsInt")]
        public virtual int SendRound
        {
            get
            {
                return _SendRound;
            }
            set
            {
                SetData(__SendRound, value);
                _SendRound = value;
            }
        }


        private string _SendResult;
        ///<summary>
        ///  回执内容
        /// </summary>
        [DbColumn("SendResult", LocalName = " 回执内容", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 200, Min = 0)]
        public virtual string SendResult
        {
            get
            {
                return _SendResult;
            }
            set
            {
                SetData(__SendResult, value);
                _SendResult = value;
            }
        }


        private DateTime? _ExpireTime;
        ///<summary>
        ///  过期时间
        /// </summary>
        [DbColumn("ExpireTime", LocalName = " 过期时间", ColumnType = "DateTime", SqlType = "datetime", ControlType = 7, MaxLength = 23, Min = 0, VaildKey = "IsDate")]
        public virtual DateTime? ExpireTime
        {
            get
            {
                return _ExpireTime;
            }
            set
            {
                SetData(__ExpireTime, value);
                _ExpireTime = value;
            }
        }


        #endregion
    }
}

