﻿
using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// COMM_SerialRules
    /// </summary>
    [Table("COMM_SerialRule", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class SerialRuleEntity : EntityBase
    {
        #region 基类属性
        /// <summary>
        /// 构造函数
        ///</summary>
        public SerialRuleEntity()
        {
            base.PrimaryKeyName = _PrimaryKeyName;
            base.TableName = _TableName;
        }
        #endregion
        #region 常量
        public const string _TableName = "COMM_SerialRules";
        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "SerialRuleId";
        /// <summary>
        ///  前缀
        /// </summary>
        public const string __SerialPrefix = "SerialPrefix";
        /// <summary>
        ///  后缀
        /// </summary>
        public const string __SerialSuffix = "SerialSuffix";
        /// <summary>
        ///  长度
        /// </summary>
        public const string __SerialLength = "SerialLength";
        /// <summary>
        ///  启用年
        /// </summary>
        public const string __IsYear = "IsYear";
        /// <summary>
        ///  启用月
        /// </summary>
        public const string __IsMonth = "IsMonth";
        /// <summary>
        ///  启用天
        /// </summary>
        public const string __IsDay = "IsDay";
        /// <summary>
        ///  启用时间
        /// </summary>
        public const string __IsTime = "IsTime";
       
        /// <summary>
        ///  计数器
        /// </summary>
        public const string __SerialCount = "SerialCount";
        /// <summary>
        ///  备注
        /// </summary>
        public const string __Remark = "Remark";
        #endregion
        #region 属性
        private string _SerialRuleId;
        ///<summary>
        ///  主键
        /// </summary>
        [DbColumn("SerialRuleId", IsPrimaryKey = true, LocalName = " 主键", AllowDBNull = false, ColumnType = "string", SqlType = "varchar", ControlType = 2, MaxLength = 20, Min = 0)]
        public virtual string SerialRuleId
        {
            get
            {
                return _SerialRuleId;
            }
            set
            {
                _SerialRuleId = value;
            }
        }
        private string _SerialPrefix;
        ///<summary>
        ///  前缀
        /// </summary>
        [DbColumn("SerialPrefix", LocalName = " 前缀", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string SerialPrefix
        {
            get
            {
                return _SerialPrefix;
            }
            set
            {

                SetData(__SerialPrefix, value);

                _SerialPrefix = value;
            }
        }
        private string _SerialSuffix;
        ///<summary>
        ///  后缀
        /// </summary>
        [DbColumn("SerialSuffix", LocalName = " 后缀", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 100, Min = 0)]
        public virtual string SerialSuffix
        {
            get
            {
                return _SerialSuffix;
            }
            set
            {

                SetData(__SerialSuffix, value);

                _SerialSuffix = value;
            }
        }
        private int _SerialLength;
        ///<summary>
        ///  长度
        /// </summary>
        [DbColumn("SerialLength", LocalName = " 长度", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 1, MaxLength = 10, Min = 0)]
        public virtual int SerialLength
        {
            get
            {
                return _SerialLength;
            }
            set
            {

                SetData(__SerialLength, value);

                _SerialLength = value;
            }
        }
        private bool _IsYear;
        ///<summary>
        ///  启用年
        /// </summary>
        [DbColumn("IsYear", LocalName = " 启用年", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 1, MaxLength = 6, Min = 0)]
        public virtual bool IsYear
        {
            get
            {
                return _IsYear;
            }
            set
            {

                SetData(__IsYear, value);

                _IsYear = value;
            }
        }
        private bool _IsMonth;
        ///<summary>
        ///  启用月
        /// </summary>
        [DbColumn("IsMonth", LocalName = " 启用月", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 1, MaxLength = 6, Min = 0)]
        public virtual bool IsMonth
        {
            get
            {
                return _IsMonth;
            }
            set
            {

                SetData(__IsMonth, value);

                _IsMonth = value;
            }
        }
        private bool _IsDay;
        ///<summary>
        ///  启用天
        /// </summary>
        [DbColumn("IsDay", LocalName = " 启用天", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 1, MaxLength = 6, Min = 0)]
        public virtual bool IsDay
        {
            get
            {
                return _IsDay;
            }
            set
            {

                SetData(__IsDay, value);

                _IsDay = value;
            }
        }
        private bool _IsTime;
        ///<summary>
        ///  启用时间
        /// </summary>
        [DbColumn("IsTime", LocalName = " 启用时间", AllowDBNull = false, ColumnType = "bool", SqlType = "bit", ControlType = 1, MaxLength = 6, Min = 0)]
        public virtual bool IsTime
        {
            get
            {
                return _IsTime;
            }
            set
            {

                SetData(__IsTime, value);

                _IsTime = value;
            }
        }
       
        private int _SerialCount;
        ///<summary>
        ///  计数器
        /// </summary>
        [DbColumn("SerialCount", LocalName = " 计数器", AllowDBNull = false, ColumnType = "int", SqlType = "int", ControlType = 1, MaxLength = 10, Min = 0)]
        public virtual int SerialCount
        {
            get
            {
                return _SerialCount;
            }
            set
            {

                SetData(__SerialCount, value);

                _SerialCount = value;
            }
        }
        private string _Remark;
        ///<summary>
        ///  备注
        /// </summary>
        [DbColumn("Remark", LocalName = " 备注", ColumnType = "string", SqlType = "nvarchar", ControlType = 1, MaxLength = 300, Min = 0)]
        public virtual string Remark
        {
            get
            {
                return _Remark;
            }
            set
            {

                SetData(__Remark, value);

                _Remark = value;
            }
        }
        #endregion
    }
}