﻿using System;
using System.Data;
using WHLRDF.ORM;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 消息管理实体
    /// </summary>
    [Table("COMM_Notify", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class NotifyEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "COMM_Notify";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "NotifyId";

        /// <summary>
        ///  表单id
        /// </summary>
        public const string __FormId = "FormId";

        /// <summary>
        ///  类别
        /// </summary>
        public const string __FormType = "FormType";

        /// <summary>
        ///  接收人
        /// </summary>
        public const string __ReceiveUserId = "ReceiveUserId";

        /// <summary>
        ///  发送人
        /// </summary>
        public const string __SenderUserId = "SenderUserId";

        /// <summary>
        ///  标题
        /// </summary>
        public const string __Title = "Title";

        /// <summary>
        ///  发送时间
        /// </summary>
        public const string __SendDate = "SendDate";

        /// <summary>
        ///  内容
        /// </summary>
        public const string __Content = "Content";

        /// <summary>
        ///  状态
        /// </summary>
        public const string __Status = "Status";

        /// <summary>
        ///  已读时间
        /// </summary>
        public const string __ExpireTime = "ExpireTime";

        /// <summary>
        ///  目标url
        /// </summary>
        public const string __TargetUrl = "TargetUrl";

        #endregion
        #region 属性
        private string _NotifyId;
        /// <summary>
        ///  主键
        /// </summary>
        [DbColumn("NotifyId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string NotifyId
        {
            get
            {
                return _NotifyId;
            }
            set
            {
                _NotifyId = value;

            }
        }
        private string _FormId;
        /// <summary>
        ///  表单id
        /// </summary>
        [DbColumn("FormId", LocalName = "表单id", ColumnType = "string", MaxLength = 50, ControlType = 1)]
        public virtual string FormId
        {
            get
            {
                return _FormId;
            }
            set
            {
                SetData(__FormId, value);
                _FormId = value;

            }
        }
        private int _FormType;
        /// <summary>
        ///  类别
        /// </summary>
        [DbColumn("FormType", LocalName = "类别", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int FormType
        {
            get
            {
                return _FormType;
            }
            set
            {
                SetData(__FormType, value);
                _FormType = value;

            }
        }
        private string _ReceiveUserId;
        /// <summary>
        ///  接收人
        /// </summary>
        [DbColumn("ReceiveUserId", LocalName = "接收人", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string ReceiveUserId
        {
            get
            {
                return _ReceiveUserId;
            }
            set
            {
                SetData(__ReceiveUserId, value);
                _ReceiveUserId = value;

            }
        }
        private string _SenderUserId;
        /// <summary>
        ///  发送人
        /// </summary>
        [DbColumn("SenderUserId", LocalName = "发送人", ColumnType = "string", AllowDBNull = false, MaxLength = 50, ControlType = 1)]
        public virtual string SenderUserId
        {
            get
            {
                return _SenderUserId;
            }
            set
            {
                SetData(__SenderUserId, value);
                _SenderUserId = value;

            }
        }
        private string _Title;
        /// <summary>
        ///  标题
        /// </summary>
        [DbColumn("Title", LocalName = "标题", ColumnType = "string", AllowDBNull = false, MaxLength = 3000, ControlType = 1)]
        public virtual string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                SetData(__Title, value);
                _Title = value;

            }
        }
        private DateTime _SendDate;
        /// <summary>
        ///  发送时间
        /// </summary>
        [DbColumn("SendDate", LocalName = "发送时间", ColumnType = "datetime", AllowDBNull = false, VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime SendDate
        {
            get
            {
                return _SendDate;
            }
            set
            {
                SetData(__SendDate, value);
                _SendDate = value;

            }
        }
        private string _Content;
        /// <summary>
        ///  内容
        /// </summary>
        [DbColumn("Content", LocalName = "内容", ColumnType = "string", AllowDBNull = false, ControlType = 1)]
        public virtual string Content
        {
            get
            {
                return _Content;
            }
            set
            {
                SetData(__Content, value);
                _Content = value;

            }
        }
        private int _Status;
        /// <summary>
        ///  状态
        /// </summary>
        [DbColumn("Status", LocalName = "状态", ColumnType = "int", AllowDBNull = false, VaildKey = "IsInt", ControlType = 6)]
        public virtual int Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetData(__Status, value);
                _Status = value;

            }
        }
        private DateTime? _ExpireTime;
        /// <summary>
        ///  已读时间
        /// </summary>
        [DbColumn("ExpireTime", LocalName = "已读时间", ColumnType = "datetime", VaildKey = "IsDate", ControlType = 7)]
        public virtual DateTime? ExpireTime
        {
            get
            {
                return _ExpireTime;
            }
            set
            {
                SetData(__ExpireTime, value);
                _ExpireTime = value;

            }
        }
        private string _TargetUrl;
        /// <summary>
        ///  目标url
        /// </summary>
        [DbColumn("TargetUrl", LocalName = "目标url", ColumnType = "string", MaxLength = 500, ControlType = 1)]
        public virtual string TargetUrl
        {
            get
            {
                return _TargetUrl;
            }
            set
            {
                SetData(__TargetUrl, value);
                _TargetUrl = value;

            }
        }
        #endregion
    }
}