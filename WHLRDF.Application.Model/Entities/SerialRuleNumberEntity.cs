﻿
using WHLRDF.ORM;
using System;
using System.Data;
namespace WHLRDF.Application.Model
{
    /// <summary>
    /// 流水号记录表
    /// </summary>
    [Table("COMM_SerialRuleNumber", IsDeleted = true, IsCreated = true, IsLastModify = true, IsTable = true)]
    public partial class SerialRuleNumberEntity : EntityBase
    {
        #region 常量
        public const string _TableName = "COMM_SerialRuleNumber";

        /// <summary>
        /// 主键字段名
        /// </summary>
        public const string _PrimaryKeyName = "SerialRuleNumberId";

        /// <summary>
        /// 规则编号
        /// </summary>
        public const string __SerialRuleId = "SerialRuleId";

        /// <summary>
        /// 流水号前缀
        /// </summary>
        public const string __SerialRuleNo = "SerialRuleNo";

        /// <summary>
        /// 最大记录数
        /// </summary>
        public const string __MaxCount = "MaxCount";
        #endregion
        #region 属性
        private string _SerialRuleNumberId;
        /// <summary>
        /// 主键
        /// </summary>
        [DbColumn("SerialRuleNumberId", LocalName = "主键", ColumnType = "string", IsPrimaryKey = true, AllowDBNull = false, MaxLength = 50, ControlType = 2)]
        public virtual string SerialRuleNumberId
        {
            get
            {
                return _SerialRuleNumberId;
            }
            set
            {
                _SerialRuleNumberId = value;

            }
        }
        private string _SerialRuleId;
        /// <summary>
        /// 规则编号
        /// </summary>
        [DbColumn("SerialRuleId", LocalName = "规则编号", ColumnType = "string", AllowDBNull = false, MaxLength = 20, ControlType = 1)]
        public virtual string SerialRuleId
        {
            get
            {
                return _SerialRuleId;
            }
            set
            {
                SetData(__SerialRuleId, value);
                _SerialRuleId = value;

            }
        }
        private string _SerialRuleNo;
        /// <summary>
        /// 流水号前缀
        /// </summary>
        [DbColumn("SerialRuleNo", LocalName = "流水号前缀", ColumnType = "string", AllowDBNull = false, MaxLength =100, ControlType = 1)]
        public virtual string SerialRuleNo
        {
            get
            {
                return _SerialRuleNo;
            }
            set
            {
                SetData(__SerialRuleNo, value);
                _SerialRuleNo = value;

            }
        }
        private int _MaxCount;
        /// <summary>
        /// 最大记录数
        /// </summary>
        [DbColumn("MaxCount", LocalName = "最大记录数", ColumnType = "int", AllowDBNull = false, VaildKey = "number", ControlType = 6)]
        public virtual int MaxCount
        {
            get
            {
                return _MaxCount;
            }
            set
            {
                SetData(__MaxCount, value);
                _MaxCount = value;

            }
        }
        #endregion
    }
}
